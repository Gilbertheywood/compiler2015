/** Target: Check the struct assignment.
 * Possible optimization: Dead code elimination, common expression, strength reduction, inline function
 * REMARK: Pay attention to the addressing of the struct. The struct assignment should be supported.
 *
**/

#include <stdio.h>

struct node
{
    int a[5][5];
    char ch[2];
    int count;
    struct inside{int p;} in;
} a[2];

struct node getNode(int i)
{
    a[i].count++;
    return a[i];
}

void print(struct node node1, struct node node2) {
	int i, j;
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 5; j++)
			printf("(%d, %d)", node1.a[i][j], node2.a[i][j]);
		printf("\n");
	}
}

int main()
{
	int i, j;
    for (i = 0; i < 5; i++)
    	for (j = 0; j < 5; j++)
    		a[0].a[i][j] = 1;
    for (i = 0; i < 5; i++)
    	for (j = 0; j < 5; j++)
    		a[1].a[i][j] = 12;
    //system("pause");
    print(getNode(0), getNode(1));
    return 0;
}
