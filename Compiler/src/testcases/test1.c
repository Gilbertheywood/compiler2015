#include <stdio.h>

union A {
	char c;
	union B{
		char a, b;
	} b;
	int a;
	char ac;
	char ab;
};

int main() {
	union A ob;
	ob.c = 'a';
	ob.a = 134324415;
	ob.ac = 'c';
	ob.ab = 'b';
	printf("%d", ob.a);
}
