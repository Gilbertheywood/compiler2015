.data
string0:
.asciiz  "%d"
.text
printf:
printf_loop:
lb       $a0,0($a2)
beq      $a0,0,printf_end
add      $a2,$a2,1
beq      $a0,'%',printf_fmt
li       $v0,11
syscall
b        printf_loop
printf_fmt:
lb       $a0,0($a2)
add      $a2,$a2,1
beq      $a0,'d',printf_int
beq      $a0,'s',printf_str
beq      $a0,'c',printf_char
beq      $a0,'.',printf_width
printf_int:
lw       $a0,0($a1)
add      $a1,$a1,4
li       $v0,1
syscall
b        printf_loop
printf_str:
move     $a0,$a1
add      $a1,$a1,4
li       $v0,4
syscall
b        printf_loop
printf_char:
lb       $a0,0($a1)
add      $a1,$a1,4
li       $v0,11
syscall
b        printf_loop
li       $v0,1
li       $a0,0
add      $a2,$a2,1
lb       $a3,0($a2)
add      $a3,-48
li       $t0,0
WidthLoop:
mul      $t0,$t0,10
add      $t0,$t0,9
add      $a3,$a3,-1
bne      $a3,0,WidthLoop
lw       $t1,0($a1)
ZeroLoop:
div      $t0,$t0,10
bgt      $t1,$t0,printf_int
syscall
b        ZeroLoop
jr       $31
nop
printf_end:
j $31
nop
main :
addiu    $sp,$sp,-56
sw       $31,52($sp)
sw       $fp,48($sp)
move     $fp,$sp
li       $a1,12
sw       $a1,44($sp)
lw       $a1,44($sp)
sw       $a1,0($sp)
la       $a1,string0
sw       $a1,4($sp)
lw       $a2,4($sp)
move     $a1,$sp
jal      printf
nop
lw       $31,52($sp)
lw       $fp,48($sp)
j        $31
nop
