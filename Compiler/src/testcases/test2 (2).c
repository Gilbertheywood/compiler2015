/** Target: Check the struct assignment.
 * Possible optimization: Dead code elimination, common expression, strength reduction, inline function
 * REMARK: Pay attention to the addressing of the struct. The struct assignment should be supported.
 *
**/

#include <stdio.h>
int size1=5,size2=5;
struct node
{
    int a[2][2];
    char ch[2];
    int count;
    struct inside{int p;} in;
};

void insideprint(struct inside in1, struct inside in2) {
	printf("%d %d\n", in1.p, in2.p);
}

void print(struct node node1, struct node node2) {
	printf("%d %d\n", node1.in.p, node2.in.p);
	insideprint(node1.in, node2.in);
}

int main()
{
    struct node node1, node2;
    node1.in.p = 2;
    node2.in.p = 3;
    print(node1, node2);
    //system("pause");
    return 0;
}
