#include <stdio.h>

struct A {
	char c;
	struct B {
		char b;
		char c;
		int d;
	}b;
	int a;
};

int main() {
	//int a[4][4] = {{1, 2, 3, 4}};
	//int i, j;
	//if (a[1][1] > 5) i = 3;
	//else i = 4;
	
	struct A* ob1, *ob2;
	ob1 = (struct A *)malloc(sizeof(struct A));
	ob2 = (struct A *)malloc(sizeof(struct A));
	
	printf("%d\n", ob2->b);
	
	ob2->c = 'a';
	ob2->b.b = 123;
	ob2->b.c = 234;
	ob2->b.d = 345;
	ob2->a = 'b';
	ob1 = ob2;
	
	printf("%d\n", ob2->b);
	
	//a[2][2] = 4;
}

/*int main() {
	int **a;
	int i, j;
	int cnt = 0;
	a = (int**)malloc(sizeof(int)*3);
	for (i = 0; i < 3; i++) {
		*(a+i) = (int*)malloc(sizeof(int)*3);
		for (j = 0; j < 3; j++)
			*(*(a+i)+j) = cnt++;
	}
	//int b[3][3] = {1,2,3};
	//a = (int**)b;
	printf("%d\n", (*(a+1)+1));
	for (i = 0; i < 3; i++) {
		for (j = 0;j < 3; j++)
			printf("%d ", (*(a+i)+j));
		printf("\n");
	}
	
	int b[2][2];
	printf("%d", b[1]);
}*/
