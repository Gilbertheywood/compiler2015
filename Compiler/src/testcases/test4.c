struct A {
    char x, y;
    struct A* ptr;
} *a;

int main() {
    a->ptr->x++;
    /*
     * t0 = a + 8
     * t1 = *t0, 4
     * t2 = t0 + 0
     * t3 = *t2, 1
     * t4 = t3
     * t5 = t4 + 1
     * *t2 = t5, 1
     */
}
