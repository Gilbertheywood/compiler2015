#include<stdio.h>
#include<string.h>
int perm[3][6] = {
	{5, 4, 2, 3, 0, 1},
	{3, 2, 0, 1, 4, 5},
	{0, 1, 4, 5, 3, 2}
};

int n;
int a[1111][6] = {
	{1,2,3,4,5,6},
	{3,4,2,1,5,6},
	{4,3,1,2,5,6},
	{5,6,3,4,2,1},
	{1,2,3,4,6,5},
	{6,6,6,6,6,6}
};
 
void transform(int a[6], int k) {
	int b[6], j, i;
	for (j = 0; j < 6; ++j) {
		b[j] = a[j];	
	}
	for (i = 0; i < 6; ++i) {
		a[perm[k][i]] = b[i];	
	}
}

int getInt() {
	char c = getchar();
	int res = 0;
	while(c < '0' || c > '9') {
		c = getchar();
	}
	while(c >= '0' && c <= '9') {
		res = res * 10 + c - '0';
		c = getchar();
	}
	return res;
}

int main() {
	int i, j, ret, t0, t1, t2, b[6], same, ok;
	n = 6;
	a[0][0]=1;a[0][1]=2;a[0][2]=3;a[0][3]=4;a[0][4]=5;a[0][5]=6;
	a[1][0]=3;a[1][1]=4;a[1][2]=2;a[1][3]=1;a[1][4]=5;a[1][5]=6;
	a[2][0]=4;a[2][1]=3;a[2][2]=1;a[2][3]=2;a[2][4]=5;a[2][5]=6;
	a[3][0]=5;a[3][1]=6;a[3][2]=3;a[3][3]=4;a[3][4]=2;a[3][5]=1;
	a[4][0]=1;a[4][1]=2;a[4][2]=3;a[4][3]=4;a[4][4]=6;a[4][5]=5;
	a[5][0]=6;a[5][1]=6;a[5][2]=6;a[5][3]=6;a[5][4]=6;a[5][5]=6;
	//n = getInt();
	/*for(i = 0; i < n; ++i) {
		for(j = 0; j < 6; ++j) {
			a[i][j] = getInt();
			//printf("(%d)", a[i][j]);
		}	
	}*/
	
	ret = 0;
	for (i = 1; i < n; ++i) {
		ok = 0;
		for (t0 = 0; t0 < 4; ++t0) {
			for (t1 = 0; t1 < 4; ++t1) {
				for (t2 = 0; t2 < 4; ++t2) {
					for (j = 0; j < 6; ++j) {
						b[j] = a[i][j];	
					}
					for (j = 0; j < 6; ++j) {
						printf("%d ", b[j]);
					}
					printf("\n");
					for (j = 0; j < t0; ++j) {
						transform(b, 0);
					}
					for (j = 0; j < t1; ++j) {
						transform(b, 1);
					}
					for (j = 0; j < t2; ++j) {
						transform(b, 2);
					}
					same = 1;
					for (j = 0; j < 6; ++j) {
						//printf("%d %d\n", b[j], a[0][j]);
						if (b[j] != a[0][j]) {
							same = 0;
							break;
						}
					}
					if (same) {
						ok = 1;
					}
				}	
			}	
		}
		ret += ok;
	}
	printf("%d\n", ret);
	return 0;
}

