.data
string0: .asciiz "abc"

.text

printf:
printf_loop:
lb $a0,0($a1)
add $a1,$a1,1
beq $a0,0,printf_end
li $v0,11
syscall
b printf_loop
printf_end:
j $31
nop

main:
la $a1,string0
jal printf
nop
j $31
nop
