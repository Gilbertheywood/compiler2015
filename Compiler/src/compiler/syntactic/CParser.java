// Generated from C.g4 by ANTLR 4.2

package compiler.syntactic;
import compiler.ast.*;
import java.util.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__55=1, T__54=2, T__53=3, T__52=4, T__51=5, T__50=6, T__49=7, T__48=8, 
		T__47=9, T__46=10, T__45=11, T__44=12, T__43=13, T__42=14, T__41=15, T__40=16, 
		T__39=17, T__38=18, T__37=19, T__36=20, T__35=21, T__34=22, T__33=23, 
		T__32=24, T__31=25, T__30=26, T__29=27, T__28=28, T__27=29, T__26=30, 
		T__25=31, T__24=32, T__23=33, T__22=34, T__21=35, T__20=36, T__19=37, 
		T__18=38, T__17=39, T__16=40, T__15=41, T__14=42, T__13=43, T__12=44, 
		T__11=45, T__10=46, T__9=47, T__8=48, T__7=49, T__6=50, T__5=51, T__4=52, 
		T__3=53, T__2=54, T__1=55, T__0=56, WhiteSpace=57, MultiComment=58, SingleComment=59, 
		Lib=60, Hex=61, Dec=62, Oct=63, Chr=64, Identifier=65, String=66;
	public static final String[] tokenNames = {
		"<INVALID>", "'+='", "'%='", "'char'", "'!='", "'while'", "'void'", "'{'", 
		"'>>'", "'&&'", "'^='", "'='", "'^'", "'<<='", "'for'", "'|='", "'int'", 
		"'union'", "'('", "'-='", "','", "'/='", "'>='", "'++'", "'<'", "']'", 
		"'~'", "'sizeof'", "'+'", "'struct'", "'/'", "'*='", "'continue'", "'&='", 
		"'return'", "'||'", "'>>='", "';'", "'<<'", "'}'", "'if'", "'<='", "'break'", 
		"'&'", "'*'", "'.'", "'->'", "'['", "'--'", "'=='", "'|'", "'>'", "'!'", 
		"'%'", "'else'", "')'", "'-'", "WhiteSpace", "MultiComment", "SingleComment", 
		"Lib", "Hex", "Dec", "Oct", "Chr", "Identifier", "String"
	};
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_function_definition = 2, 
		RULE_parameters = 3, RULE_declarators = 4, RULE_init_declarators = 5, 
		RULE_init_declarator = 6, RULE_initializer = 7, RULE_type_specifier = 8, 
		RULE_struct_or_union = 9, RULE_plain_declaration = 10, RULE_declarator = 11, 
		RULE_plain_declarator = 12, RULE_statement = 13, RULE_expression_statement = 14, 
		RULE_compound_statement = 15, RULE_selection_statement = 16, RULE_iteration_statement = 17, 
		RULE_jump_statement = 18, RULE_expression = 19, RULE_assignment_expression = 20, 
		RULE_assignment_operator = 21, RULE_constant_expression = 22, RULE_logical_or_expression = 23, 
		RULE_logical_and_expression = 24, RULE_inclusive_or_expression = 25, RULE_exclusive_or_expression = 26, 
		RULE_and_expression = 27, RULE_equality_expression = 28, RULE_equality_operator = 29, 
		RULE_relational_expression = 30, RULE_relational_operator = 31, RULE_shift_expression = 32, 
		RULE_shift_operator = 33, RULE_additive_expression = 34, RULE_additive_operator = 35, 
		RULE_multiplicative_expression = 36, RULE_multiplicative_operator = 37, 
		RULE_cast_expression = 38, RULE_type_name = 39, RULE_unary_expression = 40, 
		RULE_unary_operator = 41, RULE_postfix_expression = 42, RULE_postfix = 43, 
		RULE_arguments = 44, RULE_primary_expression = 45, RULE_constant = 46, 
		RULE_integer_constant = 47, RULE_character_constant = 48;
	public static final String[] ruleNames = {
		"program", "declaration", "function_definition", "parameters", "declarators", 
		"init_declarators", "init_declarator", "initializer", "type_specifier", 
		"struct_or_union", "plain_declaration", "declarator", "plain_declarator", 
		"statement", "expression_statement", "compound_statement", "selection_statement", 
		"iteration_statement", "jump_statement", "expression", "assignment_expression", 
		"assignment_operator", "constant_expression", "logical_or_expression", 
		"logical_and_expression", "inclusive_or_expression", "exclusive_or_expression", 
		"and_expression", "equality_expression", "equality_operator", "relational_expression", 
		"relational_operator", "shift_expression", "shift_operator", "additive_expression", 
		"additive_operator", "multiplicative_expression", "multiplicative_operator", 
		"cast_expression", "type_name", "unary_expression", "unary_operator", 
		"postfix_expression", "postfix", "arguments", "primary_expression", "constant", 
		"integer_constant", "character_constant"
	};

	@Override
	public String getGrammarFileName() { return "C.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	List<Decl> curDecls = new LinkedList<Decl>();
	List<Decl> curFields = new LinkedList<Decl>();
	Type curType;
	int souCount = 0;
	Expr curExpr = null;
	Symbol curSymbol = null;

	public CParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public AST ele;
		public DeclarationContext dcltn;
		public Function_definitionContext funcdef;
		public List<Function_definitionContext> function_definition() {
			return getRuleContexts(Function_definitionContext.class);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public Function_definitionContext function_definition(int i) {
			return getRuleContext(Function_definitionContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ProgramContext)_localctx).ele =  new AST();
			setState(103); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(103);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					curDecls = _localctx.ele.decls;
					setState(100); ((ProgramContext)_localctx).dcltn = declaration();
					}
					break;

				case 2:
					{
					curDecls = _localctx.ele.decls;
					setState(102); ((ProgramContext)_localctx).funcdef = function_definition();
					}
					break;
				}
				}
				setState(105); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 6) | (1L << 16) | (1L << 17) | (1L << 29))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Type_specifierContext tyspc;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Init_declaratorsContext init_declarators() {
			return getRuleContext(Init_declaratorsContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			curFields = curDecls;
			setState(108); ((DeclarationContext)_localctx).tyspc = type_specifier();
			curType = ((DeclarationContext)_localctx).tyspc.ele;
			setState(111);
			_la = _input.LA(1);
			if (_la==44 || _la==Identifier) {
				{
				setState(110); init_declarators();
				}
			}

			setState(113); match(37);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definitionContext extends ParserRuleContext {
		public Type_specifierContext tyspc;
		public Plain_declaratorContext pldcltr;
		public ParametersContext pm;
		public Compound_statementContext compstmt;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Plain_declaratorContext plain_declarator() {
			return getRuleContext(Plain_declaratorContext.class,0);
		}
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public Function_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterFunction_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitFunction_definition(this);
		}
	}

	public final Function_definitionContext function_definition() throws RecognitionException {
		Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_function_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			List<VarDecl> params = new LinkedList<VarDecl>();
			setState(116); ((Function_definitionContext)_localctx).tyspc = type_specifier();
			curType = ((Function_definitionContext)_localctx).tyspc.ele;
			setState(118); ((Function_definitionContext)_localctx).pldcltr = plain_declarator();
			setState(119); match(18);
			setState(123);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 6) | (1L << 16) | (1L << 17) | (1L << 29))) != 0)) {
				{
				setState(120); ((Function_definitionContext)_localctx).pm = parameters();
				params = ((Function_definitionContext)_localctx).pm.ele;
				}
			}

			setState(125); match(55);
			setState(126); ((Function_definitionContext)_localctx).compstmt = compound_statement();
			curDecls.add(new FunctionDecl(((Function_definitionContext)_localctx).pldcltr.ele.type, ((Function_definitionContext)_localctx).pldcltr.ele.name, params, ((Function_definitionContext)_localctx).compstmt.ele));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public List<VarDecl> ele;
		public Plain_declarationContext pldcltn;
		public List<Plain_declarationContext> plain_declaration() {
			return getRuleContexts(Plain_declarationContext.class);
		}
		public Plain_declarationContext plain_declaration(int i) {
			return getRuleContext(Plain_declarationContext.class,i);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitParameters(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_parameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ParametersContext)_localctx).ele =  new LinkedList<VarDecl>();
			setState(130); ((ParametersContext)_localctx).pldcltn = plain_declaration();
			_localctx.ele.add(((ParametersContext)_localctx).pldcltn.ele);
			setState(138);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==20) {
				{
				{
				setState(132); match(20);
				setState(133); ((ParametersContext)_localctx).pldcltn = plain_declaration();
				_localctx.ele.add(((ParametersContext)_localctx).pldcltn.ele);
				}
				}
				setState(140);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorsContext extends ParserRuleContext {
		public DeclaratorContext dcltr;
		public List<DeclaratorContext> declarator() {
			return getRuleContexts(DeclaratorContext.class);
		}
		public DeclaratorContext declarator(int i) {
			return getRuleContext(DeclaratorContext.class,i);
		}
		public DeclaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterDeclarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitDeclarators(this);
		}
	}

	public final DeclaratorsContext declarators() throws RecognitionException {
		DeclaratorsContext _localctx = new DeclaratorsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_declarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141); ((DeclaratorsContext)_localctx).dcltr = declarator();
			curFields.add(((DeclaratorsContext)_localctx).dcltr.ele);
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==20) {
				{
				{
				setState(143); match(20);
				setState(144); ((DeclaratorsContext)_localctx).dcltr = declarator();
				curFields.add(((DeclaratorsContext)_localctx).dcltr.ele);
				}
				}
				setState(151);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorsContext extends ParserRuleContext {
		public Init_declaratorContext init_declarator(int i) {
			return getRuleContext(Init_declaratorContext.class,i);
		}
		public List<Init_declaratorContext> init_declarator() {
			return getRuleContexts(Init_declaratorContext.class);
		}
		public Init_declaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterInit_declarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitInit_declarators(this);
		}
	}

	public final Init_declaratorsContext init_declarators() throws RecognitionException {
		Init_declaratorsContext _localctx = new Init_declaratorsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_init_declarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152); init_declarator();
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==20) {
				{
				{
				setState(153); match(20);
				setState(154); init_declarator();
				}
				}
				setState(159);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorContext extends ParserRuleContext {
		public DeclaratorContext d;
		public InitializerContext initializer;
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public InitializerContext initializer() {
			return getRuleContext(InitializerContext.class,0);
		}
		public Init_declaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterInit_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitInit_declarator(this);
		}
	}

	public final Init_declaratorContext init_declarator() throws RecognitionException {
		Init_declaratorContext _localctx = new Init_declaratorContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_init_declarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			Initializer init = null;
			setState(161); ((Init_declaratorContext)_localctx).d = declarator();
			setState(166);
			_la = _input.LA(1);
			if (_la==11) {
				{
				setState(162); match(11);
				setState(163); ((Init_declaratorContext)_localctx).initializer = initializer();
				init = ((Init_declaratorContext)_localctx).initializer.ele;
				}
			}


			            ((Init_declaratorContext)_localctx).d.ele.init = init;
			            curDecls.add(((Init_declaratorContext)_localctx).d.ele);
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitializerContext extends ParserRuleContext {
		public Initializer ele;
		public Assignment_expressionContext assignexpr;
		public InitializerContext init;
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public List<InitializerContext> initializer() {
			return getRuleContexts(InitializerContext.class);
		}
		public InitializerContext initializer(int i) {
			return getRuleContext(InitializerContext.class,i);
		}
		public InitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitInitializer(this);
		}
	}

	public final InitializerContext initializer() throws RecognitionException {
		InitializerContext _localctx = new InitializerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_initializer);
		int _la;
		try {
			setState(189);
			switch (_input.LA(1)) {
			case 18:
			case 23:
			case 26:
			case 27:
			case 28:
			case 43:
			case 44:
			case 48:
			case 52:
			case 56:
			case Hex:
			case Dec:
			case Oct:
			case Chr:
			case Identifier:
			case String:
				enterOuterAlt(_localctx, 1);
				{
				setState(170); ((InitializerContext)_localctx).assignexpr = assignment_expression();
				((InitializerContext)_localctx).ele =  new InitValue(((InitializerContext)_localctx).assignexpr.ele);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 2);
				{
				List<Initializer> list = new ArrayList<Initializer>();
				setState(174); match(7);
				setState(175); ((InitializerContext)_localctx).init = initializer();
				list.add(((InitializerContext)_localctx).init.ele);
				setState(183);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==20) {
					{
					{
					setState(177); match(20);
					setState(178); ((InitializerContext)_localctx).init = initializer();
					list.add(((InitializerContext)_localctx).init.ele);
					}
					}
					setState(185);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(186); match(39);
				((InitializerContext)_localctx).ele =  new InitList(list);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_specifierContext extends ParserRuleContext {
		public Type ele;
		public Struct_or_unionContext sou;
		public Token idt;
		public Type_specifierContext typspc;
		public Token id;
		public List<Type_specifierContext> type_specifier() {
			return getRuleContexts(Type_specifierContext.class);
		}
		public TerminalNode Identifier() { return getToken(CParser.Identifier, 0); }
		public Type_specifierContext type_specifier(int i) {
			return getRuleContext(Type_specifierContext.class,i);
		}
		public DeclaratorsContext declarators(int i) {
			return getRuleContext(DeclaratorsContext.class,i);
		}
		public List<DeclaratorsContext> declarators() {
			return getRuleContexts(DeclaratorsContext.class);
		}
		public Struct_or_unionContext struct_or_union() {
			return getRuleContext(Struct_or_unionContext.class,0);
		}
		public Type_specifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_specifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterType_specifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitType_specifier(this);
		}
	}

	public final Type_specifierContext type_specifier() throws RecognitionException {
		Type_specifierContext _localctx = new Type_specifierContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type_specifier);
		int _la;
		try {
			setState(220);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(191); match(6);
				((Type_specifierContext)_localctx).ele =  new VoidType();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(193); match(3);
				((Type_specifierContext)_localctx).ele =  new CharType();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(195); match(16);
				((Type_specifierContext)_localctx).ele =  new IntType();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{

				       String id = "#"+(Integer)(souCount++);
				       List<Decl> fields = new LinkedList<Decl>();
				       List<Decl> oldCurfields = curFields;
				       curFields = fields;
				      
				setState(198); ((Type_specifierContext)_localctx).sou = struct_or_union();
				setState(201);
				_la = _input.LA(1);
				if (_la==Identifier) {
					{
					setState(199); ((Type_specifierContext)_localctx).idt = match(Identifier);
					id = (((Type_specifierContext)_localctx).idt!=null?((Type_specifierContext)_localctx).idt.getText():null);souCount--;
					}
				}

				setState(203); match(7);
				setState(209); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(204); ((Type_specifierContext)_localctx).typspc = type_specifier();
					curType = ((Type_specifierContext)_localctx).typspc.ele;
					setState(206); declarators();
					setState(207); match(37);
					}
					}
					setState(211); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 6) | (1L << 16) | (1L << 17) | (1L << 29))) != 0) );
				setState(213); match(39);

				       curFields = oldCurfields;
				       if (((Type_specifierContext)_localctx).sou.ele == Symbol.get("struct")) {
				          curFields.add(new StructDecl(Symbol.get(id), fields));
				          ((Type_specifierContext)_localctx).ele =  new StructType(Symbol.get(id));
				       } else {
				          curFields.add(new UnionDecl(Symbol.get(id), fields));
				          ((Type_specifierContext)_localctx).ele =  new UnionType(Symbol.get(id));
				       }
				      
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(216); ((Type_specifierContext)_localctx).sou = struct_or_union();
				setState(217); ((Type_specifierContext)_localctx).id = match(Identifier);

				       if (((Type_specifierContext)_localctx).sou.ele == Symbol.get("struct")) {
				          ((Type_specifierContext)_localctx).ele =  new StructType(Symbol.get((((Type_specifierContext)_localctx).id!=null?((Type_specifierContext)_localctx).id.getText():null)));
				       } else {
				          ((Type_specifierContext)_localctx).ele =  new UnionType(Symbol.get((((Type_specifierContext)_localctx).id!=null?((Type_specifierContext)_localctx).id.getText():null)));
				       }
				      
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_or_unionContext extends ParserRuleContext {
		public Symbol ele;
		public Struct_or_unionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_or_union; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterStruct_or_union(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitStruct_or_union(this);
		}
	}

	public final Struct_or_unionContext struct_or_union() throws RecognitionException {
		Struct_or_unionContext _localctx = new Struct_or_unionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_struct_or_union);
		try {
			setState(226);
			switch (_input.LA(1)) {
			case 29:
				enterOuterAlt(_localctx, 1);
				{
				setState(222); match(29);
				((Struct_or_unionContext)_localctx).ele =  Symbol.get("struct");
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 2);
				{
				setState(224); match(17);
				((Struct_or_unionContext)_localctx).ele =  Symbol.get("union");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Plain_declarationContext extends ParserRuleContext {
		public VarDecl ele;
		public Type_specifierContext tyspc;
		public DeclaratorContext dclr;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public Plain_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plain_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterPlain_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitPlain_declaration(this);
		}
	}

	public final Plain_declarationContext plain_declaration() throws RecognitionException {
		Plain_declarationContext _localctx = new Plain_declarationContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_plain_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(228); ((Plain_declarationContext)_localctx).tyspc = type_specifier();
			curType = ((Plain_declarationContext)_localctx).tyspc.ele;
			setState(230); ((Plain_declarationContext)_localctx).dclr = declarator();
			((Plain_declarationContext)_localctx).ele =  ((Plain_declarationContext)_localctx).dclr.ele;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorContext extends ParserRuleContext {
		public VarDecl ele;
		public Plain_declaratorContext pd;
		public Constant_expressionContext cons;
		public Constant_expressionContext constant_expression(int i) {
			return getRuleContext(Constant_expressionContext.class,i);
		}
		public Plain_declaratorContext plain_declarator() {
			return getRuleContext(Plain_declaratorContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public List<Constant_expressionContext> constant_expression() {
			return getRuleContexts(Constant_expressionContext.class);
		}
		public DeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitDeclarator(this);
		}
	}

	public final DeclaratorContext declarator() throws RecognitionException {
		DeclaratorContext _localctx = new DeclaratorContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_declarator);
		int _la;
		try {
			setState(253);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(233); plain_declarator();
				setState(234); match(18);
				setState(236);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 6) | (1L << 16) | (1L << 17) | (1L << 29))) != 0)) {
					{
					setState(235); parameters();
					}
				}

				setState(238); match(55);
				System.out.println("function initializer error");System.exit(1);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(241); ((DeclaratorContext)_localctx).pd = plain_declarator();
				((DeclaratorContext)_localctx).ele =  ((DeclaratorContext)_localctx).pd.ele;
				setState(250);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==47) {
					{
					{
					setState(243); match(47);
					setState(244); ((DeclaratorContext)_localctx).cons = constant_expression();
					setState(245); match(25);
					_localctx.ele.type = new ArrayType(_localctx.ele.type,((DeclaratorContext)_localctx).cons.ele);
					}
					}
					setState(252);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Plain_declaratorContext extends ParserRuleContext {
		public VarDecl ele;
		public Token id;
		public TerminalNode Identifier() { return getToken(CParser.Identifier, 0); }
		public Plain_declaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plain_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterPlain_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitPlain_declarator(this);
		}
	}

	public final Plain_declaratorContext plain_declarator() throws RecognitionException {
		Plain_declaratorContext _localctx = new Plain_declaratorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_plain_declarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			Type type = curType;
			setState(260);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==44) {
				{
				{
				setState(256); match(44);
				type = new PointerType(type);
				}
				}
				setState(262);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(263); ((Plain_declaratorContext)_localctx).id = match(Identifier);
			((Plain_declaratorContext)_localctx).ele =  new VarDecl(type, Symbol.get((((Plain_declaratorContext)_localctx).id!=null?((Plain_declaratorContext)_localctx).id.getText():null)), null);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Stmt ele;
		public Expression_statementContext es;
		public Compound_statementContext cs;
		public Selection_statementContext ss;
		public Iteration_statementContext is;
		public Jump_statementContext js;
		public Selection_statementContext selection_statement() {
			return getRuleContext(Selection_statementContext.class,0);
		}
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public Jump_statementContext jump_statement() {
			return getRuleContext(Jump_statementContext.class,0);
		}
		public Expression_statementContext expression_statement() {
			return getRuleContext(Expression_statementContext.class,0);
		}
		public Iteration_statementContext iteration_statement() {
			return getRuleContext(Iteration_statementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_statement);
		try {
			setState(281);
			switch (_input.LA(1)) {
			case 18:
			case 23:
			case 26:
			case 27:
			case 28:
			case 37:
			case 43:
			case 44:
			case 48:
			case 52:
			case 56:
			case Hex:
			case Dec:
			case Oct:
			case Chr:
			case Identifier:
			case String:
				enterOuterAlt(_localctx, 1);
				{
				setState(266); ((StatementContext)_localctx).es = expression_statement();
				((StatementContext)_localctx).ele =  ((StatementContext)_localctx).es.ele;
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 2);
				{
				setState(269); ((StatementContext)_localctx).cs = compound_statement();
				((StatementContext)_localctx).ele =  ((StatementContext)_localctx).cs.ele;
				}
				break;
			case 40:
				enterOuterAlt(_localctx, 3);
				{
				setState(272); ((StatementContext)_localctx).ss = selection_statement();
				((StatementContext)_localctx).ele =  ((StatementContext)_localctx).ss.ele;
				}
				break;
			case 5:
			case 14:
				enterOuterAlt(_localctx, 4);
				{
				setState(275); ((StatementContext)_localctx).is = iteration_statement();
				((StatementContext)_localctx).ele =  ((StatementContext)_localctx).is.ele;
				}
				break;
			case 32:
			case 34:
			case 42:
				enterOuterAlt(_localctx, 5);
				{
				setState(278); ((StatementContext)_localctx).js = jump_statement();
				((StatementContext)_localctx).ele =  ((StatementContext)_localctx).js.ele;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_statementContext extends ParserRuleContext {
		public Expr ele;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterExpression_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitExpression_statement(this);
		}
	}

	public final Expression_statementContext expression_statement() throws RecognitionException {
		Expression_statementContext _localctx = new Expression_statementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_expression_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((Expression_statementContext)_localctx).ele =  new EmptyExpr();
			setState(287);
			_la = _input.LA(1);
			if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
				{
				setState(284); ((Expression_statementContext)_localctx).expr = expression();
				((Expression_statementContext)_localctx).ele =  ((Expression_statementContext)_localctx).expr.ele;
				}
			}

			setState(289); match(37);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_statementContext extends ParserRuleContext {
		public CompoundStmt ele;
		public StatementContext s;
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public Compound_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterCompound_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitCompound_statement(this);
		}
	}

	public final Compound_statementContext compound_statement() throws RecognitionException {
		Compound_statementContext _localctx = new Compound_statementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_compound_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

			         List<Decl> decls = new LinkedList<Decl>();
			         List<Stmt> stmts = new LinkedList<Stmt>();
			         List<Decl> oldCurDecls = curDecls;
			         curDecls = decls;
			         
			setState(292); match(7);
			setState(296);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 6) | (1L << 16) | (1L << 17) | (1L << 29))) != 0)) {
				{
				{
				setState(293); declaration();
				}
				}
				setState(298);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(304);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (5 - 5)) | (1L << (7 - 5)) | (1L << (14 - 5)) | (1L << (18 - 5)) | (1L << (23 - 5)) | (1L << (26 - 5)) | (1L << (27 - 5)) | (1L << (28 - 5)) | (1L << (32 - 5)) | (1L << (34 - 5)) | (1L << (37 - 5)) | (1L << (40 - 5)) | (1L << (42 - 5)) | (1L << (43 - 5)) | (1L << (44 - 5)) | (1L << (48 - 5)) | (1L << (52 - 5)) | (1L << (56 - 5)) | (1L << (Hex - 5)) | (1L << (Dec - 5)) | (1L << (Oct - 5)) | (1L << (Chr - 5)) | (1L << (Identifier - 5)) | (1L << (String - 5)))) != 0)) {
				{
				{
				setState(299); ((Compound_statementContext)_localctx).s = statement();
				stmts.add(((Compound_statementContext)_localctx).s.ele);
				}
				}
				setState(306);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(307); match(39);

			            curDecls = oldCurDecls;
			            ((Compound_statementContext)_localctx).ele =  new CompoundStmt(decls, stmts);
			         
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Selection_statementContext extends ParserRuleContext {
		public IfStmt ele;
		public ExpressionContext expr;
		public StatementContext stmt1;
		public StatementContext stmt2;
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Selection_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selection_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterSelection_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitSelection_statement(this);
		}
	}

	public final Selection_statementContext selection_statement() throws RecognitionException {
		Selection_statementContext _localctx = new Selection_statementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_selection_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			Stmt s2 = null;
			setState(311); match(40);
			setState(312); match(18);
			setState(313); ((Selection_statementContext)_localctx).expr = expression();
			setState(314); match(55);
			setState(315); ((Selection_statementContext)_localctx).stmt1 = statement();
			setState(320);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
				setState(316); match(54);
				setState(317); ((Selection_statementContext)_localctx).stmt2 = statement();
				s2 = ((Selection_statementContext)_localctx).stmt2.ele;
				}
				break;
			}
			((Selection_statementContext)_localctx).ele =  new IfStmt(((Selection_statementContext)_localctx).expr.ele, ((Selection_statementContext)_localctx).stmt1.ele, s2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Iteration_statementContext extends ParserRuleContext {
		public Stmt ele;
		public ExpressionContext expr;
		public StatementContext stmt;
		public ExpressionContext e1;
		public ExpressionContext e2;
		public ExpressionContext e3;
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public Iteration_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iteration_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterIteration_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitIteration_statement(this);
		}
	}

	public final Iteration_statementContext iteration_statement() throws RecognitionException {
		Iteration_statementContext _localctx = new Iteration_statementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_iteration_statement);
		int _la;
		try {
			setState(355);
			switch (_input.LA(1)) {
			case 5:
				enterOuterAlt(_localctx, 1);
				{
				setState(324); match(5);
				setState(325); match(18);
				setState(326); ((Iteration_statementContext)_localctx).expr = expression();
				setState(327); match(55);
				setState(328); ((Iteration_statementContext)_localctx).stmt = statement();
				((Iteration_statementContext)_localctx).ele =  new WhileLoop(((Iteration_statementContext)_localctx).expr.ele, ((Iteration_statementContext)_localctx).stmt.ele);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 2);
				{
				Expr tmpe1 = null; Expr tmpe2 = null; Expr tmpe3 = null;
				setState(332); match(14);
				setState(333); match(18);
				setState(337);
				_la = _input.LA(1);
				if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
					{
					setState(334); ((Iteration_statementContext)_localctx).e1 = expression();
					tmpe1 = ((Iteration_statementContext)_localctx).e1.ele;
					}
				}

				setState(339); match(37);
				setState(343);
				_la = _input.LA(1);
				if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
					{
					setState(340); ((Iteration_statementContext)_localctx).e2 = expression();
					tmpe2 = ((Iteration_statementContext)_localctx).e2.ele;
					}
				}

				setState(345); match(37);
				setState(349);
				_la = _input.LA(1);
				if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
					{
					setState(346); ((Iteration_statementContext)_localctx).e3 = expression();
					tmpe3 = ((Iteration_statementContext)_localctx).e3.ele;
					}
				}

				setState(351); match(55);
				setState(352); ((Iteration_statementContext)_localctx).stmt = statement();
				((Iteration_statementContext)_localctx).ele =  new ForLoop(tmpe1, tmpe2, tmpe3, ((Iteration_statementContext)_localctx).stmt.ele);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jump_statementContext extends ParserRuleContext {
		public Stmt ele;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Jump_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jump_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterJump_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitJump_statement(this);
		}
	}

	public final Jump_statementContext jump_statement() throws RecognitionException {
		Jump_statementContext _localctx = new Jump_statementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_jump_statement);
		int _la;
		try {
			setState(372);
			switch (_input.LA(1)) {
			case 32:
				enterOuterAlt(_localctx, 1);
				{
				setState(357); match(32);
				setState(358); match(37);
				((Jump_statementContext)_localctx).ele =  new ContinueStmt();
				}
				break;
			case 42:
				enterOuterAlt(_localctx, 2);
				{
				setState(360); match(42);
				setState(361); match(37);
				((Jump_statementContext)_localctx).ele =  new BreakStmt();
				}
				break;
			case 34:
				enterOuterAlt(_localctx, 3);
				{
				Expr exprtmp = new EmptyExpr();
				setState(364); match(34);
				setState(368);
				_la = _input.LA(1);
				if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
					{
					setState(365); ((Jump_statementContext)_localctx).expr = expression();
					exprtmp = ((Jump_statementContext)_localctx).expr.ele;
					}
				}

				setState(370); match(37);
				((Jump_statementContext)_localctx).ele =  new ReturnStmt(exprtmp);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Expr ele;
		public Assignment_expressionContext ae;
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(374); ((ExpressionContext)_localctx).ae = assignment_expression();
			((ExpressionContext)_localctx).ele =  ((ExpressionContext)_localctx).ae.ele;
			setState(382);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==20) {
				{
				{
				setState(376); match(20);
				setState(377); ((ExpressionContext)_localctx).ae = assignment_expression();
				((ExpressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele,BinaryOp.COMMA, ((ExpressionContext)_localctx).ae.ele);
				}
				}
				setState(384);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Logical_or_expressionContext l;
		public Unary_expressionContext u;
		public Assignment_operatorContext ao;
		public Assignment_expressionContext ae;
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Assignment_operatorContext assignment_operator() {
			return getRuleContext(Assignment_operatorContext.class,0);
		}
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Assignment_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterAssignment_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitAssignment_expression(this);
		}
	}

	public final Assignment_expressionContext assignment_expression() throws RecognitionException {
		Assignment_expressionContext _localctx = new Assignment_expressionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_assignment_expression);
		try {
			setState(393);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(385); ((Assignment_expressionContext)_localctx).l = logical_or_expression();
				((Assignment_expressionContext)_localctx).ele =  ((Assignment_expressionContext)_localctx).l.ele;
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(388); ((Assignment_expressionContext)_localctx).u = unary_expression();
				setState(389); ((Assignment_expressionContext)_localctx).ao = assignment_operator();
				setState(390); ((Assignment_expressionContext)_localctx).ae = assignment_expression();
				((Assignment_expressionContext)_localctx).ele =  new BinaryExpr(((Assignment_expressionContext)_localctx).u.ele, ((Assignment_expressionContext)_localctx).ao.ele, ((Assignment_expressionContext)_localctx).ae.ele);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Assignment_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterAssignment_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitAssignment_operator(this);
		}
	}

	public final Assignment_operatorContext assignment_operator() throws RecognitionException {
		Assignment_operatorContext _localctx = new Assignment_operatorContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_assignment_operator);
		try {
			setState(417);
			switch (_input.LA(1)) {
			case 11:
				enterOuterAlt(_localctx, 1);
				{
				setState(395); match(11);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN;
				}
				break;
			case 31:
				enterOuterAlt(_localctx, 2);
				{
				setState(397); match(31);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_MUL;
				}
				break;
			case 21:
				enterOuterAlt(_localctx, 3);
				{
				setState(399); match(21);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_DIV;
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 4);
				{
				setState(401); match(2);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_MOD;
				}
				break;
			case 1:
				enterOuterAlt(_localctx, 5);
				{
				setState(403); match(1);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_ADD;
				}
				break;
			case 19:
				enterOuterAlt(_localctx, 6);
				{
				setState(405); match(19);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_SUB;
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 7);
				{
				setState(407); match(13);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_SHL;
				}
				break;
			case 36:
				enterOuterAlt(_localctx, 8);
				{
				setState(409); match(36);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_SHR;
				}
				break;
			case 33:
				enterOuterAlt(_localctx, 9);
				{
				setState(411); match(33);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_AND;
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(413); match(10);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_XOR;
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 11);
				{
				setState(415); match(15);
				((Assignment_operatorContext)_localctx).ele =  BinaryOp.ASSIGN_OR;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constant_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Logical_or_expressionContext loe;
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Constant_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterConstant_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitConstant_expression(this);
		}
	}

	public final Constant_expressionContext constant_expression() throws RecognitionException {
		Constant_expressionContext _localctx = new Constant_expressionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_constant_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419); ((Constant_expressionContext)_localctx).loe = logical_or_expression();
			((Constant_expressionContext)_localctx).ele =  ((Constant_expressionContext)_localctx).loe.ele;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_or_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Logical_and_expressionContext l;
		public Logical_and_expressionContext logical_and_expression(int i) {
			return getRuleContext(Logical_and_expressionContext.class,i);
		}
		public List<Logical_and_expressionContext> logical_and_expression() {
			return getRuleContexts(Logical_and_expressionContext.class);
		}
		public Logical_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterLogical_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitLogical_or_expression(this);
		}
	}

	public final Logical_or_expressionContext logical_or_expression() throws RecognitionException {
		Logical_or_expressionContext _localctx = new Logical_or_expressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_logical_or_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(422); ((Logical_or_expressionContext)_localctx).l = logical_and_expression();
			((Logical_or_expressionContext)_localctx).ele =  ((Logical_or_expressionContext)_localctx).l.ele;
			setState(430);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==35) {
				{
				{
				setState(424); match(35);
				setState(425); ((Logical_or_expressionContext)_localctx).l = logical_and_expression();
				((Logical_or_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, BinaryOp.OR, ((Logical_or_expressionContext)_localctx).l.ele);
				}
				}
				setState(432);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_and_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Inclusive_or_expressionContext i;
		public Inclusive_or_expressionContext inclusive_or_expression(int i) {
			return getRuleContext(Inclusive_or_expressionContext.class,i);
		}
		public List<Inclusive_or_expressionContext> inclusive_or_expression() {
			return getRuleContexts(Inclusive_or_expressionContext.class);
		}
		public Logical_and_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterLogical_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitLogical_and_expression(this);
		}
	}

	public final Logical_and_expressionContext logical_and_expression() throws RecognitionException {
		Logical_and_expressionContext _localctx = new Logical_and_expressionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_logical_and_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(433); ((Logical_and_expressionContext)_localctx).i = inclusive_or_expression();
			((Logical_and_expressionContext)_localctx).ele =  ((Logical_and_expressionContext)_localctx).i.ele;
			setState(441);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==9) {
				{
				{
				setState(435); match(9);
				setState(436); ((Logical_and_expressionContext)_localctx).i = inclusive_or_expression();
				((Logical_and_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, BinaryOp.AND, ((Logical_and_expressionContext)_localctx).i.ele);
				}
				}
				setState(443);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Inclusive_or_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Exclusive_or_expressionContext e;
		public List<Exclusive_or_expressionContext> exclusive_or_expression() {
			return getRuleContexts(Exclusive_or_expressionContext.class);
		}
		public Exclusive_or_expressionContext exclusive_or_expression(int i) {
			return getRuleContext(Exclusive_or_expressionContext.class,i);
		}
		public Inclusive_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusive_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterInclusive_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitInclusive_or_expression(this);
		}
	}

	public final Inclusive_or_expressionContext inclusive_or_expression() throws RecognitionException {
		Inclusive_or_expressionContext _localctx = new Inclusive_or_expressionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_inclusive_or_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(444); ((Inclusive_or_expressionContext)_localctx).e = exclusive_or_expression();
			((Inclusive_or_expressionContext)_localctx).ele =  ((Inclusive_or_expressionContext)_localctx).e.ele;
			setState(452);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==50) {
				{
				{
				setState(446); match(50);
				setState(447); ((Inclusive_or_expressionContext)_localctx).e = exclusive_or_expression();
				((Inclusive_or_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, BinaryOp.LOGICAL_OR, ((Inclusive_or_expressionContext)_localctx).e.ele);
				}
				}
				setState(454);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Exclusive_or_expressionContext extends ParserRuleContext {
		public Expr ele;
		public And_expressionContext a;
		public And_expressionContext and_expression(int i) {
			return getRuleContext(And_expressionContext.class,i);
		}
		public List<And_expressionContext> and_expression() {
			return getRuleContexts(And_expressionContext.class);
		}
		public Exclusive_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusive_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterExclusive_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitExclusive_or_expression(this);
		}
	}

	public final Exclusive_or_expressionContext exclusive_or_expression() throws RecognitionException {
		Exclusive_or_expressionContext _localctx = new Exclusive_or_expressionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_exclusive_or_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(455); ((Exclusive_or_expressionContext)_localctx).a = and_expression();
			((Exclusive_or_expressionContext)_localctx).ele =  ((Exclusive_or_expressionContext)_localctx).a.ele;
			setState(463);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==12) {
				{
				{
				setState(457); match(12);
				setState(458); ((Exclusive_or_expressionContext)_localctx).a = and_expression();
				((Exclusive_or_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, BinaryOp.XOR, ((Exclusive_or_expressionContext)_localctx).a.ele);
				}
				}
				setState(465);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class And_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Equality_expressionContext e;
		public Equality_expressionContext equality_expression(int i) {
			return getRuleContext(Equality_expressionContext.class,i);
		}
		public List<Equality_expressionContext> equality_expression() {
			return getRuleContexts(Equality_expressionContext.class);
		}
		public And_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterAnd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitAnd_expression(this);
		}
	}

	public final And_expressionContext and_expression() throws RecognitionException {
		And_expressionContext _localctx = new And_expressionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_and_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(466); ((And_expressionContext)_localctx).e = equality_expression();
			((And_expressionContext)_localctx).ele =  ((And_expressionContext)_localctx).e.ele;
			setState(474);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==43) {
				{
				{
				setState(468); match(43);
				setState(469); ((And_expressionContext)_localctx).e = equality_expression();
				((And_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, BinaryOp.LOGICAL_AND, ((And_expressionContext)_localctx).e.ele);
				}
				}
				setState(476);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Equality_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Relational_expressionContext r;
		public Equality_operatorContext e;
		public List<Relational_expressionContext> relational_expression() {
			return getRuleContexts(Relational_expressionContext.class);
		}
		public Relational_expressionContext relational_expression(int i) {
			return getRuleContext(Relational_expressionContext.class,i);
		}
		public List<Equality_operatorContext> equality_operator() {
			return getRuleContexts(Equality_operatorContext.class);
		}
		public Equality_operatorContext equality_operator(int i) {
			return getRuleContext(Equality_operatorContext.class,i);
		}
		public Equality_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterEquality_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitEquality_expression(this);
		}
	}

	public final Equality_expressionContext equality_expression() throws RecognitionException {
		Equality_expressionContext _localctx = new Equality_expressionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_equality_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(477); ((Equality_expressionContext)_localctx).r = relational_expression();
			((Equality_expressionContext)_localctx).ele =  ((Equality_expressionContext)_localctx).r.ele;
			setState(485);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4 || _la==49) {
				{
				{
				setState(479); ((Equality_expressionContext)_localctx).e = equality_operator();
				setState(480); ((Equality_expressionContext)_localctx).r = relational_expression();
				((Equality_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, ((Equality_expressionContext)_localctx).e.ele, ((Equality_expressionContext)_localctx).r.ele);
				}
				}
				setState(487);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Equality_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Equality_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterEquality_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitEquality_operator(this);
		}
	}

	public final Equality_operatorContext equality_operator() throws RecognitionException {
		Equality_operatorContext _localctx = new Equality_operatorContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_equality_operator);
		try {
			setState(492);
			switch (_input.LA(1)) {
			case 49:
				enterOuterAlt(_localctx, 1);
				{
				setState(488); match(49);
				((Equality_operatorContext)_localctx).ele =  BinaryOp.EQ;
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 2);
				{
				setState(490); match(4);
				((Equality_operatorContext)_localctx).ele =  BinaryOp.NE;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Relational_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Shift_expressionContext s;
		public Relational_operatorContext r;
		public List<Relational_operatorContext> relational_operator() {
			return getRuleContexts(Relational_operatorContext.class);
		}
		public Relational_operatorContext relational_operator(int i) {
			return getRuleContext(Relational_operatorContext.class,i);
		}
		public List<Shift_expressionContext> shift_expression() {
			return getRuleContexts(Shift_expressionContext.class);
		}
		public Shift_expressionContext shift_expression(int i) {
			return getRuleContext(Shift_expressionContext.class,i);
		}
		public Relational_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relational_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterRelational_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitRelational_expression(this);
		}
	}

	public final Relational_expressionContext relational_expression() throws RecognitionException {
		Relational_expressionContext _localctx = new Relational_expressionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_relational_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(494); ((Relational_expressionContext)_localctx).s = shift_expression();
			((Relational_expressionContext)_localctx).ele =  ((Relational_expressionContext)_localctx).s.ele;
			setState(502);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 22) | (1L << 24) | (1L << 41) | (1L << 51))) != 0)) {
				{
				{
				setState(496); ((Relational_expressionContext)_localctx).r = relational_operator();
				setState(497); ((Relational_expressionContext)_localctx).s = shift_expression();
				((Relational_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, ((Relational_expressionContext)_localctx).r.ele, ((Relational_expressionContext)_localctx).s.ele);
				}
				}
				setState(504);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Relational_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Relational_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relational_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterRelational_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitRelational_operator(this);
		}
	}

	public final Relational_operatorContext relational_operator() throws RecognitionException {
		Relational_operatorContext _localctx = new Relational_operatorContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_relational_operator);
		try {
			setState(513);
			switch (_input.LA(1)) {
			case 24:
				enterOuterAlt(_localctx, 1);
				{
				setState(505); match(24);
				((Relational_operatorContext)_localctx).ele =  BinaryOp.LT;
				}
				break;
			case 51:
				enterOuterAlt(_localctx, 2);
				{
				setState(507); match(51);
				((Relational_operatorContext)_localctx).ele =  BinaryOp.GT;
				}
				break;
			case 41:
				enterOuterAlt(_localctx, 3);
				{
				setState(509); match(41);
				((Relational_operatorContext)_localctx).ele =  BinaryOp.LE;
				}
				break;
			case 22:
				enterOuterAlt(_localctx, 4);
				{
				setState(511); match(22);
				((Relational_operatorContext)_localctx).ele =  BinaryOp.GE;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Shift_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Additive_expressionContext a;
		public Shift_operatorContext s;
		public Additive_expressionContext additive_expression(int i) {
			return getRuleContext(Additive_expressionContext.class,i);
		}
		public List<Shift_operatorContext> shift_operator() {
			return getRuleContexts(Shift_operatorContext.class);
		}
		public Shift_operatorContext shift_operator(int i) {
			return getRuleContext(Shift_operatorContext.class,i);
		}
		public List<Additive_expressionContext> additive_expression() {
			return getRuleContexts(Additive_expressionContext.class);
		}
		public Shift_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterShift_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitShift_expression(this);
		}
	}

	public final Shift_expressionContext shift_expression() throws RecognitionException {
		Shift_expressionContext _localctx = new Shift_expressionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_shift_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515); ((Shift_expressionContext)_localctx).a = additive_expression();
			((Shift_expressionContext)_localctx).ele =  ((Shift_expressionContext)_localctx).a.ele;
			setState(523);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==8 || _la==38) {
				{
				{
				setState(517); ((Shift_expressionContext)_localctx).s = shift_operator();
				setState(518); ((Shift_expressionContext)_localctx).a = additive_expression();
				((Shift_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, ((Shift_expressionContext)_localctx).s.ele, ((Shift_expressionContext)_localctx).a.ele);
				}
				}
				setState(525);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Shift_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Shift_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterShift_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitShift_operator(this);
		}
	}

	public final Shift_operatorContext shift_operator() throws RecognitionException {
		Shift_operatorContext _localctx = new Shift_operatorContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_shift_operator);
		try {
			setState(530);
			switch (_input.LA(1)) {
			case 38:
				enterOuterAlt(_localctx, 1);
				{
				setState(526); match(38);
				((Shift_operatorContext)_localctx).ele =  BinaryOp.SHL;
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 2);
				{
				setState(528); match(8);
				((Shift_operatorContext)_localctx).ele =  BinaryOp.SHR;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Additive_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Multiplicative_expressionContext m;
		public Additive_operatorContext a;
		public List<Additive_operatorContext> additive_operator() {
			return getRuleContexts(Additive_operatorContext.class);
		}
		public Multiplicative_expressionContext multiplicative_expression(int i) {
			return getRuleContext(Multiplicative_expressionContext.class,i);
		}
		public Additive_operatorContext additive_operator(int i) {
			return getRuleContext(Additive_operatorContext.class,i);
		}
		public List<Multiplicative_expressionContext> multiplicative_expression() {
			return getRuleContexts(Multiplicative_expressionContext.class);
		}
		public Additive_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additive_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterAdditive_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitAdditive_expression(this);
		}
	}

	public final Additive_expressionContext additive_expression() throws RecognitionException {
		Additive_expressionContext _localctx = new Additive_expressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_additive_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(532); ((Additive_expressionContext)_localctx).m = multiplicative_expression();
			((Additive_expressionContext)_localctx).ele =  ((Additive_expressionContext)_localctx).m.ele;
			setState(540);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==28 || _la==56) {
				{
				{
				setState(534); ((Additive_expressionContext)_localctx).a = additive_operator();
				setState(535); ((Additive_expressionContext)_localctx).m = multiplicative_expression();
				((Additive_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, ((Additive_expressionContext)_localctx).a.ele, ((Additive_expressionContext)_localctx).m.ele);
				}
				}
				setState(542);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Additive_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Additive_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additive_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterAdditive_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitAdditive_operator(this);
		}
	}

	public final Additive_operatorContext additive_operator() throws RecognitionException {
		Additive_operatorContext _localctx = new Additive_operatorContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_additive_operator);
		try {
			setState(547);
			switch (_input.LA(1)) {
			case 28:
				enterOuterAlt(_localctx, 1);
				{
				setState(543); match(28);
				((Additive_operatorContext)_localctx).ele =  BinaryOp.ADD;
				}
				break;
			case 56:
				enterOuterAlt(_localctx, 2);
				{
				setState(545); match(56);
				((Additive_operatorContext)_localctx).ele =  BinaryOp.SUB;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multiplicative_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Cast_expressionContext c;
		public Multiplicative_operatorContext m;
		public Multiplicative_operatorContext multiplicative_operator(int i) {
			return getRuleContext(Multiplicative_operatorContext.class,i);
		}
		public Cast_expressionContext cast_expression(int i) {
			return getRuleContext(Cast_expressionContext.class,i);
		}
		public List<Cast_expressionContext> cast_expression() {
			return getRuleContexts(Cast_expressionContext.class);
		}
		public List<Multiplicative_operatorContext> multiplicative_operator() {
			return getRuleContexts(Multiplicative_operatorContext.class);
		}
		public Multiplicative_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicative_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterMultiplicative_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitMultiplicative_expression(this);
		}
	}

	public final Multiplicative_expressionContext multiplicative_expression() throws RecognitionException {
		Multiplicative_expressionContext _localctx = new Multiplicative_expressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_multiplicative_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(549); ((Multiplicative_expressionContext)_localctx).c = cast_expression();
			((Multiplicative_expressionContext)_localctx).ele =  ((Multiplicative_expressionContext)_localctx).c.ele;
			setState(557);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 30) | (1L << 44) | (1L << 53))) != 0)) {
				{
				{
				setState(551); ((Multiplicative_expressionContext)_localctx).m = multiplicative_operator();
				setState(552); ((Multiplicative_expressionContext)_localctx).c = cast_expression();
				((Multiplicative_expressionContext)_localctx).ele =  new BinaryExpr(_localctx.ele, ((Multiplicative_expressionContext)_localctx).m.ele, ((Multiplicative_expressionContext)_localctx).c.ele);
				}
				}
				setState(559);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multiplicative_operatorContext extends ParserRuleContext {
		public BinaryOp ele;
		public Multiplicative_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicative_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterMultiplicative_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitMultiplicative_operator(this);
		}
	}

	public final Multiplicative_operatorContext multiplicative_operator() throws RecognitionException {
		Multiplicative_operatorContext _localctx = new Multiplicative_operatorContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_multiplicative_operator);
		try {
			setState(566);
			switch (_input.LA(1)) {
			case 44:
				enterOuterAlt(_localctx, 1);
				{
				setState(560); match(44);
				((Multiplicative_operatorContext)_localctx).ele =  BinaryOp.MUL;
				}
				break;
			case 30:
				enterOuterAlt(_localctx, 2);
				{
				setState(562); match(30);
				((Multiplicative_operatorContext)_localctx).ele =  BinaryOp.DIV;
				}
				break;
			case 53:
				enterOuterAlt(_localctx, 3);
				{
				setState(564); match(53);
				((Multiplicative_operatorContext)_localctx).ele =  BinaryOp.MOD;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cast_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Unary_expressionContext u;
		public Type_nameContext t;
		public Cast_expressionContext c;
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Cast_expressionContext cast_expression() {
			return getRuleContext(Cast_expressionContext.class,0);
		}
		public Cast_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cast_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterCast_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitCast_expression(this);
		}
	}

	public final Cast_expressionContext cast_expression() throws RecognitionException {
		Cast_expressionContext _localctx = new Cast_expressionContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_cast_expression);
		try {
			setState(577);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(568); ((Cast_expressionContext)_localctx).u = unary_expression();
				((Cast_expressionContext)_localctx).ele =  ((Cast_expressionContext)_localctx).u.ele;
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(571); match(18);
				setState(572); ((Cast_expressionContext)_localctx).t = type_name();
				setState(573); match(55);
				setState(574); ((Cast_expressionContext)_localctx).c = cast_expression();
				((Cast_expressionContext)_localctx).ele =  new CastExpr(((Cast_expressionContext)_localctx).t.ele, ((Cast_expressionContext)_localctx).c.ele);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public Type ele;
		public Type_specifierContext t;
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitType_name(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_type_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(579); ((Type_nameContext)_localctx).t = type_specifier();
			((Type_nameContext)_localctx).ele =  ((Type_nameContext)_localctx).t.ele;
			setState(585);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==44) {
				{
				{
				setState(581); match(44);
				((Type_nameContext)_localctx).ele =  new PointerType(_localctx.ele);
				}
				}
				setState(587);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Postfix_expressionContext p;
		public Unary_expressionContext u;
		public Unary_operatorContext o;
		public Cast_expressionContext c;
		public Type_nameContext t;
		public Postfix_expressionContext postfix_expression() {
			return getRuleContext(Postfix_expressionContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Cast_expressionContext cast_expression() {
			return getRuleContext(Cast_expressionContext.class,0);
		}
		public Unary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterUnary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitUnary_expression(this);
		}
	}

	public final Unary_expressionContext unary_expression() throws RecognitionException {
		Unary_expressionContext _localctx = new Unary_expressionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_unary_expression);
		try {
			setState(613);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(588); ((Unary_expressionContext)_localctx).p = postfix_expression();
				((Unary_expressionContext)_localctx).ele =  ((Unary_expressionContext)_localctx).p.ele;
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(591); match(23);
				setState(592); ((Unary_expressionContext)_localctx).u = unary_expression();
				((Unary_expressionContext)_localctx).ele =  new UnaryExpr(UnaryOp.INC, ((Unary_expressionContext)_localctx).u.ele);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(595); match(48);
				setState(596); ((Unary_expressionContext)_localctx).u = unary_expression();
				((Unary_expressionContext)_localctx).ele =  new UnaryExpr(UnaryOp.DEC, ((Unary_expressionContext)_localctx).u.ele);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(599); ((Unary_expressionContext)_localctx).o = unary_operator();
				setState(600); ((Unary_expressionContext)_localctx).c = cast_expression();
				((Unary_expressionContext)_localctx).ele =  new UnaryExpr(((Unary_expressionContext)_localctx).o.ele, ((Unary_expressionContext)_localctx).c.ele);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(603); match(27);
				setState(604); ((Unary_expressionContext)_localctx).u = unary_expression();
				((Unary_expressionContext)_localctx).ele =  new UnaryExpr(UnaryOp.SIZEOF, ((Unary_expressionContext)_localctx).u.ele);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(607); match(27);
				setState(608); match(18);
				setState(609); ((Unary_expressionContext)_localctx).t = type_name();
				setState(610); match(55);
				((Unary_expressionContext)_localctx).ele =  new SizeofExpr(((Unary_expressionContext)_localctx).t.ele);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public UnaryOp ele;
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitUnary_operator(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_unary_operator);
		try {
			setState(627);
			switch (_input.LA(1)) {
			case 43:
				enterOuterAlt(_localctx, 1);
				{
				setState(615); match(43);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.AMPERSAND;
				}
				break;
			case 44:
				enterOuterAlt(_localctx, 2);
				{
				setState(617); match(44);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.ASTERISK;
				}
				break;
			case 28:
				enterOuterAlt(_localctx, 3);
				{
				setState(619); match(28);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.PLUS;
				}
				break;
			case 56:
				enterOuterAlt(_localctx, 4);
				{
				setState(621); match(56);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.MINUS;
				}
				break;
			case 26:
				enterOuterAlt(_localctx, 5);
				{
				setState(623); match(26);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.TILDE;
				}
				break;
			case 52:
				enterOuterAlt(_localctx, 6);
				{
				setState(625); match(52);
				((Unary_operatorContext)_localctx).ele =  UnaryOp.NOT;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Postfix_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Primary_expressionContext p;
		public PostfixContext pf;
		public PostfixContext postfix(int i) {
			return getRuleContext(PostfixContext.class,i);
		}
		public Primary_expressionContext primary_expression() {
			return getRuleContext(Primary_expressionContext.class,0);
		}
		public List<PostfixContext> postfix() {
			return getRuleContexts(PostfixContext.class);
		}
		public Postfix_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterPostfix_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitPostfix_expression(this);
		}
	}

	public final Postfix_expressionContext postfix_expression() throws RecognitionException {
		Postfix_expressionContext _localctx = new Postfix_expressionContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_postfix_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(629); ((Postfix_expressionContext)_localctx).p = primary_expression();
			((Postfix_expressionContext)_localctx).ele =  ((Postfix_expressionContext)_localctx).p.ele;curExpr = ((Postfix_expressionContext)_localctx).p.ele;
			setState(636);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 18) | (1L << 23) | (1L << 45) | (1L << 46) | (1L << 47) | (1L << 48))) != 0)) {
				{
				{
				setState(631); ((Postfix_expressionContext)_localctx).pf = postfix();
				((Postfix_expressionContext)_localctx).ele =  ((Postfix_expressionContext)_localctx).pf.ele;curExpr = ((Postfix_expressionContext)_localctx).pf.ele;
				}
				}
				setState(638);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixContext extends ParserRuleContext {
		public Expr ele;
		public ExpressionContext e;
		public ArgumentsContext arguments;
		public Token id;
		public TerminalNode Identifier() { return getToken(CParser.Identifier, 0); }
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PostfixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterPostfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitPostfix(this);
		}
	}

	public final PostfixContext postfix() throws RecognitionException {
		PostfixContext _localctx = new PostfixContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_postfix);
		int _la;
		try {
			setState(664);
			switch (_input.LA(1)) {
			case 47:
				enterOuterAlt(_localctx, 1);
				{
				Expr bodyexpr = curExpr;
				setState(640); match(47);
				setState(641); ((PostfixContext)_localctx).e = expression();
				setState(642); match(25);
				((PostfixContext)_localctx).ele =  new ArrayAccess(bodyexpr, ((PostfixContext)_localctx).e.ele);
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 2);
				{

				             List<Expr> argu = new LinkedList<Expr>();
				             Symbol sym = curSymbol;
				            
				setState(646); match(18);
				setState(650);
				_la = _input.LA(1);
				if (((((_la - 18)) & ~0x3f) == 0 && ((1L << (_la - 18)) & ((1L << (18 - 18)) | (1L << (23 - 18)) | (1L << (26 - 18)) | (1L << (27 - 18)) | (1L << (28 - 18)) | (1L << (43 - 18)) | (1L << (44 - 18)) | (1L << (48 - 18)) | (1L << (52 - 18)) | (1L << (56 - 18)) | (1L << (Hex - 18)) | (1L << (Dec - 18)) | (1L << (Oct - 18)) | (1L << (Chr - 18)) | (1L << (Identifier - 18)) | (1L << (String - 18)))) != 0)) {
					{
					setState(647); ((PostfixContext)_localctx).arguments = arguments();
					argu = ((PostfixContext)_localctx).arguments.ele;
					}
				}

				setState(652); match(55);
				((PostfixContext)_localctx).ele =  new FunctionCall(sym, argu);
				}
				break;
			case 45:
				enterOuterAlt(_localctx, 3);
				{
				setState(654); match(45);
				setState(655); ((PostfixContext)_localctx).id = match(Identifier);
				((PostfixContext)_localctx).ele =  new RecordAccess(curExpr, Symbol.get((((PostfixContext)_localctx).id!=null?((PostfixContext)_localctx).id.getText():null)));
				}
				break;
			case 46:
				enterOuterAlt(_localctx, 4);
				{
				setState(657); match(46);
				setState(658); ((PostfixContext)_localctx).id = match(Identifier);
				((PostfixContext)_localctx).ele =  new PointerAccess(curExpr, Symbol.get((((PostfixContext)_localctx).id!=null?((PostfixContext)_localctx).id.getText():null)));
				}
				break;
			case 23:
				enterOuterAlt(_localctx, 5);
				{
				setState(660); match(23);
				((PostfixContext)_localctx).ele =  new SelfIncrement(curExpr);
				}
				break;
			case 48:
				enterOuterAlt(_localctx, 6);
				{
				setState(662); match(48);
				((PostfixContext)_localctx).ele =  new SelfDecrement(curExpr);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public List<Expr> ele;
		public Assignment_expressionContext a;
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitArguments(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ArgumentsContext)_localctx).ele =  new LinkedList<Expr>();
			setState(667); ((ArgumentsContext)_localctx).a = assignment_expression();
			_localctx.ele.add(((ArgumentsContext)_localctx).a.ele);
			setState(675);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==20) {
				{
				{
				setState(669); match(20);
				setState(670); ((ArgumentsContext)_localctx).a = assignment_expression();
				_localctx.ele.add(((ArgumentsContext)_localctx).a.ele);
				}
				}
				setState(677);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primary_expressionContext extends ParserRuleContext {
		public Expr ele;
		public Token id;
		public ConstantContext c;
		public Token s;
		public ExpressionContext e;
		public TerminalNode Identifier() { return getToken(CParser.Identifier, 0); }
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public TerminalNode String() { return getToken(CParser.String, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Primary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterPrimary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitPrimary_expression(this);
		}
	}

	public final Primary_expressionContext primary_expression() throws RecognitionException {
		Primary_expressionContext _localctx = new Primary_expressionContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_primary_expression);
		try {
			setState(690);
			switch (_input.LA(1)) {
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(678); ((Primary_expressionContext)_localctx).id = match(Identifier);
				((Primary_expressionContext)_localctx).ele =  new Identifier(Symbol.get((((Primary_expressionContext)_localctx).id!=null?((Primary_expressionContext)_localctx).id.getText():null)));curSymbol = Symbol.get((((Primary_expressionContext)_localctx).id!=null?((Primary_expressionContext)_localctx).id.getText():null));
				}
				break;
			case Hex:
			case Dec:
			case Oct:
			case Chr:
				enterOuterAlt(_localctx, 2);
				{
				setState(680); ((Primary_expressionContext)_localctx).c = constant();
				((Primary_expressionContext)_localctx).ele =  ((Primary_expressionContext)_localctx).c.ele;
				}
				break;
			case String:
				enterOuterAlt(_localctx, 3);
				{
				setState(683); ((Primary_expressionContext)_localctx).s = match(String);
				((Primary_expressionContext)_localctx).ele =  new StringConst((((Primary_expressionContext)_localctx).s!=null?((Primary_expressionContext)_localctx).s.getText():null));
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 4);
				{
				setState(685); match(18);
				setState(686); ((Primary_expressionContext)_localctx).e = expression();
				setState(687); match(55);
				((Primary_expressionContext)_localctx).ele =  ((Primary_expressionContext)_localctx).e.ele;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public Expr ele;
		public Integer_constantContext i;
		public Character_constantContext c;
		public Character_constantContext character_constant() {
			return getRuleContext(Character_constantContext.class,0);
		}
		public Integer_constantContext integer_constant() {
			return getRuleContext(Integer_constantContext.class,0);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_constant);
		try {
			setState(698);
			switch (_input.LA(1)) {
			case Hex:
			case Dec:
			case Oct:
				enterOuterAlt(_localctx, 1);
				{
				setState(692); ((ConstantContext)_localctx).i = integer_constant();
				((ConstantContext)_localctx).ele =  ((ConstantContext)_localctx).i.ele;
				}
				break;
			case Chr:
				enterOuterAlt(_localctx, 2);
				{
				setState(695); ((ConstantContext)_localctx).c = character_constant();
				((ConstantContext)_localctx).ele =  ((ConstantContext)_localctx).c.ele;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Integer_constantContext extends ParserRuleContext {
		public Expr ele;
		public Token Hex;
		public Token Dec;
		public Token Oct;
		public TerminalNode Hex() { return getToken(CParser.Hex, 0); }
		public TerminalNode Oct() { return getToken(CParser.Oct, 0); }
		public TerminalNode Dec() { return getToken(CParser.Dec, 0); }
		public Integer_constantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterInteger_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitInteger_constant(this);
		}
	}

	public final Integer_constantContext integer_constant() throws RecognitionException {
		Integer_constantContext _localctx = new Integer_constantContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_integer_constant);
		try {
			setState(706);
			switch (_input.LA(1)) {
			case Hex:
				enterOuterAlt(_localctx, 1);
				{
				setState(700); ((Integer_constantContext)_localctx).Hex = match(Hex);
				((Integer_constantContext)_localctx).ele =  new IntConst(Integer.parseInt((((Integer_constantContext)_localctx).Hex!=null?((Integer_constantContext)_localctx).Hex.getText():null).substring(2), 16));
				}
				break;
			case Dec:
				enterOuterAlt(_localctx, 2);
				{
				setState(702); ((Integer_constantContext)_localctx).Dec = match(Dec);
				((Integer_constantContext)_localctx).ele =  new IntConst(Integer.parseInt((((Integer_constantContext)_localctx).Dec!=null?((Integer_constantContext)_localctx).Dec.getText():null),10));
				}
				break;
			case Oct:
				enterOuterAlt(_localctx, 3);
				{
				setState(704); ((Integer_constantContext)_localctx).Oct = match(Oct);
				((Integer_constantContext)_localctx).ele =  new IntConst(Integer.parseInt((((Integer_constantContext)_localctx).Oct!=null?((Integer_constantContext)_localctx).Oct.getText():null),8));
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Character_constantContext extends ParserRuleContext {
		public Expr ele;
		public Token Chr;
		public TerminalNode Chr() { return getToken(CParser.Chr, 0); }
		public Character_constantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_character_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).enterCharacter_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CListener ) ((CListener)listener).exitCharacter_constant(this);
		}
	}

	public final Character_constantContext character_constant() throws RecognitionException {
		Character_constantContext _localctx = new Character_constantContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_character_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(708); ((Character_constantContext)_localctx).Chr = match(Chr);
			((Character_constantContext)_localctx).ele =  new CharConst((((Character_constantContext)_localctx).Chr!=null?((Character_constantContext)_localctx).Chr.getText():null));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3D\u02ca\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\3\2\3\2\3\2\3"+
		"\2\6\2j\n\2\r\2\16\2k\3\3\3\3\3\3\3\3\5\3r\n\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\5\4~\n\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\7\5\u008b\n\5\f\5\16\5\u008e\13\5\3\6\3\6\3\6\3\6\3\6\3\6\7\6\u0096\n"+
		"\6\f\6\16\6\u0099\13\6\3\7\3\7\3\7\7\7\u009e\n\7\f\7\16\7\u00a1\13\7\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a9\n\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\7\t\u00b8\n\t\f\t\16\t\u00bb\13\t\3\t\3\t\3\t\5\t\u00c0"+
		"\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00cc\n\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\6\n\u00d4\n\n\r\n\16\n\u00d5\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\5\n\u00df\n\n\3\13\3\13\3\13\3\13\5\13\u00e5\n\13\3\f\3\f\3\f\3\f\3\f"+
		"\3\r\3\r\3\r\5\r\u00ef\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r"+
		"\u00fb\n\r\f\r\16\r\u00fe\13\r\5\r\u0100\n\r\3\16\3\16\3\16\7\16\u0105"+
		"\n\16\f\16\16\16\u0108\13\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u011c\n\17\3\20"+
		"\3\20\3\20\3\20\5\20\u0122\n\20\3\20\3\20\3\21\3\21\3\21\7\21\u0129\n"+
		"\21\f\21\16\21\u012c\13\21\3\21\3\21\3\21\7\21\u0131\n\21\f\21\16\21\u0134"+
		"\13\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\5\22\u0143\n\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\5\23\u0154\n\23\3\23\3\23\3\23\3\23\5\23\u015a\n"+
		"\23\3\23\3\23\3\23\3\23\5\23\u0160\n\23\3\23\3\23\3\23\3\23\5\23\u0166"+
		"\n\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u0173"+
		"\n\24\3\24\3\24\5\24\u0177\n\24\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u017f"+
		"\n\25\f\25\16\25\u0182\13\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5"+
		"\26\u018c\n\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u01a4\n\27"+
		"\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u01af\n\31\f\31\16"+
		"\31\u01b2\13\31\3\32\3\32\3\32\3\32\3\32\3\32\7\32\u01ba\n\32\f\32\16"+
		"\32\u01bd\13\32\3\33\3\33\3\33\3\33\3\33\3\33\7\33\u01c5\n\33\f\33\16"+
		"\33\u01c8\13\33\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u01d0\n\34\f\34\16"+
		"\34\u01d3\13\34\3\35\3\35\3\35\3\35\3\35\3\35\7\35\u01db\n\35\f\35\16"+
		"\35\u01de\13\35\3\36\3\36\3\36\3\36\3\36\3\36\7\36\u01e6\n\36\f\36\16"+
		"\36\u01e9\13\36\3\37\3\37\3\37\3\37\5\37\u01ef\n\37\3 \3 \3 \3 \3 \3 "+
		"\7 \u01f7\n \f \16 \u01fa\13 \3!\3!\3!\3!\3!\3!\3!\3!\5!\u0204\n!\3\""+
		"\3\"\3\"\3\"\3\"\3\"\7\"\u020c\n\"\f\"\16\"\u020f\13\"\3#\3#\3#\3#\5#"+
		"\u0215\n#\3$\3$\3$\3$\3$\3$\7$\u021d\n$\f$\16$\u0220\13$\3%\3%\3%\3%\5"+
		"%\u0226\n%\3&\3&\3&\3&\3&\3&\7&\u022e\n&\f&\16&\u0231\13&\3\'\3\'\3\'"+
		"\3\'\3\'\3\'\5\'\u0239\n\'\3(\3(\3(\3(\3(\3(\3(\3(\3(\5(\u0244\n(\3)\3"+
		")\3)\3)\7)\u024a\n)\f)\16)\u024d\13)\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*"+
		"\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\5*\u0268\n*\3+\3+\3+\3+\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\5+\u0276\n+\3,\3,\3,\3,\3,\7,\u027d\n,\f,\16,\u0280"+
		"\13,\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\5-\u028d\n-\3-\3-\3-\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\5-\u029b\n-\3.\3.\3.\3.\3.\3.\3.\7.\u02a4\n.\f.\16.\u02a7"+
		"\13.\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\5/\u02b5\n/\3\60\3\60\3\60\3"+
		"\60\3\60\3\60\5\60\u02bd\n\60\3\61\3\61\3\61\3\61\3\61\3\61\5\61\u02c5"+
		"\n\61\3\62\3\62\3\62\3\62\2\2\63\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36"+
		" \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`b\2\2\u02f4\2d\3\2\2\2\4m\3"+
		"\2\2\2\6u\3\2\2\2\b\u0083\3\2\2\2\n\u008f\3\2\2\2\f\u009a\3\2\2\2\16\u00a2"+
		"\3\2\2\2\20\u00bf\3\2\2\2\22\u00de\3\2\2\2\24\u00e4\3\2\2\2\26\u00e6\3"+
		"\2\2\2\30\u00ff\3\2\2\2\32\u0101\3\2\2\2\34\u011b\3\2\2\2\36\u011d\3\2"+
		"\2\2 \u0125\3\2\2\2\"\u0138\3\2\2\2$\u0165\3\2\2\2&\u0176\3\2\2\2(\u0178"+
		"\3\2\2\2*\u018b\3\2\2\2,\u01a3\3\2\2\2.\u01a5\3\2\2\2\60\u01a8\3\2\2\2"+
		"\62\u01b3\3\2\2\2\64\u01be\3\2\2\2\66\u01c9\3\2\2\28\u01d4\3\2\2\2:\u01df"+
		"\3\2\2\2<\u01ee\3\2\2\2>\u01f0\3\2\2\2@\u0203\3\2\2\2B\u0205\3\2\2\2D"+
		"\u0214\3\2\2\2F\u0216\3\2\2\2H\u0225\3\2\2\2J\u0227\3\2\2\2L\u0238\3\2"+
		"\2\2N\u0243\3\2\2\2P\u0245\3\2\2\2R\u0267\3\2\2\2T\u0275\3\2\2\2V\u0277"+
		"\3\2\2\2X\u029a\3\2\2\2Z\u029c\3\2\2\2\\\u02b4\3\2\2\2^\u02bc\3\2\2\2"+
		"`\u02c4\3\2\2\2b\u02c6\3\2\2\2di\b\2\1\2ef\b\2\1\2fj\5\4\3\2gh\b\2\1\2"+
		"hj\5\6\4\2ie\3\2\2\2ig\3\2\2\2jk\3\2\2\2ki\3\2\2\2kl\3\2\2\2l\3\3\2\2"+
		"\2mn\b\3\1\2no\5\22\n\2oq\b\3\1\2pr\5\f\7\2qp\3\2\2\2qr\3\2\2\2rs\3\2"+
		"\2\2st\7\'\2\2t\5\3\2\2\2uv\b\4\1\2vw\5\22\n\2wx\b\4\1\2xy\5\32\16\2y"+
		"}\7\24\2\2z{\5\b\5\2{|\b\4\1\2|~\3\2\2\2}z\3\2\2\2}~\3\2\2\2~\177\3\2"+
		"\2\2\177\u0080\79\2\2\u0080\u0081\5 \21\2\u0081\u0082\b\4\1\2\u0082\7"+
		"\3\2\2\2\u0083\u0084\b\5\1\2\u0084\u0085\5\26\f\2\u0085\u008c\b\5\1\2"+
		"\u0086\u0087\7\26\2\2\u0087\u0088\5\26\f\2\u0088\u0089\b\5\1\2\u0089\u008b"+
		"\3\2\2\2\u008a\u0086\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3\2\2\2\u008c"+
		"\u008d\3\2\2\2\u008d\t\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0090\5\30\r"+
		"\2\u0090\u0097\b\6\1\2\u0091\u0092\7\26\2\2\u0092\u0093\5\30\r\2\u0093"+
		"\u0094\b\6\1\2\u0094\u0096\3\2\2\2\u0095\u0091\3\2\2\2\u0096\u0099\3\2"+
		"\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\13\3\2\2\2\u0099\u0097"+
		"\3\2\2\2\u009a\u009f\5\16\b\2\u009b\u009c\7\26\2\2\u009c\u009e\5\16\b"+
		"\2\u009d\u009b\3\2\2\2\u009e\u00a1\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0"+
		"\3\2\2\2\u00a0\r\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a3\b\b\1\2\u00a3"+
		"\u00a8\5\30\r\2\u00a4\u00a5\7\r\2\2\u00a5\u00a6\5\20\t\2\u00a6\u00a7\b"+
		"\b\1\2\u00a7\u00a9\3\2\2\2\u00a8\u00a4\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9"+
		"\u00aa\3\2\2\2\u00aa\u00ab\b\b\1\2\u00ab\17\3\2\2\2\u00ac\u00ad\5*\26"+
		"\2\u00ad\u00ae\b\t\1\2\u00ae\u00c0\3\2\2\2\u00af\u00b0\b\t\1\2\u00b0\u00b1"+
		"\7\t\2\2\u00b1\u00b2\5\20\t\2\u00b2\u00b9\b\t\1\2\u00b3\u00b4\7\26\2\2"+
		"\u00b4\u00b5\5\20\t\2\u00b5\u00b6\b\t\1\2\u00b6\u00b8\3\2\2\2\u00b7\u00b3"+
		"\3\2\2\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba"+
		"\u00bc\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7)\2\2\u00bd\u00be\b\t"+
		"\1\2\u00be\u00c0\3\2\2\2\u00bf\u00ac\3\2\2\2\u00bf\u00af\3\2\2\2\u00c0"+
		"\21\3\2\2\2\u00c1\u00c2\7\b\2\2\u00c2\u00df\b\n\1\2\u00c3\u00c4\7\5\2"+
		"\2\u00c4\u00df\b\n\1\2\u00c5\u00c6\7\22\2\2\u00c6\u00df\b\n\1\2\u00c7"+
		"\u00c8\b\n\1\2\u00c8\u00cb\5\24\13\2\u00c9\u00ca\7C\2\2\u00ca\u00cc\b"+
		"\n\1\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd"+
		"\u00d3\7\t\2\2\u00ce\u00cf\5\22\n\2\u00cf\u00d0\b\n\1\2\u00d0\u00d1\5"+
		"\n\6\2\u00d1\u00d2\7\'\2\2\u00d2\u00d4\3\2\2\2\u00d3\u00ce\3\2\2\2\u00d4"+
		"\u00d5\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2"+
		"\2\2\u00d7\u00d8\7)\2\2\u00d8\u00d9\b\n\1\2\u00d9\u00df\3\2\2\2\u00da"+
		"\u00db\5\24\13\2\u00db\u00dc\7C\2\2\u00dc\u00dd\b\n\1\2\u00dd\u00df\3"+
		"\2\2\2\u00de\u00c1\3\2\2\2\u00de\u00c3\3\2\2\2\u00de\u00c5\3\2\2\2\u00de"+
		"\u00c7\3\2\2\2\u00de\u00da\3\2\2\2\u00df\23\3\2\2\2\u00e0\u00e1\7\37\2"+
		"\2\u00e1\u00e5\b\13\1\2\u00e2\u00e3\7\23\2\2\u00e3\u00e5\b\13\1\2\u00e4"+
		"\u00e0\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\25\3\2\2\2\u00e6\u00e7\5\22\n"+
		"\2\u00e7\u00e8\b\f\1\2\u00e8\u00e9\5\30\r\2\u00e9\u00ea\b\f\1\2\u00ea"+
		"\27\3\2\2\2\u00eb\u00ec\5\32\16\2\u00ec\u00ee\7\24\2\2\u00ed\u00ef\5\b"+
		"\5\2\u00ee\u00ed\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f0\3\2\2\2\u00f0"+
		"\u00f1\79\2\2\u00f1\u00f2\b\r\1\2\u00f2\u0100\3\2\2\2\u00f3\u00f4\5\32"+
		"\16\2\u00f4\u00fc\b\r\1\2\u00f5\u00f6\7\61\2\2\u00f6\u00f7\5.\30\2\u00f7"+
		"\u00f8\7\33\2\2\u00f8\u00f9\b\r\1\2\u00f9\u00fb\3\2\2\2\u00fa\u00f5\3"+
		"\2\2\2\u00fb\u00fe\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd"+
		"\u0100\3\2\2\2\u00fe\u00fc\3\2\2\2\u00ff\u00eb\3\2\2\2\u00ff\u00f3\3\2"+
		"\2\2\u0100\31\3\2\2\2\u0101\u0106\b\16\1\2\u0102\u0103\7.\2\2\u0103\u0105"+
		"\b\16\1\2\u0104\u0102\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2"+
		"\u0106\u0107\3\2\2\2\u0107\u0109\3\2\2\2\u0108\u0106\3\2\2\2\u0109\u010a"+
		"\7C\2\2\u010a\u010b\b\16\1\2\u010b\33\3\2\2\2\u010c\u010d\5\36\20\2\u010d"+
		"\u010e\b\17\1\2\u010e\u011c\3\2\2\2\u010f\u0110\5 \21\2\u0110\u0111\b"+
		"\17\1\2\u0111\u011c\3\2\2\2\u0112\u0113\5\"\22\2\u0113\u0114\b\17\1\2"+
		"\u0114\u011c\3\2\2\2\u0115\u0116\5$\23\2\u0116\u0117\b\17\1\2\u0117\u011c"+
		"\3\2\2\2\u0118\u0119\5&\24\2\u0119\u011a\b\17\1\2\u011a\u011c\3\2\2\2"+
		"\u011b\u010c\3\2\2\2\u011b\u010f\3\2\2\2\u011b\u0112\3\2\2\2\u011b\u0115"+
		"\3\2\2\2\u011b\u0118\3\2\2\2\u011c\35\3\2\2\2\u011d\u0121\b\20\1\2\u011e"+
		"\u011f\5(\25\2\u011f\u0120\b\20\1\2\u0120\u0122\3\2\2\2\u0121\u011e\3"+
		"\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0124\7\'\2\2\u0124"+
		"\37\3\2\2\2\u0125\u0126\b\21\1\2\u0126\u012a\7\t\2\2\u0127\u0129\5\4\3"+
		"\2\u0128\u0127\3\2\2\2\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b"+
		"\3\2\2\2\u012b\u0132\3\2\2\2\u012c\u012a\3\2\2\2\u012d\u012e\5\34\17\2"+
		"\u012e\u012f\b\21\1\2\u012f\u0131\3\2\2\2\u0130\u012d\3\2\2\2\u0131\u0134"+
		"\3\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0135\3\2\2\2\u0134"+
		"\u0132\3\2\2\2\u0135\u0136\7)\2\2\u0136\u0137\b\21\1\2\u0137!\3\2\2\2"+
		"\u0138\u0139\b\22\1\2\u0139\u013a\7*\2\2\u013a\u013b\7\24\2\2\u013b\u013c"+
		"\5(\25\2\u013c\u013d\79\2\2\u013d\u0142\5\34\17\2\u013e\u013f\78\2\2\u013f"+
		"\u0140\5\34\17\2\u0140\u0141\b\22\1\2\u0141\u0143\3\2\2\2\u0142\u013e"+
		"\3\2\2\2\u0142\u0143\3\2\2\2\u0143\u0144\3\2\2\2\u0144\u0145\b\22\1\2"+
		"\u0145#\3\2\2\2\u0146\u0147\7\7\2\2\u0147\u0148\7\24\2\2\u0148\u0149\5"+
		"(\25\2\u0149\u014a\79\2\2\u014a\u014b\5\34\17\2\u014b\u014c\b\23\1\2\u014c"+
		"\u0166\3\2\2\2\u014d\u014e\b\23\1\2\u014e\u014f\7\20\2\2\u014f\u0153\7"+
		"\24\2\2\u0150\u0151\5(\25\2\u0151\u0152\b\23\1\2\u0152\u0154\3\2\2\2\u0153"+
		"\u0150\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0159\7\'"+
		"\2\2\u0156\u0157\5(\25\2\u0157\u0158\b\23\1\2\u0158\u015a\3\2\2\2\u0159"+
		"\u0156\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u015f\7\'"+
		"\2\2\u015c\u015d\5(\25\2\u015d\u015e\b\23\1\2\u015e\u0160\3\2\2\2\u015f"+
		"\u015c\3\2\2\2\u015f\u0160\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0162\79"+
		"\2\2\u0162\u0163\5\34\17\2\u0163\u0164\b\23\1\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0146\3\2\2\2\u0165\u014d\3\2\2\2\u0166%\3\2\2\2\u0167\u0168\7\"\2\2"+
		"\u0168\u0169\7\'\2\2\u0169\u0177\b\24\1\2\u016a\u016b\7,\2\2\u016b\u016c"+
		"\7\'\2\2\u016c\u0177\b\24\1\2\u016d\u016e\b\24\1\2\u016e\u0172\7$\2\2"+
		"\u016f\u0170\5(\25\2\u0170\u0171\b\24\1\2\u0171\u0173\3\2\2\2\u0172\u016f"+
		"\3\2\2\2\u0172\u0173\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0175\7\'\2\2\u0175"+
		"\u0177\b\24\1\2\u0176\u0167\3\2\2\2\u0176\u016a\3\2\2\2\u0176\u016d\3"+
		"\2\2\2\u0177\'\3\2\2\2\u0178\u0179\5*\26\2\u0179\u0180\b\25\1\2\u017a"+
		"\u017b\7\26\2\2\u017b\u017c\5*\26\2\u017c\u017d\b\25\1\2\u017d\u017f\3"+
		"\2\2\2\u017e\u017a\3\2\2\2\u017f\u0182\3\2\2\2\u0180\u017e\3\2\2\2\u0180"+
		"\u0181\3\2\2\2\u0181)\3\2\2\2\u0182\u0180\3\2\2\2\u0183\u0184\5\60\31"+
		"\2\u0184\u0185\b\26\1\2\u0185\u018c\3\2\2\2\u0186\u0187\5R*\2\u0187\u0188"+
		"\5,\27\2\u0188\u0189\5*\26\2\u0189\u018a\b\26\1\2\u018a\u018c\3\2\2\2"+
		"\u018b\u0183\3\2\2\2\u018b\u0186\3\2\2\2\u018c+\3\2\2\2\u018d\u018e\7"+
		"\r\2\2\u018e\u01a4\b\27\1\2\u018f\u0190\7!\2\2\u0190\u01a4\b\27\1\2\u0191"+
		"\u0192\7\27\2\2\u0192\u01a4\b\27\1\2\u0193\u0194\7\4\2\2\u0194\u01a4\b"+
		"\27\1\2\u0195\u0196\7\3\2\2\u0196\u01a4\b\27\1\2\u0197\u0198\7\25\2\2"+
		"\u0198\u01a4\b\27\1\2\u0199\u019a\7\17\2\2\u019a\u01a4\b\27\1\2\u019b"+
		"\u019c\7&\2\2\u019c\u01a4\b\27\1\2\u019d\u019e\7#\2\2\u019e\u01a4\b\27"+
		"\1\2\u019f\u01a0\7\f\2\2\u01a0\u01a4\b\27\1\2\u01a1\u01a2\7\21\2\2\u01a2"+
		"\u01a4\b\27\1\2\u01a3\u018d\3\2\2\2\u01a3\u018f\3\2\2\2\u01a3\u0191\3"+
		"\2\2\2\u01a3\u0193\3\2\2\2\u01a3\u0195\3\2\2\2\u01a3\u0197\3\2\2\2\u01a3"+
		"\u0199\3\2\2\2\u01a3\u019b\3\2\2\2\u01a3\u019d\3\2\2\2\u01a3\u019f\3\2"+
		"\2\2\u01a3\u01a1\3\2\2\2\u01a4-\3\2\2\2\u01a5\u01a6\5\60\31\2\u01a6\u01a7"+
		"\b\30\1\2\u01a7/\3\2\2\2\u01a8\u01a9\5\62\32\2\u01a9\u01b0\b\31\1\2\u01aa"+
		"\u01ab\7%\2\2\u01ab\u01ac\5\62\32\2\u01ac\u01ad\b\31\1\2\u01ad\u01af\3"+
		"\2\2\2\u01ae\u01aa\3\2\2\2\u01af\u01b2\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b0"+
		"\u01b1\3\2\2\2\u01b1\61\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b3\u01b4\5\64\33"+
		"\2\u01b4\u01bb\b\32\1\2\u01b5\u01b6\7\13\2\2\u01b6\u01b7\5\64\33\2\u01b7"+
		"\u01b8\b\32\1\2\u01b8\u01ba\3\2\2\2\u01b9\u01b5\3\2\2\2\u01ba\u01bd\3"+
		"\2\2\2\u01bb\u01b9\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\63\3\2\2\2\u01bd"+
		"\u01bb\3\2\2\2\u01be\u01bf\5\66\34\2\u01bf\u01c6\b\33\1\2\u01c0\u01c1"+
		"\7\64\2\2\u01c1\u01c2\5\66\34\2\u01c2\u01c3\b\33\1\2\u01c3\u01c5\3\2\2"+
		"\2\u01c4\u01c0\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7"+
		"\3\2\2\2\u01c7\65\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9\u01ca\58\35\2\u01ca"+
		"\u01d1\b\34\1\2\u01cb\u01cc\7\16\2\2\u01cc\u01cd\58\35\2\u01cd\u01ce\b"+
		"\34\1\2\u01ce\u01d0\3\2\2\2\u01cf\u01cb\3\2\2\2\u01d0\u01d3\3\2\2\2\u01d1"+
		"\u01cf\3\2\2\2\u01d1\u01d2\3\2\2\2\u01d2\67\3\2\2\2\u01d3\u01d1\3\2\2"+
		"\2\u01d4\u01d5\5:\36\2\u01d5\u01dc\b\35\1\2\u01d6\u01d7\7-\2\2\u01d7\u01d8"+
		"\5:\36\2\u01d8\u01d9\b\35\1\2\u01d9\u01db\3\2\2\2\u01da\u01d6\3\2\2\2"+
		"\u01db\u01de\3\2\2\2\u01dc\u01da\3\2\2\2\u01dc\u01dd\3\2\2\2\u01dd9\3"+
		"\2\2\2\u01de\u01dc\3\2\2\2\u01df\u01e0\5> \2\u01e0\u01e7\b\36\1\2\u01e1"+
		"\u01e2\5<\37\2\u01e2\u01e3\5> \2\u01e3\u01e4\b\36\1\2\u01e4\u01e6\3\2"+
		"\2\2\u01e5\u01e1\3\2\2\2\u01e6\u01e9\3\2\2\2\u01e7\u01e5\3\2\2\2\u01e7"+
		"\u01e8\3\2\2\2\u01e8;\3\2\2\2\u01e9\u01e7\3\2\2\2\u01ea\u01eb\7\63\2\2"+
		"\u01eb\u01ef\b\37\1\2\u01ec\u01ed\7\6\2\2\u01ed\u01ef\b\37\1\2\u01ee\u01ea"+
		"\3\2\2\2\u01ee\u01ec\3\2\2\2\u01ef=\3\2\2\2\u01f0\u01f1\5B\"\2\u01f1\u01f8"+
		"\b \1\2\u01f2\u01f3\5@!\2\u01f3\u01f4\5B\"\2\u01f4\u01f5\b \1\2\u01f5"+
		"\u01f7\3\2\2\2\u01f6\u01f2\3\2\2\2\u01f7\u01fa\3\2\2\2\u01f8\u01f6\3\2"+
		"\2\2\u01f8\u01f9\3\2\2\2\u01f9?\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fb\u01fc"+
		"\7\32\2\2\u01fc\u0204\b!\1\2\u01fd\u01fe\7\65\2\2\u01fe\u0204\b!\1\2\u01ff"+
		"\u0200\7+\2\2\u0200\u0204\b!\1\2\u0201\u0202\7\30\2\2\u0202\u0204\b!\1"+
		"\2\u0203\u01fb\3\2\2\2\u0203\u01fd\3\2\2\2\u0203\u01ff\3\2\2\2\u0203\u0201"+
		"\3\2\2\2\u0204A\3\2\2\2\u0205\u0206\5F$\2\u0206\u020d\b\"\1\2\u0207\u0208"+
		"\5D#\2\u0208\u0209\5F$\2\u0209\u020a\b\"\1\2\u020a\u020c\3\2\2\2\u020b"+
		"\u0207\3\2\2\2\u020c\u020f\3\2\2\2\u020d\u020b\3\2\2\2\u020d\u020e\3\2"+
		"\2\2\u020eC\3\2\2\2\u020f\u020d\3\2\2\2\u0210\u0211\7(\2\2\u0211\u0215"+
		"\b#\1\2\u0212\u0213\7\n\2\2\u0213\u0215\b#\1\2\u0214\u0210\3\2\2\2\u0214"+
		"\u0212\3\2\2\2\u0215E\3\2\2\2\u0216\u0217\5J&\2\u0217\u021e\b$\1\2\u0218"+
		"\u0219\5H%\2\u0219\u021a\5J&\2\u021a\u021b\b$\1\2\u021b\u021d\3\2\2\2"+
		"\u021c\u0218\3\2\2\2\u021d\u0220\3\2\2\2\u021e\u021c\3\2\2\2\u021e\u021f"+
		"\3\2\2\2\u021fG\3\2\2\2\u0220\u021e\3\2\2\2\u0221\u0222\7\36\2\2\u0222"+
		"\u0226\b%\1\2\u0223\u0224\7:\2\2\u0224\u0226\b%\1\2\u0225\u0221\3\2\2"+
		"\2\u0225\u0223\3\2\2\2\u0226I\3\2\2\2\u0227\u0228\5N(\2\u0228\u022f\b"+
		"&\1\2\u0229\u022a\5L\'\2\u022a\u022b\5N(\2\u022b\u022c\b&\1\2\u022c\u022e"+
		"\3\2\2\2\u022d\u0229\3\2\2\2\u022e\u0231\3\2\2\2\u022f\u022d\3\2\2\2\u022f"+
		"\u0230\3\2\2\2\u0230K\3\2\2\2\u0231\u022f\3\2\2\2\u0232\u0233\7.\2\2\u0233"+
		"\u0239\b\'\1\2\u0234\u0235\7 \2\2\u0235\u0239\b\'\1\2\u0236\u0237\7\67"+
		"\2\2\u0237\u0239\b\'\1\2\u0238\u0232\3\2\2\2\u0238\u0234\3\2\2\2\u0238"+
		"\u0236\3\2\2\2\u0239M\3\2\2\2\u023a\u023b\5R*\2\u023b\u023c\b(\1\2\u023c"+
		"\u0244\3\2\2\2\u023d\u023e\7\24\2\2\u023e\u023f\5P)\2\u023f\u0240\79\2"+
		"\2\u0240\u0241\5N(\2\u0241\u0242\b(\1\2\u0242\u0244\3\2\2\2\u0243\u023a"+
		"\3\2\2\2\u0243\u023d\3\2\2\2\u0244O\3\2\2\2\u0245\u0246\5\22\n\2\u0246"+
		"\u024b\b)\1\2\u0247\u0248\7.\2\2\u0248\u024a\b)\1\2\u0249\u0247\3\2\2"+
		"\2\u024a\u024d\3\2\2\2\u024b\u0249\3\2\2\2\u024b\u024c\3\2\2\2\u024cQ"+
		"\3\2\2\2\u024d\u024b\3\2\2\2\u024e\u024f\5V,\2\u024f\u0250\b*\1\2\u0250"+
		"\u0268\3\2\2\2\u0251\u0252\7\31\2\2\u0252\u0253\5R*\2\u0253\u0254\b*\1"+
		"\2\u0254\u0268\3\2\2\2\u0255\u0256\7\62\2\2\u0256\u0257\5R*\2\u0257\u0258"+
		"\b*\1\2\u0258\u0268\3\2\2\2\u0259\u025a\5T+\2\u025a\u025b\5N(\2\u025b"+
		"\u025c\b*\1\2\u025c\u0268\3\2\2\2\u025d\u025e\7\35\2\2\u025e\u025f\5R"+
		"*\2\u025f\u0260\b*\1\2\u0260\u0268\3\2\2\2\u0261\u0262\7\35\2\2\u0262"+
		"\u0263\7\24\2\2\u0263\u0264\5P)\2\u0264\u0265\79\2\2\u0265\u0266\b*\1"+
		"\2\u0266\u0268\3\2\2\2\u0267\u024e\3\2\2\2\u0267\u0251\3\2\2\2\u0267\u0255"+
		"\3\2\2\2\u0267\u0259\3\2\2\2\u0267\u025d\3\2\2\2\u0267\u0261\3\2\2\2\u0268"+
		"S\3\2\2\2\u0269\u026a\7-\2\2\u026a\u0276\b+\1\2\u026b\u026c\7.\2\2\u026c"+
		"\u0276\b+\1\2\u026d\u026e\7\36\2\2\u026e\u0276\b+\1\2\u026f\u0270\7:\2"+
		"\2\u0270\u0276\b+\1\2\u0271\u0272\7\34\2\2\u0272\u0276\b+\1\2\u0273\u0274"+
		"\7\66\2\2\u0274\u0276\b+\1\2\u0275\u0269\3\2\2\2\u0275\u026b\3\2\2\2\u0275"+
		"\u026d\3\2\2\2\u0275\u026f\3\2\2\2\u0275\u0271\3\2\2\2\u0275\u0273\3\2"+
		"\2\2\u0276U\3\2\2\2\u0277\u0278\5\\/\2\u0278\u027e\b,\1\2\u0279\u027a"+
		"\5X-\2\u027a\u027b\b,\1\2\u027b\u027d\3\2\2\2\u027c\u0279\3\2\2\2\u027d"+
		"\u0280\3\2\2\2\u027e\u027c\3\2\2\2\u027e\u027f\3\2\2\2\u027fW\3\2\2\2"+
		"\u0280\u027e\3\2\2\2\u0281\u0282\b-\1\2\u0282\u0283\7\61\2\2\u0283\u0284"+
		"\5(\25\2\u0284\u0285\7\33\2\2\u0285\u0286\b-\1\2\u0286\u029b\3\2\2\2\u0287"+
		"\u0288\b-\1\2\u0288\u028c\7\24\2\2\u0289\u028a\5Z.\2\u028a\u028b\b-\1"+
		"\2\u028b\u028d\3\2\2\2\u028c\u0289\3\2\2\2\u028c\u028d\3\2\2\2\u028d\u028e"+
		"\3\2\2\2\u028e\u028f\79\2\2\u028f\u029b\b-\1\2\u0290\u0291\7/\2\2\u0291"+
		"\u0292\7C\2\2\u0292\u029b\b-\1\2\u0293\u0294\7\60\2\2\u0294\u0295\7C\2"+
		"\2\u0295\u029b\b-\1\2\u0296\u0297\7\31\2\2\u0297\u029b\b-\1\2\u0298\u0299"+
		"\7\62\2\2\u0299\u029b\b-\1\2\u029a\u0281\3\2\2\2\u029a\u0287\3\2\2\2\u029a"+
		"\u0290\3\2\2\2\u029a\u0293\3\2\2\2\u029a\u0296\3\2\2\2\u029a\u0298\3\2"+
		"\2\2\u029bY\3\2\2\2\u029c\u029d\b.\1\2\u029d\u029e\5*\26\2\u029e\u02a5"+
		"\b.\1\2\u029f\u02a0\7\26\2\2\u02a0\u02a1\5*\26\2\u02a1\u02a2\b.\1\2\u02a2"+
		"\u02a4\3\2\2\2\u02a3\u029f\3\2\2\2\u02a4\u02a7\3\2\2\2\u02a5\u02a3\3\2"+
		"\2\2\u02a5\u02a6\3\2\2\2\u02a6[\3\2\2\2\u02a7\u02a5\3\2\2\2\u02a8\u02a9"+
		"\7C\2\2\u02a9\u02b5\b/\1\2\u02aa\u02ab\5^\60\2\u02ab\u02ac\b/\1\2\u02ac"+
		"\u02b5\3\2\2\2\u02ad\u02ae\7D\2\2\u02ae\u02b5\b/\1\2\u02af\u02b0\7\24"+
		"\2\2\u02b0\u02b1\5(\25\2\u02b1\u02b2\79\2\2\u02b2\u02b3\b/\1\2\u02b3\u02b5"+
		"\3\2\2\2\u02b4\u02a8\3\2\2\2\u02b4\u02aa\3\2\2\2\u02b4\u02ad\3\2\2\2\u02b4"+
		"\u02af\3\2\2\2\u02b5]\3\2\2\2\u02b6\u02b7\5`\61\2\u02b7\u02b8\b\60\1\2"+
		"\u02b8\u02bd\3\2\2\2\u02b9\u02ba\5b\62\2\u02ba\u02bb\b\60\1\2\u02bb\u02bd"+
		"\3\2\2\2\u02bc\u02b6\3\2\2\2\u02bc\u02b9\3\2\2\2\u02bd_\3\2\2\2\u02be"+
		"\u02bf\7?\2\2\u02bf\u02c5\b\61\1\2\u02c0\u02c1\7@\2\2\u02c1\u02c5\b\61"+
		"\1\2\u02c2\u02c3\7A\2\2\u02c3\u02c5\b\61\1\2\u02c4\u02be\3\2\2\2\u02c4"+
		"\u02c0\3\2\2\2\u02c4\u02c2\3\2\2\2\u02c5a\3\2\2\2\u02c6\u02c7\7B\2\2\u02c7"+
		"\u02c8\b\62\1\2\u02c8c\3\2\2\2<ikq}\u008c\u0097\u009f\u00a8\u00b9\u00bf"+
		"\u00cb\u00d5\u00de\u00e4\u00ee\u00fc\u00ff\u0106\u011b\u0121\u012a\u0132"+
		"\u0142\u0153\u0159\u015f\u0165\u0172\u0176\u0180\u018b\u01a3\u01b0\u01bb"+
		"\u01c6\u01d1\u01dc\u01e7\u01ee\u01f8\u0203\u020d\u0214\u021e\u0225\u022f"+
		"\u0238\u0243\u024b\u0267\u0275\u027e\u028c\u029a\u02a5\u02b4\u02bc\u02c4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}