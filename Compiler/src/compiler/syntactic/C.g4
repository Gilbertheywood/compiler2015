grammar C;

@header {
package compiler.syntactic;
import compiler.ast.*;
import java.util.*;
}

@parser::members {
List<Decl> curDecls = new LinkedList<Decl>();
List<Decl> curFields = new LinkedList<Decl>();
Type curType;
int souCount = 0;
Expr curExpr = null;
Symbol curSymbol = null;
}

program returns [AST ele] // a program
    : {$ele = new AST();} 
     ( {curDecls = $ele.decls;}
        dcltn = declaration 
      | {curDecls = $ele.decls;}
        funcdef = function_definition 
    )+ ;

declaration
    : 
        {curFields = curDecls;}tyspc = type_specifier {curType = $tyspc.ele;}
        init_declarators? ';' 
    ;

function_definition
    : {List<VarDecl> params = new LinkedList<VarDecl>();}
        tyspc = type_specifier {curType = $tyspc.ele;}
        pldcltr = plain_declarator 
        '(' (pm = parameters {params = $pm.ele;})? ')' 
        compstmt = compound_statement 
        {curDecls.add(new FunctionDecl($pldcltr.ele.type, $pldcltr.ele.name, params, $compstmt.ele));}
    ;

parameters returns [List<VarDecl> ele]
    :{$ele = new LinkedList<VarDecl>();}
        pldcltn = plain_declaration {$ele.add($pldcltn.ele);}
        (',' pldcltn = plain_declaration {$ele.add($pldcltn.ele);})* 
    ;

declarators
    : 
        dcltr = declarator {curFields.add($dcltr.ele);}
        (',' dcltr = declarator {curFields.add($dcltr.ele);})* 
    ;

init_declarators
    : 
        init_declarator
        (',' init_declarator)* 
    ;

init_declarator
    : {Initializer init = null;}
        d = declarator
        ('=' initializer {init = $initializer.ele;})? 
        {
            $d.ele.init = init;
            curDecls.add($d.ele);
        }
    ;

initializer returns [Initializer ele]
    : 
        assignexpr = assignment_expression 
        {$ele = new InitValue($assignexpr.ele);}
        | {List<Initializer> list = new ArrayList<Initializer>();}
        '{' 
        init = initializer {list.add($init.ele);}
        (',' init = initializer {list.add($init.ele);})* '}' 
        {$ele = new InitList(list);}
    ;

type_specifier returns [Type ele]
    : 'void' {$ele = new VoidType();}
    | 
      'char' {$ele = new CharType();}
    | 
      'int' {$ele = new IntType();}
    | {
       String id = "#"+(Integer)(souCount++);
       List<Decl> fields = new LinkedList<Decl>();
       List<Decl> oldCurfields = curFields;
       curFields = fields;
      }
      sou = struct_or_union 
      (idt = Identifier {id = $idt.text;souCount--;})? '{' 
      (typspc = type_specifier {curType = $typspc.ele;} declarators ';')+ '}'
      {
       curFields = oldCurfields;
       if ($sou.ele == Symbol.get("struct")) {
          curFields.add(new StructDecl(Symbol.get(id), fields));
          $ele = new StructType(Symbol.get(id));
       } else {
          curFields.add(new UnionDecl(Symbol.get(id), fields));
          $ele = new UnionType(Symbol.get(id));
       }
      }
    | 
      sou = struct_or_union 
      id = Identifier 
      {
       if ($sou.ele == Symbol.get("struct")) {
          $ele = new StructType(Symbol.get($id.text));
       } else {
          $ele = new UnionType(Symbol.get($id.text));
       }
      }
    ;

struct_or_union returns [Symbol ele]
    : 
        'struct' {$ele = Symbol.get("struct");}
    | 
        'union' {$ele = Symbol.get("union");}
    ;

plain_declaration returns [VarDecl ele]
    : 
        tyspc = type_specifier  {curType = $tyspc.ele;}
        dclr = declarator 
        {$ele = $dclr.ele;}
    ;

declarator returns [VarDecl ele]
    :
        plain_declarator '(' ( parameters )? ')'{System.out.println("function initializer error");System.exit(1);}
    | 
        pd = plain_declarator {$ele = $pd.ele;}
        ('[' cons = constant_expression  ']'{$ele.type = new ArrayType($ele.type,$cons.ele);})* 
    ;

plain_declarator returns [VarDecl ele]
    : {Type type = curType;}
       ( '*' {type = new PointerType(type);})* id = Identifier 
       {$ele = new VarDecl(type, Symbol.get($id.text), null);}
    ;


// statements


statement returns [Stmt ele]
    : 
        es = expression_statement {$ele = $es.ele;} 
          | cs = compound_statement {$ele = $cs.ele;}
          | ss = selection_statement {$ele = $ss.ele;}
          | is = iteration_statement {$ele = $is.ele;}
          | js = jump_statement {$ele = $js.ele;}
    ;

expression_statement returns [Expr ele]
    : {$ele = new EmptyExpr();}
        (expr = expression {$ele = $expr.ele;})? ';' 
    ;

compound_statement returns [CompoundStmt ele]
    :   {
         List<Decl> decls = new LinkedList<Decl>();
         List<Stmt> stmts = new LinkedList<Stmt>();
         List<Decl> oldCurDecls = curDecls;
         curDecls = decls;
         }
        '{' (declaration)* 
        (s = statement {stmts.add($s.ele);} )* '}' 
        {
            curDecls = oldCurDecls;
            $ele = new CompoundStmt(decls, stmts);
         }
    ;

selection_statement returns [IfStmt ele]
    : {Stmt s2 = null;}
      'if' '(' expr = expression ')' stmt1 = statement ('else' stmt2 = statement {s2 = $stmt2.ele;})? 
      {$ele = new IfStmt($expr.ele, $stmt1.ele, s2);}
    ;

iteration_statement returns [Stmt ele]
    :
        'while' '(' expr = expression ')' stmt = statement
        {$ele = new WhileLoop($expr.ele, $stmt.ele);}
    | {Expr tmpe1 = null; Expr tmpe2 = null; Expr tmpe3 = null;}
        'for' '(' (e1 = expression {tmpe1 = $e1.ele;})? ';' 
        (e2 = expression {tmpe2 = $e2.ele;})? ';' 
        (e3 = expression {tmpe3 = $e3.ele;})? ')' 
        stmt = statement 
        {$ele = new ForLoop(tmpe1, tmpe2, tmpe3, $stmt.ele);}
    ;

jump_statement returns [Stmt ele]
    : 'continue' ';' {$ele = new ContinueStmt();}
               | 'break' ';' {$ele = new BreakStmt();}
               | {Expr exprtmp = new EmptyExpr();} 
                'return' (expr = expression {exprtmp = $expr.ele;})? ';' 
                {$ele = new ReturnStmt(exprtmp);}
    ;
                  
//  expression

expression returns [Expr ele]
    :
        ae = assignment_expression {$ele = $ae.ele;}
        (',' ae = assignment_expression {$ele = new BinaryExpr($ele,BinaryOp.COMMA, $ae.ele);})* 
    ;

 assignment_expression returns [Expr ele]
     : 
         l = logical_or_expression {$ele = $l.ele;}                 
     | 
         u = unary_expression ao = assignment_operator ae = assignment_expression 
         {$ele = new BinaryExpr($u.ele, $ao.ele, $ae.ele);}
     ;

 assignment_operator returns [BinaryOp ele]
     : '=' {$ele = BinaryOp.ASSIGN;}
     | 
       '*=' {$ele = BinaryOp.ASSIGN_MUL;}
     | 
       '/=' {$ele = BinaryOp.ASSIGN_DIV;}
     | 
       '%=' {$ele = BinaryOp.ASSIGN_MOD;}
     |
       '+=' {$ele = BinaryOp.ASSIGN_ADD;}
     |
       '-=' {$ele = BinaryOp.ASSIGN_SUB;}
     | 
       '<<=' {$ele = BinaryOp.ASSIGN_SHL;}
     | 
       '>>=' {$ele = BinaryOp.ASSIGN_SHR;}
     | 
       '&=' {$ele = BinaryOp.ASSIGN_AND;}
     | 
       '^=' {$ele = BinaryOp.ASSIGN_XOR;}
     |
       '|=' {$ele = BinaryOp.ASSIGN_OR;}
     ;

 constant_expression returns [Expr ele]
	: loe = logical_or_expression {$ele = $loe.ele;}
	;

 logical_or_expression returns [Expr ele]
	: l = logical_and_expression {$ele = $l.ele;}
	  ('||' l = logical_and_expression {$ele = new BinaryExpr($ele, BinaryOp.OR, $l.ele);})*
	;

 logical_and_expression returns [Expr ele]
	: i = inclusive_or_expression {$ele = $i.ele;}
	  ('&&' i = inclusive_or_expression {$ele = new BinaryExpr($ele, BinaryOp.AND, $i.ele);})*
	;
 
 inclusive_or_expression returns [Expr ele]
	: e = exclusive_or_expression  {$ele = $e.ele;}
	  ('|' e = exclusive_or_expression {$ele = new BinaryExpr($ele, BinaryOp.LOGICAL_OR, $e.ele);})*
	;

 exclusive_or_expression returns [Expr ele]
	: a = and_expression {$ele = $a.ele;}
	  ('^' a = and_expression {$ele = new BinaryExpr($ele, BinaryOp.XOR, $a.ele);})*
	;

 and_expression returns [Expr ele]
	: e = equality_expression {$ele = $e.ele;}
	  ('&' e = equality_expression {$ele = new BinaryExpr($ele, BinaryOp.LOGICAL_AND, $e.ele);})*
	;

 equality_expression returns [Expr ele]
	: r = relational_expression {$ele = $r.ele;}
	  (e = equality_operator
	  r = relational_expression {$ele = new BinaryExpr($ele, $e.ele, $r.ele);})*
	;

 equality_operator returns [BinaryOp ele]
	: '==' {$ele = BinaryOp.EQ;}
	| '!=' {$ele = BinaryOp.NE;}
	;

 relational_expression returns [Expr ele]
	: s = shift_expression {$ele = $s.ele;}
	  (r = relational_operator
	  s = shift_expression {$ele = new BinaryExpr($ele, $r.ele, $s.ele);})*
	;

 relational_operator returns [BinaryOp ele]
	: '<' {$ele = BinaryOp.LT;}
	| '>' {$ele = BinaryOp.GT;}
	| '<=' {$ele = BinaryOp.LE;}
	| '>=' {$ele = BinaryOp.GE;}
	;

 shift_expression returns [Expr ele]
	: a = additive_expression {$ele = $a.ele;}
	  (s = shift_operator
	  a = additive_expression {$ele = new BinaryExpr($ele, $s.ele, $a.ele);})*
	;

 shift_operator returns [BinaryOp ele]
	: '<<' {$ele = BinaryOp.SHL;}
	| '>>' {$ele = BinaryOp.SHR;}
	;

 additive_expression returns [Expr ele]
    : m = multiplicative_expression {$ele = $m.ele;}
      (a = additive_operator
      m = multiplicative_expression {$ele = new BinaryExpr($ele, $a.ele, $m.ele);})*
    ;

 additive_operator returns [BinaryOp ele]
	: '+' {$ele = BinaryOp.ADD;}
	| '-' {$ele = BinaryOp.SUB;}
	;

 multiplicative_expression returns [Expr ele]
	: c = cast_expression {$ele = $c.ele;}
	  (m = multiplicative_operator
	  c = cast_expression {$ele = new BinaryExpr($ele, $m.ele, $c.ele);})*
	;

 multiplicative_operator returns [BinaryOp ele]
	: '*' {$ele = BinaryOp.MUL;}
	| '/' {$ele = BinaryOp.DIV;}
	| '%' {$ele = BinaryOp.MOD;}
	;

 cast_expression returns [Expr ele]
	: u = unary_expression {$ele = $u.ele;}
	| '(' t = type_name ')' c = cast_expression
	  {$ele = new CastExpr($t.ele, $c.ele);}
	;

 type_name returns [Type ele]
	:
	  t = type_specifier {$ele = $t.ele;} ('*' {$ele = new PointerType($ele);})*
	;

 unary_expression returns [Expr ele]
	: p = postfix_expression {$ele = $p.ele;}
	| '++' u = unary_expression {$ele = new UnaryExpr(UnaryOp.INC, $u.ele);}
	| '--' u = unary_expression {$ele = new UnaryExpr(UnaryOp.DEC, $u.ele);}
	| o = unary_operator c=cast_expression
	  {$ele = new UnaryExpr($o.ele, $c.ele);}
	| 'sizeof' u = unary_expression {$ele = new UnaryExpr(UnaryOp.SIZEOF, $u.ele);}
	| 'sizeof' '(' t = type_name ')'{$ele = new SizeofExpr($t.ele);}
	;

 unary_operator  returns [UnaryOp ele]
	: '&' {$ele = UnaryOp.AMPERSAND;}
	| '*' {$ele = UnaryOp.ASTERISK;}
	| '+' {$ele = UnaryOp.PLUS;}
	| '-' {$ele = UnaryOp.MINUS;}
	| '~' {$ele = UnaryOp.TILDE;}
	| '!' {$ele = UnaryOp.NOT;}
	;

 postfix_expression returns [Expr ele]
	: p = primary_expression{$ele = $p.ele;curExpr = $p.ele;}
	  (pf = postfix {$ele = $pf.ele;curExpr = $pf.ele;})*
	;

 postfix returns [Expr ele]
	: {Expr bodyexpr = curExpr;}
            '[' e = expression ']' {$ele = new ArrayAccess(bodyexpr, $e.ele);}
	|  {
             List<Expr> argu = new LinkedList<Expr>();
             Symbol sym = curSymbol;
            }
	  '(' (arguments {argu = $arguments.ele;})? ')'
	  {$ele = new FunctionCall(sym, argu);}
	| '.' id = Identifier {$ele = new RecordAccess(curExpr, Symbol.get($id.text));}
	| '->' id = Identifier {$ele = new PointerAccess(curExpr, Symbol.get($id.text));}
	| '++' {$ele = new SelfIncrement(curExpr);}
	| '--' {$ele = new SelfDecrement(curExpr);}
	;

 arguments returns [List<Expr> ele]
	: {$ele = new LinkedList<Expr>();}
          a = assignment_expression{$ele.add($a.ele);}
	  (',' a = assignment_expression {$ele.add($a.ele);})*
	;

 primary_expression returns [Expr ele]
	: id = Identifier {$ele = new Identifier(Symbol.get($id.text));curSymbol = Symbol.get($id.text);}
	| c = constant {$ele = $c.ele;}
        | s = String {$ele = new StringConst($s.text);}
	| '(' e = expression ')' {$ele = $e.ele;}
	;

 constant returns [Expr ele]
	: i = integer_constant {$ele = $i.ele;}
	| c = character_constant {$ele = $c.ele;}
	;

 integer_constant returns[Expr ele]
  : Hex {$ele = new IntConst(Integer.parseInt($Hex.text.substring(2), 16));}
  | Dec {$ele = new IntConst(Integer.parseInt($Dec.text,10));}
  | Oct {$ele = new IntConst(Integer.parseInt($Oct.text,8));}
  ;
 
  character_constant returns [Expr ele]
      : Chr {$ele = new CharConst($Chr.text);}
      ;
 
// Lexer 
 
 WhiteSpace : [ \r\t\n]+ -> channel(HIDDEN) ;

 fragment EOL : '\r' | '\n' | ('\r''\n') ;
 
 MultiComment : '/*' .*? '*/' -> channel(HIDDEN) ;
 SingleComment : '//' ~[\r\n]* EOL -> channel(HIDDEN) ;
 Lib : '#' ~[\r\n]* EOL -> channel(HIDDEN) ;
 
 fragment Digit : [0-9] ;
 fragment HexDigit : (Digit | [a-f] | [A-F]) ;
 fragment OctDigit : [0-7];
 fragment Letter : '$' | [a-z] | '_' | [A-Z] ;
 
 Hex : '0' ('x'|'X') HexDigit+ ;
 Dec : '0' | ([1-9] Digit*) ;
 Oct : '0' OctDigit+ ;
 
 fragment EscapeChar : ('\\' ('\''|'"'|'?'|'\\'|'a'|'b'|'f'|'n'|'r'|'t'|'v')
                       | '\\' (OctDigit | OctDigit OctDigit | OctDigit OctDigit OctDigit)
                       | '\\x' HexDigit+ ) ;
 
 Chr : '\'' (~[\'\\\n] | EscapeChar)+ '\'' ;
 
 Identifier: Letter (Letter|Digit)* ;

 String: '\"' ('\\'([btnfr] | '\'' | '\"' | '\\') | ~('\\'|'\"'|'\n'|'\r'))* '\"';
