package compiler.codeGeneration;

import compiler.ast.*;
import compiler.ir.*;
import compiler.semantic.*;
import compiler.syntactic.*;

import java.util.*;
import java.io.*;

public class PeepHole {
	public IR ir;
	
	public PeepHole(IR ir) {
		this.ir = ir;
	}
	
	public void Optimize() {
		for (Function func: ir.fragments) {
			OptimizeFunction(func);
		}
	}
	
	public boolean tempEq(Address t1, Address t2) {
		if (t1 instanceof Temp && t2 instanceof Temp) {
			Temp tmp1 = (Temp) t1;
			Temp tmp2 = (Temp) t2;
			if (tmp1.num == tmp2.num) return true;
		}
		return false;
	}
	
	public boolean adrsEq(Address t1, Address t2) {
		if (t1 instanceof Temp && t2 instanceof Temp) {
			Temp tmp1 = (Temp) t1;
			Temp tmp2 = (Temp) t2;
			if (tmp1.num == tmp2.num) return true;
		}
		if (t1 instanceof Name && t2 instanceof Name) {
			Name n1 = (Name) t1;
			Name n2 = (Name) t2;
			if (n1.name.equals(n2.name)) return true;
		}
		return false;
	}
	
	public String AddressName(Address adrs) {
		if (adrs instanceof Name) return ((Name)adrs).name;
		if (adrs instanceof Temp) return "#t"+((Temp)adrs).num;
		return null;
	}
	
	public Triple<String, String, String> getTriple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			return new Triple<String, String, String>(AddressName(assign.dest), AddressName(assign.src), null);
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			return new Triple<String, String, String>(AddressName(ae.dest), AddressName(ae.src1), AddressName(ae.src2));
		}
		if (q instanceof MemoryRead) {
			MemoryRead mr = (MemoryRead) q;
			return new Triple<String, String, String>(AddressName(mr.dest), AddressName(mr.src), null);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			return new Triple<String, String, String>(null, AddressName(mw.dest), AddressName(mw.src));
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			return new Triple<String, String, String>(AddressName(ao.dest), AddressName(ao.src), null);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			return new Triple<String, String, String>(null, AddressName(iff.src1), AddressName(iff.src2));
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			return new Triple<String, String, String>(null, AddressName(ift.src1), AddressName(ift.src2));
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			return new Triple<String, String, String>(null, AddressName(bp.src), null);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			return new Triple<String, String, String>(null, AddressName(mp.src), null);
		}
		if (q instanceof Return) {
			Param p = ((Return)q).value;
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				return new Triple<String, String, String>(null, AddressName(bp.src), null);
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				return new Triple<String, String, String>(null, AddressName(mp.src), null);
			}
		}
		if (q instanceof Call) {
			Param p = ((Call)q).returnValue;
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				return new Triple<String, String, String>(AddressName(bp.src), null, null);
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				return new Triple<String, String, String>(AddressName(mp.src), null, null);
			}
		}
		return new Triple<String, String, String>(null, null, null);
	}
	
	public void OptimizeFunction(Function func) {
		List<Quadruple> list = new LinkedList<Quadruple>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<Quadruple> prelist = new LinkedList<Quadruple>();
		/*System.out.println("----function----");
		for (Quadruple q: func.body) {
			writeQuadruple(q);
		}*/
		
		int i = 0;
		for (Quadruple q: func.body) {
			Triple<String, String, String> tri = getTriple(q);
			if (tri.second != null) {
				map.put(tri.second, i);
			}
			if (tri.third != null) {
				map.put(tri.third, i);
			}
			i++;
		}
		
		for (Quadruple q: func.body) {
			boolean valid = false;
			if (q instanceof ArithmeticExpr) {
				ArithmeticExpr ae = (ArithmeticExpr) q;
				if (ae.op == ArithmeticOp.ADD) {
					if (ae.src2 instanceof IntegerConst) {
						IntegerConst ic = (IntegerConst) ae.src2;
						if (ic.value == 0) {
							if (adrsEq(ae.dest, ae.src1)) {}
							else prelist.add(new Assign(ae.dest, ae.src1));
							valid = true;;
						}
					}
				}
				if (ae.op == ArithmeticOp.SUB) {
					if (ae.src2 instanceof IntegerConst) {
						IntegerConst ic = (IntegerConst) ae.src2;
						if (ic.value == 0) {
							if (adrsEq(ae.dest, ae.src1)) {}
							else prelist.add(new Assign(ae.dest, ae.src1));
							valid = true;
						}
					}
				}
				if (ae.op == ArithmeticOp.MUL) {
					if (ae.src2 instanceof IntegerConst) {
						IntegerConst ic = (IntegerConst) ae.src2;
						if (ic.value == 0) {
							prelist.add(new Assign(ae.dest, new IntegerConst(0)));
							valid = true;
						}
						if (ic.value == 1) {
							if (adrsEq(ae.dest, ae.src1)) {}
							else prelist.add(new Assign(ae.dest, ae.src1));
							valid = true;
						}
					}
				}
			}
			if (!valid) {
				prelist.add(q);
			}
		}
		
		Quadruple lastq = null;
		
		i = 0;
		for (Quadruple q: prelist) {
			boolean valid = false;
			if (q instanceof Assign) {
				Assign assign = (Assign) q;
				if (lastq instanceof ArithmeticExpr) {
					ArithmeticExpr ae = (ArithmeticExpr) lastq;
					if (tempEq(assign.src, ae.dest) && map.get(AddressName(assign.src)) <= i) {
						ae.dest = assign.dest;
						valid = true;
					}
				} else if (lastq instanceof Assign) {
					Assign ass = (Assign) lastq;
					if (tempEq(assign.src, ass.dest) && map.get(AddressName(assign.src)) <= i) {
						ass.dest = assign.dest;
						valid = true;
					}
				}
			}
			if (!valid) {
				list.add(q);
				lastq = q;
			}
			i++;
		}
		
		prelist = list;
		list = new LinkedList<Quadruple>();
		
		i = 0;
		lastq = null;
		for (Quadruple q: prelist) {
			boolean valid = false;
			if (q instanceof ArithmeticExpr) {
				ArithmeticExpr ae = (ArithmeticExpr) q;
				if (lastq instanceof Assign) {
					Assign assign = (Assign) lastq;
					if (assign.src instanceof IntegerConst) {
						if (tempEq(assign.dest, ae.src2)) {

							//System.out.println("#lalala");
							ae.src2 = assign.src;
							list.remove(list.size()-1);
							list.add(ae);
							valid = true;
						}
					}
				}
			}
			if (!valid) {
				list.add(q);
				lastq = q;
			}
			i++;
		}
		
		prelist = list;
		list = new LinkedList<Quadruple>();
		i = 0;
		lastq = null;
		for (Quadruple q: prelist) {
			boolean valid = false;
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				if (lastq instanceof Assign) {
					Assign assign = (Assign) lastq;
					if (assign.src instanceof IntegerConst) {
						if (tempEq(iff.src2, assign.dest)) {

							//System.out.println("#lalala");
							iff.src2 = assign.src;
							list.remove(list.size()-1);
							list.add(iff);
							valid = true;
						}
					}
				}
			} else
				if (q instanceof IfTrueGoto) {
					IfTrueGoto ift = (IfTrueGoto) q;
					if (lastq instanceof Assign) {
						Assign assign = (Assign) lastq;
						if (assign.src instanceof IntegerConst) {
							if (tempEq(ift.src2, assign.dest)) {

								//System.out.println("#lalala");
								ift.src2 = assign.src;
								list.remove(list.size()-1);
								list.add(ift);
								valid = true;
							}
						}
					}
				}
			if (!valid) {
				list.add(q);
				lastq = q;
			}
			i++;
		}
		
		prelist = list;
		list = new LinkedList<Quadruple>();
		
		i = 0;
		lastq = null;
		for (Quadruple q: prelist) {
			boolean valid = false;
			if (q instanceof ArithmeticExpr) {
				ArithmeticExpr ae = (ArithmeticExpr) q;
				if (lastq instanceof Assign) {
					Assign assign = (Assign) lastq;
					if (assign.src instanceof IntegerConst) {
						IntegerConst ic = (IntegerConst)assign.src;
						if (tempEq(assign.dest, ae.src1)) {
							//System.out.println("#lalala");
							ae.src1 = assign.src;
							list.remove(list.size()-1);
							list.add(ae);
							valid = true;
						}
					}
				}
			}
			if (!valid) {
				list.add(q);
				lastq = q;
			}
			i++;
		}
		
		/*System.out.println("----list----");
		for (Quadruple q: list) {
			writeQuadruple(q);
		}*/
		
		func.body = list;
	}
	
	public void writeQuadruple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			writeAddress(assign.dest);
			System.out.print(" = ");
			writeAddress(assign.src);
			System.out.println();
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			writeAddress(ae.dest);
			System.out.print(" = ");
			writeAddress(ae.src1);
			writeArithmeticOp(ae.op);
			writeAddress(ae.src2);
			System.out.println();
		}
		if (q instanceof ArrayRead) {
			ArrayRead ar = (ArrayRead) q;
			writeAddress(ar.dest);
			System.out.print(" = ");
			writeAddress(ar.src);
			System.out.print("[");
			writeAddress(ar.offset);
			System.out.print("], "+ar.size);
			System.out.println();
		}
		if (q instanceof ArrayWrite) {
			ArrayWrite aw = (ArrayWrite) q;
			writeAddress(aw.dest);
			System.out.print("[");
			writeAddress(aw.offset);
			System.out.print("] = ");
			writeAddress(aw.src);
			System.out.println(", "+aw.size);
		}
		if (q instanceof MemoryRead) {
			MemoryRead pr = (MemoryRead) q;
			writeAddress(pr.dest);
			System.out.print(" = *");
			writeAddress(pr.src);
			System.out.println(", "+pr.size);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			System.out.print("*");
			writeAddress(mw.dest);
			System.out.print(" = ");
			writeAddress(mw.src);
			System.out.println(", "+mw.size);
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			writeAddress(ao.dest);
			System.out.print(" = &");
			writeAddress(ao.src);
			System.out.println();
		}
		if (q instanceof Label) {
			Label l = (Label) q;
			System.out.println("Label"+l.num);
		}
		if (q instanceof Goto) {
			Goto g = (Goto) q;
			System.out.println("Goto: Label"+g.label.num);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			System.out.print("If False ");
			writeAddress(iff.src1);
			writeRelationalOp(iff.op);
			writeAddress(iff.src2);
			System.out.println(" Goto: Label"+iff.label.num);
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			System.out.print("If True ");
			writeAddress(ift.src1);
			writeRelationalOp(ift.op);
			writeAddress(ift.src2);
			System.out.println(" Goto: Label"+ift.label.num);
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			System.out.print("Param ");
			writeAddress(bp.src);
			System.out.println();
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			System.out.print("Param ");
			writeAddress(ap.name);
			System.out.print("[");
			writeAddress(ap.offset);
			System.out.println("], "+ap.size);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			System.out.print("Param *");
			writeAddress(mp.src);
			System.out.println(" "+mp.size);
		}
		if (q instanceof Call) {
			Call call = (Call) q;
			Address adrs = null;
			if (call.returnValue instanceof BasicParam)
				adrs = ((BasicParam)call.returnValue).src;
			if (call.returnValue instanceof ArrayParam)
				adrs = ((ArrayParam)call.returnValue).name;
			if (call.returnValue instanceof MemoryParam)
				adrs = ((MemoryParam)call.returnValue).src;
			if (adrs != null) {
				writeAddress(adrs);
				System.out.print(" = ");
			}
			System.out.println("Call "+call.callee+" "+call.numOfParams);
		}
		if (q instanceof Return) {
			System.out.print("Return ");
			writeQuadruple(((Return)q).value);
		}
	}
	
	public void writeAddress(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			System.out.print(sac.value);
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			System.out.print(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			System.out.print(n.name);
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			System.out.print("#t"+tmp.num);
		}
	}
	
	public void writeRelationalOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			System.out.print("==");
		}
		if (op == RelationalOp.GE) {
			System.out.print(">=");
		}
		if (op == RelationalOp.GT) {
			System.out.print(">");
		}
		if (op == RelationalOp.LE) {
			System.out.print("<=");
		}
		if (op == RelationalOp.LT) {
			System.out.print("<");
		}
		if (op == RelationalOp.NE) {
			System.out.print("!=");
		}
	}
	
	public void writeArithmeticOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			System.out.print("+");
		}
		if (op == ArithmeticOp.AND) {
			System.out.print("&");
		}
		if (op == ArithmeticOp.DIV) {
			System.out.print("/");
		}
		if (op == ArithmeticOp.MINUS) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.MOD) {
			System.out.print("%");
		}
		if (op == ArithmeticOp.MUL) {
			System.out.print("*");
		}
		if (op == ArithmeticOp.NOT) {
			System.out.print("!");
		}
		if (op == ArithmeticOp.OR) {
			System.out.print("|");
		}
		if (op == ArithmeticOp.SHL) {
			System.out.print("<<");
		}
		if (op == ArithmeticOp.SHR) {
			System.out.print(">>");
		}
		if (op == ArithmeticOp.SUB) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.TILDE) {
			System.out.print("~");
		}
		if (op == ArithmeticOp.XOR) {
			System.out.print("^");
		}
	}
	
}
