package compiler.codeGeneration;

import compiler.ast.*;
import compiler.ir.*;
import compiler.semantic.*;
import compiler.syntactic.*;

import java.util.*;
import java.io.*;

public class CodeGenerator {
	public IR ir;
	public Map<String, Integer> dict; // record variable offset
	public Map<String, Variable> varDict; // link string to variable 
	public int curOffset;
	public int stringCnt;
	public int offset;
	public Stack<Param> paramStack;
	public Set<String> globalSet;
	public Function curFunc;
	public int stringPos;
	public Map<String, Integer> funcArgSize;
	
	public void comment(List<String> list, String s) {
		list.add("#  "+s);
	}
	
	public String addressName(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			return sac.value;
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			return Integer.toString(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			return n.name;
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			return "#t"+tmp.num;
		}
		return "";
	}
	
	public CodeGenerator(IR ir) {
		this.ir = ir;
		dict = new HashMap<String, Integer>();
		varDict = new HashMap<String, Variable>();
		stringCnt = 0;
		paramStack = new Stack<Param>();
		globalSet = new HashSet<String>();
		funcArgSize = new HashMap<String, Integer>();
	}
	
	public void PrintfIntCode(List<String> list) {
		list.add("printf_int:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "1", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfCharCode(List<String> list) {
		list.add("printf_char:");
		list.add(GenString("lb", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfStrCode(List<String> list) {
		list.add("printf_str:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "4", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfWidthCode(List<String> list) {
		list.add("printf_width:");
		list.add(GenString("li", "$v0", "1", null));
		list.add(GenString("li", "$a0", "0", null));
		list.add(GenString("lb", "$a3", "0($a2)", null));
		list.add(GenString("add","$a2", "$a2", "2"));
		list.add(GenString("add", "$a3", "$a3", "-48"));
		
		list.add(GenString("li", "$t0", "0", null));
		list.add("WidthLoop:");
		list.add(GenString("mul", "$t0", "$t0", "10"));
		list.add(GenString("add", "$t0", "$t0", "9"));
		list.add(GenString("add", "$a3", "$a3", "-1"));
		list.add(GenString("bne", "$a3", "0", "WidthLoop"));
		
		list.add(GenString("lw", "$t1", "0($a1)", null));
		
		list.add("ZeroLoop:");
		list.add(GenString("div", "$t0", "$t0", "10"));
		list.add(GenString("bgt", "$t1", "$t0", "printf_int"));
		list.add("syscall");
		list.add(GenString("b", "ZeroLoop", null, null));
	}
	
	public void PrintFmtCode(List<String> list) {
		list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'d\'", "printf_int"));
		list.add(GenString("beq", "$a0", "\'s\'", "printf_str"));
		list.add(GenString("beq", "$a0", "\'c\'", "printf_char"));
		list.add(GenString("beq", "$a0", "\'.\'", "printf_width"));
		list.add(GenString("beq", "$a0", "\'0\'", "printf_width"));
		PrintfIntCode(list);
		PrintfStrCode(list);
		PrintfCharCode(list);
		PrintfWidthCode(list);
	}
	
	public void PrintfCode(List<String> list) {
		list.add("printf:");
		list.add("printf_loop:");
		list.add(GenString("lb", "$a0", "0($a2)",null));
		list.add(GenString("beq", "$a0", "0", "printf_end"));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'%\'", "printf_fmt"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
		/*list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");*/
		PrintFmtCode(list);
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
		list.add("printf_end:");
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void ReadCode(List<String> list) {
		list.add("getchar:");
		list.add(GenString("li", "$v0", "12", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public void PreWork(List<String> list) {
		PrintfCode(list);
		ReadCode(list);
		MallocCode(list);
	}
	
	public void MallocCode(List<String> list) {
		list.add("malloc:");
		list.add(GenString("li", "$v0", "9", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public int getFunctionSize(Function func) {
		int sum = 0;
		sum += func.size;
		for (Variable var: func.args) {
			sum += var.size;
		}
		return sum;
	}
	
	public List<String> GenerateCode() {
		List<String> list = new LinkedList<String>();
		list.add(".data");
		list.add(".text");
		PreWork(list);
		/*System.out.println("-------");
		for (Function func: ir.fragments) {
			System.out.println(func.name);
		}*/
		funcArgSize.put("printf", 32);
		funcArgSize.put("malloc", 12);
		funcArgSize.put("getchar", 12);
		for (Function func: ir.fragments) {
			funcArgSize.put(func.name, new Integer(getFunctionSize(func)));
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("_start")) {
				//System.out.println("yes!!!");
				curFunc = func;
				StartCode(func, list);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main")) {
				curFunc = func;
				FunctionCode(func, list);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main") || func.name.equals("_start")) continue;
			curFunc = func;
			FunctionCode(func, list);
		}
		PrintCode(list);
		return list;
	}
	
	public void StartCode(Function func, List<String> list) {
		int offset = 12;
		comment(list, "function: "+func.name);
		for (Variable var: func.vars) {
			//System.out.println(var.name);
			if (var.name.charAt(0) != '#') globalSet.add(var.name);
			varDict.put(var.name, var);
			
			//System.out.printf("%s, %d\n", var.name, var.size);
			
			if (var.name.charAt(0) == '#') offset += var.size;
		}
		
		stringPos = 1;
		
		for (Variable var: func.vars) {
			if (!(var instanceof BasicVariable)) {
				if (globalSet.contains(var.name)) { 
					list.add(1, var.name+": .space "+var.size);
					stringPos++;
				}
			}
		}
		
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				if (globalSet.contains(var.name)) {
					list.add(1, var.name+": .word 0");
					stringPos++;
				}
			}
		}
		
		list.add("_start"+":");
		curOffset = offset;
		list.add(GenString("add", "$sp", "$sp", "-"+offset));
		list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		for (Quadruple q: func.body) {
			QuadrupleCode(q, list);
		}
		
		list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public String GenString(String s1, String s2, String s3, String s4) {
		if (s3 == null) return String.format("%-8s %s", s1, s2);
		if (s4 == null) return String.format("%-8s %s,%s", s1, s2, s3);
		return String.format("%-8s %s,%s,%s", s1, s2, s3, s4);
	}
	
	public int max(int a, int b){
		if (a > b) return a;
		else return b;
	}
	
	public void FunctionCode(Function func, List<String> list) {
		comment(list, "function: "+func.name);
		String s = func.name;
		if (s.equals("move")) s = "move1";
		list.add(s+":");
		offset = 0;
		
		//System.out.println("#1: "+offset);
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#2: "+offset);
		
		int maxsize = offset+func.size;
		if (offset < 32) offset = 32;
		
		//System.out.println("#3: "+offset);
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#4: "+offset);
		
		int funcsize = 0;
		for (Quadruple q: func.body) {
			if (q instanceof Call) {
				funcsize = max(funcsize, funcArgSize.get(((Call)q).callee).intValue());
			}
		}
		offset += funcsize; // reserve for args & returnvalue
		
		//System.out.println("#5: "+offset);
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				dict.put(bv.name, new Integer(offset+maxsize-4));
				maxsize -= 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				dict.put(av.name, new Integer(offset+maxsize-av.size-4));
				maxsize -= av.size+4;
			}
		}
		if (offset % 4 != 0) offset = (offset / 4 + 1) * 4;
		curOffset = offset;
		list.add(GenString("add", "$sp", "$sp", "-"+offset));
		list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		for (Quadruple q: func.body) {
			QuadrupleCode(q, list);
		}
		
		functionEndCode(list);

		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
	}
	
	public void functionEndCode(List<String> list) {
		list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		if (!curFunc.name.equals("main"))
			list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void QuadrupleCode(Quadruple q, List<String> list) {
		if (q instanceof Assign) AssignCode((Assign)q, list);
		if (q instanceof ArithmeticExpr) ArithmeticExprCode((ArithmeticExpr)q, list);
		if (q instanceof ArrayRead) ArrayReadCode((ArrayRead)q, list);
		if (q instanceof ArrayWrite) ArrayWriteCode((ArrayWrite)q, list);
		if (q instanceof MemoryRead) MemoryReadCode((MemoryRead)q, list);
		if (q instanceof MemoryWrite) MemoryWriteCode((MemoryWrite)q, list);
		if (q instanceof AddressOf) AddressOfCode((AddressOf)q, list);
		if (q instanceof Label) LabelCode((Label)q, list);
		if (q instanceof Goto) GotoCode((Goto)q, list);
		if (q instanceof IfFalseGoto) IfFalseGotoCode((IfFalseGoto)q, list);
		if (q instanceof IfTrueGoto) IfTrueGotoCode((IfTrueGoto)q, list);
		if (q instanceof Call) CallCode((Call)q, list);
		if (q instanceof Return) ReturnCode((Return)q, list);
		if (q instanceof Param) paramStack.add((Param)q);
	}
	
	public int VariableOffset(Address adrs, List<String> list) {
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			/*System.out.println(s);
			if (dict.containsKey(s)) System.out.println(dict.get(s).intValue());
			else {
				System.out.printf("new: %d\n", varDict.get(s).size);
			}*/
			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				//System.out.println(s);
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					list.add(GenString("add", "$t1", "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", "$t1", curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					return curOffset;
				}
			}
		} else {
			String s = ((Name)adrs).name;
			
			/*System.out.println(s);
			if (dict.containsKey(s)) System.out.println(dict.get(s).intValue());
			else System.out.println(varDict.get(s).size);*/

			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				//if (varDict.containsKey(s)) System.out.println("contain");
				//else System.out.println("not contain");
				//System.out.println(s);
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					list.add(GenString("add", "$t1", "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", "$t1", curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					return curOffset;
				}
			}
		}
	}
	
	public void AssignCode(Assign assign, List<String> list) {
		comment(list, addressName(assign.dest)+"="+addressName(assign.src));
		if (assign.src instanceof IntegerConst) {
			int offset = VariableOffset(assign.dest, list);
			int value = ((IntegerConst)assign.src).value;
			
			list.add(GenString("li", "$a1", Integer.toString(value), null));
			//list.add(GenString("sw", "$a1", offset+"($sp)", null));
			storeValue(assign.dest, "$a1", list, 4);
		} else {
			/*int offset1 = VariableOffset(assign.dest);
			int offset2 = VariableOffset(assign.src);
			list.add(GenString("lw", "$a1", offset2+"($sp)", null));
			list.add(GenString("sw", "$a1", offset1+"($sp)", null));*/
			String s = "";
			if (assign.src instanceof Name) {
				Name name = (Name) assign.src;
				s = name.name;
			} else if (assign.src instanceof Temp) {
				Temp t = (Temp) assign.src;
				s = "#t"+t.num;
			}
			/*int size = 0;
			if (!s.equals("")) size = varDict.get(s).size;
			if (size > 4) {
				int offset = VariableOffset(assign.src);
				list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
			} else*/ 
			loadValue(assign.src, "$a1", list, 4);
			
			storeValue(assign.dest, "$a1", list, 4);
		}
	}
	
	public void loadValue(Address adrs, String reg, List<String> list, int size) {
		if (adrs instanceof Name) {
			Name n = (Name) adrs;
			//System.out.println("loadValue:"+n.name);
			if (globalSet.contains(n.name)) {
				Variable v = varDict.get(n.name);
				if (v instanceof BasicVariable) {
					list.add(GenString("lw", reg, n.name, null));
				} else {
					list.add(GenString("la", reg, n.name, null));
				}
				return;
			}
		}
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp) adrs).num;
			//System.out.println("loadValue:"+s);
			if (globalSet.contains(s)) {
				//System.out.println("bingo"+s);
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					list.add(GenString("lw", reg, s, null));
				} else {
					list.add(GenString("la", reg, s, null));
				}
				return;
			}
		}
		if (adrs == null) return;
		if (adrs instanceof IntegerConst) {
			IntegerConst i = (IntegerConst) adrs;
			list.add(GenString("li", reg, Integer.toString(i.value), null));
		}
		if (adrs instanceof StringAddressConst) {
			StringAddressConst s = (StringAddressConst) adrs;
			list.add(stringPos, ".align 2");
			list.add(stringPos+1, "string"+stringCnt+":");
			list.add(stringPos+2, GenString(".asciiz", "\""+s.value+"\"", null, null));
			list.add(GenString("la", reg, "string"+stringCnt, null));
			stringCnt++;
		}
		if (adrs instanceof Name) {
			Name name = (Name) adrs;
			//System.out.println("----------");
			//PrintCode(list);
			int offset = VariableOffset(name, list);
			if (size == 4) list.add(GenString("lw", reg, offset+"($sp)", null));
			else list.add(GenString("lb", reg, offset+"($sp)", null));
		}
		if (adrs instanceof Temp) {
			Temp tmp = (Temp) adrs;
			int offset = VariableOffset(tmp, list);
			if (size == 4) list.add(GenString("lw", reg, offset+"($sp)", null));
			else list.add(GenString("lb", reg, offset+"($sp)", null));
		}
	}
	
	public void storeValue(Address adrs, String reg, List<String> list, int size) {
		if (adrs instanceof Name) {
			Name name = (Name) adrs;
			String s = name.name;
			if (globalSet.contains(s)) {
				list.add(GenString("sw", reg, s, null));
				return;
			}
			int offset = VariableOffset(name, list);
			if (size == 4) list.add(GenString("sw", reg, offset+"($sp)", null));
			else list.add(GenString("sw", reg, offset+"($sp)", null));
		}
		if (adrs instanceof Temp) {
			Temp tmp = (Temp) adrs;
			String s = "#t"+tmp.num;
			if (globalSet.contains(s)) {
				list.add(GenString("sw", reg, s, null));
				return;
			}
			int offset = VariableOffset(tmp, list);
			if (size == 4) list.add(GenString("sw", reg, offset+"($sp)", null));
			else list.add(GenString("sw", reg, offset+"($sp)", null));
		}
	}
	
	public String transOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			return "+" ;
		}
		if (op == ArithmeticOp.AND) {
			return "&";
		}
		if (op == ArithmeticOp.DIV) {
			return "/";
		}
		if (op == ArithmeticOp.MINUS) {
			return "-";
		}
		if (op == ArithmeticOp.MOD) {
			return "%";
		}
		if (op == ArithmeticOp.MUL) {
			return "*";
		}
		if (op == ArithmeticOp.NOT) {
			return "!";
		}
		if (op == ArithmeticOp.OR) {
			return "|";
		}
		if (op == ArithmeticOp.SHL) {
			return "<<";
		}
		if (op == ArithmeticOp.SHR) {
			return ">>";
		}
		if (op == ArithmeticOp.SUB) {
			return "-";
		}
		if (op == ArithmeticOp.TILDE) {
			return "~";
		}
		if (op == ArithmeticOp.XOR) {
			return "^";
		}
		return "null";
	}
	
	public void ArithmeticExprCode(ArithmeticExpr expr, List<String> list) {
		comment(list, addressName(expr.dest)+"="+addressName(expr.src1)+transOp(expr.op)+addressName(expr.src2));
		loadValue(expr.src1, "$a1", list, 4);
		loadValue(expr.src2, "$a2", list, 4);
		if (expr.op == ArithmeticOp.ADD) {
			list.add(GenString("add", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.AND) {
			list.add(GenString("and", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.DIV) {
			list.add(GenString("div", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.MUL) {
			list.add(GenString("mul", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.MOD) {
			list.add(GenString("div", "$a1", "$a2", null));
			list.add(GenString("mfhi", "$a3", null, null));
		}
		if (expr.op == ArithmeticOp.MINUS) {
			list.add(GenString("neg", "$a3", "$a1", null));
		}
		if (expr.op == ArithmeticOp.NOT) {
			//list.add(GenString("not", "$a3", "$a1", null));
			Label l = new Label();
			list.add(GenString("li", "$a3", "0", null));
			list.add(GenString("bne", "$a1", "$zero", "label"+l.num));
			list.add(GenString("li", "$a3", "1", null));
			list.add("label"+l.num+":");
		}
		if (expr.op == ArithmeticOp.OR) {
			list.add(GenString("or", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.SHL) {
			list.add(GenString("sll", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.SHR) {
			list.add(GenString("srl", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.SUB) {
			list.add(GenString("sub", "$a3", "$a1", "$a2"));
		}
		if (expr.op == ArithmeticOp.TILDE) {
			list.add(GenString("not", "$a3", "$a1", null));
		}
		if (expr.op == ArithmeticOp.XOR) {
			list.add(GenString("xor", "$a3", "$a1", "$a2"));
		}
		storeValue(expr.dest, "$a3", list, 4);
	}
	
	public void ArrayReadCode(ArrayRead ar, List<String> list) {
		comment(list, addressName(ar.dest)+"="+addressName(ar.src)+"["+addressName(ar.offset)+"]"+", "+Integer.toString(ar.size));
		int offset = VariableOffset(ar.src, list);
		list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
		if (ar.offset instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) ar.offset;
			list.add(GenString("li", "$a2", Integer.toString(ic.value), null));
		}
		else loadValue(ar.offset, "$a2", list, 4);
		list.add(GenString("add", "$a1", "$a1", "$a2"));
		if (ar.size == 4) list.add(GenString("lw", "$a3", "0($a1)", null));
		else list.add(GenString("lb", "$a3", "0($a1)", null));
		storeValue(ar.dest, "$a3", list, 4);
	}
	
	public void ArrayWriteCode(ArrayWrite aw, List<String> list) {
		comment(list, addressName(aw.dest)+"["+addressName(aw.offset)+"]"+"="+addressName(aw.src)+", "+Integer.toString(aw.size));
		loadValue(aw.src, "$a1", list, aw.size);
		
		/*if (aw.dest instanceof Name) {
			String s = ((Name)aw.dest).name;
			if (globalSet.contains(s)) {
				list.add(GenString("la", "$a2", s, null));
			}
		} else if (aw.dest instanceof Temp) {
			String s = "#t"+((Temp)aw.dest).num;
			if (globalSet.contains(s)) {
				list.add(GenString("la", "$a2", s, null));
			}
		} else {
			int offset = VariableOffset(aw.dest);
			list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
		}*/
		getAddress(aw.dest, "$a2", list);
		
		if (aw.offset instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) aw.offset;
			list.add(GenString("li", "$a3", Integer.toString(ic.value), null));
		} else loadValue(aw.offset, "$a3", list, 4);
		list.add(GenString("add", "$a2", "$a2", "$a3"));
		if (aw.size == 4) list.add(GenString("sw", "$a1", "0($a2)", null));
		else list.add(GenString("sb", "$a1", "0($a2)", null));
	}
	
	public void MemoryReadCode(MemoryRead mr, List<String> list) {
		comment(list, addressName(mr.dest)+"=*"+addressName(mr.src)+", "+Integer.toString(mr.size));
		
		loadValue(mr.src, "$a1", list, 4);
		if (mr.size == 4 && mr.type == 0) {
			list.add(GenString("lw", "$a2", "0($a1)", null));
			storeValue(mr.dest, "$a2", list, mr.size);
		}
		else if (mr.size == 1 && mr.type == 0) {
			list.add(GenString("lb", "$a2", "0($a1)", null));
			storeValue(mr.dest, "$a2", list, mr.size);
		}
		else {
			//int offset = VariableOffset(mr.dest);  struct possibly will fail
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			loadValue(mr.dest, "$a2", list, 4);
			list.add(GenString("li", "$a3", Integer.toString(mr.size), null));
			Label l1 = new Label();
			Label l2 = new Label();
			list.add("label"+l1.num+":");
			list.add(GenString("ble", "$a3", "$zero", "label"+l2.num));
			list.add(GenString("lw", "$t0", "0($a1)", null));
			list.add(GenString("sw", "$t0", "0($a2)", null));
			list.add(GenString("add","$a1", "$a1", "4"));
			list.add(GenString("add","$a2", "$a2", "4"));
			list.add(GenString("add","$a3", "$a3", "-4"));
			list.add(GenString("b", "label"+l1.num, null, null));
			list.add("label"+l2.num+":");
		}
	}
	
	public void MemoryWriteCode(MemoryWrite mw, List<String> list) {
		comment(list, "*"+addressName(mw.dest)+"="+addressName(mw.src)+", "+Integer.toString(mw.size));

		loadValue(mw.src, "$a1", list, mw.size);
		if (mw.size <= 4 && mw.type == 0) {
			loadValue(mw.dest, "$a2", list, 4);
			if (mw.size == 4) {
				list.add(GenString("sw", "$a1", "0($a2)", null));
				//list.add(GenString("sw", "$a3", "0($a2)", null));
			}
			else {
				list.add(GenString("sb", "$a1", "0($a2)", null));
				//list.add(GenString("sb", "$a3", "0($a2)", null));
			}
		} else {
			//int offset = VariableOffset(mw.dest);
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			loadValue(mw.dest, "$a2", list, 4);
			list.add(GenString("li", "$a3", Integer.toString(mw.size), null));
			Label l1 = new Label();
			Label l2 = new Label();
			list.add("label"+l1.num+":");
			list.add(GenString("ble", "$a3", "$zero", "label"+l2.num));
			list.add(GenString("lw", "$t0", "0($a1)", null));
			list.add(GenString("sw", "$t0", "0($a2)", null));
			list.add(GenString("add","$a1", "$a1", "4"));
			list.add(GenString("add","$a2", "$a2", "4"));
			list.add(GenString("add","$a3", "$a3", "-4"));
			list.add(GenString("b", "label"+l1.num, null, null));
			list.add("label"+l2.num+":");
		}
	}
	
	public void getAddress(Address adrs, String reg, List<String> list) {
		//System.out.println("getAddress!!!");
		if (adrs instanceof Name) {
			String s = ((Name)adrs).name;
			if (globalSet.contains(s)) {
				list.add(GenString("la", reg, s, null));
				return;
			}
		} else if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			if (globalSet.contains(s)) {
				list.add(GenString("la", reg, s, null));
				return;
			}
		}
		int offset = VariableOffset(adrs, list);
		Variable v = varDict.get(addressName(adrs));
		if (v instanceof BasicVariable) {
			list.add(GenString("add", reg, "$sp", Integer.toString(offset)));
		} else {
			list.add(GenString("lw", reg, offset+"($sp)", null));
		}
	}
	
	public void AddressOfCode(AddressOf ao, List<String> list) {
		comment(list, addressName(ao.dest)+"=&"+addressName(ao.src));
		//int offset = VariableOffset(ao.src);
		//list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
		getAddress(ao.src, "$a1", list);
		//loadValue(ao.src, "$a1", list, 4);
		storeValue(ao.dest, "$a1", list, 4);
	}
	
	public void LabelCode(Label l, List<String> list) {
		list.add("label"+l.num+":");
	}
	
	public void GotoCode(Goto g, List<String> list) {
		list.add(GenString("b", "label"+g.label.num, null, null));
	}
	
	public String transRelOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			return "==";
		}
		if (op == RelationalOp.GE) {
			return ">=";
		}
		if (op == RelationalOp.GT) {
			return ">";
		}
		if (op == RelationalOp.LE) {
			return "<=";
		}
		if (op == RelationalOp.LT) {
			return "<";
		}
		if (op == RelationalOp.NE) {
			return "!=";
		}
		return "null";
	}
	
	public void IfFalseGotoCode(IfFalseGoto iff, List<String> list) {
		comment(list, "if false "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		loadValue(iff.src1, "$a1", list, 4);
		loadValue(iff.src2, "$a2", list, 4);
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("bne", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("blt", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("ble", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("bgt", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("bge", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("beq", "$a1", "$a2", "label"+iff.label.num));
		}
	}
	
	public void IfTrueGotoCode(IfTrueGoto iff, List<String> list) {
		comment(list, "if True "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		loadValue(iff.src1, "$a1", list, 4);
		loadValue(iff.src2, "$a2", list, 4);
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("beq", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("bge", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("bgt", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("ble", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("blt", "$a1", "$a2", "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("bne", "$a1", "$a2", "label"+iff.label.num));
		}
	}
	
	public int getParamSize(Param p) {
		if (p instanceof BasicParam) {
			return 4;
		}
		if (p instanceof ArrayParam) {
			return ((ArrayParam)p).size;
		}
		if (p instanceof MemoryParam) {
			//System.out.println(((MemoryParam)p).size);
			return ((MemoryParam)p).size;
		}
		return 0;
	}
	
	public void commentOfCall(List<String> list, Call call) {
		Address adrs = null;
		if (call.returnValue instanceof BasicParam)
			adrs = ((BasicParam)call.returnValue).src;
		if (call.returnValue instanceof ArrayParam)
			adrs = ((ArrayParam)call.returnValue).name;
		if (call.returnValue instanceof MemoryParam)
			adrs = ((MemoryParam)call.returnValue).src;
		comment(list, addressName(adrs)+" = call "+call.callee+", "+call.numOfParams);
	}
	
	public void changeOffset(Address adrs, int offset) {
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			dict.put(s, new Integer(offset));
		} else {
			String s = ((Name)adrs).name;
			dict.put(s, new Integer(offset));
		}
	}
	
	public void CallCode(Call call, List<String> list) {
		commentOfCall(list, call);
		int num = call.numOfParams;
		int pos = getParamSize(call.returnValue);
		int printf1 = 0, printf2 = 0;
		int malloc1 = 0;
		
		list.add("#call "+call.callee);
		while (num > 0) {
			Param p = paramStack.pop();
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				loadValue(bp.src, "$a1", list, 4);
				list.add(GenString("sw", "$a1", pos+"($sp)", null));
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				pos += 4;
			}
			if (p instanceof ArrayParam) {
				ArrayParam ap = (ArrayParam) p;
				int offset = VariableOffset(ap.name, list);
				list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
				loadValue(ap.offset, "$a2", list, 4);
				list.add(GenString("add", "$a2", "$a1", "$a2"));
				int size = ap.size;
				int posa2 = 0;
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", "$a3", posa2+"($a2)", null));
					list.add(GenString("sw", "$a3", pos+"($sp)", null));
					posa2 += 4;
					pos += 4;
				}
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				//storeValue(mp.src, "$sp", list, 4);
				loadValue(mp.src, "$a1", list, 4);
				/*list.add("#lalala: "+curOffset);
				int offset = VariableOffset(mp.src);
				list.add("#lalala: "+curOffset);
				list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));*/
				int size = mp.size;
				int posa1 = 0;
				
				list.add(GenString("add", "$a2", "$sp", Integer.toString(pos+4)));
				list.add(GenString("sw", "$a2", pos+"($sp)", null));
				pos += 4;
				
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", "$a2", posa1+"($a1)", null));
					list.add(GenString("sw", "$a2", pos+"($sp)", null));
					posa1 += 4;
					pos += 4;
				}
			}
			num--;
		}
		if (call.callee.equals("printf")) {
			list.add(GenString("lw", "$a2", printf1+"($sp)", null));
			list.add(GenString("add", "$a1", "$sp", Integer.toString(printf2)));
		}
		if (call.callee.equals("malloc")) {
			list.add(GenString("lw", "$a0", malloc1+"($sp)", null));
		}
		
		String s = call.callee;
		if (s.equals("move")) s = "move1";
		list.add(GenString("jal", s, null, null));
		//list.add("nop");
		
		if (call.returnValue instanceof BasicParam) {
			BasicParam bp = (BasicParam) call.returnValue;
			list.add(GenString("lw", "$a1", "0($sp)", null));
			storeValue(bp.src, "$a1", list, 4);
		}
		if (call.returnValue instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) call.returnValue;
			int offset = VariableOffset(ap.name, list);
			list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
			loadValue(ap.offset, "$a2", list, 4);
			list.add(GenString("add", "$a2", "$a2", "$a1"));
			int size = ap.size;
			int posa2 = 0, possp = 0;
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", "$a3", possp+"($sp)", null));
				list.add(GenString("sw", "$a3", posa2+"($a2)", null));
				posa2 += 4;
				possp += 4;
			}
		}
		if (call.returnValue instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam)call.returnValue;
			//storeValue(mp.src, "$sp", list, 4);
			//list.add("#lalala "+Integer.toString(curOffset));
			int offset = VariableOffset(mp.src, list);
			/*curOffset -= 4;
			changeOffset(mp.src, curOffset);
			list.add(GenString("add", "$a1", "$sp", Integer.toString(curOffset+4)));
			list.add(GenString("sw", "$a1", curOffset+"($sp)", null));
			//list.add("#lalala "+Integer.toString(curOffset));
			list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));*/
			list.add(GenString("lw", "$a1", offset+"($sp)",null));
			
			int size = mp.size;
			list.add(GenString("li", "$a3", Integer.toString(size), null));
			list.add(GenString("add", "$t0", "$sp", "0"));
			list.add(GenString("add", "$t1", "$a1", "0"));
			Label beginLabel = new Label();
			Label endLabel = new Label();
			list.add("label"+beginLabel.num+":");
			
			list.add(GenString("beq", "$a3", "$zero", "label"+endLabel.num));
			list.add(GenString("lw", "$a2", "0($t0)", null));
			list.add(GenString("sw", "$a2", "0($t1)", null));
			list.add(GenString("add", "$t0", "$t0", "4"));
			list.add(GenString("add", "$t1", "$t1", "4"));
			list.add(GenString("add", "$a3", "$a3", "-4"));
			list.add(GenString("b", "label"+beginLabel.num, null, null));
			list.add("label"+endLabel.num+":");
		}
	}

	public String ParamString(Param q) {
		String s = "";
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			s = s+"Param ";
			s = s+addressName(bp.src);
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			s = s+"Param ";
			s = s+addressName(ap.name);
			s = s+ "[";
			s = s+addressName(ap.offset);
			s = s+"], "+ap.size;
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			s = s+"Param *";
			s = s+addressName(mp.src);
			s = s+" "+mp.size;
		}
		return s;
	}
	
	public void ReturnCode(Return r, List<String> list) {
		comment(list, "return "+ParamString(r.value));
		if (r.value instanceof BasicParam) {
			BasicParam pb = (BasicParam) r.value;
			loadValue(pb.src, "$a1", list, 4);
			list.add(GenString("sw", "$a1", offset+"($sp)", null));
		}
		if (r.value instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) r.value;
			int offset = VariableOffset(ap.name, list);
			list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
			loadValue(ap.offset, "$a2", list, 4);
			list.add(GenString("add", "$a2", "$a1", "$a2"));
			int size = ap.size;
			int posa2 = 0;
			int posvalue = offset;
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", "$a3", posa2+"($a2)", null));
				list.add(GenString("sw", "$a3", posvalue+"($sp)", null));
				posvalue += 4;
				posa2 += 4;
			}
		}
		if (r.value instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) r.value;
			loadValue(mp.src, "$a1", list, 4);
			int size = mp.size;
			int posa1 = 0;
			int posvalue = offset;
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", "$a3", posa1+"($a1)", null));
				list.add(GenString("sw", "$a3", posvalue+"($sp)", null));
				posa1 += 4;
				posvalue += 4;
			}
		}
		functionEndCode(list);
	}
	
	public void PrintCode(List<String> list) {
		for (String s: list) {
			System.out.println(s);
		}
	}

}
