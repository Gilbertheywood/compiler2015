package compiler.codeGeneration;

import compiler.ast.*;
import compiler.ir.*;
import compiler.semantic.*;
import compiler.syntactic.*;

import java.util.*;
import java.io.*;

class Info {
	public int y;
	
	public Info() {
		y = 0;
	}
	
	public Info(int y) {
		this.y = y;
	}
}

class Node {
	public List<Info> list;
	
	public Node() {
		list = new LinkedList<Info>();
	}
}

public class OptimizationOld {
	public Node[] nodes;
	public IR ir;
	public List<Quadruple> list;
	public int[] mark;
	public int[] block;
	public int blockNum;
	public int len;
	public Map<String, Integer> labelDict;
	public Map<String, Function> funcDict;
	public Map<String, Integer> varMap;
	public int[] regSet;  //the set of used regs
	public int regUseTime;
	public int[] regTime;
	
	//old value
	public Map<String, Integer> dict; // record variable offset
	public Map<String, Variable> varDict; // link string to variable 
	public int curOffset;
	public int stringCnt;
	public int offset;
	public Stack<Param> paramStack;
	public Set<String> globalSet;
	public Function curFunc;
	public int stringPos;
	public Map<String, Integer> funcArgSize;
	
	public OptimizationOld(IR ir) {
		this.ir = ir;
		list = new LinkedList<Quadruple>();
		labelDict = new HashMap<String, Integer>();
		funcDict = new HashMap<String, Function>();
		varMap = new HashMap<String, Integer>();
		regSet = new int[33];
		regTime = new int[33];
		regUseTime = 0;
		
		dict = new HashMap<String, Integer>();
		varDict = new HashMap<String, Variable>();
		stringCnt = 0;
		paramStack = new Stack<Param>();
		globalSet = new HashSet<String>();
		funcArgSize = new HashMap<String, Integer>();
	}
	
	public void comment(List<String> list, String s) {
		list.add("#  "+s);
	}
	
	public String addressName(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			return sac.value;
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			return Integer.toString(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			return n.name;
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			return "#t"+tmp.num;
		}
		return "";
	}
	
	public void PrintfIntCode(List<String> list) {
		list.add("printf_int:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "1", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfCharCode(List<String> list) {
		list.add("printf_char:");
		list.add(GenString("lb", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfStrCode(List<String> list) {
		list.add("printf_str:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "4", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfWidthCode(List<String> list) {
		list.add("printf_width:");
		list.add(GenString("li", "$v0", "1", null));
		list.add(GenString("li", "$a0", "0", null));
		list.add(GenString("lb", "$a3", "0($a2)", null));
		list.add(GenString("add","$a2", "$a2", "2"));
		list.add(GenString("add", "$a3", "$a3", "-48"));
		
		list.add(GenString("li", "$t0", "0", null));
		list.add("WidthLoop:");
		list.add(GenString("mul", "$t0", "$t0", "10"));
		list.add(GenString("add", "$t0", "$t0", "9"));
		list.add(GenString("add", "$a3", "$a3", "-1"));
		list.add(GenString("bne", "$a3", "0", "WidthLoop"));
		
		list.add(GenString("lw", "$t1", "0($a1)", null));
		
		list.add("ZeroLoop:");
		list.add(GenString("div", "$t0", "$t0", "10"));
		list.add(GenString("bgt", "$t1", "$t0", "printf_int"));
		list.add("syscall");
		list.add(GenString("b", "ZeroLoop", null, null));
	}
	
	public void PrintFmtCode(List<String> list) {
		list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'d\'", "printf_int"));
		list.add(GenString("beq", "$a0", "\'s\'", "printf_str"));
		list.add(GenString("beq", "$a0", "\'c\'", "printf_char"));
		list.add(GenString("beq", "$a0", "\'.\'", "printf_width"));
		list.add(GenString("beq", "$a0", "\'0\'", "printf_width"));
		PrintfIntCode(list);
		PrintfStrCode(list);
		PrintfCharCode(list);
		PrintfWidthCode(list);
	}
	
	public void PrintfCode(List<String> list) {
		list.add("printf:");
		list.add("printf_loop:");
		list.add(GenString("lb", "$a0", "0($a2)",null));
		list.add(GenString("beq", "$a0", "0", "printf_end"));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'%\'", "printf_fmt"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
		/*list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");*/
		PrintFmtCode(list);
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
		list.add("printf_end:");
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void ReadCode(List<String> list) {
		list.add("getchar:");
		list.add(GenString("li", "$v0", "12", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public void PreWork(List<String> list) {
		PrintfCode(list);
		ReadCode(list);
		MallocCode(list);
	}
	
	public void MallocCode(List<String> list) {
		list.add("malloc:");
		list.add(GenString("li", "$v0", "9", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public int getFunctionSize(Function func) {
		int sum = 0;
		sum += func.size;
		for (Variable var: func.args) {
			sum += var.size;
		}
		return sum;
	}
	
	public List<String> GenerateCode() {
		List<String> list = new LinkedList<String>();
		list.add(".data");
		list.add(".text");
		PreWork(list);
		/*System.out.println("-------");
		for (Function func: ir.fragments) {
			System.out.println(func.name);
		}*/
		funcArgSize.put("printf", 32);
		funcArgSize.put("malloc", 12);
		funcArgSize.put("getchar", 12);
		for (Function func: ir.fragments) {
			funcArgSize.put(func.name, new Integer(getFunctionSize(func)));
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("_start")) {
				//System.out.println("yes!!!");
				curFunc = func;
				StartCode(func, list);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main")) {
				curFunc = func;
				FunctionCode(func, list);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main") || func.name.equals("_start")) continue;
			curFunc = func;
			FunctionCode(func, list);
		}
		PrintCode(list);
		return list;
	}
	
	public void clean(List<String> list) {
		registerWB(list);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
	}
	
	public void cleanGlobal(List<String> list) {
		globalRegWB(list);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
	}
	
	public void StartCode(Function func, List<String> list) {
		int offset = 12;
		comment(list, "function: "+func.name);
		for (Variable var: func.vars) {
			//System.out.println(var.name);
			if (var.name.charAt(0) != '#') globalSet.add(var.name);
			varDict.put(var.name, var);
			
			//System.out.printf("%s, %d\n", var.name, var.size);
			
			if (var.name.charAt(0) == '#') offset += var.size;
		}
		
		stringPos = 1;
		
		for (Variable var: func.vars) {
			if (!(var instanceof BasicVariable)) {
				if (globalSet.contains(var.name)) { 
					list.add(1, var.name+": .space "+var.size);
					stringPos++;
				}
			}
		}
		
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				if (globalSet.contains(var.name)) {
					list.add(1, var.name+": .word 0");
					stringPos++;
				}
			}
		}
		
		list.add("_start"+":");
		curOffset = offset;
		list.add(GenString("add", "$sp", "$sp", "-"+offset));
		list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		len = func.body.size();
		mark = new int[len+10];
		nodes = new Node[len+10];
		block = new int[len+10];
		
		for (int i = 0; i < len+10; i++)
			nodes[i] = new Node();
		
		int i = 0;
		mark[0] = 1;
		
		for (Quadruple q: func.body) {
			if (q instanceof FunctionLabel) {
				FunctionLabel fl = (FunctionLabel) q;
				labelDict.put(fl.s, new Integer(i));
				mark[i] = 1;
			}
			if (q instanceof Label) {
				String ss= "label"+((Label)q).num;
				labelDict.put(ss, new Integer(i));
			}
			i++;
		}
		
		i = 0;
		
		for (Iterator<Quadruple> itr = func.body.iterator(); itr.hasNext(); ) {
			Quadruple q = itr.next();
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				String ss = "label"+g.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof Return) {
				//Return r = (Return) q;
				mark[i+1] = 1;
			}
			if (q instanceof Call) {
				//mark[i] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				String ss = "label"+iff.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				String ss = "label"+ift.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			i++;
		}
		
		blockNum = 0;
		i = 0;
		while (i < len) {
			blockNum++;
			block[i] = blockNum;
			i++;
			while (i < len && mark[i] == 0) {
				block[i] = blockNum;
				i++;
			}
		}
		block[i] = blockNum+1;
		
		/*System.out.println("-----------");
		i = 0;
		for (Quadruple q: func.body) {
			System.out.printf("mark : %d block : %d  ", mark[i], block[i]);
			writeQuadruple(q);
			i++;
		}*/
		
		for (i = 0; i <= blockNum; i++)
			build(i, i+1);
		
		i = 0;
		for (Iterator<Quadruple> itr = func.body.iterator(); itr.hasNext(); ) {
			Quadruple q = itr.next();
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				String ss = "label"+g.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				String ss = "label"+iff.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				String ss = "label"+ift.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof Return) {
				Return r = (Return) q;
				build(block[i], blockNum+1);
			}
			i++;
		}
		
		i = 0;
		//System.out.println(func.name);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;

		for (Quadruple q: func.body) {
			if (i != 0 && block[i] != block[i-1]) {
				//registerWB(list);
				Arrays.fill(regSet, 0);
				Arrays.fill(regTime, 0);
				varMap.clear();
				regUseTime = 0;
			}
			QuadrupleCode(q, list);
			i++;
			if (q instanceof Goto || q instanceof IfFalseGoto || q instanceof IfTrueGoto || q instanceof Call)
				continue;
			if (block[i] != block[i-1]) {
				if (i < len) clean(list);
				else cleanGlobal(list);
			}
		}
		cleanGlobal(list);
		
		list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public String GenString(String s1, String s2, String s3, String s4) {
		if (s3 == null) return String.format("%-8s %s", s1, s2);
		if (s4 == null) return String.format("%-8s %s,%s", s1, s2, s3);
		return String.format("%-8s %s,%s,%s", s1, s2, s3, s4);
	}
	
	public int max(int a, int b){
		if (a > b) return a;
		else return b;
	}
	
	public void build(int x, int y) {
		//System.out.println(x);
		//System.out.println(y);
		for (Info i: nodes[x].list) {
			if (i.y == y) return;
		}
		nodes[x].list.add(new Info(y));
	}
	
	public void FunctionCode(Function func, List<String> list) {
		labelDict.clear();
		comment(list, "function: "+func.name);
		String s = func.name;
		if (s.equals("move")) s = "move1";
		list.add(s+":");
		offset = 0;
		
		//System.out.println("#1: "+offset);
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#2: "+offset);
		
		int maxsize = offset+func.size;
		if (offset < 32) offset = 32;
		
		//System.out.println("#3: "+offset);
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#4: "+offset);
		
		int funcsize = 0;
		for (Quadruple q: func.body) {
			if (q instanceof Call) {
				funcsize = max(funcsize, funcArgSize.get(((Call)q).callee).intValue());
			}
		}
		offset += funcsize; // reserve for args & returnvalue
		
		//System.out.println("#5: "+offset);
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				dict.put(bv.name, new Integer(offset+maxsize-4));
				maxsize -= 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				dict.put(av.name, new Integer(offset+maxsize-av.size-4));
				maxsize -= av.size+4;
			}
		}
		if (offset % 4 != 0) offset = (offset / 4 + 1) * 4;
		curOffset = offset;
		list.add(GenString("add", "$sp", "$sp", "-"+offset));
		list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		//-----------------------------------
		len = func.body.size();
		mark = new int[len+10];
		nodes = new Node[len+10];
		block = new int[len+10];
		
		for (int i = 0; i < len+10; i++)
			nodes[i] = new Node();
		
		int i = 0;
		mark[0] = 1;
		
		for (Quadruple q: func.body) {
			if (q instanceof FunctionLabel) {
				FunctionLabel fl = (FunctionLabel) q;
				labelDict.put(fl.s, new Integer(i));
				mark[i] = 1;
			}
			if (q instanceof Label) {
				String ss= "label"+((Label)q).num;
				labelDict.put(ss, new Integer(i));
			}
			i++;
		}
		
		i = 0;
		
		for (Iterator<Quadruple> itr = func.body.iterator(); itr.hasNext(); ) {
			Quadruple q = itr.next();
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				String ss = "label"+g.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof Return) {
				//Return r = (Return) q;
				mark[i+1] = 1;
			}
			if (q instanceof Call) {
				//mark[i] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				String ss = "label"+iff.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				String ss = "label"+ift.label.num;
				mark[labelDict.get(ss).intValue()] = 1;
				mark[i+1] = 1;
			}
			i++;
		}
		
		blockNum = 0;
		i = 0;
		while (i < len) {
			blockNum++;
			block[i] = blockNum;
			i++;
			while (i < len && mark[i] == 0) {
				block[i] = blockNum;
				i++;
			}
		}
		
		/*System.out.println("-----------");
		i = 0;
		for (Quadruple q: func.body) {
			System.out.printf("mark : %d block : %d  ", mark[i], block[i]);
			writeQuadruple(q);
			i++;
		}*/
		
		for (i = 0; i <= blockNum; i++)
			build(i, i+1);
		
		i = 0;
		for (Iterator<Quadruple> itr = func.body.iterator(); itr.hasNext(); ) {
			Quadruple q = itr.next();
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				String ss = "label"+g.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				String ss = "label"+iff.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				String ss = "label"+ift.label.num;
				int dest = labelDict.get(ss).intValue();
				build(block[i], block[dest]);
			}
			if (q instanceof Return) {
				Return r = (Return) q;
				build(block[i], blockNum+1);
			}
			i++;
		}
		
		i = 0;
		//System.out.println(func.name);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;

		for (Quadruple q: func.body) {
			//System.out.println("block[i]: "+block[i]);
			if (i != 0 && block[i] != block[i-1]) {
				//registerWB(list);
				Arrays.fill(regSet, 0);
				Arrays.fill(regTime, 0);
				varMap.clear();
				regUseTime = 0;
			}
			QuadrupleCode(q, list);
			i++;

			if (q instanceof Goto || q instanceof IfFalseGoto || q instanceof IfTrueGoto || q instanceof Call)
				continue;
			if (block[i] != block[i-1]) {
				if (i < len) clean(list);
				else cleanGlobal(list);
			}
		}
		cleanGlobal(list);
		
		functionEndCode(list);
		
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
	}
	
	public String getRegName(int n) {
		if (n == 0) return "$v1";
		if (n <= 3) return "$a"+n;
		if (n <= 13) return "$t"+(n-4);
		if (n <= 21) return "$s"+(n-14);
		return "null";
	}
	
	public void globalRegWB(List<String> list) {
		for (String s: varMap.keySet()) {
			//System.out.println("###"+s);
			int reg = varMap.get(s);
			//Variable v = varDict.get(s);
			if (globalSet.contains(s))
				list.add(GenString("sw", getRegName(reg), s, null));
		}
	}
	
	public void registerWB(List<String> list) {
		for (String s: varMap.keySet()) {
				//System.out.println("###"+s);
				int reg = varMap.get(s);
				//Variable v = varDict.get(s);
				if (globalSet.contains(s))
					list.add(GenString("sw", getRegName(reg), s, null));
				else {
					int offset = dict.get(s);
					list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
				}
		}
	}
	
	public void registerWB(int reg, List<String> list) {
		List<String> del = new LinkedList<String>();
		for (String s: varMap.keySet()) {
				int r = varMap.get(s);
				if (r != reg) continue;
				//Variable v = varDict.get(s);
				if (globalSet.contains(s))
					list.add(GenString("sw", getRegName(reg), s, null));
				else {
					int offset = dict.get(s);
					list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
				}
				//varMap.remove(s);
				del.add(s);
		}
		for (String s: del) {
			varMap.remove(s);
		}
	}
	
	public void functionEndCode(List<String> list) {
		list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		if (!curFunc.name.equals("main"))
			list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void QuadrupleCode(Quadruple q, List<String> list) {
		if (q instanceof Assign) AssignCode((Assign)q, list);
		if (q instanceof ArithmeticExpr) ArithmeticExprCode((ArithmeticExpr)q, list);
		if (q instanceof ArrayRead) ArrayReadCode((ArrayRead)q, list);
		if (q instanceof ArrayWrite) ArrayWriteCode((ArrayWrite)q, list);
		if (q instanceof MemoryRead) MemoryReadCode((MemoryRead)q, list);
		if (q instanceof MemoryWrite) MemoryWriteCode((MemoryWrite)q, list);
		if (q instanceof AddressOf) AddressOfCode((AddressOf)q, list);
		if (q instanceof Label) LabelCode((Label)q, list);
		if (q instanceof Goto) GotoCode((Goto)q, list);
		if (q instanceof IfFalseGoto) IfFalseGotoCode((IfFalseGoto)q, list);
		if (q instanceof IfTrueGoto) IfTrueGotoCode((IfTrueGoto)q, list);
		if (q instanceof Call) CallCode((Call)q, list);
		if (q instanceof Return) ReturnCode((Return)q, list);
		if (q instanceof Param) paramStack.add((Param)q);
	}
	
	public int VariableOffset(Address adrs, List<String> list) {
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			//System.out.println("Variable offset  "+s);
			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					list.add(GenString("add", "$t1", "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", "$t1", curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					return curOffset;
				}
			}
		} else {
			String s = ((Name)adrs).name;
			
			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					list.add(GenString("add", "$t1", "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", "$t1", curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					return curOffset;
				}
			}
		}
	}
	
	public void AssignCode(Assign assign, List<String> list) {
		comment(list, addressName(assign.dest)+"="+addressName(assign.src));
		if (assign.src instanceof IntegerConst) {
			int offset = VariableOffset(assign.dest, list);
			int value = ((IntegerConst)assign.src).value;
			
			int reg = getReg(list);
			list.add(GenString("li", getRegName(reg), Integer.toString(value), null));
			//list.add(GenString("sw", "$a1", offset+"($sp)", null));
			storeValue(assign.dest, reg, list, 4);
		} else {
			String s = "";
			if (assign.src instanceof Name) {
				Name name = (Name) assign.src;
				s = name.name;
			} else if (assign.src instanceof Temp) {
				Temp t = (Temp) assign.src;
				s = "#t"+t.num;
			} 
			int reg = loadValue(assign.src, list, 4);
			
			String ss = "";
			if (assign.dest instanceof Name) {
				Name name = (Name) assign.dest;
				ss = name.name;
			} else if (assign.dest instanceof Temp) {
				Temp t = (Temp) assign.dest;
				ss = "#t"+t.num;
			} 
			
			//System.out.println("# "+ss+"="+s);
			//System.out.println("## dest: "+varMap.containsKey(ss));
			//System.out.println("## src: "+varMap.containsKey(s));
			
			storeValue(assign.dest, reg, list, 4);
		}
	}
	
	public int getReg(List<String> list) {
		regUseTime++;
		for (int i = 0; i < 22; i++) {
			if (regSet[i] == 0) {
				regTime[i] = regUseTime;
				regSet[i] = 1;
				return i;
			}
		}
		int reg = 0, earliest = regUseTime;
		for (int i = 0; i < 22; i++) {
			if (regTime[i] < earliest) {
				reg = i;
				earliest = regTime[i];
			}
		}
		List<String> del = new LinkedList<String>();
		for (String s: varMap.keySet()) {
			if (varMap.get(s).intValue() == reg) {
				int offset = dict.get(s);
				list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
				//varMap.remove(s);
				del.add(s);
			}
		}
		for (String s: del) {
			varMap.remove(s);
		}
		regTime[reg] = regUseTime;
		regSet[reg] = 1;
		return reg;
	}
	
	public int loadValue(Address adrs, List<String> list, int size) {
		if (adrs == null) return -1;
		if (adrs instanceof Name || adrs instanceof Temp) {
			String s;
			if (adrs instanceof Name) s = ((Name) adrs).name;
			else s = "#t"+((Temp)adrs).num;
			//System.out.println("loadValue:"+s);
			//System.out.println(varMap.containsKey(s));
			if (varMap.containsKey(s)) {
				int reg = varMap.get(s).intValue();
				regTime[reg] = ++regUseTime;
				return reg;
			}
			int reg = getReg(list);
			varMap.put(s, reg);
			//regSet[reg]++;
			if (globalSet.contains(s)) {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					list.add(GenString("lw", getRegName(reg), s, null));
				} else {
					list.add(GenString("la", getRegName(reg), s, null));
				}
			} else {
				int offset = VariableOffset(adrs, list);
				if (size == 4) list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
				else list.add(GenString("lb", getRegName(reg), offset+"($sp)", null));
			}
			return reg;
		}
		int reg = getReg(list);
		if (adrs instanceof IntegerConst) {
			IntegerConst i = (IntegerConst) adrs;
			list.add(GenString("li", getRegName(reg), Integer.toString(i.value), null));
			return reg;
		}
		if (adrs instanceof StringAddressConst) {
			StringAddressConst s = (StringAddressConst) adrs;
			list.add(stringPos, ".align 2");
			list.add(stringPos+1, "string"+stringCnt+":");
			list.add(stringPos+2, GenString(".asciiz", "\""+s.value+"\"", null, null));
			list.add(GenString("la", getRegName(reg), "string"+stringCnt, null));
			stringCnt++;
			return reg;
		}
		return reg;
	}
	
	public void storeValue(Address adrs, int reg, List<String> list, int size) {
		String s = "";
		if (adrs instanceof Name) {
			s = ((Name)adrs).name;
		} else {
			s = "#t"+((Temp)adrs).num;
		}
		int offset = VariableOffset(adrs, list);
		if (varMap.containsKey(s)) {
			int register = varMap.get(s).intValue();
			regSet[register]--;
			varMap.put(s, reg);
			regSet[reg]++;
		} else {
			varMap.put(s, reg);
			regSet[reg]++;
		}
	}
	
	public String transOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			return "+" ;
		}
		if (op == ArithmeticOp.AND) {
			return "&";
		}
		if (op == ArithmeticOp.DIV) {
			return "/";
		}
		if (op == ArithmeticOp.MINUS) {
			return "-";
		}
		if (op == ArithmeticOp.MOD) {
			return "%";
		}
		if (op == ArithmeticOp.MUL) {
			return "*";
		}
		if (op == ArithmeticOp.NOT) {
			return "!";
		}
		if (op == ArithmeticOp.OR) {
			return "|";
		}
		if (op == ArithmeticOp.SHL) {
			return "<<";
		}
		if (op == ArithmeticOp.SHR) {
			return ">>";
		}
		if (op == ArithmeticOp.SUB) {
			return "-";
		}
		if (op == ArithmeticOp.TILDE) {
			return "~";
		}
		if (op == ArithmeticOp.XOR) {
			return "^";
		}
		return "null";
	}
	
	public void ArithmeticExprCode(ArithmeticExpr expr, List<String> list) {
		comment(list, addressName(expr.dest)+"="+addressName(expr.src1)+transOp(expr.op)+addressName(expr.src2));
		int reg1 = loadValue(expr.src1, list, 4);
		int reg2 = loadValue(expr.src2, list, 4);
		int reg3 = getReg(list);
		if (expr.op == ArithmeticOp.ADD) {
			list.add(GenString("add", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.AND) {
			list.add(GenString("and", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.DIV) {
			list.add(GenString("div", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.MUL) {
			list.add(GenString("mul", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.MOD) {
			list.add(GenString("div", getRegName(reg1), getRegName(reg2), null));
			list.add(GenString("mfhi", getRegName(reg3), null, null));
		}
		if (expr.op == ArithmeticOp.MINUS) {
			list.add(GenString("neg", getRegName(reg3), getRegName(reg1), null));
		}
		/*if (expr.op == ArithmeticOp.NOT) {
			//list.add(GenString("not", "$a3", "$a1", null));
			Label l = new Label();
			list.add(GenString("li", getRegName(reg3), "0", null));
			list.add(GenString("bne", getRegName(reg1) , "$zero", "label"+l.num));
			list.add(GenString("li", getRegName(reg3), "1", null));
			list.add("label"+l.num+":");
		}*/
		if (expr.op == ArithmeticOp.OR) {
			list.add(GenString("or", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.SHL) {
			list.add(GenString("sll", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.SHR) {
			list.add(GenString("srl", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.SUB) {
			list.add(GenString("sub", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.TILDE) {
			//list.add(GenString("not", "$a3", "$a1", null));
			list.add(GenString("not", getRegName(reg3), getRegName(reg1), null));
		}
		if (expr.op == ArithmeticOp.XOR) {
			//list.add(GenString("xor", "$a3", "$a1", "$a2"));
			list.add(GenString("xor", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		storeValue(expr.dest, reg3, list, 4);
	}
	
	public void ArrayReadCode(ArrayRead ar, List<String> list) {
		comment(list, addressName(ar.dest)+"="+addressName(ar.src)+"["+addressName(ar.offset)+"]"+", "+Integer.toString(ar.size));
		
		int reg1 = getReg(list), tip1 = 0;
		int offset = VariableOffset(ar.src, list);
		list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
		int reg2, tip2 = 0;
		if (ar.offset instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) ar.offset;
			reg2 = getReg(list);
			list.add(GenString("li", getRegName(reg2), Integer.toString(ic.value), null));
			tip2 = 0;
		}
		else {
			reg2 = loadValue(ar.offset, list, 4);
			tip2 = 1;
		}
		int reg3 = getReg(list);
		list.add(GenString("add", getRegName(reg1), getRegName(reg1), getRegName(reg2)));
		if (ar.size == 4) list.add(GenString("lw", getRegName(reg3), "0("+getRegName(reg1)+")", null));
		else list.add(GenString("lb", getRegName(reg3), "0("+getRegName(reg1)+")", null));
		storeValue(ar.dest, reg3, list, 4);
		
		if (tip1 == 0) regSet[reg1] = 0;
		if (tip2 == 0) regSet[reg2] = 0;
	}
	
	public void ArrayWriteCode(ArrayWrite aw, List<String> list) {
		comment(list, addressName(aw.dest)+"["+addressName(aw.offset)+"]"+"="+addressName(aw.src)+", "+Integer.toString(aw.size));
		int reg1 = loadValue(aw.src, list, aw.size);
		int reg2 = getAddress(aw.dest, list);  //probably can be optimized
		
		int reg3, tip3 = 0;
		
		if (aw.offset instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) aw.offset;
			reg3 = getReg(list);
			list.add(GenString("li", getRegName(reg3), Integer.toString(ic.value), null));
			tip3 = 0;
		} else {
			reg3 = loadValue(aw.offset, list, 4);
			tip3 = 1;
		}
		
		//list.add(GenString("add", "$a2", "$a2", "$a3"));
		list.add(GenString("add", getRegName(reg2), getRegName(reg2), getRegName(reg3)));
		if (aw.size == 4) list.add(GenString("sw", getRegName(reg1), "0("+getRegName(reg2)+")", null));
		else list.add(GenString("sb", getRegName(reg1), "0("+getRegName(reg2)+")", null));
		
		if (tip3 == 0) regSet[reg3] = 0;
	}
	
	public void MemoryReadCode(MemoryRead mr, List<String> list) {
		comment(list, addressName(mr.dest)+"=*"+addressName(mr.src)+", "+Integer.toString(mr.size));
		
		int reg1 = loadValue(mr.src, list, 4);
		int reg2;
		if (mr.size == 4 && mr.type == 0) {
			reg2 = getReg(list);
			list.add(GenString("lw", getRegName(reg2), "0("+getRegName(reg1)+")", null));
			storeValue(mr.dest, reg2, list, mr.size);
		}
		else if (mr.size == 1 && mr.type == 0) {
			reg2 = getReg(list);
			list.add(GenString("lb", getRegName(reg2), "0("+getRegName(reg1)+")", null));
			storeValue(mr.dest, reg2, list, mr.size);
		}
		else {
			//int offset = VariableOffset(mr.dest);  struct possibly will fail
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			reg2 = loadValue(mr.dest, list, 4);
			int reg3 = getReg(list);
			int size = mr.size;
			int pos1 = 0, pos2 = 0;
			
			while (size > 0) {
				list.add(GenString("lw", getRegName(reg3), pos1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), pos2+"("+getRegName(reg2)+")", null));
				size -= 4;
				pos1 += 4;
				pos2 += 4;
			}
			
			regSet[reg3] = 0;
		}
	}
	
	public void MemoryWriteCode(MemoryWrite mw, List<String> list) {
		comment(list, "*"+addressName(mw.dest)+"="+addressName(mw.src)+", "+Integer.toString(mw.size));

		int reg1 = loadValue(mw.src, list, mw.size);
		int reg2;
		if (mw.size <= 4 && mw.type == 0) {
			reg2 = loadValue(mw.dest, list, 4);
			if (mw.size == 4) {
				list.add(GenString("sw", getRegName(reg1), "0("+getRegName(reg2)+")", null));
				//list.add(GenString("sw", "$a3", "0($a2)", null));
			}
			else {
				list.add(GenString("sb", getRegName(reg1), "0("+getRegName(reg2)+")", null));
				//list.add(GenString("sb", "$a3", "0($a2)", null));
			}
		} else {
			//int offset = VariableOffset(mw.dest);
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			reg2 = loadValue(mw.dest, list, 4);
			int reg3 = getReg(list);
			
			int size = mw.size;
			int pos1 = 0, pos2 = 0;
			
			while (size > 0) {
				list.add(GenString("lw", getRegName(reg3), pos1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), pos2+"("+getRegName(reg2)+")", null));
				size -= 4;
				pos1 += 4;
				pos2 += 4;
			}
			
			regSet[reg3] = 0;
		}
	}
	
	public int getAddress(Address adrs, List<String> list) {
		//System.out.println("getAddress!!!");
		
		String ss;
		if (adrs instanceof Name) ss = ((Name)adrs).name;
		else ss = "#t"+((Temp)adrs).num;
		
		if (varMap.containsKey(ss)) {
			int reg = varMap.get(ss);
			
			if (globalSet.contains(ss))
				list.add(GenString("sw", getRegName(reg), ss, null));
			else {
				int offset = dict.get(ss);
				list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
			}
		}
		
		int reg = getReg(list);
		if (adrs instanceof Name) {
			String s = ((Name)adrs).name;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				return reg;
			}
		} else if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				return reg;
			}
		}
		int offset = VariableOffset(adrs, list);
		Variable v = varDict.get(addressName(adrs));
		if (v instanceof BasicVariable) {
			list.add(GenString("add", getRegName(reg), "$sp", Integer.toString(offset)));
		} else {
			list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
		}
		return reg;
	}
	
	public void AddressOfCode(AddressOf ao, List<String> list) {
		comment(list, addressName(ao.dest)+"=&"+addressName(ao.src));
		//int offset = VariableOffset(ao.src);
		//list.add(GenString("add", "$a1", "$sp", Integer.toString(offset)));
		int reg1 = getAddress(ao.src, list);
		//loadValue(ao.src, "$a1", list, 4);
		storeValue(ao.dest, reg1, list, 4);
	}
	
	public void LabelCode(Label l, List<String> list) {
		list.add("label"+l.num+":");
	}
	
	public void GotoCode(Goto g, List<String> list) {
		clean(list);
		list.add(GenString("b", "label"+g.label.num, null, null));
	}
	
	public String transRelOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			return "==";
		}
		if (op == RelationalOp.GE) {
			return ">=";
		}
		if (op == RelationalOp.GT) {
			return ">";
		}
		if (op == RelationalOp.LE) {
			return "<=";
		}
		if (op == RelationalOp.LT) {
			return "<";
		}
		if (op == RelationalOp.NE) {
			return "!=";
		}
		return "null";
	}
	
	public void IfFalseGotoCode(IfFalseGoto iff, List<String> list) {
		comment(list, "if false "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		int reg1 = loadValue(iff.src1, list, 4);
		int reg2 = loadValue(iff.src2, list, 4);
		registerWB(list);
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("bne", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("blt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("ble", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("bgt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("bge", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("beq", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
	}
	
	public void IfTrueGotoCode(IfTrueGoto iff, List<String> list) {
		comment(list, "if false "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		int reg1 = loadValue(iff.src1, list, 4);
		int reg2 = loadValue(iff.src2, list, 4);
		registerWB(list);
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("beq", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("bge", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("bgt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("ble", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("blt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("bne", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
	}
	
	public int getParamSize(Param p) {
		if (p instanceof BasicParam) {
			return 4;
		}
		if (p instanceof ArrayParam) {
			return ((ArrayParam)p).size;
		}
		if (p instanceof MemoryParam) {
			//System.out.println(((MemoryParam)p).size);
			return ((MemoryParam)p).size;
		}
		return 0;
	}
	
	public void commentOfCall(List<String> list, Call call) {
		Address adrs = null;
		if (call.returnValue instanceof BasicParam)
			adrs = ((BasicParam)call.returnValue).src;
		if (call.returnValue instanceof ArrayParam)
			adrs = ((ArrayParam)call.returnValue).name;
		if (call.returnValue instanceof MemoryParam)
			adrs = ((MemoryParam)call.returnValue).src;
		comment(list, addressName(adrs)+" = call "+call.callee+", "+call.numOfParams);
	}
	
	public void changeOffset(Address adrs, int offset) {
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			dict.put(s, new Integer(offset));
		} else {
			String s = ((Name)adrs).name;
			dict.put(s, new Integer(offset));
		}
	}
	
	public void CallCode(Call call, List<String> list) {
		commentOfCall(list, call);
		int num = call.numOfParams;
		int pos = getParamSize(call.returnValue);
		int printf1 = 0, printf2 = 0;
		int malloc1 = 0;
		
		list.add("#call "+call.callee);
		while (num > 0) {
			Param p = paramStack.pop();
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				int reg1 = loadValue(bp.src, list, 4);
				list.add(GenString("sw", getRegName(reg1), pos+"($sp)", null));
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				pos += 4;
			}
			if (p instanceof ArrayParam) {
				ArrayParam ap = (ArrayParam) p;
				int reg1 = getReg(list);
				int offset = VariableOffset(ap.name, list);
				list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
				int reg2 = loadValue(ap.offset, list, 4);
				list.add(GenString("add", getRegName(reg2), getRegName(reg1), getRegName(reg2)));
				int size = ap.size;
				int posa2 = 0;
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				
				int reg3 = getReg(list);
				
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", getRegName(reg3), posa2+"("+getRegName(reg2)+")", null));
					list.add(GenString("sw", getRegName(reg3), pos+"($sp)", null));
					posa2 += 4;
					pos += 4;
				}
				
				regSet[reg1] = regSet[reg2] = regSet[reg3] = 0;
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				//storeValue(mp.src, "$sp", list, 4);
				int reg1 = loadValue(mp.src, list, 4);

				int size = mp.size;
				int posa1 = 0;
				
				int reg2 = getReg(list);
				list.add(GenString("add", getRegName(reg2), "$sp", Integer.toString(pos+4)));
				list.add(GenString("sw", getRegName(reg2), pos+"($sp)", null));
				pos += 4;
				
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", getRegName(reg2), posa1+"("+getRegName(reg1)+")", null));
					list.add(GenString("sw", getRegName(reg2), pos+"($sp)", null));
					posa1 += 4;
					pos += 4;
				}
				
				regSet[reg2] = 0;
			}
			num--;
		}
		if (call.callee.equals("printf")) {
			registerWB(1, list);
			registerWB(2, list);
			list.add(GenString("lw", "$a2", printf1+"($sp)", null));
			list.add(GenString("add", "$a1", "$sp", Integer.toString(printf2)));
		}
		if (call.callee.equals("malloc")) {
			list.add(GenString("lw", "$a0", malloc1+"($sp)", null));
		}
		
		String s = call.callee;
		if (s.equals("move")) s = "move1";
		registerWB(list);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
		list.add(GenString("jal", s, null, null));
		//list.add("nop");
		
		if (call.returnValue instanceof BasicParam) {
			BasicParam bp = (BasicParam) call.returnValue;
			int reg1 = getReg(list);
			list.add(GenString("lw", getRegName(reg1), "0($sp)", null));
			storeValue(bp.src, reg1, list, 4);
		}
		if (call.returnValue instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) call.returnValue;
			int offset = VariableOffset(ap.name, list);
			int reg1 = getReg(list);
			list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
			int reg2 = loadValue(ap.offset, list, 4);
			list.add(GenString("add", getRegName(reg2), getRegName(reg2), getRegName(reg1)));
			int size = ap.size;
			int posa2 = 0, possp = 0;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), possp+"($sp)", null));
				list.add(GenString("sw", getRegName(reg3), posa2+"("+getRegName(reg2)+")", null));
				posa2 += 4;
				possp += 4;
			}
			
			regSet[reg3] = regSet[reg1] = 0;
		}
		if (call.returnValue instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam)call.returnValue;
			//storeValue(mp.src, "$sp", list, 4);
			//list.add("#lalala "+Integer.toString(curOffset));
			int offset = VariableOffset(mp.src, list);
			
			int reg1 = getReg(list);
			
			list.add(GenString("lw", getRegName(reg1), offset+"($sp)",null));
			
			int size = mp.size;
			int possp, posa2;
			possp = posa2 = 0;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), possp+"($sp)", null));
				list.add(GenString("sw", getRegName(reg3), posa2+"("+getRegName(reg1)+")", null));
				posa2 += 4;
				possp += 4;
			}
		}
		
		registerWB(list);
		
	}

	public String ParamString(Param q) {
		String s = "";
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			s = s+"Param ";
			s = s+addressName(bp.src);
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			s = s+"Param ";
			s = s+addressName(ap.name);
			s = s+ "[";
			s = s+addressName(ap.offset);
			s = s+"], "+ap.size;
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			s = s+"Param *";
			s = s+addressName(mp.src);
			s = s+" "+mp.size;
		}
		return s;
	}
	
	public void ReturnCode(Return r, List<String> list) {
		comment(list, "return "+ParamString(r.value));
		if (r.value instanceof BasicParam) {
			BasicParam pb = (BasicParam) r.value;
			int reg1 = loadValue(pb.src, list, 4);
			list.add(GenString("sw", getRegName(reg1), offset+"($sp)", null));
		}
		if (r.value instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) r.value;
			int offset = VariableOffset(ap.name, list);
			int reg1 = getReg(list);
			list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
			int reg2 = loadValue(ap.offset, list, 4);
			list.add(GenString("add", getRegName(reg2), getRegName(reg1), getRegName(reg2)));
			int size = ap.size;
			int posa2 = 0;
			int posvalue = offset;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), posa2+"("+getRegName(reg2)+")", null));
				list.add(GenString("sw", getRegName(reg3), posvalue+"($sp)", null));
				posvalue += 4;
				posa2 += 4;
			}
			
			regSet[reg3] = regSet[reg1] = 0;
		}
		if (r.value instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) r.value;
			int reg1 = loadValue(mp.src, list, 4);
			int size = mp.size;
			int posa1 = 0;
			int posvalue = offset;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), posa1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), posvalue+"($sp)", null));
				posa1 += 4;
				posvalue += 4;
			}
			
			regSet[reg3] = 0;
		}
		registerWB(list);
		Arrays.fill(regSet, 0);
		Arrays.fill(regTime, 0);
		varMap.clear();
		regUseTime = 0;
		
		functionEndCode(list);
	}
	
	public void PrintCode(List<String> list) {
		for (String s: list) {
			System.out.println(s);
		}
	}
	
	public void writeQuadruple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			writeAddress(assign.dest);
			System.out.print(" = ");
			writeAddress(assign.src);
			System.out.println();
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			writeAddress(ae.dest);
			System.out.print(" = ");
			writeAddress(ae.src1);
			writeArithmeticOp(ae.op);
			writeAddress(ae.src2);
			System.out.println();
		}
		if (q instanceof ArrayRead) {
			ArrayRead ar = (ArrayRead) q;
			writeAddress(ar.dest);
			System.out.print(" = ");
			writeAddress(ar.src);
			System.out.print("[");
			writeAddress(ar.offset);
			System.out.print("], "+ar.size);
			System.out.println();
		}
		if (q instanceof ArrayWrite) {
			ArrayWrite aw = (ArrayWrite) q;
			writeAddress(aw.dest);
			System.out.print("[");
			writeAddress(aw.offset);
			System.out.print("] = ");
			writeAddress(aw.src);
			System.out.println(", "+aw.size);
		}
		if (q instanceof MemoryRead) {
			MemoryRead pr = (MemoryRead) q;
			writeAddress(pr.dest);
			System.out.print(" = *");
			writeAddress(pr.src);
			System.out.println(", "+pr.size);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			System.out.print("*");
			writeAddress(mw.dest);
			System.out.print(" = ");
			writeAddress(mw.src);
			System.out.println(", "+mw.size);
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			writeAddress(ao.dest);
			System.out.print(" = &");
			writeAddress(ao.src);
			System.out.println();
		}
		if (q instanceof Label) {
			Label l = (Label) q;
			System.out.println("Label"+l.num);
		}
		if (q instanceof Goto) {
			Goto g = (Goto) q;
			System.out.println("Goto: Label"+g.label.num);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			System.out.print("If False ");
			writeAddress(iff.src1);
			writeRelationalOp(iff.op);
			writeAddress(iff.src2);
			System.out.println(" Goto: Label"+iff.label.num);
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			System.out.print("If True ");
			writeAddress(ift.src1);
			writeRelationalOp(ift.op);
			writeAddress(ift.src2);
			System.out.println(" Goto: Label"+ift.label.num);
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			System.out.print("Param ");
			writeAddress(bp.src);
			System.out.println();
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			System.out.print("Param ");
			writeAddress(ap.name);
			System.out.print("[");
			writeAddress(ap.offset);
			System.out.println("], "+ap.size);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			System.out.print("Param *");
			writeAddress(mp.src);
			System.out.println(" "+mp.size);
		}
		if (q instanceof Call) {
			Call call = (Call) q;
			Address adrs = null;
			if (call.returnValue instanceof BasicParam)
				adrs = ((BasicParam)call.returnValue).src;
			if (call.returnValue instanceof ArrayParam)
				adrs = ((ArrayParam)call.returnValue).name;
			if (call.returnValue instanceof MemoryParam)
				adrs = ((MemoryParam)call.returnValue).src;
			if (adrs != null) {
				writeAddress(adrs);
				System.out.print(" = ");
			}
			System.out.println("Call "+call.callee+" "+call.numOfParams);
		}
		if (q instanceof Return) {
			System.out.print("Return ");
			writeQuadruple(((Return)q).value);
		}
	}
	
	public void writeAddress(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			System.out.print(sac.value);
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			System.out.print(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			System.out.print(n.name);
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			System.out.print("#t"+tmp.num);
		}
	}
	
	public void writeRelationalOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			System.out.print("==");
		}
		if (op == RelationalOp.GE) {
			System.out.print(">=");
		}
		if (op == RelationalOp.GT) {
			System.out.print(">");
		}
		if (op == RelationalOp.LE) {
			System.out.print("<=");
		}
		if (op == RelationalOp.LT) {
			System.out.print("<");
		}
		if (op == RelationalOp.NE) {
			System.out.print("!=");
		}
	}
	
	public void writeArithmeticOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			System.out.print("+");
		}
		if (op == ArithmeticOp.AND) {
			System.out.print("&");
		}
		if (op == ArithmeticOp.DIV) {
			System.out.print("/");
		}
		if (op == ArithmeticOp.MINUS) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.MOD) {
			System.out.print("%");
		}
		if (op == ArithmeticOp.MUL) {
			System.out.print("*");
		}
		if (op == ArithmeticOp.NOT) {
			System.out.print("!");
		}
		if (op == ArithmeticOp.OR) {
			System.out.print("|");
		}
		if (op == ArithmeticOp.SHL) {
			System.out.print("<<");
		}
		if (op == ArithmeticOp.SHR) {
			System.out.print(">>");
		}
		if (op == ArithmeticOp.SUB) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.TILDE) {
			System.out.print("~");
		}
		if (op == ArithmeticOp.XOR) {
			System.out.print("^");
		}
	}
	
}
