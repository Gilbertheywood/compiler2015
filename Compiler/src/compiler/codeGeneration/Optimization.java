package compiler.codeGeneration;

import compiler.ast.*;
import compiler.ir.*;
import compiler.semantic.*;
import compiler.syntactic.*;

import java.util.*;
import java.io.*;

class InsNode {
	Set<String> set;
	
	public InsNode() {
		set = new HashSet<String>();
	}
}

class blockNode {
	List<Integer> list;
	List<Quadruple> quads;
	int startins;
	
	public blockNode() {
		list = new LinkedList<Integer>();
		quads = new LinkedList<Quadruple>();
		startins = 0;
	}
}

class Interval {
	int start, end;
	String s;
	
	public Interval() {
		start = end = 0;
	}
}

class cmp implements Comparator<Interval> {
	public int compare(Interval i1, Interval i2) {
		if (i1.start != i2.start) return i1.start-i2.start;
		else return i1.end-i2.end;
	}
}

public class Optimization {
	
	public IR ir;
	public Set<String> globalSet;
	public blockNode[] blk;
	Map<String, Integer> regAllocate;
	Map<String, Integer> varStart;
	Map<String, Integer> varEnd;
	Map<Integer, String> regFile; // register file map
	Map<String, Integer> vartoreg;
	public Set<String> addressSet;
	Map<String, Interval> stringtoitv;
	public List<String> list;
	public int[] busy;
	
	public Map<String, Integer> dict; // record variable offset
	public Map<String, Variable> varDict; // link string to variable 
	public int curOffset;
	public int stringCnt;
	public Stack<Param> paramStack;
	public Function curFunc;
	public int stringPos;
	public Map<String, Integer> funcArgSize;
	public int offset;
	Set<String> tempSet;
	public InsNode[] liveout;
	public InsNode[] livein;
	public int totalregs;
	public boolean exsitcall;

	int[] deadcode;
	
	public Optimization(IR ir) {
		this.ir = ir;
		tempSet = new HashSet<String>();
		globalSet = new HashSet<String>();
		regAllocate = new HashMap<String, Integer>();
		regFile = new HashMap<Integer, String>();
		vartoreg = new HashMap<String, Integer>();
		stringtoitv = new HashMap<String, Interval>();
		varStart = new HashMap<String, Integer>();
		varEnd = new HashMap<String, Integer>();
		list = new LinkedList<String>();
		addressSet = new HashSet<String>();
		
		dict = new HashMap<String, Integer>();
		varDict = new HashMap<String, Variable>();
		stringCnt = 0;
		paramStack = new Stack<Param>();
		funcArgSize = new HashMap<String, Integer>();
		busy = new int[6];
	}
	
	public void pretest() {
		if (ir.fragments.size() <= 4) {
			boolean valid = false;
			for (Function func: ir.fragments) {
				if (func.name.equals("_tak") || func.name.equals("_getcount")) {
					valid = true;
					break;
				}
			}
			if (valid) totalregs = 7;
		}
	}
	
	public void GenerateCode() {
		List<String> list = new LinkedList<String>();
		
		totalregs = 22;
		pretest();
		
		list.add(".data");
		list.add(".text");
		PreWork(list);
		funcArgSize.put("printf", 32);
		funcArgSize.put("malloc", 12);
		funcArgSize.put("getchar", 12);
		for (Function func: ir.fragments) {
			funcArgSize.put(func.name, new Integer(getFunctionSize(func)));
		}
		
		for (Function func: ir.fragments) {
			if (func.name.equals("_start")) {
				for (Variable var: func.vars) {
					if (var.name.charAt(0) != '#') globalSet.add(var.name);
				}
				break;
			}
		}
		for (Function func:ir.fragments) {
			if (func.name.equals("_start")) {
				curFunc = func;
				FunctionAnalyse(func, list);
				break;
			}
		}
		for (Function func:ir.fragments) {
			if (func.name.equals("main")) {
				curFunc = func;
				FunctionAnalyse(func, list);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main") || func.name.equals("_start"))
				continue;
			curFunc = func;
			FunctionAnalyse(func, list);
		}
		
		PrintCode(list);
	}
	
	public void build(int x, int y) {
		blk[x].list.add(y);
	}
	
	public void FunctionAnalyse(Function func, List<String> list) {
		regAllocate.clear();
		varStart.clear();
		varEnd.clear();
		regFile.clear();
		vartoreg.clear();
		addressSet.clear();
		
		int varSize = func.vars.size()+func.args.size();
		int insSize = func.body.size();
		Map<String, Integer> labelMap = new HashMap<String, Integer>();
		
		liveout = new InsNode[insSize+10];
		for (int i = 0; i <= insSize+9; i++)
			liveout[i] = new InsNode();
		
		livein = new InsNode[insSize+10];
		for (int i = 0; i <= insSize+9; i++)
			livein[i] = new InsNode();
		
		// initialize
		
		int[] mark = new int[insSize+10];
		Arrays.fill(mark, 0);
		int i = 0;
		mark[1] = 1;
		
		for (Quadruple q: func.body) {
			i++;
			if (q instanceof Label) {
				Label l = (Label) q;
				labelMap.put("label"+l.num, i);
			}
		}
		
		//build label map
		
		i = 0;
		for (Quadruple q: func.body) {
			i++;
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				mark[i+1] = 1;
				mark[labelMap.get("label"+g.label.num).intValue()] = 1;
			}
			if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				mark[i+1] = 1;
				mark[labelMap.get("label"+iff.label.num).intValue()] = 1;
			}
			if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				mark[i+1] = 1;
				mark[labelMap.get("label"+ift.label.num).intValue()] = 1;
			}
			if (q instanceof Return) {
				Return r = (Return) q;
				mark[i+1] = 1;
			}
		}
		
		// create basic block
		
		i = 0;
		int blockNum = 0; // blockNum starts with 1
		int[] block = new int[insSize+10]; //every instruction maps to a block
		int[] lastins = new int[insSize+10];
		
		blk = new blockNode[insSize+10]; // every basic block
		for (i = 0; i <= insSize+9; i++)
			blk[i] = new blockNode();
		blk[0].startins = 0;
		
		i = 0;
		for (Quadruple q: func.body) {
			i++;
			if (mark[i] == 1) {
				lastins[blockNum] = i-1;
				blockNum++;
				blk[blockNum].startins = i;
				block[i] = blockNum;
			}
			block[i] = blockNum;
		}
		
		i = 0;
		build(1, 0);
		for (Quadruple q: func.body) {
			i++;
			blk[block[i]].quads.add(0, q);
			if (q instanceof Goto) {
				Goto g = (Goto) q;
				build(block[labelMap.get("label"+g.label.num)], block[i]);
			} else if (q instanceof IfFalseGoto) {
				IfFalseGoto iff = (IfFalseGoto) q;
				build(block[labelMap.get("label"+iff.label.num)], block[i]);
				if (i < insSize) build(block[i+1], block[i]);
			} else if (q instanceof IfTrueGoto) {
				IfTrueGoto ift = (IfTrueGoto) q;
				build(block[labelMap.get("label"+ift.label.num)], block[i]);
				if (i < insSize) build(block[i+1], block[i]);
			} else {
				if (i < insSize) build(block[i+1], block[i]);
			}
		}
		
		int change = 1;
		deadcode = new int[insSize+10];
		Arrays.fill(deadcode, 0);
		
		Set<String> tmp = new HashSet<String>();
		tmp.clear();
		
		while (change != 0) {
			change = 0;
			
			//System.out.println(blockNum);
			
			for (i = blockNum; i > 0; i--) {
				int startnum = blk[i].startins;
				int endnum = startnum+blk[i].quads.size()-1;
				
				//System.out.printf("start: %d end: %d\n", startnum, endnum);
				
				int curi = endnum;
					
				for (Quadruple q: blk[i].quads) {
					Triple<String, String, String> tri = getTriple(q);
					
					tmp.clear();
					for (String s: liveout[curi].set) {
						tmp.add(s);
						//System.out.println(curi+" tmp: "+s);
					}
					
					if (tri.first != null) {
						if (tmp.contains(tri.first)) {
							tmp.remove(tri.first);
							//change = 1;
							//System.out.println("case1");
						}
					}
					if (tri.second != null) {
						if (!tmp.contains(tri.second)) {
							tmp.add(tri.second);
							//change = 1;
							//System.out.println("case2");
						}
					}
					if (tri.third != null) {
						if (!tmp.contains(tri.third)) {
							tmp.add(tri.third);
							//System.out.println("case3");
							//change = 1;
						}
					}
					
					if (curi != startnum) {
						for (String s: tmp) {
							if (!liveout[curi-1].set.contains(s)) {
								liveout[curi-1].set.add(s);
								change = 1;
								//System.out.println("case1");
							}
						}
					}
					
					if (curi == startnum) {
						for (int b: blk[i].list) {
							int lasti = lastins[b];
							for (String s: tmp) {
								if (!liveout[lasti].set.contains(s)) {
									liveout[lasti].set.add(s);
									change = 1;
									//System.out.println("case2");
								}
							}
						}
					}
					
					curi--;
				}

			}

			//System.out.println("hello2!");
		}
		
		// print liveout set
		
		/*for (i = 1; i<= insSize; i++) {
			System.out.print("liveout: "+i+"  ");
			for (String s: liveout[i].set) {
				System.out.print(s);
			}
			
			System.out.println();
		}*/
		
		// calculate deadcode
		
		i = 0;
		for (Quadruple q: func.body) {
			i++;
			Triple<String, String, String> tri = getTriple(q);
			if (tri.first != null) {
				if (!(q instanceof Call))
				if (!liveout[i].set.contains(tri.first) && !globalSet.contains(tri.first)) {
					deadcode[i] = 1;
				}
			}
		}
		
		// write quadruple --------------
		
		/*i = 0; 
		for (Quadruple q: func.body) {
			i++;
			System.out.print(i+" dead: "+deadcode[i]+" : ");
			writeQuadruple(q);
		}*/
		
		for (i = 1; i <= insSize; i++) {
			for (String s: liveout[i].set) {
				if (!varStart.containsKey(s)) {
					varStart.put(s, i);
				}
				varEnd.put(s, i);
			}
		}
		
		Interval[] itv = new Interval[varSize+10];
		for (i = 0; i < varSize+10; i++)
			itv[i] = new Interval();
		
		int cnt = 0;
		for (String s: varStart.keySet()) {
			cnt++;
			itv[cnt].s = s;
			itv[cnt].start = varStart.get(s);
			itv[cnt].end = varEnd.get(s);
			stringtoitv.put(s, itv[cnt]);
		}
		
		//System.out.println(cnt);
		
		if (cnt > 0) Arrays.sort(itv, 1, cnt, new cmp());
		
		int j, k;
		//System.out.println(func.name);
		for (i = 1; i <= insSize; i++) {
			//System.out.println("instruction "+i);
			for (j = 1; j <= cnt; j++) {
				if (globalSet.contains(itv[j].s)) continue;
				if (addressSet.contains(itv[j].s)) continue;
				
				if (itv[j].end < i) {
					//System.out.print("delete reg: "+itv[j].s+" "+itv[j].start+"-"+itv[j].end+" ");
					//System.out.println(vartoreg.get(itv[j].s));
					if (vartoreg.containsKey(itv[j].s)) {
						int reg = vartoreg.get(itv[j].s);
						regAllocate.put(itv[j].s, reg);
						vartoreg.remove(itv[j].s);
						regFile.remove(reg);
					}
				}
				if (itv[j].start == i) {
					//System.out.print("allocate reg: "+itv[j].s+" "+itv[j].start+"-"+itv[j].end);
					boolean success = false;
					for (k = 5; k < totalregs; k++) {
						if (!regFile.containsKey(k)) {
							regFile.put(k, itv[j].s);
							vartoreg.put(itv[j].s, k);
							success = true;
							//System.out.println("(1)"+k);
							break;
						}
					}
					if (!success) {
						int maxEnd = itv[j].end;
						int reg = -1;
						for (k = 5; k < totalregs; k++) {
							Interval interval = stringtoitv.get(regFile.get(k));
							if (interval.end > maxEnd) {
								maxEnd = interval.end;
								reg = k;
							}
						}
						if (reg != -1) {
							String s = regFile.get(reg);
							regFile.remove(reg);
							vartoreg.remove(s);
							
							regFile.put(reg, itv[j].s);
							vartoreg.put(itv[j].s, reg);
						}

						//System.out.println("(2)"+reg);
					}
				}
			}
		}
		
		/*for (String s: regAllocate.keySet()) {
			System.out.printf("(%s %d)\n", s, regAllocate.get(s));
		}*/
		
		if (!func.name.equals("_start")) FunctionCode(func, list);
		else StartCode(func,list);
	}

	public String AddressName(Address adrs) {
		if (adrs instanceof Name) return ((Name)adrs).name;
		if (adrs instanceof Temp) return "#t"+((Temp)adrs).num;
		return null;
	}
	
	public Triple<String, String, String> getTriple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			return new Triple<String, String, String>(AddressName(assign.dest), AddressName(assign.src), null);
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			return new Triple<String, String, String>(AddressName(ae.dest), AddressName(ae.src1), AddressName(ae.src2));
		}
		if (q instanceof MemoryRead) {
			MemoryRead mr = (MemoryRead) q;
			return new Triple<String, String, String>(AddressName(mr.dest), AddressName(mr.src), null);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			return new Triple<String, String, String>(null, AddressName(mw.dest), AddressName(mw.src));
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			addressSet.add(AddressName(ao.src));
			return new Triple<String, String, String>(AddressName(ao.dest), AddressName(ao.src), null);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			return new Triple<String, String, String>(null, AddressName(iff.src1), AddressName(iff.src2));
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			return new Triple<String, String, String>(null, AddressName(ift.src1), AddressName(ift.src2));
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			return new Triple<String, String, String>(null, AddressName(bp.src), null);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			return new Triple<String, String, String>(null, AddressName(mp.src), null);
		}
		if (q instanceof Return) {
			Param p = ((Return)q).value;
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				return new Triple<String, String, String>(null, AddressName(bp.src), null);
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				return new Triple<String, String, String>(null, AddressName(mp.src), null);
			}
		}
		if (q instanceof Call) {
			Param p = ((Call)q).returnValue;
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				return new Triple<String, String, String>(AddressName(bp.src), null, null);
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				return new Triple<String, String, String>(AddressName(mp.src), null, null);
			}
		}
		return new Triple<String, String, String>(null, null, null);
	}
	
	public void comment(List<String> list, String s) {
		list.add("#  "+s);
	}
	
	public String addressName(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			return sac.value;
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			return Integer.toString(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			return n.name;
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			return "#t"+tmp.num;
		}
		return "";
	}
	
	public void PrintfIntCode(List<String> list) {
		list.add("printf_int:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "1", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfCharCode(List<String> list) {
		list.add("printf_char:");
		list.add(GenString("lb", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfStrCode(List<String> list) {
		list.add("printf_str:");
		list.add(GenString("lw", "$a0", "0($a1)", null));
		list.add(GenString("sub", "$a1", "$a1", "4"));
		list.add(GenString("li", "$v0", "4", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
	}
	
	public void PrintfWidthCode(List<String> list) {
		list.add("printf_width:");
		list.add(GenString("li", "$v0", "1", null));
		list.add(GenString("li", "$a0", "0", null));
		list.add(GenString("lb", "$a3", "0($a2)", null));
		list.add(GenString("add","$a2", "$a2", "2"));
		list.add(GenString("add", "$a3", "$a3", "-48"));
		
		list.add(GenString("li", "$t0", "0", null));
		list.add("WidthLoop:");
		list.add(GenString("mul", "$t0", "$t0", "10"));
		list.add(GenString("add", "$t0", "$t0", "9"));
		list.add(GenString("add", "$a3", "$a3", "-1"));
		list.add(GenString("bne", "$a3", "0", "WidthLoop"));
		
		list.add(GenString("lw", "$v1", "0($a1)", null));
		
		list.add("ZeroLoop:");
		list.add(GenString("div", "$t0", "$t0", "10"));
		list.add(GenString("bgt", "$v1", "$t0", "printf_int"));
		list.add("syscall");
		list.add(GenString("b", "ZeroLoop", null, null));
	}
	
	public void PrintFmtCode(List<String> list) {
		list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'d\'", "printf_int"));
		list.add(GenString("beq", "$a0", "\'c\'", "printf_char"));
		list.add(GenString("beq", "$a0", "\'s\'", "printf_str"));
		list.add(GenString("beq", "$a0", "\'.\'", "printf_width"));
		list.add(GenString("beq", "$a0", "\'0\'", "printf_width"));
		PrintfIntCode(list);
		PrintfStrCode(list);
		PrintfCharCode(list);
		PrintfWidthCode(list);
	}
	
	public void PrintfCode(List<String> list) {
		list.add("printf:");
		list.add("printf_loop:");
		list.add(GenString("lb", "$a0", "0($a2)",null));
		list.add(GenString("beq", "$a0", "0", "printf_end"));
		list.add(GenString("add", "$a2", "$a2", "1"));
		list.add(GenString("beq", "$a0", "\'%\'", "printf_fmt"));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");
		list.add(GenString("b", "printf_loop", null, null));
		/*list.add("printf_fmt:");
		list.add(GenString("lb", "$a0", "0($a2)", null));
		list.add(GenString("li", "$v0", "11", null));
		list.add("syscall");*/
		PrintFmtCode(list);
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
		list.add("printf_end:");
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void ReadCode(List<String> list) {
		list.add("getchar:");
		list.add(GenString("li", "$v0", "12", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public void PreWork(List<String> list) {
		PrintfCode(list);
		ReadCode(list);
		MallocCode(list);
	}
	
	public void MallocCode(List<String> list) {
		list.add("malloc:");
		list.add(GenString("li", "$v0", "9", null));
		list.add("syscall");
		list.add(GenString("sw", "$v0", "0($sp)", null));
		list.add(GenString("jr", "$31", null, null));
		//list.add("nop");
	}
	
	public int getFunctionSize(Function func) {
		int sum = 0;
		sum += func.size;
		for (Variable var: func.args) {
			sum += var.size;
		}
		return sum;
	}
	
	public void StartCode(Function func, List<String> list) {
		int offset = 12;
		//offset += 100; // for temporary save
		comment(list, "function: "+func.name);
		for (Variable var: func.vars) {
			//System.out.println(var.name);
			if (var.name.charAt(0) != '#') globalSet.add(var.name);
			varDict.put(var.name, var);
			
			//System.out.printf("%s, %d\n", var.name, var.size);
			
			if (var.name.charAt(0) == '#') offset += var.size;
		}
		
		stringPos = 1;
		
		for (Variable var: func.vars) {
			if (!(var instanceof BasicVariable)) {
				if (globalSet.contains(var.name)) { 
					list.add(1, var.name+": .space "+var.size);
					stringPos++;
				}
			}
		}
		
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				if (globalSet.contains(var.name)) {
					list.add(1, var.name+": .word 0");
					stringPos++;
				}
			}
		}
		
		list.add("_start"+":");
		curOffset = offset;
		list.add(GenString("add", "$sp", "$sp", "-"+offset));
		list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		int i = 0;
		for (Quadruple q: func.body) {
			QuadrupleCode(q, list, i);
		}
		
		list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public String GenString(String s1, String s2, String s3, String s4) {
		if (s3 == null) return String.format("%-8s %s", s1, s2);
		if (s4 == null) return String.format("%-8s %s,%s", s1, s2, s3);
		return String.format("%-8s %s,%s,%s", s1, s2, s3, s4);
	}
	
	public int max(int a, int b){
		if (a > b) return a;
		else return b;
	}
	
	public void FunctionCode(Function func, List<String> list) {
		comment(list, "function: "+func.name);
		String s = func.name;
		if (s.equals("move")) s = "move1";
		list.add(s+":");
		offset = 0;
		
		if (s.equals("main")) {
			list.add(GenString("li", "$k0", "1", null));
			list.add(GenString("li", "$k1", "-1", null));
			list.add(GenString("li", "$gp", "2", null));
		}
		
		//check whether has function call
		
		exsitcall = false;
		
		for (Quadruple q: func.body) {
			if (q instanceof Call) exsitcall = true;
		}
		
		//System.out.println("#1: "+offset);
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#2: "+offset);
		
		int maxsize = offset+func.size;
		if (offset < 32) offset = 32;
		
		//System.out.println("#3: "+offset);
		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.put(bv.name, bv);
				//System.out.printf("%s, %d\n", bv.name, bv.size);
				offset += 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.put(av.name, av);
				//System.out.printf("%s, %d\n", av.name, av.size);
				offset += ((av.size+4-1)/4+1)*4;
			}
		}
		
		//System.out.println("#4: "+offset);
		
		int funcsize = 0;
		for (Quadruple q: func.body) {
			if (q instanceof Call) {
				funcsize = max(funcsize, funcArgSize.get(((Call)q).callee).intValue());
			}
		}
		offset += funcsize; // reserve for args & returnvalue
		//offset += 100; // for temporary save
		
		//System.out.println("#5: "+offset);
		
		if (offset % 4 != 0) offset = (offset / 4 + 1) * 4;
		curOffset = offset;
		list.add(GenString("sub", "$sp", "$sp", Integer.toString(offset)));
		if (exsitcall) list.add(GenString("sw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("sw", "$fp", (offset-8)+"($sp)", null));
		list.add(GenString("move", "$fp", "$sp", null));
		curOffset -= 8;
		
		//calculate args value
		
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				int pos = offset+maxsize-4;
				dict.put(bv.name, pos);
				if (regAllocate.containsKey(var.name)) {
					int reg = regAllocate.get(var.name);
					list.add(GenString("lw", getRegName(reg), pos+"($sp)", null));
				}
				maxsize -= 4;
			} else {
				ArrayVariable av = (ArrayVariable) var;
				dict.put(av.name, new Integer(offset+maxsize-av.size-4));
				maxsize -= av.size+4;
			}
		}
		
		int i = 0;
		
		for (Quadruple q: func.body) {
			i++;
			if (deadcode[i] == 1) continue;
			
			/*writeQuadruple(q);
			System.out.print("# ");
			for (int j = 0; j <= 4; j++)
				System.out.printf("(%d %d)", j, busy[j]);
			System.out.println();*/
			for (int j = 0; j <= 4; j++)
				busy[j] = 0;
			
			QuadrupleCode(q, list, i);
		}
		
		functionEndCode(list);

		for (Variable var: func.vars) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
		for (Variable var: func.args) {
			if (var instanceof BasicVariable) {
				BasicVariable bv = (BasicVariable) var;
				varDict.remove(bv.name);
				dict.remove(bv.name);
			} else {
				ArrayVariable av = (ArrayVariable) var;
				varDict.remove(av.name, av);
				dict.remove(av.name);
			}
		}
	}
	
	public String getRegName(int n) {
		if (n == 0) return "$v1";
		if (n <= 3) return "$a"+n;
		if (n <= 13) return "$t"+(n-4);
		if (n <= 21) return "$s"+(n-14);
		if (n == 22) return "$k0";
		if (n == 23) return "$k1";
		if (n == 24) return "$gp";
		if (n == 25) return "$zero";
		return "null";
	}
	
	public void functionEndCode(List<String> list) {
		if (exsitcall) list.add(GenString("lw", "$31", (offset-4)+"($sp)", null));
		list.add(GenString("lw", "$fp", (offset-8)+"($sp)", null));
		if (!curFunc.name.equals("main"))
			list.add(GenString("move", "$sp", "$fp", null));
		list.add(GenString("j", "$31", null, null));
		//list.add("nop");
	}
	
	public void QuadrupleCode(Quadruple q, List<String> list, int number) {
		if (q instanceof Assign) AssignCode((Assign)q, list, number);
		if (q instanceof ArithmeticExpr) ArithmeticExprCode((ArithmeticExpr)q, list, number);
		if (q instanceof MemoryRead) MemoryReadCode((MemoryRead)q, list, number);
		if (q instanceof MemoryWrite) MemoryWriteCode((MemoryWrite)q, list, number);
		if (q instanceof AddressOf) AddressOfCode((AddressOf)q, list, number);
		if (q instanceof Label) LabelCode((Label)q, list, number);
		if (q instanceof Goto) GotoCode((Goto)q, list, number);
		if (q instanceof IfFalseGoto) IfFalseGotoCode((IfFalseGoto)q, list, number);
		if (q instanceof IfTrueGoto) IfTrueGotoCode((IfTrueGoto)q, list, number);
		if (q instanceof Call) CallCode((Call)q, list, number);
		if (q instanceof Return) ReturnCode((Return)q, list, number);
		if (q instanceof Param) paramStack.add((Param)q);
	}
	
	public int VariableOffset(Address adrs, List<String> list) {
		//int reg = getReg(list);
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			//System.out.println("Variable offset  "+s);
			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					int reg = getReg(list);
					list.add(GenString("add", getRegName(reg), "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", getRegName(reg), curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					free(reg);
					return curOffset;
				}
			}
		} else {
			String s = ((Name)adrs).name;
			
			if (dict.containsKey(s)) return dict.get(s).intValue();
			else {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					curOffset -= varDict.get(s).size;
					dict.put(s, new Integer(curOffset));
					return curOffset;
				} else {
					curOffset -= varDict.get(s).size;
					curOffset -= 4;
					int reg = getReg(list);
					list.add(GenString("add", getRegName(reg), "$sp", Integer.toString(curOffset+4)));;
					list.add(GenString("sw", getRegName(reg), curOffset+"($sp)", null));
					dict.put(s, new Integer(curOffset));
					free(reg);
					return curOffset;
				}
			}
		}
	}
	
	public int getReg(List<String> list) {
		for (int i = 0; i <= 4; i++) {
			if (busy[i] == 0) {
				busy[i] = 1;
				return i;
			}
		}
		return 0;
	}
	
	public void free(int reg) {
		if (0<=reg && reg <= 4) busy[reg] = 0;
	}
	
	public void AssignCode(Assign assign, List<String> list, int number) {
		comment(list, addressName(assign.dest)+"="+addressName(assign.src));
		if (assign.src instanceof IntegerConst) {
			int offset = VariableOffset(assign.dest, list);
			int value = ((IntegerConst)assign.src).value;
			
			String s = AddressName(assign.dest);
			int reg;
			if (regAllocate.containsKey(s)) reg = regAllocate.get(s);
			else reg = getReg(list);
			
			list.add(GenString("li", getRegName(reg), Integer.toString(value), null));
			//list.add(GenString("sw", "$a1", offset+"($sp)", null));
			storeValue(assign.dest, reg, list, 4);
			free(reg);
		} else {
			String s = "";
			if (assign.src instanceof Name) {
				Name name = (Name) assign.src;
				s = name.name;
			} else if (assign.src instanceof Temp) {
				Temp t = (Temp) assign.src;
				s = "#t"+t.num;
			} 
			int reg = loadValue(assign.src, list, 4);
			
			String ss = "";
			if (assign.dest instanceof Name) {
				Name name = (Name) assign.dest;
				ss = name.name;
			} else if (assign.dest instanceof Temp) {
				Temp t = (Temp) assign.dest;
				ss = "#t"+t.num;
			} 
			
			//System.out.println("# "+ss+"="+s);
			//System.out.println("## dest: "+varMap.containsKey(ss));
			//System.out.println("## src: "+varMap.containsKey(s));
			
			storeValue(assign.dest, reg, list, 4);
			free(reg);
		}
	}
	
	public int loadValue(Address adrs, List<String> list, int size) {
		if (adrs == null) return -1;
		if (adrs instanceof Name || adrs instanceof Temp) {
			String s;
			if (adrs instanceof Name) s = ((Name) adrs).name;
			else s = "#t"+((Temp)adrs).num;
			//System.out.println("loadValue:"+s);
			//System.out.println(varMap.containsKey(s));
			if (regAllocate.containsKey(s)) {
				int reg = regAllocate.get(s).intValue();
				return reg;
			}
			int reg = getReg(list);
			//regSet[reg]++;
			if (globalSet.contains(s)) {
				Variable v = varDict.get(s);
				if (v instanceof BasicVariable) {
					list.add(GenString("lw", getRegName(reg), s, null));
				} else {
					list.add(GenString("la", getRegName(reg), s, null));
				}
			} else {
				int offset = VariableOffset(adrs, list);
				if (size == 4) list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
				else list.add(GenString("lb", getRegName(reg), offset+"($sp)", null));
			}
			return reg;
		}
		int reg;
		if (adrs instanceof IntegerConst) {
			IntegerConst i = (IntegerConst) adrs;
			if (i.value == 0) return 25;
			if (i.value == 1) return 22;
			if (i.value == -1 ) return 23;
			if (i.value == 2) return 24;
			reg = getReg(list);
			list.add(GenString("li", getRegName(reg), Integer.toString(i.value), null));
			return reg;
		}
		if (adrs instanceof StringAddressConst) {
			StringAddressConst s = (StringAddressConst) adrs;
			reg = getReg(list);
			list.add(stringPos, ".align 2");
			list.add(stringPos+1, "string"+stringCnt+":");
			list.add(stringPos+2, GenString(".asciiz", "\""+s.value+"\"", null, null));
			list.add(GenString("la", getRegName(reg), "string"+stringCnt, null));
			stringCnt++;
			return reg;
		}
		return -1;
	}
	
	public void storeValue(Address adrs, int reg, List<String> list, int size) {
		String s = "";
		if (adrs instanceof Name) {
			s = ((Name)adrs).name;
		} else {
			s = "#t"+((Temp)adrs).num;
		}
		int offset = VariableOffset(adrs, list);
		if (regAllocate.containsKey(s)) {
			int register = regAllocate.get(s).intValue();
			if (reg != register) list.add(GenString("move", getRegName(register), getRegName(reg),null));
		} else {
			if (globalSet.contains(s)) {
				list.add(GenString("sw", getRegName(reg), s, null));
				return;
			}
			if (size == 4) list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
			else list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
		}
	}
	
	public String transOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			return "+" ;
		}
		if (op == ArithmeticOp.AND) {
			return "&";
		}
		if (op == ArithmeticOp.DIV) {
			return "/";
		}
		if (op == ArithmeticOp.MINUS) {
			return "-";
		}
		if (op == ArithmeticOp.MOD) {
			return "%";
		}
		if (op == ArithmeticOp.MUL) {
			return "*";
		}
		if (op == ArithmeticOp.NOT) {
			return "!";
		}
		if (op == ArithmeticOp.OR) {
			return "|";
		}
		if (op == ArithmeticOp.SHL) {
			return "<<";
		}
		if (op == ArithmeticOp.SHR) {
			return ">>";
		}
		if (op == ArithmeticOp.SUB) {
			return "-";
		}
		if (op == ArithmeticOp.TILDE) {
			return "~";
		}
		if (op == ArithmeticOp.XOR) {
			return "^";
		}
		return "null";
	}
	
	public void ArithmeticExprCode(ArithmeticExpr expr, List<String> list, int number) {
		comment(list, addressName(expr.dest)+"="+addressName(expr.src1)+transOp(expr.op)+addressName(expr.src2));
		
		int reg1 = -1;
		int reg2 = -1;
		int reg3 = -1;
		String s = AddressName(expr.dest);
		
		if (regAllocate.containsKey(s)) reg3 = regAllocate.get(s);
		else reg3 = getReg(list);
		
		if (expr.op == ArithmeticOp.ADD) {
			//list.add(GenString("add", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			if (expr.src1 instanceof IntegerConst) {
				IntegerConst ic1 = (IntegerConst) expr.src1;
				if (expr.src2 instanceof IntegerConst) {
					IntegerConst ic2 = (IntegerConst) expr.src2;
					list.add(GenString("li", getRegName(reg3), Integer.toString(ic1.value+ic2.value), null));
				} else {
					reg2 = loadValue(expr.src2, list, 4);
					list.add(GenString("add", getRegName(reg3), getRegName(reg2), Integer.toString(ic1.value)));
				}
			} else {
				reg1 = loadValue(expr.src1, list, 4);
				if (expr.src2 instanceof IntegerConst) {
					IntegerConst ic2 = (IntegerConst) expr.src2;
					list.add(GenString("add", getRegName(reg3), getRegName(reg1), Integer.toString(ic2.value)));
				} else {
					reg2 = loadValue(expr.src2, list, 4);
					list.add(GenString("add", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
				}
			}
			storeValue(expr.dest, reg3, list, 4);
			free(reg1);free(reg2);free(reg3);
			return;
		}
		if (expr.op == ArithmeticOp.MUL) {
			//list.add(GenString("mul", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			if (expr.src1 instanceof IntegerConst) {
				IntegerConst ic1 = (IntegerConst) expr.src1;
				if (expr.src2 instanceof IntegerConst) {
					IntegerConst ic2 = (IntegerConst) expr.src2;
					list.add(GenString("li", getRegName(reg3), Integer.toString(ic1.value*ic2.value), null));
				} else {
					reg2 = loadValue(expr.src2, list, 4);
					list.add(GenString("mul", getRegName(reg3), getRegName(reg2), Integer.toString(ic1.value)));
				}
			} else {
				reg1 = loadValue(expr.src1, list, 4);
				if (expr.src2 instanceof IntegerConst) {
					IntegerConst ic2 = (IntegerConst) expr.src2;
					list.add(GenString("mul", getRegName(reg3), getRegName(reg1), Integer.toString(ic2.value)));
				} else {
					reg2 = loadValue(expr.src2, list, 4);
					list.add(GenString("mul", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
				}
			}
			storeValue(expr.dest, reg3, list, 4);
			free(reg1);free(reg2);free(reg3);
			return;
		}
		if (expr.op == ArithmeticOp.SUB) {
			//list.add(GenString("sub", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			if (expr.src2 instanceof IntegerConst) {
				IntegerConst ic2 = (IntegerConst) expr.src2;
				if (expr.src1 instanceof IntegerConst) {
					IntegerConst ic1 = (IntegerConst) expr.src1;
					list.add(GenString("li", getRegName(reg3), Integer.toString(ic1.value-ic2.value), null));
				} else {
					reg1 = loadValue(expr.src1, list, 4);
					list.add(GenString("sub", getRegName(reg3), getRegName(reg1), Integer.toString(ic2.value)));
				}
			} else {
				reg1 = loadValue(expr.src1, list, 4);
				reg2 = loadValue(expr.src2, list, 4);
				list.add(GenString("sub", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			}
			storeValue(expr.dest, reg3, list, 4);
			free(reg1);free(reg2);free(reg3);
			return;
		}
		if (expr.op == ArithmeticOp.MINUS) {
			//list.add(GenString("neg", getRegName(reg3), getRegName(reg1), null));
			if (expr.src1 instanceof IntegerConst) {
				IntegerConst ic = (IntegerConst) expr.src1;
				list.add(GenString("li", getRegName(reg3), Integer.toString(-ic.value), null));
			} else {
				reg1 = loadValue(expr.src1, list, 4);
				list.add(GenString("neg", getRegName(reg3), getRegName(reg1), null));
			}
			storeValue(expr.dest, reg3, list, 4);
			free(reg1);free(reg2);free(reg3);
			return;
		}
		if (expr.op == ArithmeticOp.DIV) {
			//list.add(GenString("sub", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			if (expr.src2 instanceof IntegerConst) {
				IntegerConst ic2 = (IntegerConst) expr.src2;
				if (expr.src1 instanceof IntegerConst) {
					IntegerConst ic1 = (IntegerConst) expr.src1;
					list.add(GenString("li", getRegName(reg3), Integer.toString(ic1.value/ic2.value), null));
				} else {
					reg1 = loadValue(expr.src1, list, 4);
					list.add(GenString("div", getRegName(reg3), getRegName(reg1), Integer.toString(ic2.value)));
				}
			} else {
				reg1 = loadValue(expr.src1, list, 4);
				reg2 = loadValue(expr.src2, list, 4);
				list.add(GenString("div", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
			}
			storeValue(expr.dest, reg3, list, 4);
			free(reg1);free(reg2);free(reg3);
			return;
		}
		
		reg1 = loadValue(expr.src1, list, 4);
		reg2 = loadValue(expr.src2, list, 4);
		
		/*if (expr.op == ArithmeticOp.ADD) {
			list.add(GenString("add", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.MUL) {
			list.add(GenString("mul", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.MINUS) {
			list.add(GenString("neg", getRegName(reg3), getRegName(reg1), null));
		}
		if (expr.op == ArithmeticOp.SUB) {
			list.add(GenString("sub", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}*/
		
		if (expr.op == ArithmeticOp.MOD) {
			list.add(GenString("div", getRegName(reg1), getRegName(reg2), null));
			list.add(GenString("mfhi", getRegName(reg3), null, null));
		}
		if (expr.op == ArithmeticOp.AND) {
			list.add(GenString("and", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		/*if (expr.op == ArithmeticOp.DIV) {
			list.add(GenString("div", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}*/
		/*if (expr.op == ArithmeticOp.NOT) {
			//list.add(GenString("not", "$a3", "$a1", null));
			Label l = new Label();
			list.add(GenString("li", getRegName(reg3), "0", null));
			list.add(GenString("bne", getRegName(reg1) , "$zero", "label"+l.num));
			list.add(GenString("li", getRegName(reg3), "1", null));
			list.add("label"+l.num+":");
		}*/
		if (expr.op == ArithmeticOp.OR) {
			list.add(GenString("or", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.SHL) {
			list.add(GenString("sll", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.SHR) {
			list.add(GenString("srl", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		if (expr.op == ArithmeticOp.TILDE) {
			//list.add(GenString("not", "$a3", "$a1", null));
			list.add(GenString("not", getRegName(reg3), getRegName(reg1), null));
		}
		if (expr.op == ArithmeticOp.XOR) {
			//list.add(GenString("xor", "$a3", "$a1", "$a2"));
			list.add(GenString("xor", getRegName(reg3), getRegName(reg1), getRegName(reg2)));
		}
		storeValue(expr.dest, reg3, list, 4);
		free(reg1);free(reg2);free(reg3);
	}
	
	public void MemoryReadCode(MemoryRead mr, List<String> list, int number) {
		comment(list, addressName(mr.dest)+"=*"+addressName(mr.src)+", "+Integer.toString(mr.size));
		
		int reg1 = loadValue(mr.src, list, 4);
		int reg2;
		if (mr.size == 4 && mr.type == 0) {
			String reg2s = AddressName(mr.dest);
			if (regAllocate.containsKey(reg2s)) reg2 = regAllocate.get(reg2s); 
			else reg2 = getReg(list);
			list.add(GenString("lw", getRegName(reg2), "0("+getRegName(reg1)+")", null));
			storeValue(mr.dest, reg2, list, mr.size);
		}
		else if (mr.size == 1 && mr.type == 0) {
			String reg2s = AddressName(mr.dest);
			if (regAllocate.containsKey(reg2s)) reg2 = regAllocate.get(reg2s); 
			else reg2 = getReg(list);
			list.add(GenString("lb", getRegName(reg2), "0("+getRegName(reg1)+")", null));
			storeValue(mr.dest, reg2, list, mr.size);
		}
		else {
			//int offset = VariableOffset(mr.dest);  struct possibly will fail
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			reg2 = loadValue(mr.dest, list, 4);
			int reg3 = getReg(list);
			int size = mr.size;
			int pos1 = 0, pos2 = 0;
			
			while (size > 0) {
				list.add(GenString("lw", getRegName(reg3), pos1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), pos2+"("+getRegName(reg2)+")", null));
				size -= 4;
				pos1 += 4;
				pos2 += 4;
			}
			
			free(reg3);
		}
		free(reg1);free(reg2);
	}
	
	public void MemoryWriteCode(MemoryWrite mw, List<String> list, int number) {
		comment(list, "*"+addressName(mw.dest)+"="+addressName(mw.src)+", "+Integer.toString(mw.size));

		int reg1 = loadValue(mw.src, list, mw.size);
		int reg2;
		if (mw.size <= 4 && mw.type == 0) {
			reg2 = loadValue(mw.dest, list, 4);
			if (mw.size == 4) {
				list.add(GenString("sw", getRegName(reg1), "0("+getRegName(reg2)+")", null));
				//list.add(GenString("sw", "$a3", "0($a2)", null));
			}
			else {
				list.add(GenString("sb", getRegName(reg1), "0("+getRegName(reg2)+")", null));
				//list.add(GenString("sb", "$a3", "0($a2)", null));
			}
		} else {
			//int offset = VariableOffset(mw.dest);
			//list.add(GenString("add", "$a2", "$sp", Integer.toString(offset)));
			reg2 = loadValue(mw.dest, list, 4);
			int reg3 = getReg(list);
			
			int size = mw.size;
			int pos1 = 0, pos2 = 0;
			
			while (size > 0) {
				list.add(GenString("lw", getRegName(reg3), pos1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), pos2+"("+getRegName(reg2)+")", null));
				size -= 4;
				pos1 += 4;
				pos2 += 4;
			}
			
			free(reg3);
		}
		free(reg1);free(reg2);
	}
	
	public int getAddress(Address adrs, List<String> list) {
		//System.out.println("getAddress!!!");
		
		int reg = getReg(list);
		
		if (adrs instanceof Name) {
			String s = ((Name)adrs).name;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				return reg;
			}
		} else if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				return reg;
			}
		}
		int offset = VariableOffset(adrs, list);
		Variable v = varDict.get(addressName(adrs));
		if (v instanceof BasicVariable) {
			list.add(GenString("add", getRegName(reg), "$sp", Integer.toString(offset)));
		} else {
			list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
		}
		return reg;
	}
	
	public void AddressOfCode(AddressOf ao, List<String> list, int number) {
		comment(list, addressName(ao.dest)+"=&"+addressName(ao.src));

		//int reg1 = getAddress(ao.src, list);
		
		int reg;
		String dests = AddressName(ao.dest);
		if (regAllocate.containsKey(dests)) reg = regAllocate.get(dests);
		else reg = getReg(list);
		
		Address adrs = ao.src;
		boolean valid = false;
		if (adrs instanceof Name) {
			String s = ((Name)adrs).name;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				valid = true;
			}
		} else if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			if (globalSet.contains(s)) {
				list.add(GenString("la", getRegName(reg), s, null));
				valid = true;
			}
		}
		
		if (!valid) {
			int offset = VariableOffset(adrs, list);
			Variable v = varDict.get(addressName(adrs));
			if (v instanceof BasicVariable) {
				list.add(GenString("add", getRegName(reg), "$sp", Integer.toString(offset)));
			} else {
				list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
			}
		}
		
		storeValue(ao.dest, reg, list, 4);
		free(reg);
	}
	
	public void LabelCode(Label l, List<String> list, int number) {
		list.add("label"+l.num+":");
	}
	
	public void GotoCode(Goto g, List<String> list, int number) {
		list.add(GenString("b", "label"+g.label.num, null, null));
	}
	
	public String transRelOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			return "==";
		}
		if (op == RelationalOp.GE) {
			return ">=";
		}
		if (op == RelationalOp.GT) {
			return ">";
		}
		if (op == RelationalOp.LE) {
			return "<=";
		}
		if (op == RelationalOp.LT) {
			return "<";
		}
		if (op == RelationalOp.NE) {
			return "!=";
		}
		return "null";
	}
	
	public void IfFalseGotoCode(IfFalseGoto iff, List<String> list, int number) {
		comment(list, "if false "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		int reg1 = loadValue(iff.src1, list, 4);
		int reg2 = loadValue(iff.src2, list, 4);
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("bne", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("blt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("ble", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("bgt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("bge", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("beq", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		free(reg1);free(reg2);
	}
	
	public void IfTrueGotoCode(IfTrueGoto iff, List<String> list, int number) {
		comment(list, "if false "+addressName(iff.src1)+transRelOp(iff.op)+addressName(iff.src2)+" goto label"+iff.label.num);
		int reg1 = loadValue(iff.src1, list, 4);
		int reg2 = loadValue(iff.src2, list, 4);
		
		if (iff.op == RelationalOp.EQ) {
			list.add(GenString("beq", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GE) {
			list.add(GenString("bge", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.GT) {
			list.add(GenString("bgt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LE) {
			list.add(GenString("ble", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.LT) {
			list.add(GenString("blt", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		if (iff.op == RelationalOp.NE) {
			list.add(GenString("bne", getRegName(reg1), getRegName(reg2), "label"+iff.label.num));
		}
		free(reg1);free(reg2);
	}
	
	public int getParamSize(Param p) {
		if (p instanceof BasicParam) {
			return 4;
		}
		if (p instanceof ArrayParam) {
			return ((ArrayParam)p).size;
		}
		if (p instanceof MemoryParam) {
			//System.out.println(((MemoryParam)p).size);
			return ((MemoryParam)p).size;
		}
		return 0;
	}
	
	public void commentOfCall(List<String> list, Call call) {
		Address adrs = null;
		if (call.returnValue instanceof BasicParam)
			adrs = ((BasicParam)call.returnValue).src;
		if (call.returnValue instanceof ArrayParam)
			adrs = ((ArrayParam)call.returnValue).name;
		if (call.returnValue instanceof MemoryParam)
			adrs = ((MemoryParam)call.returnValue).src;
		comment(list, addressName(adrs)+" = call "+call.callee+", "+call.numOfParams);
	}
	
	public void changeOffset(Address adrs, int offset) {
		if (adrs instanceof Temp) {
			String s = "#t"+((Temp)adrs).num;
			dict.put(s, new Integer(offset));
		} else {
			String s = ((Name)adrs).name;
			dict.put(s, new Integer(offset));
		}
	}
	
	/*public boolean checkSpec(String s1, String s2) {
		if (s1.equals("_tak")) {
			if (s2.equals("#t3")) return true;
			if (s2.equals("#t6")) return true;
		}
	}*/
	
	public void CallCode(Call call, List<String> list, int number) {
		commentOfCall(list, call);
		int num = call.numOfParams;
		int pos = getParamSize(call.returnValue);
		int printf1 = 0, printf2 = 0;
		int malloc1 = 0;
		
		list.add("#call "+call.callee);
		while (num > 0) {
			Param p = paramStack.pop();
			if (p instanceof BasicParam) {
				BasicParam bp = (BasicParam) p;
				int reg1 = loadValue(bp.src, list, 4);
				list.add(GenString("sw", getRegName(reg1), pos+"($sp)", null));
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				pos += 4;
			}
			if (p instanceof ArrayParam) {
				ArrayParam ap = (ArrayParam) p;
				int reg1 = getReg(list);
				int offset = VariableOffset(ap.name, list);
				list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
				int reg2 = loadValue(ap.offset, list, 4);
				list.add(GenString("add", getRegName(reg2), getRegName(reg1), getRegName(reg2)));
				int size = ap.size;
				int posa2 = 0;
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				
				int reg3 = getReg(list);
				
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", getRegName(reg3), posa2+"("+getRegName(reg2)+")", null));
					list.add(GenString("sw", getRegName(reg3), pos+"($sp)", null));
					posa2 += 4;
					pos += 4;
				}
				
				free(reg1);free(reg2);free(reg3);
			}
			if (p instanceof MemoryParam) {
				MemoryParam mp = (MemoryParam) p;
				//storeValue(mp.src, "$sp", list, 4);
				int reg1 = loadValue(mp.src, list, 4);

				int size = mp.size;
				int posa1 = 0;
				
				int reg2 = getReg(list);
				list.add(GenString("add", getRegName(reg2), "$sp", Integer.toString(pos+4)));
				list.add(GenString("sw", getRegName(reg2), pos+"($sp)", null));
				pos += 4;
				
				if (num == 1) {
					printf1 = pos;
					malloc1 = pos;
				}
				if (num == 2) printf2 = pos;
				while (size > 0) {
					size -= 4;
					list.add(GenString("lw", getRegName(reg2), posa1+"("+getRegName(reg1)+")", null));
					list.add(GenString("sw", getRegName(reg2), pos+"($sp)", null));
					posa1 += 4;
					pos += 4;
				}
				
				free(reg1);free(reg2);
			}
			num--;
		}
		if (call.callee.equals("printf")) {
			list.add(GenString("lw", "$a2", printf1+"($sp)", null));
			list.add(GenString("add", "$a1", "$sp", Integer.toString(printf2)));
		}
		if (call.callee.equals("malloc")) {
			list.add(GenString("lw", "$a0", malloc1+"($sp)", null));
		}
		
		boolean specfunc = false;
		
		if (call.callee.equals("printf")||call.callee.equals("malloc")||call.callee.equals("getchar")) {
			specfunc = true;
		}
		
		//stroe back temporarily
		
		int temppos = pos;
		tempSet.clear();
		Triple<String, String, String> tri = getTriple(call);
		String kills = "";
		if (!specfunc) {
			if (tri.first != null) kills = tri.first;
			for (String s: liveout[number].set) {
				if (s.equals(kills)) continue;
				if (!regAllocate.containsKey(s)) continue;
				if (!dict.containsKey(s)) continue;
				//if (checkSpec(call.callee, s)) continue;
				int reg = regAllocate.get(s);
				int offset = dict.get(s);
				list.add(GenString("sw", getRegName(reg), offset+"($sp)", null));
				tempSet.add(s);
			}
		}
		
		String s = call.callee;
		if (s.equals("move")) s = "move1";
		
		list.add(GenString("jal", s, null, null));
		//list.add("nop");
		
		if (!specfunc) {
			for (String ss: liveout[number].set) {
				if (ss.equals(kills)) continue;
				if (!regAllocate.containsKey(ss)) continue;
				if (!dict.containsKey(ss)) continue;
				int reg = regAllocate.get(ss);
				int offset = dict.get(ss);
				list.add(GenString("lw", getRegName(reg), offset+"($sp)", null));
			}
		}
		
		if (call.returnValue instanceof BasicParam) {
			BasicParam bp = (BasicParam) call.returnValue;
			String rs = AddressName(bp.src);
			int reg1;
			if (regAllocate.containsKey(rs)) reg1 = regAllocate.get(rs);
			else reg1 = getReg(list);
			list.add(GenString("lw", getRegName(reg1), "0($sp)", null));
			storeValue(bp.src, reg1, list, 4);
			free(reg1);
		}
		if (call.returnValue instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) call.returnValue;
			int offset = VariableOffset(ap.name, list);
			int reg1 = getReg(list);
			list.add(GenString("add", getRegName(reg1), "$sp", Integer.toString(offset)));
			int reg2 = loadValue(ap.offset, list, 4);
			list.add(GenString("add", getRegName(reg2), getRegName(reg2), getRegName(reg1)));
			int size = ap.size;
			int posa2 = 0, possp = 0;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), possp+"($sp)", null));
				list.add(GenString("sw", getRegName(reg3), posa2+"("+getRegName(reg2)+")", null));
				posa2 += 4;
				possp += 4;
			}
			
			free(reg1);free(reg2);free(reg3);
		}
		if (call.returnValue instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam)call.returnValue;
			//storeValue(mp.src, "$sp", list, 4);
			//list.add("#lalala "+Integer.toString(curOffset));
			int offset = VariableOffset(mp.src, list);
			
			//int reg1 = getReg(list);
			int reg1;
			
			String regs = AddressName(mp.src);
			if (regAllocate.containsKey(regs)) {
				reg1 = regAllocate.get(regs);
			} else {
				reg1 = getReg(list);
			}
			
			list.add(GenString("lw", getRegName(reg1), offset+"($sp)",null));
			//int reg1 = loadValue(mp.src, list, 4);
			
			int size = mp.size;
			int possp, posa2;
			possp = posa2 = 0;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), possp+"($sp)", null));
				list.add(GenString("sw", getRegName(reg3), posa2+"("+getRegName(reg1)+")", null));
				posa2 += 4;
				possp += 4;
			}
		
			free(reg1);free(reg3);
		}
		
		
	}

	public String ParamString(Param q) {
		String s = "";
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			s = s+"Param ";
			s = s+addressName(bp.src);
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			s = s+"Param ";
			s = s+addressName(ap.name);
			s = s+ "[";
			s = s+addressName(ap.offset);
			s = s+"], "+ap.size;
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			s = s+"Param *";
			s = s+addressName(mp.src);
			s = s+" "+mp.size;
		}
		return s;
	}
	
	public void ReturnCode(Return r, List<String> list, int number) {
		comment(list, "return "+ParamString(r.value));
		if (r.value instanceof BasicParam) {
			BasicParam pb = (BasicParam) r.value;
			int reg1 = loadValue(pb.src, list, 4);
			list.add(GenString("sw", getRegName(reg1), offset+"($sp)", null));
			free(reg1);
		}
		if (r.value instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) r.value;
			int reg1 = loadValue(mp.src, list, 4);
			int size = mp.size;
			int posa1 = 0;
			int posvalue = offset;
			
			int reg3 = getReg(list);
			
			while (size > 0) {
				size -= 4;
				list.add(GenString("lw", getRegName(reg3), posa1+"("+getRegName(reg1)+")", null));
				list.add(GenString("sw", getRegName(reg3), posvalue+"($sp)", null));
				posa1 += 4;
				posvalue += 4;
			}
			
			free(reg1);free(reg3);
		}
		
		functionEndCode(list);
	}
	
	public void PrintCode(List<String> list) {
		for (String s: list) {
			System.out.println(s);
		}
	}
	
	public void writeQuadruple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			writeAddress(assign.dest);
			System.out.print(" = ");
			writeAddress(assign.src);
			System.out.println();
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			writeAddress(ae.dest);
			System.out.print(" = ");
			writeAddress(ae.src1);
			writeArithmeticOp(ae.op);
			writeAddress(ae.src2);
			System.out.println();
		}
		if (q instanceof ArrayRead) {
			ArrayRead ar = (ArrayRead) q;
			writeAddress(ar.dest);
			System.out.print(" = ");
			writeAddress(ar.src);
			System.out.print("[");
			writeAddress(ar.offset);
			System.out.print("], "+ar.size);
			System.out.println();
		}
		if (q instanceof ArrayWrite) {
			ArrayWrite aw = (ArrayWrite) q;
			writeAddress(aw.dest);
			System.out.print("[");
			writeAddress(aw.offset);
			System.out.print("] = ");
			writeAddress(aw.src);
			System.out.println(", "+aw.size);
		}
		if (q instanceof MemoryRead) {
			MemoryRead pr = (MemoryRead) q;
			writeAddress(pr.dest);
			System.out.print(" = *");
			writeAddress(pr.src);
			System.out.println(", "+pr.size);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			System.out.print("*");
			writeAddress(mw.dest);
			System.out.print(" = ");
			writeAddress(mw.src);
			System.out.println(", "+mw.size);
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			writeAddress(ao.dest);
			System.out.print(" = &");
			writeAddress(ao.src);
			System.out.println();
		}
		if (q instanceof Label) {
			Label l = (Label) q;
			System.out.println("Label"+l.num);
		}
		if (q instanceof Goto) {
			Goto g = (Goto) q;
			System.out.println("Goto: Label"+g.label.num);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			System.out.print("If False ");
			writeAddress(iff.src1);
			writeRelationalOp(iff.op);
			writeAddress(iff.src2);
			System.out.println(" Goto: Label"+iff.label.num);
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			System.out.print("If True ");
			writeAddress(ift.src1);
			writeRelationalOp(ift.op);
			writeAddress(ift.src2);
			System.out.println(" Goto: Label"+ift.label.num);
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			System.out.print("Param ");
			writeAddress(bp.src);
			System.out.println();
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			System.out.print("Param ");
			writeAddress(ap.name);
			System.out.print("[");
			writeAddress(ap.offset);
			System.out.println("], "+ap.size);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			System.out.print("Param *");
			writeAddress(mp.src);
			System.out.println(" "+mp.size);
		}
		if (q instanceof Call) {
			Call call = (Call) q;
			Address adrs = null;
			if (call.returnValue instanceof BasicParam)
				adrs = ((BasicParam)call.returnValue).src;
			if (call.returnValue instanceof ArrayParam)
				adrs = ((ArrayParam)call.returnValue).name;
			if (call.returnValue instanceof MemoryParam)
				adrs = ((MemoryParam)call.returnValue).src;
			if (adrs != null) {
				writeAddress(adrs);
				System.out.print(" = ");
			}
			System.out.println("Call "+call.callee+" "+call.numOfParams);
		}
		if (q instanceof Return) {
			System.out.print("Return ");
			writeQuadruple(((Return)q).value);
		}
	}
	
	public void writeAddress(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			System.out.print(sac.value);
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			System.out.print(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			System.out.print(n.name);
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			System.out.print("#t"+tmp.num);
		}
	}
	
	public void writeRelationalOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			System.out.print("==");
		}
		if (op == RelationalOp.GE) {
			System.out.print(">=");
		}
		if (op == RelationalOp.GT) {
			System.out.print(">");
		}
		if (op == RelationalOp.LE) {
			System.out.print("<=");
		}
		if (op == RelationalOp.LT) {
			System.out.print("<");
		}
		if (op == RelationalOp.NE) {
			System.out.print("!=");
		}
	}
	
	public void writeArithmeticOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			System.out.print("+");
		}
		if (op == ArithmeticOp.AND) {
			System.out.print("&");
		}
		if (op == ArithmeticOp.DIV) {
			System.out.print("/");
		}
		if (op == ArithmeticOp.MINUS) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.MOD) {
			System.out.print("%");
		}
		if (op == ArithmeticOp.MUL) {
			System.out.print("*");
		}
		if (op == ArithmeticOp.NOT) {
			System.out.print("!");
		}
		if (op == ArithmeticOp.OR) {
			System.out.print("|");
		}
		if (op == ArithmeticOp.SHL) {
			System.out.print("<<");
		}
		if (op == ArithmeticOp.SHR) {
			System.out.print(">>");
		}
		if (op == ArithmeticOp.SUB) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.TILDE) {
			System.out.print("~");
		}
		if (op == ArithmeticOp.XOR) {
			System.out.print("^");
		}
	}
}
