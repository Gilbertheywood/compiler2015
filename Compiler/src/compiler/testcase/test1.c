#include "stdio.h"
#include "string.h"

struct A {
    union B {
        char ch;
        int i;
    } first;
    int second;
} a, b, c;

int main() {
	//char s[] = "abc";
	struct C {
		struct D {
			char ch;
			int i;
		} dada;
		char a, b, c;
	};
	struct cc;
    a.first.ch = 2;
    a.second = 3;
    b = a;
    c.second = b.first.i + a.second;
}
