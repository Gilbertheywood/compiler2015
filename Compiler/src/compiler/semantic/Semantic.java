package compiler.semantic;

import compiler.ast.*;
import compiler.syntactic.*;
import compiler.ir.*;

import java.io.*;
import java.util.*;

class Error {
	public String s;
	
	public Error(String s) {
		this.s = s;
	}
}

class ExprAtt {
	public Type type;
	public int lvalue;
	public int isconst;
	public int definite;
	public int constvalue;
	
	public IR ir;
	
	public ExprAtt(Type t, int l, int i, int c) {
		type = t;
		lvalue = l;
		isconst = i;
		constvalue = c;
	}
	
	public ExprAtt() {
		type = null;
		lvalue = 0;
		isconst = 0;
	}
}

public class Semantic {
	public Environment env;
	public List<Error> errors = new ArrayList<Error>();
	public boolean initCheck;
	public int isLoop;
	
	public Semantic() {
		env = new Environment();
		initCheck = true;
		isLoop = 0;
	}
	
	public void error(String s) {
		//errors.add(new Error(s));
		System.err.println(s);
		System.exit(1);
	}
	
	public boolean hasError() {
		return errors.size() > 0;
	}
	
	// start semantic check
	
	public void checkAST(AST ast) {
		for (Decl decl: ast.decls) {
			checkDecl(decl);
		}
	}
	
	public void checkDecl(Decl decl) {
		if (decl instanceof FunctionDecl) {
			checkFunctionDecl((FunctionDecl)decl);
		}
		if (decl instanceof StructDecl) {
			checkStructDecl((StructDecl)decl);
		}
		if (decl instanceof UnionDecl) {
			checkUnionDecl((UnionDecl)decl);
		}
		if (decl instanceof VarDecl) {
			checkVarDecl((VarDecl)decl);
		}
	}
	
	public void checkFunctionDecl(FunctionDecl decl) {
		if (!env.checkFunctionEnv(decl.name.toString())) {
			error("The function id "+decl.name.toString()+" has been declarated.");
		}
		
		if (!(decl.returnType instanceof VoidType)) checkType(decl.returnType);
		
		env.putFuncIdenEnv(decl, decl.name.toString());
		
		//parameters
		env.beginScope();
		
		for (VarDecl d: decl.params) {
			/*if (!env.checkIdentifierEnv(d.name.toString())) {
				System.err.println("Identifier definition error");
				System.exit(1);
			}*/
			checkVarDecl(d);
			env.putFuncIdenEnv(d, d.name.toString());
		}
		
		checkCompoundStmt(decl.body, decl.returnType);
		
		env.endScope();
	}
	
	public void checkStructDecl(StructDecl decl) {
		if (!env.checkRecordEnv(decl.tag.toString())) {
			error("Struct type definition error.");
			System.exit(1);
		}
		decl.level = env.Recordlevel;
		env.putRecordEnv(decl, decl.tag.toString());
		env.beginStructUnionScope();
		for (Decl d: decl.fields) {
			if (d instanceof StructDecl) checkStructDecl((StructDecl)d);
			if (d instanceof UnionDecl) checkUnionDecl((UnionDecl)d);
			if (d instanceof VarDecl) checkVarDecl((VarDecl)d);
		}
		env.endStructUnionScope();
	}
	
	public void checkUnionDecl(UnionDecl decl) {
		if (!env.checkRecordEnv(decl.tag.toString())) {
			error("Union type definition error.");
			System.exit(1);
		}
		decl.level = env.Recordlevel;
		env.putRecordEnv(decl, decl.tag.toString());
		for (Decl d: decl.fields) {
			if (d instanceof StructDecl) checkStructDecl((StructDecl)d);
			if (d instanceof UnionDecl) checkUnionDecl((UnionDecl)d);
		}
	}
	
	public int max(int a, int b) {
		if (a > b) return a;
		else return b;
	}
	
	public int getSize(Type type) {
		if (type instanceof IntType) return 4;
		if (type instanceof CharType) return 1;
		if (type instanceof PointerType) return 4;
		if (type instanceof ArrayType) {
			ArrayType at = (ArrayType) type;
			return at.size*at.baseSize;
		}
		if (type instanceof StructType) {
			Decl d = env.getRecord(((StructType) type).tag.toString(), ((StructType) type).level);
			int size = 0;
			for (Decl decl: ((StructDecl) d).fields)
			if (decl instanceof VarDecl) {
				Type t = ((VarDecl) decl).type;
				if (t instanceof IntType)
					if (size % 4 != 0) {
						size = (size/4+1)*4;
					}
				//printType(type);
				//System.out.println(size);
				size += getSize(t);
			}
			if (size % 4 != 0) size = (size / 4 + 1) * 4;
			return size;
		}
		if (type instanceof UnionType) {
			Decl d = env.getRecord(((UnionType) type).tag.toString());
			int size = 0;
			for (Decl decl: ((UnionDecl) d).fields) {
				if (decl instanceof VarDecl) {
					Type t = ((VarDecl) decl).type;
					size = max(getSize(t), size);
				}
			}
			if (size % 4 != 0) size = (size / 4 + 1) * 4;
			return size;
		}
		return 0;
	}
	
	public ArrayType inverseArrayType(ArrayType type) {
		List<Expr> list = new LinkedList<Expr>();
		Type t = type;
		while (t instanceof ArrayType) {
			list.add(((ArrayType) t).arraySize);
			t = ((ArrayType) t).baseType;
		}
		int size = getSize(t);
		int sum = 1;
		for (int i = 0; i<list.size(); i++) {
			Expr ex = list.get(i);
			if (ex instanceof IntConst) {
				IntConst ic = (IntConst) ex;
				t = new ArrayType(t, ic, sum * ic.value, ic.value, size);
				sum = sum * ic.value;
			} else {
				ExprAtt exprAtt = checkExpr(ex);
				int v = exprAtt.constvalue;
				//System.out.printf("isconst: %d constvalue: %d\n", exprAtt.isconst, exprAtt.constvalue);
				IntConst ic = new IntConst(v);
				t = new ArrayType(t, ic, sum * v, v, size);
				sum = sum * v;
			}
		}
		return (ArrayType) t;
	}
	
	public void checkVarDecl(VarDecl decl) {
		ArrayList<Integer> list = null;
		Type type = ((VarDecl) decl).type;
		if (type instanceof ArrayType)
			decl.type = inverseArrayType((ArrayType) type);
		if (!env.checkIdentifierEnv(decl.name.toString())) {
			error("Identifier definition error");
			//System.exit(1);
		}
		env.putFuncIdenEnv(decl, ((VarDecl) decl).name.toString());
		initCheck = true;
		list = checkType(decl.type);
		if (decl.type instanceof StructType) {
			Decl d = env.getRecord(((StructType) decl.type).tag.toString());
			((StructType) decl.type).level = ((StructDecl) d).level;
		}
		if (decl.type instanceof UnionType) {
			Decl d = env.getRecord(((UnionType) decl.type).tag.toString());
			((UnionType) decl.type).level = ((UnionDecl) d).level;
		}
		
		//type = getType(decl.type);
		if (decl.init != null && initCheck)
			checkInitializer(decl.init, decl.type, list, list.size()-1);
	}
	
	public Type getType(Type type) {
		//if (type instanceof PointerType)
		//	return getType(((PointerType) type).baseType);
		if (type instanceof ArrayType)
			return getType(((ArrayType) type).baseType);
		return type;
	}
	
	public ArrayList<Integer> checkType(Type type) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(new Integer(1));
		if (type instanceof VoidType)
			error("VoidType can just be the return-type of functions");
		if (!(type instanceof PointerType)) {
			if (type instanceof StructType) {
				StructType t = (StructType)type;
				if (env.getRecord(t.tag.toString()) == null)
					error("No such StructType");
				if (env.getRecord(t.tag.toString()) instanceof UnionDecl) 
					error(t.tag.toString()+" is struct rather than union");
			}
			if (type instanceof UnionType) {
				UnionType t = (UnionType)type;
				if (env.getRecord(t.tag.toString()) == null)
					error("No such UnionType");
				if (env.getRecord(t.tag.toString()) instanceof StructDecl)
					error(t.tag.toString()+" is union rather than struct");
			}
		}
		if (type instanceof ArrayType) {
			list = checkType(((ArrayType) type).baseType);
			ArrayType t= (ArrayType)type;
			ExprAtt exprAtt= checkExpr(t.arraySize);
			
			/*if (env.FuncIdenlevel == 0) {
				if (exprAtt.isconst != 1)  error("global arraytype definition error");
				//if (!(t.arraySize instanceof IntConst) && !(t.arraySize instanceof CharConst))
				//	error("global arraytype definition error");
			}*/
			if (exprAtt.isconst != 1) error("arraytype subscript error");
			if (exprAtt.constvalue < 0) error("arraytype subscript negative");
			
			if (!(exprAtt.type instanceof IntType) && !(exprAtt.type instanceof CharType)) {
				error("array definition's subscript type error");
			}
			
			if (t.arraySize instanceof IntConst) {
				list.add(new Integer(((IntConst) t.arraySize).value));
			} else if (t.arraySize instanceof CharConst) {
				char[] valuearr = ((CharConst) t.arraySize).value.toCharArray();
				int v = (int) valuearr[0];
				list.add(new Integer(v));
			} else {
				initCheck = false;
			}
		}
		return list;
	}
	
	public void checkInitializer(Initializer init, Type type, ArrayList<Integer> list, int index) {
		if (init instanceof InitValue) {
			/*int size = list.size();
			for (int i = 0; i < size; i++) System.out.print(list.get(i)+" ");
			System.out.println();
			if (index+1 == 1 && list.get(0).intValue() == 1) {
				checkInitValue((InitValue)init, type);
			} else {
				error("Initializer error");
			}*/
			checkInitValue((InitValue)init, type);
		}
		if (init instanceof InitList) {
			checkInitList((InitList)init, type, list, index);
		}
	}
	
	public boolean checkInitAssign(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof ArrayType) {
			//return checkInitAssign(((ArrayType) t1).baseType, t2);
			Type type = getType(t1);
			if (type instanceof IntType) return checkInitAssign(type, t2);
			if (type instanceof CharType) {
				if (t2 instanceof IntType) return true;
				if (t2 instanceof CharType) return true;
				if (t2 instanceof PointerType) return true;
			}
			if (type instanceof PointerType)
				return checkInitAssign(type, t2);
			if (type instanceof StructType)
				return checkInitAssign(type, t2);
			if (type instanceof UnionType)
				return checkInitAssign(type, t2);
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public void checkInitValue(InitValue init, Type type) {
		ExprAtt exprAtt = checkExpr(init.expr);
		if (env.FuncIdenlevel == 0) {
			if (exprAtt.isconst != 1) error("global initializer error");
		}
		
		Type initType = exprAtt.type;
		if (type instanceof StructType) {
			if (!(initType instanceof StructType)) error("struct initializer error");
			if (initType instanceof StructType) {
				if (!checkSameType(initType, type)) {
					error("not the same struct");
				}
			}
		}
		if (type instanceof UnionType) {
			if (!(initType instanceof UnionType)) error("union initializer error");
			if (initType instanceof UnionType) {
				//if (!(((UnionType) initType).tag.equal(((UnionType) type).tag))) 
				if (!checkSameType(initType, type)) {
					error("not the same union");
				}
			}
		}
		//if (initType == null || (!(initType instanceof IntType) && !(initType instanceof CharType))) {
		//if (initType == null) System.out.println("initType is null");
		//if (type instanceof IntType) System.out.println("type is int");
		//if (initType instanceof IntType) System.out.println("initType is int");
		//printType(type);
		//printType(initType);
		if (initType == null || !checkInitAssign(type, initType)) {
			//System.out.println(checkAssign(type, initType));
			error("initializer value error");
		}
	}
	
	public void checkInitList(InitList init, Type type, ArrayList<Integer> list, int index) {
		//if (init.inits.size() != list.get(index)) error("initializer size error");
		int cnt = -1;
		for (Initializer i: init.inits) {
			cnt++;
			if (cnt > index) return;
			checkInitializer(i, type, list, index-1);
		}
	}
	
	public void checkCompoundStmt(CompoundStmt stmt, Type returnType) {
		for (Decl d: stmt.decls) {
			checkDecl(d);
		}
		for (Stmt s: stmt.stats) {
			if (s instanceof CompoundStmt) {
				env.beginScope();
				checkCompoundStmt((CompoundStmt)s, returnType);
				env.endScope();
			} else {
				checkStmt(s, returnType);
			}
		}
	}
	
	public boolean isIntCharType(Type type) {
		if (type instanceof IntType || type instanceof CharType) return true;
		return false;
	}
	
	public void checkStmt(Stmt s, Type returnType) {
		if (s == null) return;
		if (s instanceof BreakStmt) {
			if (isLoop == 0) error("BreakStmt's position is not right");
		}
		if (s instanceof ContinueStmt) {
			if (isLoop == 0) error("ContinueStmt's position is not right");
		}
		if (s instanceof IfStmt) {
			IfStmt ts = (IfStmt) s;
			ExprAtt exprAtt = checkExpr(ts.condition);
			if (exprAtt.type instanceof StructType || exprAtt.type instanceof UnionType) error("IfStmt's condition error");
			env.beginScope();
			checkStmt(ts.consequent, returnType);
			env.endScope();
			
			env.beginScope();
			checkStmt(ts.alternative, returnType);
			env.endScope();
		}
		if (s instanceof ReturnStmt) {
			ReturnStmt ts = (ReturnStmt) s;
			ExprAtt exprAtt = checkExpr(ts.expr);
			if (exprAtt.type instanceof VoidType) return;
			if (!checkAssign(returnType, exprAtt.type))
				error("return type error");
		}
		if (s instanceof CompoundStmt) {
			checkCompoundStmt((CompoundStmt) s, returnType);
		}
		if (s instanceof Expr) {
			checkExpr((Expr)s);
		}
		if (s instanceof ForLoop) {
			ForLoop ts = (ForLoop) s;
			env.beginScope();
			isLoop++;
			ExprAtt exprAtt = null;
			//System.out.println("yes");
			checkExpr(ts.init);
			checkExpr(ts.condition);
			checkExpr(ts.step);
			checkStmt(ts.body, returnType);
			isLoop--;
			env.endScope();
		}
		if (s instanceof WhileLoop) {
			WhileLoop ts = (WhileLoop) s;
			env.beginScope();
			isLoop++;
			ExprAtt exprAtt = checkExpr(ts.condition);
			if (exprAtt.type instanceof StructType || exprAtt.type instanceof UnionType) 
				error("WhileLoop's condition error");
			checkStmt(ts.body, returnType);
			isLoop--;
			env.endScope();
		}
	}

	public ExprAtt checkExpr(Expr expr) {
		if (expr instanceof EmptyExpr) return new ExprAtt(new VoidType(), 0, 0, 0);
		if (expr instanceof BinaryExpr) {
			/*ExprAtt e = checkBinaryExpr((BinaryExpr)expr);
			if (e.isconst == 1) {
				System.out.println("hello: "+e.constvalue);
				expr = new IntConst(e.constvalue);
				e = new ExprAtt(new IntType(), 0, 1, e.constvalue);
			}
			return e;*/
			return checkBinaryExpr((BinaryExpr) expr);
		}
		if (expr instanceof UnaryExpr) {
			/*ExprAtt e = checkUnaryExpr((UnaryExpr)expr);
			if (e.isconst == 1) {
				expr = new IntConst(e.constvalue);
				e = new ExprAtt(new IntType(), 0, 1, e.constvalue);
			}
			return e;*/
			return checkUnaryExpr((UnaryExpr) expr);
		}
		if (expr instanceof SizeofExpr) {
			//return checkSizeofExpr((SizeofExpr)expr);
			int value = 0;
			if (((SizeofExpr) expr).type instanceof IntType) value = 4;
			if (((SizeofExpr) expr).type instanceof CharType) value = 1;
			return new ExprAtt(new IntType(), 0, 1, value);
		}
		if (expr instanceof CastExpr) return checkCastExpr((CastExpr)expr);
		if (expr instanceof PointerAccess)
			return checkPointerAccess((PointerAccess)expr);
		if (expr instanceof RecordAccess)
			return checkRecordAccess((RecordAccess)expr);
		if (expr instanceof SelfIncrement) {
			ExprAtt exprAtt = checkExpr(((SelfIncrement) expr).body);
			if (exprAtt.lvalue == 0) error("Expr++ is not lvalue");
			if (!(exprAtt.type instanceof IntType) && !(exprAtt.type instanceof CharType) && !(exprAtt.type instanceof PointerType)) {
				error("Expr++ is not valid type");
			}
			return new ExprAtt(exprAtt.type, 0, 0, 0);
		}
		if (expr instanceof SelfDecrement) {
			ExprAtt exprAtt = checkExpr(((SelfDecrement) expr).body);
			if (exprAtt.lvalue == 0) error("Expr++ is not lvalue");
			if (!(exprAtt.type instanceof IntType) && !(exprAtt.type instanceof CharType) && !(exprAtt.type instanceof PointerType))
				error("Expr++ is not valid type");
			return new ExprAtt(exprAtt.type, 0, 0, 0);
		}
		if (expr instanceof ArrayAccess) {
			return checkArrayAccess((ArrayAccess)expr);
		}
		if (expr instanceof FunctionCall) {
			return checkFunctionCall((FunctionCall)expr);
		}
		if (expr instanceof Identifier) {
			return checkIdentifier((Identifier)expr);
		}
		if (expr instanceof IntConst) {
			return new ExprAtt(new IntType(), 0, 1, ((IntConst)expr).value);
		}
		if (expr instanceof CharConst) {
			return new ExprAtt(new CharType(), 0, 1, ((CharConst)expr).value.charAt(0));
		}
		if (expr instanceof StringConst) {
			return new ExprAtt(new PointerType(new CharType()), 0, 1, 0);
		}
		return null;
	}
	
	public boolean isArithOp(BinaryOp op) {
		if (op == BinaryOp.ADD) return true;
		if (op == BinaryOp.SUB) return true;
		return false;
	}
	
	public ExprAtt checkBinaryExpr(BinaryExpr expr) {
		ExprAtt e1 = checkExpr(expr.left);
		ExprAtt e2 = checkExpr(expr.right);
		if (e2.isconst == 1 && isArithOp(expr.op)) {
			expr.right = new IntConst(e2.constvalue);
			e2 = new ExprAtt(new IntType(), 0, 1, e2.constvalue);
		}
		ExprAtt e = checkBinaryOp(e1, e2, expr.op);
		return e;
	}
	
	public boolean checkAssign(Type t1, Type t2){
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignOp(Type t1, Type t2) { // except assign_sub assign_add
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return true;
			if (t2 instanceof IntType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof PointerType) return false;
			if (t2 instanceof ArrayType) return false;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignAdd(Type t1, Type t2) {//assign_add
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType){
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return false;
			if (t2 instanceof ArrayType) return false;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignSub(Type t1, Type t2) {//assign_sub
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType){
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType || t2 instanceof ArrayType)
				if (checkSameType(t1, t2)) return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkLogicalOp(Type t1, Type t2) {
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof IntType || t1 instanceof CharType || t1 instanceof PointerType || t1 instanceof ArrayType)
			if (t2 instanceof IntType || t2 instanceof CharType || t2 instanceof PointerType || t2 instanceof ArrayType)
				return true;
		return false;
	}
	
	public boolean checkSameType(Type t1, Type t2) { // PointerType ArrayType
		if (t1.getClass() != t2.getClass()) return false;
		if (t1 instanceof PointerType) {
			boolean b = checkSameType(((PointerType) t1).baseType, ((PointerType) t2).baseType);
			if (!b) return false;
		}
		if (t1 instanceof ArrayType) {
			boolean b = checkSameType(((ArrayType) t1).baseType, ((ArrayType) t2).baseType);
			if (!b) return false;
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (((StructType) t1).level != ((StructType) t2).level) return false;
				if (!((StructType) t1).tag.equal(((StructType) t2).tag)) return false;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (((UnionType) t1).level != ((UnionType) t2).level) return false;
				if (!((UnionType) t1).tag.equal(((UnionType) t2).tag)) return false;
			}
		}
		return true;
	}
	
	public Type checkSubOp(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
		}
		if (t1 instanceof PointerType || t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType || t2 instanceof ArrayType)
				if (checkSameType(t1, t2)) return new IntType();
		}
		//if (t1.getClass() == t2.getClass()) return t1;
		return null;
	}
	
	public Type checkAddOp(Type t1, Type t2) {
		//if (t1.getClass() == t2.getClass()) return t1;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType) return t2;
			if (t2 instanceof ArrayType) return t2;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
			if (t2 instanceof PointerType) return t2;
			if (t2 instanceof ArrayType) return t2;
		}
		if (t1 instanceof PointerType || t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType || t2 instanceof ArrayType) return null;
		}
		//if (t1.getClass() == t2.getClass()) return t1;
		return null;
	}
	
	public Type checkMDMOp(Type t1, Type t2) {// MUL DIV MOD
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
		}
		return null;
		//if (t1.getClass() == t2.getClass()) return t1;
	}
	
	public Type checkBitOp(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return t2;
			if (t2 instanceof CharType) return t1;
		}
		return null;
	}
	
	public ExprAtt checkBinaryOp(ExprAtt e1, ExprAtt e2, BinaryOp op) {
		if (op == BinaryOp.COMMA) {
			return new ExprAtt(e2.type, e2.lvalue, e2.isconst, e2.constvalue);
		}
		
		// ASSIGN Operation
		if (op == BinaryOp.ASSIGN) {
			if (e1.lvalue == 0) error("assign lvalue error");
			if (!checkAssign(e1.type, e2.type)) error("assign error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_MUL) {
			if (e1.lvalue == 0) error("assign_mul lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_mul error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_DIV) {
			if (e1.lvalue == 0) error("assign_div lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_div error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_MOD) {
			if (e1.lvalue == 0) error("assign_mod lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_mod error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_ADD) {
			if (e1.lvalue == 0) error("assign_add lvalue error");
			if (!checkAssignAdd(e1.type, e2.type)) error("assign_add error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SUB) {
			if (e1.lvalue == 0) error("assign_sub lvalue error");
			if (!checkAssignSub(e1.type, e2.type)) error("assign_sub error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SHL) {
			if (e1.lvalue == 0) error("assign_shl lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_shl error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SHR) {
			if (e1.lvalue == 0) error("assign_shr lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_shr error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_AND) {
			if (e1.lvalue == 0) error("assign_and lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_and error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_XOR) {
			if (e1.lvalue == 0) error("assign_xor lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_xor error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_OR) {
			if (e1.lvalue == 0) error("assign_or lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_or error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		
		// LOGICAL Operation
		
		if (op == BinaryOp.LOGICAL_OR) {
			if (!checkLogicalOp(e1.type, e2.type)) error("logical_or error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue!=0) || (e2.constvalue!=0)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LOGICAL_AND) {
			if (!checkLogicalOp(e1.type, e2.type)) error("logical_and error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue!=0) && (e2.constvalue!=0)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.EQ) {
			if (!checkLogicalOp(e1.type, e2.type)) error("eq error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) == (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.NE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("ne error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) != (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LT) {
			if (!checkLogicalOp(e1.type, e2.type)) error("lt error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) < (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.GT) {
			if (!checkLogicalOp(e1.type, e2.type)) error("gt error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) > (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("le error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) <= (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.GE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("ge error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) >= (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		
		// NUMBER Operation
		if (op == BinaryOp.ADD) {
			Type t = checkAddOp(e1.type, e2.type);
			if (t == null) error("add error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue + e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SUB) {
			Type t = checkSubOp(e1.type, e2.type);
			if (t == null) error("sub error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue - e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.MUL) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("mul error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue * e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.MOD) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("mod error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				if (e2.constvalue == 0) error("divide 0");
				value = e1.constvalue % e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.DIV) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("div error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				if (e2.constvalue == 0) error("divide 0");
				value = e1.constvalue / e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		
		//OR AND XOR SHL SHR
		if (op == BinaryOp.OR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("or error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue | e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.AND) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("and error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue & e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.XOR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("xor error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue ^ e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SHL) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("shl error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue << e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SHR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("shr error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue >> e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		
		return null;
	}
	
	public ExprAtt checkUnaryExpr(UnaryExpr expr) {
		ExprAtt e = checkExpr(expr.expr);
		return checkUnaryOp(e, expr.op);
	}
	
	public Type checkIncDecUnaryOp(Type t) {
		if (t instanceof IntType) return t;
		if (t instanceof CharType) return t;
		if (t instanceof PointerType) return t;
		return null;
	}
	
	public Type checkAsterisk(Type t) {
		if (t instanceof PointerType) {
			return ((PointerType) t).baseType;
		}
		if (t instanceof ArrayType) {
			return ((ArrayType) t).baseType;
		}
		return null;
	}
	
	public Type checkPMTUnaryOp(Type t) { //PLUS MINUS TILDE
		if (t instanceof IntType) {
			return new IntType();
		}
		if (t instanceof CharType) {
			return new IntType();
		}
		return null;
	}
	
	public Type checkNOTUnaryOp(Type t) {
		if (t instanceof IntType) return new IntType();
		if (t instanceof CharType) return new IntType();
		if (t instanceof PointerType) return new IntType();
		if (t instanceof ArrayType) return new IntType();
		return null;
	}
	
	public ExprAtt checkUnaryOp(ExprAtt e, UnaryOp op) {
		if (op == UnaryOp.INC) {
			if (e.lvalue == 0) error("unary inc lvalue error");
			Type type = checkIncDecUnaryOp(e.type);
			if (type == null) error("unary inc error");
			return new ExprAtt(type, 0, 0, 0);
		}
		if (op == UnaryOp.DEC) {
			if (e.lvalue == 0) error("unary dec lvalue error");
			Type type = checkIncDecUnaryOp(e.type);
			if (type == null) error("unary dec error");
			return new ExprAtt(type, 0, 0, 0);
		}
		if (op == UnaryOp.SIZEOF) {
			int value = 4;
			if (e.type instanceof IntType) value = 4;
			if (e.type instanceof CharType) value = 1;
			return new ExprAtt(new IntType(), 0, 1, value);
		}
		if (op == UnaryOp.AMPERSAND) {
			//System.out.println("functionCall returns "+e.lvalue);
			if (e.lvalue == 0 && !(e.type instanceof ArrayType) && !(e.type instanceof PointerType))
				error("unary ampersand lvalue error");
			return new ExprAtt(new PointerType(e.type), 0, 1, 0);
		}
		if (op == UnaryOp.ASTERISK) {
			if (e.lvalue == 0 && !(e.type instanceof ArrayType) && !(e.type instanceof PointerType))
				error("unary asterisk lvalue error");
			Type type = checkAsterisk(e.type);
			int lv;
			if (type == null) error("unary asterisk error");
			//if (!(type instanceof PointerType)) lv = 1;
			//else lv = 0;
			return new ExprAtt(type, 1, 0, 0);
		}
		if (op == UnaryOp.PLUS) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary plus error");
			int value = 0;
			if (e.isconst == 1) value = e.constvalue;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.MINUS) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary minus error");
			int value = 0;
			if (e.isconst == 1) value = -e.constvalue;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.TILDE) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary tilde error");
			int value = 0;
			if (e.isconst == 1) value = ~(e.constvalue);
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.NOT) {
			Type type = checkNOTUnaryOp(e.type);
			if (type == null) error("unary not error");
			int value = 0;
			if (e.isconst == 1)
				if (e.isconst != 0) value = 0;
				else value = 1;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		return null;
	}
	
	public ExprAtt checkIdentifier(Identifier expr) {
		Decl decl = env.getDecl(expr.symbol.toString());
		Type type;
		if (decl instanceof StructDecl) {
			type = new StructType(((StructDecl) decl).tag);
			((StructType) type).level = ((StructDecl) decl).level;
			return new ExprAtt(type, 1, 0, 0);
		}
		if (decl instanceof UnionDecl) {
			type = new UnionType(((UnionDecl) decl).tag);
			type = new UnionType(((UnionDecl) decl).tag);
			((UnionType) type).level = ((UnionDecl) decl).level;
			return new ExprAtt(type, 1, 0, 0);
		}
		if (decl instanceof FunctionDecl) {
			error(((Identifier)expr).symbol.toString()+" is not an id name");
		}
		if (decl instanceof VarDecl) {
			type = ((VarDecl) decl).type;
			/*if (type instanceof StructType)
				System.out.println(expr.symbol+" : "+((StructType)type).level);
			if (type instanceof UnionType)
				System.out.println(expr.symbol+" : "+((UnionType)type).level);*/
			if (type instanceof ArrayType) return new ExprAtt(type, 0, 1, 0);
			else return new ExprAtt(type, 1, 0, 0);
		}
		return null;
	}
	
	public boolean checkCast(Type t1, Type t2) {
		if (t1 instanceof IntType || t1 instanceof CharType || t1 instanceof PointerType) {
			if (t2 instanceof IntType || t2 instanceof CharType || t2 instanceof PointerType || t2 instanceof ArrayType)
				return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public ExprAtt checkCastExpr(CastExpr expr) {
		ExprAtt e = checkExpr(expr.expr);
		if (!checkCast(expr.cast, e.type)) error("castExpr error");
		return new ExprAtt(expr.cast, 0, e.isconst, e.constvalue);
	}

	public ExprAtt checkPointerAccess(PointerAccess expr) {
		ExprAtt e = checkExpr(expr.body);
		if (!(e.type instanceof PointerType)) error("not a PointerType");
		Type type = ((PointerType) e.type).baseType;
		if (type instanceof StructType) {
			StructType t = (StructType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof StructDecl)) error(t.tag.toString()+" is not a structtype");
			boolean exist = false;
			Type getType = null;
			for (Decl d: ((StructDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
					}
				}
			}
			if (!exist) error("struct member does not exist");
			if (getType instanceof ArrayType) return new ExprAtt(getType, 0, 0, 0);
			else return new ExprAtt(getType, 1, 0, 0);
		}
		if (type instanceof UnionType) {
			UnionType t = (UnionType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof UnionDecl)) error(t.tag.toString()+" is not a uniontype");
			boolean exist = false;
			Type getType = null;
			for (Decl d: ((UnionDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
					}
				}
			}
			if (!exist) error("union member does not exist");
			if (getType instanceof ArrayType) return new ExprAtt(getType, 0, 0, 0);
			else return new ExprAtt(getType, 1, 0, 0);
		}
		return null;
	}

	public ExprAtt checkRecordAccess(RecordAccess expr) {
		ExprAtt e = checkExpr(expr.body);
		Type type = e.type;
		if (type instanceof StructType) {
			StructType t = (StructType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof StructDecl)) error(t.tag.toString()+" is not a structtype");
			boolean exist = false;
			Type getType = null;
			for (Decl d: ((StructDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
					}
				}
			}
			if (!exist) error("struct member does not exist");
			if (getType instanceof ArrayType) return new ExprAtt(getType, 0, 0, 0);
			else return new ExprAtt(getType, 1, 0, 0);
		}
		if (type instanceof UnionType) {
			UnionType t = (UnionType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof UnionDecl)) error(t.tag.toString()+" is not a uniontype");
			boolean exist = false;
			Type getType = null;
			for (Decl d: ((UnionDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
					}
				}
			}
			if (!exist) error("union member does not exist");
			if (getType instanceof ArrayType) return new ExprAtt(getType, 0, 0, 0);
			else return new ExprAtt(getType, 1, 0, 0);
		}
		error("RecordAccess error:expr not a struct/union type");
		return null;
	}
	
	public void printType(Type type) {
		if (type == null) System.out.println("null");
		if (type instanceof IntType) System.out.println("IntType");
		if (type instanceof CharType) System.out.println("CharType");
		if (type instanceof ArrayType) System.out.println("ArrayType");
		if (type instanceof PointerType) System.out.println("PointerType");
		if (type instanceof StructType) System.out.println("StructType "+((StructType) type).tag.toString()+" "+((StructType) type).level);
		if (type instanceof UnionType) System.out.println("UnionType "+((UnionType) type).tag.toString()+" "+((StructType) type).level);
	}
	
	public ExprAtt checkArrayAccess(ArrayAccess expr) {
		ExprAtt ebody = checkExpr(expr.body);
		ExprAtt esub = checkExpr(expr.subscript);
		//printType(ebody.type);
		if (!(ebody.type instanceof ArrayType) && !(ebody.type instanceof PointerType)) {
			error("ArrayAccess error");
		}
		if (!(esub.type instanceof IntType) && ! (esub.type instanceof CharType)) {
			error("ArrayAccess subscript error");
		}
		Type type = null;
		if (ebody.type instanceof ArrayType)
			type = ((ArrayType) ebody.type).baseType;
		if (ebody.type instanceof PointerType)
			type = ((PointerType) ebody.type).baseType;
		if (type instanceof ArrayType) return new ExprAtt(type, 0, 0, 0);
		else return new ExprAtt(type, 1, 0, 0);
	}
	
	public boolean checkFuncParas(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType)
				if (checkSameType(t1, t2)) return true;
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType)
				if (checkSameType(t1, t2)) return true;
		}
		return false;
	}
	
	public ExprAtt checkFunctionCall(FunctionCall expr) {
		
		String s = expr.body.toString();
		if (s.equals("printf")) {
			int size = expr.args.size();
			if (size == 0) error("printf args num error");
			ExprAtt e = checkExpr(expr.args.get(0));
			if (!checkCast(new PointerType(new CharType()), e.type)) error("printf args error");
			return new ExprAtt(new IntType(), 0, 0, 0);
		}
		if (s.equals("getchar")) {
			int size = expr.args.size();
			if (size != 0) error("getchar args num error");
			return new ExprAtt(new CharType(), 0, 0, 0);
		}
		if (s.equals("malloc")) {
			int size = expr.args.size();
			if (size != 1) error("malloc args num error");
			ExprAtt e = checkExpr(expr.args.get(0));
			if (!checkCast(new IntType(), e.type)) error("malloc args error");
			return new ExprAtt(new PointerType(new VoidType()), 0, 0, 0);
		}
		
		Decl decl = env.getDecl(expr.body.toString());
		if (!(decl instanceof FunctionDecl)) error(expr.body.toString()+" is not functionDecl");
		FunctionDecl d = (FunctionDecl)decl;
		//if (d.params == null) System.out.println("d.params is null");
		//if (expr.args == null) System.out.println("expr.args is null");
		if (d.params.size() != expr.args.size()) error("parameters number error");
		int size = d.params.size();
		boolean valid = true;
		for (int i = 0; i < size; i++) {
			VarDecl vd = d.params.get(i);
			Expr e = expr.args.get(i);
			ExprAtt exprAtt = checkExpr(e);
			if (!checkFuncParas(vd.type, exprAtt.type)) {
				valid = false;
			}
			//printType(vd.type);
			//printType(exprAtt.type);
			//System.out.println();
		}
		if (!valid) error("function "+expr.body.toString()+" parameters type error");
		return new ExprAtt(d.returnType, 0, 0, 0);
	}
}
