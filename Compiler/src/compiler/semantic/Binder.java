package compiler.semantic;

public class Binder {
	public Object value;
	public Symb prevTop;
	public Binder lastBinder;
	public int level;
	
	public Binder(Object v, Symb p, Binder t, int l) {
		value = v;
		prevTop = p;
		lastBinder = t;
		level = l;
	} 
}