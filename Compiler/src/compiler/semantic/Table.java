package compiler.semantic;

import compiler.ast.*;
import compiler.syntactic.*;

import java.io.*;
import java.util.*;

class Symb {
	public String name;
	public Symb(String s) {
		name = s;
	}
	public String toString() {
		return name;
	}
	public static Map<String, Symb> dict = new Hashtable<String, Symb>();
	
	public static Symb symbol(String str) {
		String u = str.intern();
		Symb s = (Symb) dict.get(u);
		if (s == null) {
			s = new Symb(u);
			dict.put(u, s);
		}
		return s;
	}
}

public class Table {
	public Map<Symb, Binder> dict = new HashMap<Symb, Binder>();
	public Symb top = null;
	public Binder curScope = null;
	
	public Object get(Symb s) {
		Binder b = dict.get(s);
		if (b == null) return null;
		else return b.value;
	}
	
	public Object get(Symb s, int level) {
		Binder b = dict.get(s);
		if (b == null) return null;
		while (b.level != level) {
			b = b.lastBinder;
		}
		return b.value;
	}
	
	public Object get(String s) {
		Binder b = dict.get(Symb.symbol(s));
		if (b == null) return null;
		else return b.value;
	}
	
	public Object get(String s, int level) {
		Binder b = dict.get(s);
		if (b == null) return null;
		while (b.level != level) {
			b = b.lastBinder;
		}
		return b.value;
	}
	
	public Binder getBinder(String s) {
		return dict.get(Symb.symbol(s));
	}
	
	public void put(Symb s, Object v, int l) {
		if (s == null) s = Symb.symbol("");
		dict.put(s,  new Binder(v, top, dict.get(s), l));
		top = s;
	}
	
	public void beginScope(int l) {
		curScope = new Binder(null, top, curScope, l);
		top = null;
	}
	
	public void endScope(int l) {
		while (top != null) {
			Binder b = dict.get(top);
			//if (top == null) System.out.println("top is null");
			//System.out.println(top.name);
			//if (b == null) System.out.println("b is null");
			if (b.lastBinder != null)
				dict.put(top, b.lastBinder);
			else
				dict.remove(top);
			top = b.prevTop;
		}
		top = curScope.prevTop;
		curScope = curScope.lastBinder;
	}
}
