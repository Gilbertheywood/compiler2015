package compiler.semantic;

import java.io.*;
import java.util.*;
import compiler.ast.*;
import compiler.syntactic.*;

public class Environment {
	public Table FuncIdenEnv = null;
	public Table RecordEnv = null;
	public static int FuncIdenlevel, Recordlevel;
	
	public Environment() {
		FuncIdenlevel = 0;
		Recordlevel = 0;
		InitFuncIdenEnv();
		InitRecordEnv();
	}
	
	public void InitFuncIdenEnv() {
		FuncIdenEnv = new Table();
		FuncIdenEnv.put(Symb.symbol("getchar"), new FunctionDecl(), FuncIdenlevel);
		FuncIdenEnv.put(Symb.symbol("malloc"), new FunctionDecl(), FuncIdenlevel);
		FuncIdenEnv.put(Symb.symbol("printf"), new FunctionDecl(), FuncIdenlevel);
	}
	
	public void InitRecordEnv() {
		RecordEnv = new Table();
		RecordEnv.put(Symb.symbol("int"), new VarDecl(), Recordlevel);
		RecordEnv.put(Symb.symbol("char"), new VarDecl(), Recordlevel);
		RecordEnv.put(Symb.symbol("void"), new VarDecl(), Recordlevel);
	}
	
	public void beginStructUnionScope() {
		FuncIdenlevel++;
		FuncIdenEnv.beginScope(FuncIdenlevel);
	}
	
	public void endStructUnionScope() {
		FuncIdenEnv.endScope(FuncIdenlevel);
		FuncIdenlevel--;
	}
	
	public void beginScope() {
		FuncIdenlevel++;
		Recordlevel++;
		RecordEnv.beginScope(Recordlevel);
		FuncIdenEnv.beginScope(FuncIdenlevel);
	}
	
	public void endScope() {
		RecordEnv.endScope(Recordlevel);
		FuncIdenEnv.endScope(FuncIdenlevel);
		FuncIdenlevel--;
		Recordlevel--;
	}
	
	public void putRecordEnv(Decl decl, String name) {
		RecordEnv.put(Symb.symbol(name), decl, Recordlevel);
	}
	
	public void putFuncIdenEnv(Decl decl, String name) {
		FuncIdenEnv.put(Symb.symbol(name), decl, FuncIdenlevel);
	}
	
	public Binder getRecordBinder(String s) {
		return RecordEnv.getBinder(s);
	}
	
	public Binder getFuncIdenBinder(String s) {
		return FuncIdenEnv.getBinder(s);
	}
	
	public void error(String s) {
		//System.out.println(s);
		System.exit(1);
	}
	
	public Decl getDecl(Symb name) { //get type by id name
		Object obj = FuncIdenEnv.get(name);
		if (obj == null) {
			error("No such Function or ID declaration: "+name.toString());
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a declaration: "+name.toString());
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getDecl(Symb name, int level) {
		Object obj = FuncIdenEnv.get(name, level);
		if (obj == null) {
			error("No such Function or ID declaration: "+name.toString());
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a declaration: "+name.toString());
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getDecl(String s) {
		Object obj = FuncIdenEnv.get(Symb.symbol(s));
		if (obj == null) {
			error("No such Function or ID declaration: "+s);
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a declaration: "+s);
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getDecl(String s, int level) {
		Object obj = FuncIdenEnv.get(Symb.symbol(s), level);
		if (obj == null) {
			error("No such Function or ID declaration: "+s);
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a declaration: "+s);
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getRecord(Symb name) { // get type decl by type name
		Object obj = RecordEnv.get(name);
		if (obj == null) {
			error("No such struct or union declaration: "+name.toString());
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a record: "+name.toString());
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getRecord(Symb name, int level) { // get type decl by type name
		Object obj = RecordEnv.get(name, level);
		if (obj == null) {
			error("No such struct or union declaration: "+name.toString());
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a record: "+name.toString());
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getRecord(String s) {
		Object obj = RecordEnv.get(Symb.symbol(s));
		if (obj == null) {
			error("No such struct or union declaration: "+s);
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a record: "+s);
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public Decl getRecord(String s, int level) {
		Object obj = RecordEnv.get(Symb.symbol(s), level);
		if (obj == null) {
			error("No such struct or union declaration: "+s);
			System.exit(1);
		}
		if (!(obj instanceof Decl)) {
			error("It's not a record: "+s);
			System.exit(1);
		}
		return (Decl) obj;
	}
	
	public boolean checkFunctionEnv(String s) {
		if (FuncIdenEnv.get(Symb.symbol(s)) == null) return true;
		else return false;
	}
	public boolean checkIdentifierEnv(String s) {
		Binder b = FuncIdenEnv.getBinder(s);
		if (b != null && b.level == FuncIdenlevel) return false;
		return true;
	}
	
	public boolean checkRecordEnv(String s) {
		Binder b = RecordEnv.getBinder(s);
		if (b != null && b.level == Recordlevel) return false;
		return true;
	}
}
