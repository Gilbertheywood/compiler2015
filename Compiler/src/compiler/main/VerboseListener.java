package compiler.main;

import java.util.*;
import org.antlr.v4.runtime.*; 
import org.antlr.v4.runtime.tree.*;

public class VerboseListener extends BaseErrorListener {
	static int error_cnt = 0;
	
	public void syntaxError(Recognizer<?, ?> recognizer, 
							Object offendingSymbol,
							int line, int charPositionInLine,
							String msg,
							RecognitionException e)
	{
		//List<String> stack = ((Parser)recognizer).getRuleInvocationStack();
		//Collections.reverse(stack);
		//System.err.println("rulestack:"+stack);
		error_cnt++;
		System.err.println("line"+line+":"+charPositionInLine+"at"+offendingSymbol+":"+msg);
	}
}
