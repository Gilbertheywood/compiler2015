package compiler.main;

import compiler.ast.*;
import compiler.syntactic.*;
import compiler.semantic.*;
import compiler.ir.*;
import compiler.codeGeneration.*;

import java.io.*;
import java.util.*;

import org.antlr.v4.runtime.*; 
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class Main {
	
	static AST ast;
	static Semantic semantic;
	static IrWriter irWriter;
	static IrGenerator irGen;
	static CodeGenerator codeGen;
	static Optimization opt;
	static PeepHole ph;
	
	public static void main(String[] args) throws IOException {
		//InputStream in = new FileInputStream("E:\\git_repo\\compiler2015\\Compiler\\src\\test.c");
		//InputStream in = new FileInputStream(System.in);
		ANTLRInputStream input = new ANTLRInputStream(System.in);
		CLexer lexer = new CLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CParser parser = new CParser(tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(new VerboseListener());
		
		//parser.removeErrorListeners();
		CParser.ProgramContext context = parser.program();
		if (VerboseListener.error_cnt > 0) {
			//System.out.println("There is/are (an) error(s) in this C file. Please check again.");
			System.exit(1);
		}
		ast = context.ele;
		
		semantic = new Semantic();
		semantic.checkAST(ast);
		//System.out.println("pass");
		
		//AstPrinter astPrinter = new AstPrinter(ast);
		//astPrinter.PrintAST();
		
		//System.out.println("sementic check pass");
		
		irGen = new IrGenerator();
		irGen.checkAST(ast);
		
		//System.out.println("irGen is OK");
		
		irWriter = new IrWriter(irGen.ir);
		//irWriter.writeIr();
		
		ph = new PeepHole(irGen.ir);
		ph.Optimize();
		
		//codeGen = new CodeGenerator(ph.ir);
		//codeGen.GenerateCode();
		opt = new Optimization(ph.ir);
		opt.GenerateCode();
		
	}
}
