package compiler.main;

import compiler.ast.*;
import compiler.syntactic.*;

import java.util.*;
import java.io.*;

public class AstPrinter {
	public AST ast;
	
	public AstPrinter(AST ast) {
		this.ast = ast;
	}
	
	public void PrintAST() {
		System.out.println("[root]");
		for (Decl decl: ast.decls) {
			if (decl instanceof FunctionDecl) {
				PrintFunctionDecl((FunctionDecl)decl, 1);
			}
			if (decl instanceof StructDecl) {
				PrintStructDecl((StructDecl)decl, 1);
			}
			if (decl instanceof UnionDecl) {
				PrintUnionDecl((UnionDecl)decl, 1);
			}
			if (decl instanceof VarDecl) {
				PrintVarDecl((VarDecl)decl, 1);
			}
		}
	}
	
	private void PrintFunctionDecl(FunctionDecl decl, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[FunctionDecl: "+decl.name.toString()+"]");
		PrintType(decl.returnType, tabs+1);
		PrintParams(decl.params, tabs+1);
		PrintCompoundStmt(decl.body, tabs+1);
	}
	
	private void PrintStructDecl(StructDecl decl, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[StructDecl: "+decl.tag.toString()+"]");
		PrintFields(decl.fields, tabs+1);
	}
	
	private void PrintUnionDecl(UnionDecl decl, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[UnionDecl: "+decl.tag.toString()+"]");
		PrintFields(decl.fields, tabs+1);
	}
	
	private void PrintVarDecl(VarDecl decl, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[VarDecl: "+decl.name.toString()+"]");
		PrintType(decl.type, tabs+1);
		PrintInitializer(decl.init, tabs+1);
	}
	

	private void PrintInitializer(Initializer init, int tabs) {
		if (init instanceof InitValue) {
			for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
			System.out.println("[InitValue]");
			InitValue tmp = (InitValue)init;
			PrintExpr(tmp.expr, tabs+1);
		}
		if (init instanceof InitList) {
			for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
			System.out.println("[InitList]");
			InitList tmp = (InitList)init;
			for (Initializer i: tmp.inits) {
				PrintInitializer(i, tabs+1);
			}
		}
	}
	
	private void PrintType(Type type, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		if (type instanceof IntType) System.out.println("[IntType]");
		if (type instanceof CharType) System.out.println("[CharType]");
		if (type instanceof VoidType) System.out.println("[VoidType]");
		if (type instanceof StructType) {
			StructType tmp = (StructType)type;
			System.out.println("[StructType: "+tmp.tag.toString()+"]");
		}
		if (type instanceof UnionType) {
			UnionType tmp = (UnionType)type;
			System.out.println("[UnionType: "+tmp.tag.toString()+"]");
		}
		if (type instanceof PointerType) {
			System.out.println("[PointerType]");
			PointerType tmp = (PointerType)type;
			PrintType(tmp.baseType, tabs+1);
		}
		if (type instanceof ArrayType) {
			System.out.println("[ArrayType]");
			ArrayType tmp = (ArrayType)type;
			PrintExpr(tmp.arraySize, tabs+1);
			PrintType(tmp.baseType, tabs+1);
		}
	}
	
	private void PrintParams(List<VarDecl> decls, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Parameters]");
		for (VarDecl decl: decls) {
			PrintVarDecl(decl, tabs+1);
		}
	}
	
	private void PrintFields(List<Decl> decls, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Fields]");
		for (Decl decl: decls) {
			PrintDecl(decl, tabs+1);
		}
	}
	
	private void PrintCompoundStmt(CompoundStmt stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[CompoundStmt]");
		for (Decl decl: stmt.decls) {
			PrintDecl(decl, tabs+1);
		}
		for (Stmt s: stmt.stats) {
			PrintStmt(s, tabs+1);
		}
	}
	
	private void PrintDecl(Decl decl, int tabs) {
		if (decl instanceof FunctionDecl) {
			PrintFunctionDecl((FunctionDecl)decl, tabs);
		}
		if (decl instanceof StructDecl) {
			PrintStructDecl((StructDecl)decl, tabs);
		}
		if (decl instanceof UnionDecl) {
			PrintUnionDecl((UnionDecl)decl, tabs);
		}
		if (decl instanceof VarDecl) {
			PrintVarDecl((VarDecl)decl, tabs);
		}
	}
	
	private void PrintStmt(Stmt stmt, int tabs) {
		if (stmt instanceof BreakStmt) {
			//System.out.println("1");
			PrintBreakStmt((BreakStmt)stmt, tabs);
		}
		if (stmt instanceof ContinueStmt) {
			//System.out.println("2");
			PrintContinueStmt((ContinueStmt)stmt, tabs);
		}
		if (stmt instanceof IfStmt) {
			//System.out.println("3");
			PrintIfStmt((IfStmt)stmt, tabs);
		}
		if (stmt instanceof ForLoop) {
			//System.out.println("4");
			PrintForLoop((ForLoop)stmt, tabs);
		}
		if (stmt instanceof WhileLoop) {
			//System.out.println("5");
			PrintWhileLoop((WhileLoop)stmt, tabs);
		}
		if (stmt instanceof ReturnStmt) {
			//System.out.println("6");
			PrintReturnStmt((ReturnStmt)stmt, tabs);
		}
		if (stmt instanceof CompoundStmt) {
			//System.out.println("7");
			PrintCompoundStmt((CompoundStmt)stmt, tabs);
		}
		if (stmt instanceof Expr) {
			//System.out.println("8");
			PrintExpr((Expr)stmt, tabs);
		}
	}
	
	private void PrintBreakStmt(BreakStmt stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[BreakStmt]");
	}
	
	private void PrintContinueStmt(ContinueStmt stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[ContinueStmt]");
	}
	
	private void PrintIfStmt(IfStmt stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[IfStmt]");
		
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Condition]");
		PrintExpr(stmt.condition, tabs+2);
		
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Consequent]");
		PrintStmt(stmt.consequent, tabs+2);
		
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Alternative]");
		PrintStmt(stmt.alternative, tabs+2);
	}
	
	private void PrintForLoop(ForLoop stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[ForLoop]");
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Init]");
		PrintExpr(stmt.init, tabs+2);
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Condition]");
		PrintExpr(stmt.condition, tabs+2);
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Step]");
		PrintExpr(stmt.step, tabs+2);
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Body]");
		PrintStmt(stmt.body, tabs+2);
	}
	
	private void PrintWhileLoop(WhileLoop stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[WhileLoop]");
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Condition]");
		PrintExpr(stmt.condition, tabs+2);
		
		for (int i = 0; i < tabs - 1+1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Body]");
		PrintStmt(stmt.body, tabs+2);
	}
	
	private void PrintReturnStmt(ReturnStmt stmt, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[ReturnStmt]");
		PrintExpr(stmt.expr, tabs+1);
	}
	
	private void PrintExpr(Expr expr, int tabs) {
		if (expr instanceof EmptyExpr || expr == null) PrintEmptyExpr((EmptyExpr)expr, tabs);
		if (expr instanceof BinaryExpr) PrintBinaryExpr((BinaryExpr)expr, tabs);
		if (expr instanceof UnaryExpr) PrintUnaryExpr((UnaryExpr)expr, tabs);
		if (expr instanceof SizeofExpr) PrintSizeofExpr((SizeofExpr)expr, tabs);
		if (expr instanceof CastExpr) PrintCastExpr((CastExpr)expr, tabs);
		if (expr instanceof PointerAccess) PrintPointerAccess((PointerAccess)expr, tabs);
		if (expr instanceof RecordAccess) PrintRecordAccess((RecordAccess)expr, tabs);
		if (expr instanceof SelfIncrement) PrintSelfIncrement((SelfIncrement)expr, tabs);
		if (expr instanceof SelfDecrement) PrintSelfDecrement((SelfDecrement)expr, tabs);
		if (expr instanceof ArrayAccess) PrintArrayAccess((ArrayAccess)expr, tabs);
		if (expr instanceof FunctionCall) PrintFunctionCall((FunctionCall)expr, tabs);
		if (expr instanceof Identifier) PrintIdentifier((Identifier)expr, tabs);
		if (expr instanceof IntConst) PrintIntConst((IntConst)expr, tabs);
		if (expr instanceof CharConst) PrintCharConst((CharConst)expr, tabs);
		if (expr instanceof StringConst) PrintStringConst((StringConst)expr, tabs);
	}
	
	private void TestExpr(Expr expr) {
		System.out.println("Test:");
		if (expr instanceof EmptyExpr || expr == null) System.out.println("EmptyExpr");
		if (expr instanceof BinaryExpr) System.out.println("BinaryExpr");
		if (expr instanceof UnaryExpr) System.out.println("UnaryExpr");
		if (expr instanceof SizeofExpr) System.out.println("SizeofExpr");
		if (expr instanceof CastExpr) System.out.println("CastExpr");
		if (expr instanceof PointerAccess) System.out.println("PointerAccess");
		if (expr instanceof RecordAccess) System.out.println("RecordAccess");
		if (expr instanceof SelfIncrement) System.out.println("SelfIncrement");
		if (expr instanceof SelfDecrement) System.out.println("SelfDecrement");
		if (expr instanceof ArrayAccess) System.out.println("ArrayAccess");
		if (expr instanceof FunctionCall) System.out.println("FunctionCall");
		if (expr instanceof Identifier) System.out.println("Identifier");
		if (expr instanceof IntConst) System.out.println("IntConst");
		if (expr instanceof CharConst) System.out.println("CharConst");
		if (expr instanceof StringConst) System.out.println("StringConst");
	}
	
	private void PrintEmptyExpr(EmptyExpr expr, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[EmptyExpr]");
	}
	
	private void PrintBinaryExpr(BinaryExpr expr, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[BinaryExpr: "+StringBinaryOp(expr.op)+"]");
		
		PrintExpr(expr.left, tabs+1);//TestExpr(expr.left);
		PrintExpr(expr.right, tabs+1);//TestExpr(expr.right);
	}
	
	private String StringBinaryOp(BinaryOp op) {
		if (op == BinaryOp.COMMA) return ",";
		if (op == BinaryOp.ASSIGN) return "=";
		if (op == BinaryOp.ASSIGN_MUL) return "*=";
		if (op == BinaryOp.ASSIGN_DIV) return "/=";
		if (op == BinaryOp.ASSIGN_MOD) return "%=";
		if (op == BinaryOp.ASSIGN_ADD) return "+=";
		if (op == BinaryOp.ASSIGN_SUB) return "-=";
		if (op == BinaryOp.ASSIGN_SHL) return "<<=";
		if (op == BinaryOp.ASSIGN_SHR) return ">>=";
		if (op == BinaryOp.ASSIGN_AND) return "&=";
		if (op == BinaryOp.ASSIGN_XOR) return "^=";
		if (op == BinaryOp.ASSIGN_OR) return "|=";
		if (op == BinaryOp.LOGICAL_OR) return "|";
		if (op == BinaryOp.LOGICAL_AND) return "&";
		if (op == BinaryOp.OR) return "||";
		if (op == BinaryOp.AND) return "&&";
		if (op == BinaryOp.EQ) return "==";
		if (op == BinaryOp.NE) return "!=";
		if (op == BinaryOp.LT) return "<";
		if (op == BinaryOp.GT) return ">";
		if (op == BinaryOp.LE) return "<=";
		if (op == BinaryOp.GE) return ">=";
		if (op == BinaryOp.SHL) return "<<";
		if (op == BinaryOp.SHR) return ">>";
		if (op == BinaryOp.ADD) return "+";
		if (op == BinaryOp.SUB) return "-";
		if (op == BinaryOp.MUL) return "*";
		if (op == BinaryOp.DIV) return "/";
		if (op == BinaryOp.MOD) return "%";
		return "";
	}
	
	private void PrintUnaryExpr(UnaryExpr e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[UnaryExpr: "+StringUnaryOp(e.op)+"]");
		
		PrintExpr(e.expr, tabs+1);
	}
	
	private String StringUnaryOp(UnaryOp op) {
		if (op == UnaryOp.INC) return "++";
		if (op == UnaryOp.DEC) return "--";
		if (op == UnaryOp.SIZEOF) return "sizeof";
		if (op == UnaryOp.AMPERSAND) return "&";
		if (op == UnaryOp.ASTERISK) return "*";
		if (op == UnaryOp.PLUS) return "+";
		if (op == UnaryOp.MINUS) return "-";
		if (op == UnaryOp.TILDE) return "~";
		if (op == UnaryOp.NOT) return "!";
		return "";
	}
	
	private void PrintSizeofExpr(SizeofExpr e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[SizeofExpr]");
		PrintType(e.type, tabs+1);
	}
	
	private void PrintCastExpr(CastExpr e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[CastExpr]");
		
		PrintType(e.cast, tabs+1);
		PrintExpr(e.expr, tabs+1);
	}
	
	private void PrintPointerAccess(PointerAccess e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[PointerAccess: "+e.attribute.toString()+"]");
		
		PrintExpr(e.body, tabs+1);
	}
	
	private void PrintRecordAccess(RecordAccess e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[RecordAccess: "+e.attribute.toString()+"]");
		
		PrintExpr(e.body, tabs+1);
	}
	
	private void PrintSelfIncrement(SelfIncrement e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[SelfIncrement]");
		
		PrintExpr(e.body, tabs+1);
	}
	
	private void PrintSelfDecrement(SelfDecrement e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[SelfDecrement]");
		
		PrintExpr(e.body, tabs+1);
	}
	
	private void PrintArrayAccess(ArrayAccess e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[ArrayAccess]");
		
		PrintExpr(e.body, tabs+1);
		PrintExpr(e.subscript, tabs+1);
	}
	
	private void PrintFunctionCall(FunctionCall e, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[FunctionCall: "+e.body.toString()+"]");
		
		for (Expr expr: e.args) {
			PrintExpr(expr, tabs+1);
		}
	}
	
	private void PrintIdentifier(Identifier id, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[Identifier: "+id.symbol.toString()+"]");
	}
	
	private void PrintIntConst(IntConst ic, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[IncConst: "+ic.value+"]");
	}
	
	private void PrintCharConst(CharConst cc, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[CharConst: "+cc.value+"]");
	}
	
	private void PrintStringConst(StringConst sc, int tabs) {
		for (int i = 0; i < tabs - 1; i++) System.out.print("|   ");System.out.print("`---");
		System.out.println("[StringConst: "+sc.value+"]");
	}
}
