package compiler.ast;

public class StructType extends BasicType {
    public Symbol tag;
    public int level;

    public StructType() {
    }

    public StructType(Symbol tag) {
        this.tag = tag;
    }
}
