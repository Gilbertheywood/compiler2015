package compiler.ast;

public class ArrayType extends Type {
    public Type baseType;
    public Expr arraySize;
    public int size;
    public int dimSize;
    public int baseSize;

    public ArrayType() {
        baseType = null;
        arraySize = null;
        size = 0;
        dimSize = 0;
    }

    public ArrayType(Type baseType, Expr arraySize) {
        this.baseType = baseType;
        this.arraySize = arraySize;
    }
    
    public ArrayType(Type baseType, Expr arraySize, int size, int d, int b) {
        this.baseType = baseType;
        this.arraySize = arraySize;
        this.size = size;
        this.dimSize = d;
        this.baseSize = b;
    }
}
