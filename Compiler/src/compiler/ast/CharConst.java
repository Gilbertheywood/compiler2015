package compiler.ast;

public class CharConst extends Expr {
    public String value;

    public CharConst() {
    }

    public CharConst(String value) {
    	int len = value.length();
        this.value = value.substring(1, len-1);
    }
}
