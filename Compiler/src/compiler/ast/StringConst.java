package compiler.ast;

public class StringConst extends Expr {
    public String value;

    public StringConst() {
        value = null;
    }

    public StringConst(String value) {
    	int len = value.length();
        this.value = value.substring(1, len-1);
    }
}
