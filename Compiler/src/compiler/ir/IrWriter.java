package compiler.ir;

public class IrWriter {
	
	IR ir;
	
	public IrWriter(IR ir) {
		this.ir = ir;
	}
	
	public void writeIr() {
		for (Function func: ir.fragments) {
			if (func.name.equals("__start")) {
				writeFunction(func);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("main")) {
				writeFunction(func);
				break;
			}
		}
		for (Function func: ir.fragments) {
			if (func.name.equals("__start") || func.name.equals("main"))
				continue;
			writeFunction(func);
		}
	}
	
	public void writeFunction(Function func) {
		System.out.println("-------------------");
		System.out.println("Function: " + func.name);
		System.out.println("ReturnSize: " + func.size);
		System.out.println("Args:");
		for (Variable v: func.args) {
			writeVariable(v);
		}
		System.out.println("Vars:");
		for (Variable v: func.vars) {
			writeVariable(v);
		}
		System.out.println("-------------------");
		for (Quadruple q: func.body) {
			writeQuadruple(q);
		}
	}
	
	public void writeVariable(Variable v) {
		if (v instanceof BasicVariable) {
			System.out.println("BasicVariable: "+v.name+" "+v.size);
		}
		if (v instanceof ArrayVariable) {
			System.out.println("ArrayVariable: "+v.name+" "+v.size);
		}
	}
	
	public void writeQuadruple(Quadruple q) {
		if (q instanceof Assign) {
			Assign assign = (Assign) q;
			writeAddress(assign.dest);
			System.out.print(" = ");
			writeAddress(assign.src);
			System.out.println();
		}
		if (q instanceof ArithmeticExpr) {
			ArithmeticExpr ae = (ArithmeticExpr) q;
			writeAddress(ae.dest);
			System.out.print(" = ");
			writeAddress(ae.src1);
			writeArithmeticOp(ae.op);
			writeAddress(ae.src2);
			System.out.println();
		}
		if (q instanceof ArrayRead) {
			ArrayRead ar = (ArrayRead) q;
			writeAddress(ar.dest);
			System.out.print(" = ");
			writeAddress(ar.src);
			System.out.print("[");
			writeAddress(ar.offset);
			System.out.print("], "+ar.size);
			System.out.println();
		}
		if (q instanceof ArrayWrite) {
			ArrayWrite aw = (ArrayWrite) q;
			writeAddress(aw.dest);
			System.out.print("[");
			writeAddress(aw.offset);
			System.out.print("] = ");
			writeAddress(aw.src);
			System.out.println(", "+aw.size);
		}
		if (q instanceof MemoryRead) {
			MemoryRead pr = (MemoryRead) q;
			writeAddress(pr.dest);
			System.out.print(" = *");
			writeAddress(pr.src);
			System.out.println(", "+pr.size);
		}
		if (q instanceof MemoryWrite) {
			MemoryWrite mw = (MemoryWrite) q;
			System.out.print("*");
			writeAddress(mw.dest);
			System.out.print(" = ");
			writeAddress(mw.src);
			System.out.println(", "+mw.size);
		}
		if (q instanceof AddressOf) {
			AddressOf ao = (AddressOf) q;
			writeAddress(ao.dest);
			System.out.print(" = &");
			writeAddress(ao.src);
			System.out.println();
		}
		if (q instanceof Label) {
			Label l = (Label) q;
			System.out.println("Label"+l.num);
		}
		if (q instanceof Goto) {
			Goto g = (Goto) q;
			System.out.println("Goto: Label"+g.label.num);
		}
		if (q instanceof IfFalseGoto) {
			IfFalseGoto iff = (IfFalseGoto) q;
			System.out.print("If False ");
			writeAddress(iff.src1);
			writeRelationalOp(iff.op);
			writeAddress(iff.src2);
			System.out.println(" Goto: Label"+iff.label.num);
		}
		if (q instanceof IfTrueGoto) {
			IfTrueGoto ift = (IfTrueGoto) q;
			System.out.print("If True ");
			writeAddress(ift.src1);
			writeRelationalOp(ift.op);
			writeAddress(ift.src2);
			System.out.println(" Goto: Label"+ift.label.num);
		}
		if (q instanceof BasicParam) {
			BasicParam bp = (BasicParam) q;
			System.out.print("Param ");
			writeAddress(bp.src);
			System.out.println();
		}
		if (q instanceof ArrayParam) {
			ArrayParam ap = (ArrayParam) q;
			System.out.print("Param ");
			writeAddress(ap.name);
			System.out.print("[");
			writeAddress(ap.offset);
			System.out.println("], "+ap.size);
		}
		if (q instanceof MemoryParam) {
			MemoryParam mp = (MemoryParam) q;
			System.out.print("Param *");
			writeAddress(mp.src);
			System.out.println(" "+mp.size);
		}
		if (q instanceof Call) {
			Call call = (Call) q;
			Address adrs = null;
			if (call.returnValue instanceof BasicParam)
				adrs = ((BasicParam)call.returnValue).src;
			if (call.returnValue instanceof ArrayParam)
				adrs = ((ArrayParam)call.returnValue).name;
			if (call.returnValue instanceof MemoryParam)
				adrs = ((MemoryParam)call.returnValue).src;
			if (adrs != null) {
				writeAddress(adrs);
				System.out.print(" = ");
			}
			System.out.println("Call "+call.callee+" "+call.numOfParams);
		}
		if (q instanceof Return) {
			System.out.print("Return ");
			writeQuadruple(((Return)q).value);
		}
	}
	
	public void writeAddress(Address a) {
		if (a instanceof StringAddressConst) {
			StringAddressConst sac = (StringAddressConst) a;
			System.out.print(sac.value);
		}
		if (a instanceof IntegerConst) {
			IntegerConst ic = (IntegerConst) a;
			System.out.print(ic.value);
		}
		if (a instanceof Name) {
			Name n = (Name) a;
			System.out.print(n.name);
		}
		if (a instanceof Temp) {
			Temp tmp = (Temp) a;
			System.out.print("#t"+tmp.num);
		}
	}
	
	public void writeRelationalOp(RelationalOp op) {
		if (op == RelationalOp.EQ) {
			System.out.print("==");
		}
		if (op == RelationalOp.GE) {
			System.out.print(">=");
		}
		if (op == RelationalOp.GT) {
			System.out.print(">");
		}
		if (op == RelationalOp.LE) {
			System.out.print("<=");
		}
		if (op == RelationalOp.LT) {
			System.out.print("<");
		}
		if (op == RelationalOp.NE) {
			System.out.print("!=");
		}
	}
	
	public void writeArithmeticOp(ArithmeticOp op) {
		if (op == ArithmeticOp.ADD) {
			System.out.print("+");
		}
		if (op == ArithmeticOp.AND) {
			System.out.print("&");
		}
		if (op == ArithmeticOp.DIV) {
			System.out.print("/");
		}
		if (op == ArithmeticOp.MINUS) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.MOD) {
			System.out.print("%");
		}
		if (op == ArithmeticOp.MUL) {
			System.out.print("*");
		}
		if (op == ArithmeticOp.NOT) {
			System.out.print("!");
		}
		if (op == ArithmeticOp.OR) {
			System.out.print("|");
		}
		if (op == ArithmeticOp.SHL) {
			System.out.print("<<");
		}
		if (op == ArithmeticOp.SHR) {
			System.out.print(">>");
		}
		if (op == ArithmeticOp.SUB) {
			System.out.print("-");
		}
		if (op == ArithmeticOp.TILDE) {
			System.out.print("~");
		}
		if (op == ArithmeticOp.XOR) {
			System.out.print("^");
		}
	}

}
