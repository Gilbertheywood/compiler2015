package compiler.ir;

import java.util.List;
import java.util.LinkedList;

public class IR {
    public List<Function> fragments;
    public List<Variable> vars;

    public IR() {
        fragments = new LinkedList<Function>();
        vars = new LinkedList<Variable>();
    }

    public IR(List<Function> fragments, List<Variable> vars) {
        this.fragments = fragments;
        this.vars = vars;
    }
}
