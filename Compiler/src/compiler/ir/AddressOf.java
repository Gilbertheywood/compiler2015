package compiler.ir;

public class AddressOf extends Quadruple {
    public Address dest;
    public Address src;

    public AddressOf() {
        dest = null;
        src = null;
    }

    public AddressOf(Address dest, Address src) {
        this.dest = dest;
        this.src = src;
    }
}
