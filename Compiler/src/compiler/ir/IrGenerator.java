package compiler.ir;

import compiler.ast.*;
import compiler.syntactic.*;
import compiler.semantic.*;

import java.io.*;
import java.util.*;

class Error {
	public String s;
	
	public Error(String s) {
		this.s = s;
	}
}

class ExprAtt {
	public Type type;
	public int lvalue;
	public int isconst;
	public int definite;
	public int constvalue;
	
	public IR ir;
	
	public ExprAtt(Type t, int l, int i, int c) {
		type = t;
		lvalue = l;
		isconst = i;
		constvalue = c;
	}
	
	public ExprAtt() {
		type = null;
		lvalue = 0;
		isconst = 0;
	}
}

public class IrGenerator {
	public Environment env;
	public List<Error> errors = new ArrayList<Error>();
	public boolean initCheck;
	public int isLoop;
	public IR ir;
	//public Map<String, Address> dict;
	public Stack<Triple<Label, Label, Label>> loopStack; 
	public Map<String, Function> funcDict;
	public Map<String, Variable> globalMap;
	
	public IrGenerator() {
		env = new Environment();
		globalMap = new HashMap<String, Variable>();
		initCheck = true;
		isLoop = 0;
		//dict = new HashMap<String, Address>();
		loopStack = new Stack<Triple<Label, Label, Label>>();
		ir = new IR();
		funcDict = new HashMap<String,Function>();
	}
	
	public void error(String s) {
		//errors.add(new Error(s));
		//System.err.println(s);
		System.exit(1);
	}
	
	public boolean hasError() {
		return errors.size() > 0;
	}
	
	// start semantic check
	
	public void checkAST(AST ast) {
		Function func = new Function();
		func.name = "_start";
		func.size = 0;
		
		for (Decl decl: ast.decls) {
			if (decl instanceof StructDecl) checkStructDecl((StructDecl)decl);
			if (decl instanceof UnionDecl) checkUnionDecl((UnionDecl)decl);
		}
		
		for (Decl decl: ast.decls) {
			if (decl instanceof VarDecl) checkVarDecl((VarDecl)decl, func.vars, func.body);
		}
		
		for (Variable v: func.vars) {
			globalMap.put(v.name, v);
		}
		
		for (Decl decl: ast.decls) {
			if (decl instanceof FunctionDecl) checkDecl(decl, null);
			else {
				//if (decl instanceof VarDecl) checkVarDecl((VarDecl)decl, func.vars, func.body);
				//if (decl instanceof StructDecl) checkStructDecl((StructDecl)decl);
				//if (decl instanceof UnionDecl) checkUnionDecl((UnionDecl)decl);
			}
		}
		
		//func.body.add(new Call(null, "main", 0));
		ir.fragments.add(func);
		//System.out.println("this is the end.");
	}
	
	public void checkDecl(Decl decl, List<Quadruple> body) {
		if (decl instanceof FunctionDecl) {
			Function func = new Function();
			checkFunctionDecl((FunctionDecl)decl, func);
			ir.fragments.add(func);
		}
		if (decl instanceof StructDecl) {
			checkStructDecl((StructDecl)decl);
		}
		if (decl instanceof UnionDecl) {
			checkUnionDecl((UnionDecl)decl);
		}
		if (decl instanceof VarDecl) {
			checkVarDecl((VarDecl)decl, null, body);
		}
	}
	
	public void checkFunctionDecl(FunctionDecl decl, Function func) {
		String s = decl.name.toString();
		//System.out.println(s);
		if (!env.checkFunctionEnv(s)) {
			error("The function id "+s+" has been declarated.");
		}
		
		if (!(decl.returnType instanceof VoidType)) checkType(decl.returnType);
		
		env.putFuncIdenEnv(decl, s);
		if (s.equals("main")) func.name = s;
		else func.name = "_"+s;
		func.size = getSize(decl.returnType);
		if (func.name.equals("main")) func.body.add(new Call(null, "_start", 0));
		
		//parameters
		env.beginScope();
		
		for (VarDecl d: decl.params) {
			/*if (!env.checkIdentifierEnv(d.name.toString())) {
				System.err.println("Identifier definition error");
				System.exit(1);
			}*/
			checkArgVarDecl(d, func.args, func.body);
			env.putFuncIdenEnv(d, d.name.toString());
		}
		
		checkCompoundStmt(decl.body, decl.returnType, func.vars, func.body);

		env.endScope();
	}
	
	public void checkStructDecl(StructDecl decl) {
		if (!env.checkRecordEnv(decl.tag.toString())) {
			error("Struct type definition error.");
			System.exit(1);
		}
		decl.level = env.Recordlevel;
		env.putRecordEnv(decl, decl.tag.toString());
		env.beginStructUnionScope();
		for (Decl d: decl.fields) {
			if (d instanceof StructDecl) checkStructDecl((StructDecl)d);
			if (d instanceof UnionDecl) checkUnionDecl((UnionDecl)d);
			if (d instanceof VarDecl) checkVarDecl((VarDecl)d, new LinkedList<Variable>(), null);
		}
		env.endStructUnionScope();
	}
	
	public void checkUnionDecl(UnionDecl decl) {
		if (!env.checkRecordEnv(decl.tag.toString())) {
			error("Union type definition error.");
			System.exit(1);
		}
		decl.level = env.Recordlevel;
		env.putRecordEnv(decl, decl.tag.toString());
		for (Decl d: decl.fields) {
			if (d instanceof StructDecl) checkStructDecl((StructDecl)d);
			if (d instanceof UnionDecl) checkUnionDecl((UnionDecl)d);
			if (d instanceof VarDecl) checkVarDecl((VarDecl)d, new LinkedList<Variable>(), null);
		}
	}
	
	public int max(int a, int b) {
		if (a>b) return a;
		else return b;
	}
	
	public int min(int a, int b) {
		if (a>b) return b;
		else return a;
	}
	
	public int getSize(Type type) {
		if (type instanceof IntType) return 4;
		if (type instanceof CharType) return 1;
		if (type instanceof PointerType) return 4;
		if (type instanceof ArrayType) {
			ArrayType at = (ArrayType) type;
			int size = at.size*at.baseSize;
			if (size % 4 != 0) size = (size / 4 + 1) * 4;
			return size;
		}
		if (type instanceof StructType) {
			Decl d = env.getRecord(((StructType) type).tag.toString(), ((StructType) type).level);
			int size = 0;
			for (Decl decl: ((StructDecl) d).fields)
			if (decl instanceof VarDecl) {
				Type t = ((VarDecl) decl).type;
				if (t instanceof IntType)
					if (size % 4 != 0) {
						size = (size/4+1)*4;
					}
				//printType(type);
				//System.out.println(size);
				size += getSize(t);
			}
			if (size % 4 != 0) size = (size / 4 + 1) * 4;
			return size;
		}
		if (type instanceof UnionType) {
			Decl d = env.getRecord(((UnionType) type).tag.toString());
			int size = 0;
			for (Decl decl: ((UnionDecl) d).fields) {
				if (decl instanceof VarDecl) {
					Type t = ((VarDecl) decl).type;
					size = max(getSize(t), size);
				}
			}
			if (size % 4 != 0) size = (size / 4 + 1) * 4;
			return size;
		}
		return 0;
	}
	
	public void checkArgVarDecl(VarDecl decl, List<Variable> vars, List<Quadruple> body) {
		if (decl.type instanceof ArrayType) {
			ArrayType at = (ArrayType) decl.type;
			Type type = at.baseType;
			decl.type = new PointerType(type);
		}
		env.putFuncIdenEnv(decl, ((VarDecl) decl).name.toString());
		int level = env.FuncIdenlevel;
		//Name name = new Name(((VarDecl) decl).name.toString()+level);
		
		Variable var = null;
		if (decl.type instanceof IntType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof CharType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof PointerType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof StructType) {
			Decl d = env.getRecord(((StructType) decl.type).tag.toString());
			((StructType) decl.type).level = ((StructDecl) d).level;
			var = new ArrayVariable();
			var.name = decl.name.toString()+level;
			var.size = getSize(decl.type);
		}
		if (decl.type instanceof UnionType) {
			Decl d = env.getRecord(((UnionType) decl.type).tag.toString());
			((UnionType) decl.type).level = ((UnionDecl) d).level;
			var = new ArrayVariable();
			var.name = decl.name.toString()+level;
			var.size = getSize(decl.type);
		}
		if (decl.type instanceof ArrayType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		
		vars.add(var);
	}
	
	public void checkVarDecl(VarDecl decl, List<Variable> vars, List<Quadruple> body) {
		ArrayList<Integer> list = null;
		//Type type;
		if (!env.checkIdentifierEnv(decl.name.toString())) {
			error("Identifier definition error");
			//System.exit(1);
		}
		env.putFuncIdenEnv(decl, ((VarDecl) decl).name.toString());
		int level = env.FuncIdenlevel;
		Name name = new Name(((VarDecl) decl).name.toString()+level);
		initCheck = true;
		list = checkType(decl.type);
		
		Variable var = null;
		if (decl.type instanceof IntType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof CharType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof PointerType) {
			var = new BasicVariable();
			var.name = decl.name.toString()+level;
			var.size = 4;
		}
		if (decl.type instanceof StructType) {
			Decl d = env.getRecord(((StructType) decl.type).tag.toString());
			((StructType) decl.type).level = ((StructDecl) d).level;
			var = new ArrayVariable();
			var.name = decl.name.toString()+level;
			var.size = getSize(decl.type);
		}
		if (decl.type instanceof UnionType) {
			Decl d = env.getRecord(((UnionType) decl.type).tag.toString());
			((UnionType) decl.type).level = ((UnionDecl) d).level;
			var = new ArrayVariable();
			var.name = decl.name.toString()+level;
			var.size = getSize(decl.type);
		}
		if (decl.type instanceof ArrayType) {
			var = new ArrayVariable();
			var.name = decl.name.toString()+level;
			var.size = getSize(decl.type);
			/*int tmp = 1;
			for (Integer i: list) {
				tmp *= i.intValue();
			}
			Type t = getType(decl.type);
			if (t instanceof IntType) var.size = tmp * 4;
			if (t instanceof CharType) var.size = tmp;
			if (t instanceof PointerType) var.size = tmp * 4;
			if (t instanceof StructType) var.size = tmp * getSize(t);
			if (t instanceof UnionType) var.size = tmp * getSize(t);*/
		}
		
		//type = getType(decl.type);
		int ini = 0;
		if (decl.type instanceof ArrayType) {
			ini= 1;
		}
		if (body == null) {
			vars.add(var);
			return;
		}
		if (decl.init != null && initCheck) // mark ques
			checkInitializer(name, decl.init, decl.type, vars, body, 0, ini);
		
		vars.add(var);
	}
	
	public Type getType(Type type) {
		//if (type instanceof PointerType)
		//	return getType(((PointerType) type).baseType);
		if (type instanceof ArrayType)
			return getType(((ArrayType) type).baseType);
		return type;
	}
	
	public ArrayList<Integer> checkType(Type type) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(new Integer(1));
		if (type instanceof VoidType)
			error("VoidType can just be the return-type of functions");
		if (!(type instanceof PointerType)) {
			if (type instanceof StructType) {
				StructType t = (StructType)type;
				if (env.getRecord(t.tag.toString()) == null)
					error("No such StructType");
				if (env.getRecord(t.tag.toString()) instanceof UnionDecl) 
					error(t.tag.toString()+" is struct rather than union");
			}
			if (type instanceof UnionType) {
				UnionType t = (UnionType)type;
				if (env.getRecord(t.tag.toString()) == null)
					error("No such UnionType");
				if (env.getRecord(t.tag.toString()) instanceof StructDecl)
					error(t.tag.toString()+" is union rather than struct");
			}
		}
		if (type instanceof ArrayType) {
			list = checkType(((ArrayType) type).baseType);
			ArrayType t= (ArrayType)type;
			ExprAtt exprAtt = checkExpr(t.arraySize, new LinkedList<Variable>(), new LinkedList<Quadruple>()).first;
			
			/*if (env.FuncIdenlevel == 0) {
				if (exprAtt.isconst != 1)  error("global arraytype definition error");
				//if (!(t.arraySize instanceof IntConst) && !(t.arraySize instanceof CharConst))
				//	error("global arraytype definition error");
			}*/
			if (exprAtt.isconst != 1) error("arraytype subscript error");
			if (exprAtt.constvalue < 0) error("arraytype subscript negative");
			
			if (!(exprAtt.type instanceof IntType) && !(exprAtt.type instanceof CharType)) {
				error("array definition's subscript type error");
			}
			
			if (t.arraySize instanceof IntConst) {
				list.add(new Integer(((IntConst) t.arraySize).value));
			} else if (t.arraySize instanceof CharConst) {
				char[] valuearr = ((CharConst) t.arraySize).value.toCharArray();
				int v = (int) valuearr[0];
				list.add(new Integer(v));
			} else {
				initCheck = false;
			}
		}
		return list;
	}
	
	public void checkInitializer(Name name, Initializer init, Type type, List<Variable> vars, List<Quadruple> body, int offset, int ini) {
		if (init instanceof InitValue) {
			checkInitValue(name, (InitValue)init, type, vars, body, offset, ini);
		}
		if (init instanceof InitList) {
			checkInitList(name, (InitList)init, type, vars, body, offset, ini);
		}
	}
	
	public boolean checkInitAssign(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof ArrayType) {
			//return checkInitAssign(((ArrayType) t1).baseType, t2);
			Type type = getType(t1);
			if (type instanceof IntType) return checkInitAssign(type, t2);
			if (type instanceof CharType) {
				if (t2 instanceof IntType) return true;
				if (t2 instanceof CharType) return true;
				if (t2 instanceof PointerType) return true;
			}
			if (type instanceof PointerType)
				return checkInitAssign(type, t2);
			if (type instanceof StructType)
				return checkInitAssign(type, t2);
			if (type instanceof UnionType)
				return checkInitAssign(type, t2);
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public void checkInitValue(Name name, InitValue init, Type type, List<Variable> vars, List<Quadruple> body, int offset, int ini) {
		//if (init.expr instanceof IntConst) System.out.println("Int");
		Pairs<ExprAtt, Address> pair = checkExpr(init.expr, vars , body);
		ExprAtt exprAtt = pair.first;
		
		if (ini == 1) {
			//ArrayWrite aw = new ArrayWrite(name, new IntegerConst(offset), pair.second, getSize(type));
			Temp t = new Temp();
			Type baset = getType(type);
			
			if (baset instanceof StructType || baset instanceof UnionType || baset instanceof ArrayType)
				vars.add(new ArrayVariable("#t"+t.num, getSize(type)));
			else 
				vars.add(new BasicVariable("#t"+t.num, 4));
			
			AddressOf ao = new AddressOf(t, name);
			body.add(ao);
			
			ArithmeticExpr ae = new ArithmeticExpr(t, t, ArithmeticOp.ADD, new IntegerConst(offset));
			body.add(ae);
			
			MemoryWrite mw = new MemoryWrite(t, pair.second, getSize(type));
			if (type instanceof StructType || type instanceof UnionType || type instanceof ArrayType)
				mw.type = 1;
			body.add(mw);
		} else {
			if (pair.first.type instanceof StructType || pair.first.type instanceof UnionType) {
				int size = getSize(pair.first.type);
				
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				AddressOf ao = new AddressOf(tmp, name);
				body.add(ao);
				
				RecordAssign(size, tmp ,pair.second , vars, body);
			} else {
				Assign assign = new Assign(name, pair.second);
				body.add(assign);
			}
		}
		
		Type initType = exprAtt.type;
		if (type instanceof StructType) {
			if (!(initType instanceof StructType)) error("struct initializer error");
			if (initType instanceof StructType) {
				if (!checkSameType(initType, type)) {
					error("not the same struct");
				}
			}
		}
		if (type instanceof UnionType) {
			if (!(initType instanceof UnionType)) error("union initializer error");
			if (initType instanceof UnionType) {
				//if (!(((UnionType) initType).tag.equal(((UnionType) type).tag))) 
				if (!checkSameType(initType, type)) {
					error("not the same union");
				}
			}
		}
		if (initType == null || !checkInitAssign(type, initType)) {
			//System.out.println(checkAssign(type, initType));
			error("initializer value error");
		}
	}
	
	public void checkInitList(Name name, InitList init, Type type, List<Variable> vars, List<Quadruple> body, int offset, int ini) {
		//if (init.inits.size() != list.get(index)) error("initializer size error");
		int cnt = -1;
		Type baseType;
		for (Initializer i: init.inits) {
			cnt++;
			int index, baseSize;
			if (type instanceof ArrayType) {
				ArrayType t = (ArrayType) type;
				index = ((IntConst)t.arraySize).value;
				baseSize = t.size / index * t.baseSize;
				baseType = t.baseType;
			} else {
				index = 1;
				baseSize = 1;
				baseType = type;
			}
			if (cnt >= index) return;
			checkInitializer(name, i, baseType, vars, body, offset + baseSize * cnt, ini);
		}
	}
	
	public void checkCompoundStmt(CompoundStmt stmt, Type returnType, List<Variable> vars, List<Quadruple> body) {
		//List<Pairs<Name, Temp>> list = new LinkedList<Pairs<Name, Temp>>(); 
		for (Decl d: stmt.decls) {
			//checkFuncDecl(d, vars);
			if (d instanceof StructDecl) checkStructDecl((StructDecl) d);
			if (d instanceof UnionDecl) checkUnionDecl((UnionDecl) d);
			if (d instanceof VarDecl) {
				checkVarDecl((VarDecl) d, vars, body);
				//v.name = v.name + env.FuncIdenlevel;
				//vars.add(v);
			}
		}
		for (Stmt s: stmt.stats) {
			if (s instanceof CompoundStmt) {
				env.beginScope();
				checkCompoundStmt((CompoundStmt)s, returnType, vars, body);
				env.endScope();
			} else {
				checkStmt(s, returnType, vars, body);
			}
		}
	}
	
	public boolean isIntCharType(Type type) {
		if (type instanceof IntType || type instanceof CharType) return true;
		return false;
	}
	
	public RelationalOp transToRelop(BinaryOp op) {
		if (op == BinaryOp.EQ) return RelationalOp.EQ;
		if (op == BinaryOp.NE) return RelationalOp.NE;
		if (op == BinaryOp.GT) return RelationalOp.GT;
		if (op == BinaryOp.GE) return RelationalOp.GE;
		if (op == BinaryOp.LT) return RelationalOp.LT;
		if (op == BinaryOp.LE) return RelationalOp.LE;
		return null;
	}
	
	public void checkStmt(Stmt s, Type returnType, List<Variable> vars, List<Quadruple> body) {
		if (s == null) return;
		if (s instanceof BreakStmt) {
			if (isLoop == 0) error("BreakStmt's position is not right");
			body.add(new Goto(loopStack.peek().third));
		}
		if (s instanceof ContinueStmt) {
			if (isLoop == 0) error("ContinueStmt's position is not right");
			body.add(new Goto(loopStack.peek().second));
		}
		if (s instanceof IfStmt) {
			IfStmt ts = (IfStmt) s;
			//Pairs<ExprAtt, Address> pair = checkExpr(ts.condition, vars, body);
			//ExprAtt exprAtt = pair.first;
			
			Label falseLabel = new Label();
			Label endLabel = new Label();
			
			boolean done = false;
			if (ts.condition instanceof BinaryExpr) {
				BinaryExpr be = (BinaryExpr) ts.condition;
				RelationalOp ro = transToRelop(be.op);
				if (ro != null) {
					Pairs<ExprAtt, Address> pair1 = checkExpr(be.left, vars, body);
					Pairs<ExprAtt, Address> pair2 = checkExpr(be.right, vars, body);
					IfFalseGoto iff = new IfFalseGoto(pair1.second, ro, pair2.second, falseLabel);
					body.add(iff);
					/*Pairs<ExprAtt, Address> pair = checkExpr(ts.condition, vars, body);
					ExprAtt exprAtt = pair.first;
					IfTrueGoto ift = new IfTrueGoto(pair.second, RelationalOp.EQ, new IntegerConst(0), falseLabel);
					body.add(ift);*/
					done = true;
				}
			} 
			
			if (!done) {
				Pairs<ExprAtt, Address> pair = checkExpr(ts.condition, vars, body);
				ExprAtt exprAtt = pair.first;
				//if (pair.second == null) System.out.println("walalalalalal!!!");
				IfTrueGoto ift = new IfTrueGoto(pair.second, RelationalOp.EQ, new IntegerConst(0), falseLabel);
				body.add(ift);
			}
			
			env.beginScope();
			checkStmt(ts.consequent, returnType, vars, body);
			env.endScope();
			body.add(new Goto(endLabel));
			
			body.add(falseLabel);
			env.beginScope();
			checkStmt(ts.alternative, returnType, vars, body);
			env.endScope();
			body.add(endLabel);
		}
		if (s instanceof ReturnStmt) { // mark ques
			ReturnStmt ts = (ReturnStmt) s;
			Pairs<ExprAtt, Address> pair = checkExpr(ts.expr, vars, body);
			Param p;
			//p = new BasicParam(pair.second);
			if (pair.first.type instanceof IntType || pair.first.type instanceof CharType || pair.first.type instanceof PointerType)
				p = new BasicParam(pair.second);
			else {
				p = new MemoryParam(pair.second, getSize(pair.first.type));
			}
			Return r = new Return(p);
			body.add(r);
		}
		if (s instanceof CompoundStmt) {
			checkCompoundStmt((CompoundStmt) s, returnType, vars, body);
		}
		if (s instanceof Expr) {
			checkExpr((Expr)s, vars, body);
		}
		if (s instanceof ForLoop) {
			//System.out.println("hello");
			ForLoop ts = (ForLoop) s;
			env.beginScope();
			isLoop++;
			//ExprAtt exprAtt = null;
			//System.out.println("yes");
			
			Label beginLabel = new Label();
			Label continueLabel = new Label();
			Label endLabel = new Label();
			loopStack.add(new Triple<Label, Label, Label>(beginLabel, continueLabel, endLabel));
			
			checkExpr(ts.init, vars, body);
			body.add(beginLabel);
			
			boolean done = false;
			if (ts.condition instanceof BinaryExpr) {
				BinaryExpr be = (BinaryExpr) ts.condition;
				RelationalOp ro = transToRelop(be.op);
				if (ro != null) {
					Pairs<ExprAtt, Address> pair1 = checkExpr(be.left, vars, body);
					Pairs<ExprAtt, Address> pair2 = checkExpr(be.right, vars, body);
					IfFalseGoto iff = new IfFalseGoto(pair1.second, ro, pair2.second, endLabel);
					body.add(iff);
					done = true;
				}
			} 
			
			if (!done) {
				Address src1 = new IntegerConst(1);
				if (ts.condition != null) src1 = checkExpr(ts.condition, vars, body).second;
				Address src2 = new IntegerConst(0);
				IfFalseGoto iff = new IfFalseGoto(src1, RelationalOp.NE, src2, endLabel);
				body.add(iff);
			}
			
			checkStmt(ts.body, returnType, vars, body);
			body.add(continueLabel);

			checkExpr(ts.step, vars, body);
			body.add(new Goto(beginLabel));
			body.add(endLabel);
			
			isLoop--;
			env.endScope();
			loopStack.pop();
		}
		if (s instanceof WhileLoop) {
			WhileLoop ts = (WhileLoop) s;
			
			Label beginLabel = new Label();
			Label continueLabel = new Label();
			Label endLabel = new Label();
			
			env.beginScope();
			isLoop++;
			
			body.add(beginLabel);
			
			loopStack.add(new Triple<Label, Label, Label>(beginLabel, continueLabel, endLabel));
			
			boolean done = false;
			if (ts.condition instanceof BinaryExpr) {
				BinaryExpr be = (BinaryExpr) ts.condition;
				RelationalOp ro = transToRelop(be.op);
				if (ro != null) {
					Pairs<ExprAtt, Address> pair1 = checkExpr(be.left, vars, body);
					Pairs<ExprAtt, Address> pair2 = checkExpr(be.right, vars, body);
					IfFalseGoto iff = new IfFalseGoto(pair1.second, ro, pair2.second, endLabel);
					body.add(iff);
					done = true;
				}
			} 
			
			if (!done) {
				Pairs<ExprAtt, Address> pair = checkExpr(ts.condition, vars, body);
				ExprAtt exprAtt = pair.first;
				IfFalseGoto iff = new IfFalseGoto(pair.second, RelationalOp.NE, new IntegerConst(0), endLabel);
				body.add(iff);
			}
			
			checkStmt(ts.body, returnType, vars, body);
			body.add(continueLabel);
			body.add(new Goto(beginLabel));
			body.add(endLabel);
			
			isLoop--;
			env.endScope();
			loopStack.pop();
		}
	}
	
	public boolean isArithOp(BinaryOp op) {
		if (op == BinaryOp.ADD) return true;
		if (op == BinaryOp.LOGICAL_AND) return true;
		if (op == BinaryOp.SUB) return true;
		if (op == BinaryOp.MOD) return true;
		if (op == BinaryOp.DIV) return true;
		if (op == BinaryOp.MUL) return true;
		if (op == BinaryOp.LOGICAL_OR) return true;
		return false;
	}

	public int calcArithExpr(int x, int y, BinaryOp op) {
		if (op == BinaryOp.ADD) return x + y;
		if (op == BinaryOp.LOGICAL_AND) return x & y;
		if (op == BinaryOp.SUB) return x - y;
		if (op == BinaryOp.MOD) return x % y;
		if (op == BinaryOp.DIV) return x / y;
		if (op == BinaryOp.MUL) return x * y;
		if (op == BinaryOp.LOGICAL_OR) return x | y;
		return 0;
	}
	
	public Pairs<ExprAtt, Address> checkExpr(Expr expr, List<Variable> vars, List<Quadruple> body) {
		if (expr instanceof EmptyExpr) return new Pairs<ExprAtt, Address>(new ExprAtt(new VoidType(), 0, 0, 0), null);
		if (expr instanceof BinaryExpr) {
			/*List<Variable> v = new LinkedList<Variable>();
			List<Quadruple> b = new LinkedList<Quadruple>();
			BinaryExpr be = (BinaryExpr) expr;
			BinaryOp op = be.op;
			Pairs<ExprAtt, Address> pair1 = checkExpr(be.left, v, b);
			Pairs<ExprAtt, Address> pair2 = checkExpr(be.right, v, b);
			if (pair1.first.isconst == 1 && pair2.first.isconst == 1 && isArithOp(op)) {
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				int value = calcArithExpr(pair1.first.constvalue, pair2.first.constvalue, op);
				Assign assign = new Assign(tmp, new IntegerConst(value));
				body.add(assign);
				ExprAtt e = new ExprAtt(new IntType(), 0, 1, value);
				return new Pairs<ExprAtt, Address>(e, tmp);
			}*/
			return checkBinaryExpr((BinaryExpr)expr, vars, body);
		}
		if (expr instanceof UnaryExpr) {
			/*List<Variable> v = new LinkedList<Variable>();
			List<Quadruple> b = new LinkedList<Quadruple>();
			Pairs<ExprAtt, Address> pair = checkUnaryExpr((UnaryExpr)expr, v, b);
			if (pair.first.isconst == 1) {
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				Assign assign = new Assign(tmp, new IntegerConst(pair.first.constvalue));
				body.add(assign);
				ExprAtt e = new ExprAtt(new IntType(), 0, 1, pair.first.constvalue);
				return new Pairs<ExprAtt, Address>(e, tmp);
			}*/
			return checkUnaryExpr((UnaryExpr)expr, vars, body);
		}
		if (expr instanceof SizeofExpr) {
			//return checkSizeofExpr((SizeofExpr)expr);
			//printType(((SizeofExpr) expr).type);
			//printType(((SizeofExpr) expr).type);
			int size = getSize(((SizeofExpr) expr).type);
			IntegerConst intConst = new IntegerConst(size);
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			
			Assign assign = new Assign(tmp, intConst);
			ExprAtt exprAtt = new ExprAtt(new IntType(), 0, 1, size);
			body.add(assign);
			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (expr instanceof CastExpr) return checkCastExpr((CastExpr)expr, vars, body);
		if (expr instanceof PointerAccess) {
			Triple<ExprAtt, Address, Address> tri = checkPointerAccess((PointerAccess)expr, vars, body);
			if (tri.first.type instanceof StructType || tri.first.type instanceof UnionType) {
				return new Pairs<ExprAtt, Address>(tri.first, tri.second);
			}
			return new Pairs<ExprAtt, Address>(tri.first, tri.third);
		}
		if (expr instanceof RecordAccess) {
			Triple<ExprAtt, Address, Address> tri = checkRecordAccess((RecordAccess)expr, vars, body);
			if (tri.first.type instanceof StructType || tri.first.type instanceof UnionType) {
				return new Pairs<ExprAtt, Address>(tri.first, tri.second);
			}
			return new Pairs<ExprAtt, Address>(tri.first, tri.third);
		}
		if (expr instanceof SelfIncrement) {
			SelfIncrement si = (SelfIncrement) expr;
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>();
			Triple<ExprAtt, Address, Address> tri = null;
			if (si.body instanceof PointerAccess) {
				tri = checkPointerAccess((PointerAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof ArrayAccess) {
				tri = checkArrayAccess((ArrayAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof RecordAccess) {
				tri = checkRecordAccess((RecordAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof UnaryExpr && ((UnaryExpr) si.body).op == UnaryOp.ASTERISK) {
				UnaryExpr ue = (UnaryExpr) si.body;
				Pairs<ExprAtt, Address> p = checkExpr(ue.expr, vars, body);
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				MemoryRead mr = new MemoryRead(tmp, p.second, 4);
				body.add(mr);
				
				Temp tmp2 = new Temp();
				vars.add(new BasicVariable("#t"+tmp2.num, 4));
				Assign assign = new Assign(tmp2, tmp);
				body.add(assign);
				
				body.add(new ArithmeticExpr(tmp, tmp, ArithmeticOp.ADD, new IntegerConst(1)));
				MemoryWrite mw = new MemoryWrite(p.second, tmp, 4);
				body.add(mw);
				ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
				return new Pairs<ExprAtt, Address>(e, tmp2);
			}
			else pair = checkExpr(si.body, vars, body);
			
			ExprAtt exprAtt = pair.first;
			ExprAtt e = new ExprAtt(exprAtt.type, 0, 0, 0);
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			body.add(new Assign(tmp, pair.second));
			body.add(new ArithmeticExpr(pair.second, pair.second, ArithmeticOp.ADD, new IntegerConst(1)));
			
			if (tri != null) {
				MemoryWrite mw = new MemoryWrite(tri.second, pair.second, getSize(pair.first.type));
				body.add(mw);
			}
			
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr instanceof SelfDecrement) {
			SelfDecrement si = (SelfDecrement) expr;
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>();
			Triple<ExprAtt, Address, Address> tri = null;
			if (si.body instanceof PointerAccess) {
				tri = checkPointerAccess((PointerAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof ArrayAccess) {
				tri = checkArrayAccess((ArrayAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof RecordAccess) {
				tri = checkRecordAccess((RecordAccess) si.body, vars, body);
				pair.first = tri.first;
				pair.second = tri.third;
			}
			else if (si.body instanceof UnaryExpr && ((UnaryExpr) si.body).op == UnaryOp.ASTERISK) {
				UnaryExpr ue = (UnaryExpr) si.body;
				Pairs<ExprAtt, Address> p = checkExpr(ue.expr, vars, body);
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				MemoryRead mr = new MemoryRead(tmp, p.second, 4);
				body.add(mr);
				
				Temp tmp2 = new Temp();
				vars.add(new BasicVariable("#t"+tmp2.num, 4));
				Assign assign = new Assign(tmp2, tmp);
				body.add(assign);
				
				body.add(new ArithmeticExpr(tmp, tmp, ArithmeticOp.SUB, new IntegerConst(1)));
				MemoryWrite mw = new MemoryWrite(p.second, tmp, 4);
				body.add(mw);
				ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
				return new Pairs<ExprAtt, Address>(e, tmp2);
			}
			else pair = checkExpr(si.body, vars, body);
			
			ExprAtt exprAtt = pair.first;
			ExprAtt e = new ExprAtt(exprAtt.type, 0, 0, 0);
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			body.add(new Assign(tmp, pair.second));
			body.add(new ArithmeticExpr(pair.second, pair.second, ArithmeticOp.SUB, new IntegerConst(1)));
			
			if (tri != null) {
				MemoryWrite mw = new MemoryWrite(tri.second, pair.second, getSize(pair.first.type));
				body.add(mw);
			}
			
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr instanceof ArrayAccess) {
			Triple<ExprAtt, Address, Address> tri = checkArrayAccess((ArrayAccess)expr, vars, body);
			if (tri.first.type instanceof StructType || tri.first.type instanceof UnionType) {
				return new Pairs<ExprAtt, Address>(tri.first, tri.second);
			}
			return new Pairs<ExprAtt, Address>(tri.first, tri.third);
		}
		if (expr instanceof FunctionCall) {
			return checkFunctionCall((FunctionCall)expr, vars, body);
		}
		if (expr instanceof Identifier) {
			//System.out.println(((Identifier)expr).symbol.toString());
			return checkIdentifier((Identifier)expr, vars, body);
		}
		if (expr instanceof IntConst) {
			ExprAtt exprAtt = new ExprAtt(new IntType(), 0, 1, ((IntConst)expr).value);
			//System.out.println("hello");
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Assign assign = new Assign(tmp, new IntegerConst(((IntConst)expr).value));
			body.add(assign);

			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (expr instanceof CharConst) {
			char ch = ((CharConst)expr).value.charAt(0);
			if (ch == '\\') {
				char ch2 = ((CharConst)expr).value.charAt(1);
				if (ch2 == 'b') ch = '\b';
				if (ch2 == 'f') ch = '\f';
				if (ch2 == 'n') ch = '\n';
				if (ch2 == 'r') ch = '\r';
				if (ch2 == 't') ch = '\t';
				if (ch2 == '0') {
					if (((CharConst)expr).value.length() == 2) ch = '\0';
					else {
						int t1 = ((CharConst)expr).value.charAt(2)-'0';
						int t2 = ((CharConst)expr).value.charAt(3)-'0';
						ch = (char)(t1 * 8 + t2);
					}
				}
				if (ch2 == 'x') {
					ch = (char)hexNum(((CharConst)expr).value.charAt(2));
				}
			}
			ExprAtt exprAtt = new ExprAtt(new CharType(), 0, 1, (int)ch);
			
			//char[] valuearr = ((CharConst) expr).value.toCharArray();
			//int v = (int) valuearr[0];
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Assign assign = new Assign(tmp, new IntegerConst((int)ch));
			body.add(assign);
			
			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (expr instanceof StringConst) {
			ExprAtt exprAtt = new ExprAtt(new PointerType(new CharType()), 0, 1, 0);
			StringAddressConst s = new StringAddressConst(((StringConst)expr).value);;
			return new Pairs<ExprAtt, Address>(exprAtt, s);
		}
		return null;
	}
	
	public int hexNum(char c) {
		if (c >= 'A' && c <= 'Z') return 10 + (c-'A');
		if (c >= 'a' && c <= 'z') return 10 + (c-'a');
		return c-'0';
	}
	
	public int AccessOffset(Type type, String att) {
		if (type instanceof StructType) {
			StructType t = (StructType) type;
			Decl decl = env.getRecord(t.tag.toString());
			int offset = 0;
			for (Decl d: ((StructDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.type instanceof IntType) {
						if (offset % 4 != 0) {
							offset = (offset / 4 + 1) * 4;
						}
					}
					//System.out.println(vd.name+" "+att);
					if (vd.name.toString().equals(att)) {
						return offset;
					}
					//System.out.println("vd.type: "+getSize(vd.type));
					offset += getSize(vd.type);
				}
			}
		}
		if (type instanceof UnionType) {
			return 0;
		}
		//System.out.println("alala");
		return 0;
	}
	
	public void RecordAssign(int size, Address left, Address right, List<Variable> vars, List<Quadruple> body) {
		Temp ta = new Temp();
		vars.add(new BasicVariable("#t"+ta.num, 4));
		Assign assign = new Assign(ta, left);
		body.add(assign);
		Temp tc = new Temp();
		vars.add(new BasicVariable("#t"+tc.num, 4));
		Assign assign2 = new Assign(tc, right);
		body.add(assign2);
		Temp t = new Temp();
		vars.add(new BasicVariable("#t"+t.num, 4));
		
		if (size % 4 != 0) {
			MemoryRead mr = new MemoryRead(t, tc, size % 4);
			body.add(mr);
			MemoryWrite mw = new MemoryWrite(ta, t, size % 4);
			body.add(mw);
			ArithmeticExpr ae1 = new ArithmeticExpr(ta, ta, ArithmeticOp.ADD, new IntegerConst(size % 4));
			body.add(ae1);
			ArithmeticExpr ae2 = new ArithmeticExpr(tc, tc, ArithmeticOp.ADD, new IntegerConst(size % 4));
			body.add(ae2);
		}
		
		size -= size%4;
		
		Temp count = new Temp();
		vars.add(new BasicVariable("#t"+count.num, 4));
		Assign init = new Assign(count, new IntegerConst(size));
		body.add(init);
		
		Label startLabel = new Label();
		body.add(startLabel);
		Label endLabel = new Label();
		IfTrueGoto ift = new IfTrueGoto(count, RelationalOp.EQ, new IntegerConst(0), endLabel);
		body.add(ift);
		
		MemoryRead mr = new MemoryRead(t, tc, 4);
		body.add(mr);
		MemoryWrite mw = new MemoryWrite(ta, t, 4);
		body.add(mw);
		ArithmeticExpr ae1 = new ArithmeticExpr(ta, ta, ArithmeticOp.ADD, new IntegerConst(4));
		body.add(ae1);
		ArithmeticExpr ae2 = new ArithmeticExpr(tc, tc, ArithmeticOp.ADD, new IntegerConst(4));
		body.add(ae2);
		ArithmeticExpr ae = new ArithmeticExpr(count, count, ArithmeticOp.SUB, new IntegerConst(4));
		body.add(ae);
		
		body.add(new Goto(startLabel));
		body.add(endLabel);
	}
	
	/*public Pairs<ExprAtt, Address> getAddress(Expr expr, List<Variable> vars, List<Quadruple> body) {
		if (expr instanceof UnaryExpr) {
			UnaryExpr ue = (UnaryExpr) expr;
			if (ue.op == UnaryOp.ASTERISK) {
				return checkExpr(ue.expr, vars, body);
			}
		}
		if (expr instanceof PointerAccess) {
			return checkPointerAccessIr((PointerAccess)expr, vars, body);
		}
		if (expr instanceof RecordAccess) {
			return checkRecordAccessIr((RecordAccess)expr, vars, body);
		}
		if (expr instanceof ArrayAccess) {
			return checkArrayAccessIr((ArrayAccess)expr, vars, body);
		}
		if (expr instanceof Identifier) {
			return checkIdentifier((Identifier)expr, vars, body);
		}
		if (expr instanceof )
	}*/
	
	public Pairs<ExprAtt, Address> checkAssignIr(Expr expr1, Expr expr2, List<Variable> vars, List<Quadruple> body, Pairs<ExprAtt, Address> p) {
		Pairs<ExprAtt, Address> pair;
		if (p == null) pair = checkExpr(expr2, vars, body);
		else pair = p;
		/*if (pair.first.type instanceof StructType || pair.first.type instanceof UnionType) {
			pair = getAddress(expr2, vars, body);
		}*/
		if (expr1 instanceof UnaryExpr) {
			UnaryExpr ue = (UnaryExpr) expr1;
			if (ue.op == UnaryOp.ASTERISK) {
				Pairs<ExprAtt, Address> pairLeft = checkExpr(ue.expr, vars, body);
				
				ExprAtt exprAtt = null;
				Type type = null;
				if (pairLeft.first.type instanceof PointerType) {
					PointerType pt = (PointerType) pairLeft.first.type;
					exprAtt = new ExprAtt(pt.baseType, 1, 0, 0);
					type = pt.baseType;
				}
				if (pairLeft.first.type instanceof ArrayType) {
					ArrayType at = (ArrayType) pairLeft.first.type;
					exprAtt = new ExprAtt(at.baseType, 1, 0, 0);
					type = at.baseType;
				}
				
				if (pair.first.type instanceof StructType || pair.first.type instanceof UnionType) {
					RecordAssign(getSize(pair.first.type), pairLeft.second, pair.second, vars, body);
				}
				else {
					MemoryWrite mw = new MemoryWrite(pairLeft.second, pair.second, min(getSize(pair.first.type), getSize(type)));
					body.add(mw);
				}
				
				Temp tmp = new Temp();
				if (pair.first.type instanceof StructType || pair.first.type instanceof UnionType) {
					vars.add(new ArrayVariable("#t"+tmp.num, getSize(pair.first.type)));
				} else {
					vars.add(new BasicVariable("#t"+tmp.num, 4));
				}
				MemoryRead mr = new MemoryRead(tmp, pairLeft.second, min(getSize(pair.first.type), getSize(type)));
				body.add(mr);
				
				return new Pairs<ExprAtt, Address>(exprAtt, tmp);
			}
		}
		if (expr1 instanceof PointerAccess) {
			PointerAccess pa = (PointerAccess) expr1;
			Pairs<ExprAtt, Address> pairLeft = checkPointerAccessIr(pa, vars, body);
			
			if (pairLeft.first.type instanceof StructType || pairLeft.first.type instanceof UnionType) {
				RecordAssign(getSize(pairLeft.first.type), pairLeft.second, pair.second, vars, body);
			} else {
				MemoryWrite mw = new MemoryWrite(pairLeft.second, pair.second, min(getSize(pair.first.type), getSize(pairLeft.first.type)));
				body.add(mw);
			}
			
			//Type type = ((PointerType)pairLeft.first.type).baseType;
			//int offset = AccessOffset(type, pa.attribute.toString());
			//Temp tmp = new Temp();
			//vars.add(new BasicVariable("#t"+tmp.num, 4));
			//ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pairLeft.second, ArithmeticOp.ADD, new IntegerConst(offset));
			//body.add(arithExpr);
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			MemoryRead mr = new MemoryRead(tmp, pairLeft.second, min(4, getSize(pair.first.type)));
			body.add(mr);
			
			return new Pairs<ExprAtt, Address>(pairLeft.first, tmp);
		}
		if (expr1 instanceof RecordAccess) {
			RecordAccess ra = (RecordAccess) expr1;
			Pairs<ExprAtt, Address> pairLeft = checkRecordAccessIr(ra, vars, body);
			Type type = pairLeft.first.type;
			//int offset = AccessOffset(type, ra.attribute.toString());
			//System.out.println(ra.attribute.toString()+" "+offset);
			//ArrayWrite aw = new ArrayWrite(pairLeft.second, new IntegerConst(offset), pair.second, getSize(pair.first.type));
			if (pairLeft.first.type instanceof StructType || pairLeft.first.type instanceof UnionType) {
				RecordAssign(getSize(pairLeft.first.type), pairLeft.second, pair.second, vars, body);
			}
			else {
				MemoryWrite mw = new MemoryWrite(pairLeft.second, pair.second, min(getSize(pair.first.type), getSize(pairLeft.first.type)));
				body.add(mw);
			}
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			MemoryRead ar = new MemoryRead(tmp, pairLeft.second, min(4, getSize(pair.first.type)));
			body.add(ar);
			return new Pairs<ExprAtt, Address>(pairLeft.first, tmp);
		}
		if (expr1 instanceof ArrayAccess) {
			ArrayAccess aa = (ArrayAccess) expr1;
			Pairs<ExprAtt, Address> pairArr = checkArrayAccessIr(aa, vars, body);
			
			if (pairArr.first.type instanceof StructType || pairArr.first.type instanceof UnionType) {
				RecordAssign(getSize(pairArr.first.type), pairArr.second, pair.second, vars, body);
			}
			else {
				MemoryWrite mw = new MemoryWrite(pairArr.second, pair.second, min(getSize(pair.first.type), getSize(pairArr.first.type)));
				body.add(mw);
			}
			
			ExprAtt exprAtt = new ExprAtt(pairArr.first.type, 0, 0, 0);
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			MemoryRead mr = new MemoryRead(tmp, pairArr.second, min(4, getSize(pairArr.first.type)));
			body.add(mr);
			
			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (expr1 instanceof Identifier) {
			Pairs<ExprAtt, Address> pairLeft = checkIdentifier((Identifier)expr1, vars, body);
			if (pairLeft.first.type instanceof StructType || pairLeft.first.type instanceof UnionType) {
				//StructType type = (StructType) pairLeft.first.type;
				int size = getSize(pairLeft.first.type);
				RecordAssign(size, pairLeft.second, pair.second, vars, body);
			}
			else {
				Assign assign = new Assign(pairLeft.second, pair.second);
				body.add(assign);
			}
			return pairLeft;
		}
		return null;
	}
	
	public Pairs<ExprAtt, Address> checkBinaryExpr(BinaryExpr expr, List<Variable> vars, List<Quadruple> body) { // mark ques < >
		
		if (expr.op == BinaryOp.ASSIGN) {
			//Assign arithExpr = new Assign(pair1.second, pair2.second);
			//body.add(arithExpr);
			//return new Pairs<ExprAtt, Address>(e, pair1.second);
			return checkAssignIr(expr.left, expr.right, vars, body, null);
		}
		
		if (expr.op == BinaryOp.AND) {
			Pairs<ExprAtt, Address> pair1 = checkExpr(expr.left, vars, body);
			Label endLabel = new Label();
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Assign assign = new Assign(tmp, new IntegerConst(0));
			body.add(assign);
			
			IfTrueGoto ift = new IfTrueGoto(pair1.second, RelationalOp.EQ, new IntegerConst(0), endLabel);
			body.add(ift);
			Pairs<ExprAtt, Address> pair2 = checkExpr(expr.right, vars, body);
			ift = new IfTrueGoto(pair2.second, RelationalOp.EQ, new IntegerConst(0), endLabel);
			body.add(ift);
			assign = new Assign(tmp, new IntegerConst(1));
			body.add(assign);
			body.add(endLabel);
			
			ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr.op == BinaryOp.OR) {
			Pairs<ExprAtt, Address> pair1 = checkExpr(expr.left, vars, body);
			Label endLabel = new Label();
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Assign assign = new Assign(tmp, new IntegerConst(1));
			body.add(assign);
			
			IfTrueGoto ift = new IfTrueGoto(pair1.second, RelationalOp.EQ, new IntegerConst(1), endLabel);
			body.add(ift);
			Pairs<ExprAtt, Address> pair2 = checkExpr(expr.right, vars, body);
			ift = new IfTrueGoto(pair2.second, RelationalOp.EQ, new IntegerConst(1), endLabel);
			body.add(ift);
			assign = new Assign(tmp, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			
			ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		
		
		Pairs<ExprAtt, Address> pair1 = checkExpr(expr.left, vars, body);
		Pairs<ExprAtt, Address> pair2 = checkExpr(expr.right, vars, body);
		ExprAtt e = checkBinaryOp(pair1.first, pair2.first, expr.op);
		if (expr.op == BinaryOp.COMMA) {
			return pair2;
		}
		if (expr.op == BinaryOp.ADD) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			if (pair1.first.type instanceof PointerType) {
				Temp t2 = new Temp();
				vars.add(new BasicVariable("#t"+t2.num, 4));
				ArithmeticExpr ae = new ArithmeticExpr(t2, pair2.second, ArithmeticOp.MUL, new IntegerConst(4));
				body.add(ae);
				ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.ADD, t2);
				body.add(arithExpr);
			}
			else if (pair2.first.type instanceof PointerType) {
				Temp t2 = new Temp();
				vars.add(new BasicVariable("#t"+t2.num, 4));
				ArithmeticExpr ae = new ArithmeticExpr(t2, pair1.second, ArithmeticOp.MUL, new IntegerConst(4));
				body.add(ae);
				ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair2.second, ArithmeticOp.ADD, t2);
				body.add(arithExpr);
			} else {
				ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.ADD, pair2.second);
				body.add(arithExpr);
			}
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.SUB) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			
			if (pair1.first.type instanceof PointerType) {
				Temp t2 = new Temp();
				vars.add(new BasicVariable("#t"+t2.num, 4));
				ArithmeticExpr ae = new ArithmeticExpr(t2, pair2.second, ArithmeticOp.MUL, new IntegerConst(4));
				body.add(ae);
				ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.ADD, t2);
				body.add(arithExpr);
			} else {
				ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.SUB, pair2.second);
				body.add(arithExpr);
			}
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.MUL) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.MUL, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.DIV) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.DIV, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.MOD) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.MOD, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.SHL) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.SHL, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.SHR) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.SHR, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.LOGICAL_AND) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.AND, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.LOGICAL_OR) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.OR, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.XOR) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(t, pair1.second, ArithmeticOp.XOR, pair2.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (expr.op == BinaryOp.ASSIGN_MUL) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.MUL, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_DIV) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.DIV, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_MOD) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.MOD, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_ADD) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.ADD, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_SUB) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.SUB, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_SHL) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.SHL, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_SHR) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.SHR, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_AND) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.AND, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_OR) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.OR, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		if (expr.op == BinaryOp.ASSIGN_XOR) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair1.second, ArithmeticOp.XOR, pair2.second);
			body.add(arithExpr);
			Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>(e, tmp);
			return checkAssignIr(expr.left, expr.right, vars, body, pair);
		}
		
		//Relational Op
		
		if (expr.op == BinaryOp.EQ) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.EQ, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		if (expr.op == BinaryOp.NE) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.NE, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		if (expr.op == BinaryOp.GE) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.GE, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		if (expr.op == BinaryOp.GT) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.GT, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		if (expr.op == BinaryOp.LE) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.LE, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		if (expr.op == BinaryOp.LT) {
			Label l = new Label();
			Label endLabel = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.LT, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(new Goto(endLabel));
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
			body.add(endLabel);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		/*if (expr.op == BinaryOp.LOGICAL_AND) {
			Label l = new Label();
			IfFalseGoto iff = new IfFalseGoto(pair1.second, RelationalOp.AND, pair2.second, l);
			body.add(iff);
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Assign assign = new Assign(t, new IntegerConst(1));
			body.add(assign);
			body.add(l);
			assign = new Assign(t, new IntegerConst(0));
			body.add(assign);
		}*/
		
		return new Pairs<ExprAtt, Address>(e, null);
	}
	
	public boolean checkAssign(Type t1, Type t2){
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignOp(Type t1, Type t2) { // except assign_sub assign_add
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return true;
			if (t2 instanceof IntType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof PointerType) return false;
			if (t2 instanceof ArrayType) return false;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignAdd(Type t1, Type t2) {//assign_add
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType){
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return false;
			if (t2 instanceof ArrayType) return false;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkAssignSub(Type t1, Type t2) {//assign_sub
		if (t1 instanceof ArrayType) return false;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
		}
		if (t1 instanceof PointerType){
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType || t2 instanceof ArrayType)
				if (checkSameType(t1, t2)) return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public boolean checkLogicalOp(Type t1, Type t2) {
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof IntType || t1 instanceof CharType || t1 instanceof PointerType || t1 instanceof ArrayType)
			if (t2 instanceof IntType || t2 instanceof CharType || t2 instanceof PointerType || t2 instanceof ArrayType)
				return true;
		return false;
	}
	
	public boolean checkSameType(Type t1, Type t2) { // PointerType ArrayType
		if (t1.getClass() != t2.getClass()) return false;
		if (t1 instanceof PointerType) {
			boolean b = checkSameType(((PointerType) t1).baseType, ((PointerType) t2).baseType);
			if (!b) return false;
		}
		if (t1 instanceof ArrayType) {
			boolean b = checkSameType(((ArrayType) t1).baseType, ((ArrayType) t2).baseType);
			if (!b) return false;
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (((StructType) t1).level != ((StructType) t2).level) return false;
				if (!((StructType) t1).tag.equal(((StructType) t2).tag)) return false;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (((UnionType) t1).level != ((UnionType) t2).level) return false;
				if (!((UnionType) t1).tag.equal(((UnionType) t2).tag)) return false;
			}
		}
		return true;
	}
	
	public Type checkSubOp(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
		}
		if (t1 instanceof PointerType || t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType || t2 instanceof ArrayType)
				if (checkSameType(t1, t2)) return new IntType();
		}
		//if (t1.getClass() == t2.getClass()) return t1;
		return null;
	}
	
	public Type checkAddOp(Type t1, Type t2) {
		//if (t1.getClass() == t2.getClass()) return t1;
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType) return t2;
			if (t2 instanceof ArrayType) return t2;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
			if (t2 instanceof PointerType) return t2;
			if (t2 instanceof ArrayType) return t2;
		}
		if (t1 instanceof PointerType || t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof PointerType || t2 instanceof ArrayType) return null;
		}
		//if (t1.getClass() == t2.getClass()) return t1;
		return null;
	}
	
	public Type checkMDMOp(Type t1, Type t2) {// MUL DIV MOD
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof CharType) return t1;
			if (t2 instanceof IntType) return t2;
		}
		return null;
		//if (t1.getClass() == t2.getClass()) return t1;
	}
	
	public Type checkBitOp(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return t1;
			if (t2 instanceof CharType) return t1;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return t2;
			if (t2 instanceof CharType) return t1;
		}
		return null;
	}
	
	public ExprAtt checkBinaryOp(ExprAtt e1, ExprAtt e2, BinaryOp op) {
		if (op == BinaryOp.COMMA) {
			return new ExprAtt(e2.type, e2.lvalue, e2.isconst, e2.constvalue);
		}
		
		// ASSIGN Operation
		if (op == BinaryOp.ASSIGN) {
			if (e1.lvalue == 0) error("assign lvalue error");
			if (!checkAssign(e1.type, e2.type)) error("assign error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_MUL) {
			if (e1.lvalue == 0) error("assign_mul lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_mul error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_DIV) {
			if (e1.lvalue == 0) error("assign_div lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_div error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_MOD) {
			if (e1.lvalue == 0) error("assign_mod lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_mod error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_ADD) {
			if (e1.lvalue == 0) error("assign_add lvalue error");
			if (!checkAssignAdd(e1.type, e2.type)) error("assign_add error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SUB) {
			if (e1.lvalue == 0) error("assign_sub lvalue error");
			if (!checkAssignSub(e1.type, e2.type)) error("assign_sub error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SHL) {
			if (e1.lvalue == 0) error("assign_shl lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_shl error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_SHR) {
			if (e1.lvalue == 0) error("assign_shr lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_shr error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_AND) {
			if (e1.lvalue == 0) error("assign_and lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_and error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_XOR) {
			if (e1.lvalue == 0) error("assign_xor lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_xor error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		if (op == BinaryOp.ASSIGN_OR) {
			if (e1.lvalue == 0) error("assign_or lvalue error");
			if (!checkAssignOp(e1.type, e2.type)) error("assign_or error");
			return new ExprAtt(e1.type, 0, 0, 0);
		}
		
		// LOGICAL Operation
		
		if (op == BinaryOp.LOGICAL_OR) {
			if (!checkLogicalOp(e1.type, e2.type)) error("logical_or error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue!=0) || (e2.constvalue!=0)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LOGICAL_AND) {
			if (!checkLogicalOp(e1.type, e2.type)) error("logical_and error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue!=0) && (e2.constvalue!=0)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.EQ) {
			if (!checkLogicalOp(e1.type, e2.type)) error("eq error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) == (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.NE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("ne error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) != (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LT) {
			if (!checkLogicalOp(e1.type, e2.type)) error("lt error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) < (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.GT) {
			if (!checkLogicalOp(e1.type, e2.type)) error("gt error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) > (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.LE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("le error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) <= (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.GE) {
			if (!checkLogicalOp(e1.type, e2.type)) error("ge error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				if ((e1.constvalue) >= (e2.constvalue)) value = 1;
				else value = 0;
			return new ExprAtt(new IntType(), 0, e1.isconst & e2.isconst, value);
		}
		
		// NUMBER Operation
		if (op == BinaryOp.ADD) {
			Type t = checkAddOp(e1.type, e2.type);
			if (t == null) error("add error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue + e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SUB) {
			Type t = checkSubOp(e1.type, e2.type);
			if (t == null) error("sub error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue - e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.MUL) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("mul error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0)
				value = e1.constvalue * e2.constvalue;
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.MOD) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("mod error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				if (e2.constvalue == 0) error("divide 0");
				value = e1.constvalue % e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.DIV) {
			Type t = checkMDMOp(e1.type, e2.type);
			if (t == null) error("div error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				if (e2.constvalue == 0) error("divide 0");
				value = e1.constvalue / e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		
		//OR AND XOR SHL SHR
		if (op == BinaryOp.OR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("or error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue | e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.AND) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("and error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue & e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.XOR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("xor error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue ^ e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SHL) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("shl error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue << e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		if (op == BinaryOp.SHR) {
			Type t = checkBitOp(e1.type, e2.type);
			if (t == null) error("shr error");
			int value = 0;
			if ((e1.isconst & e2.isconst)!=0) {
				value = e1.constvalue >> e2.constvalue;
			}
			return new ExprAtt(t, 0, e1.isconst & e2.isconst, value);
		}
		
		return null;
	}
	
	public Pairs<ExprAtt, Address> checkUnaryExpr(UnaryExpr expr, List<Variable> vars, List<Quadruple> body) {
		if (expr.op == UnaryOp.INC) {
			//ArithmeticExpr arithExpr = new ArithmeticExpr(pair.second, pair.second, ArithmeticOp.ADD, new IntegerConst(1));
			//body.add(arithExpr);
			//return new Pairs<ExprAtt, Address>(e, pair.second);
			Pairs<ExprAtt, Address> pair2 = new Pairs<ExprAtt, Address>();
			Triple<ExprAtt, Address, Address> tri = null;
			if (expr.expr instanceof PointerAccess) {
				tri = checkPointerAccess((PointerAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof ArrayAccess) {
				tri = checkArrayAccess((ArrayAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof RecordAccess) {
				tri = checkRecordAccess((RecordAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof UnaryExpr && ((UnaryExpr) expr.expr).op == UnaryOp.ASTERISK) {
				UnaryExpr ue = (UnaryExpr) expr.expr;
				Pairs<ExprAtt, Address> p = checkExpr(ue.expr, vars, body);
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				MemoryRead mr = new MemoryRead(tmp, p.second, 4);
				body.add(mr);
				body.add(new ArithmeticExpr(tmp, tmp, ArithmeticOp.ADD, new IntegerConst(1)));
				MemoryWrite mw = new MemoryWrite(p.second, tmp, 4);
				body.add(mw);
				ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
				return new Pairs<ExprAtt, Address>(e, tmp);
			} else pair2 = checkExpr(expr.expr, vars, body);
			
			ExprAtt exprAtt = pair2.first;
			ExprAtt e = new ExprAtt(exprAtt.type, 0, 0, 0);
			
			body.add(new ArithmeticExpr(pair2.second, pair2.second, ArithmeticOp.ADD, new IntegerConst(1)));
			
			if (tri != null) {
				MemoryWrite mw = new MemoryWrite(tri.second, pair2.second, getSize(pair2.first.type));
				body.add(mw);
			}
			
			return new Pairs<ExprAtt, Address>(e, pair2.second);
		}
		if (expr.op == UnaryOp.DEC) {
			//ArithmeticExpr arithExpr = new ArithmeticExpr(pair.second, pair.second, ArithmeticOp.SUB, new IntegerConst(1));
			//body.add(arithExpr);
			//return new Pairs<ExprAtt, Address>(e, pair.second);
			Pairs<ExprAtt, Address> pair2 = new Pairs<ExprAtt, Address>();
			Triple<ExprAtt, Address, Address> tri = null;
			if (expr.expr instanceof PointerAccess) {
				tri = checkPointerAccess((PointerAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof ArrayAccess) {
				tri = checkArrayAccess((ArrayAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof RecordAccess) {
				tri = checkRecordAccess((RecordAccess) expr.expr, vars, body);
				pair2.first = tri.first;
				pair2.second = tri.third;
			}
			else if (expr.expr instanceof UnaryExpr && ((UnaryExpr) expr.expr).op == UnaryOp.ASTERISK) {
				UnaryExpr ue = (UnaryExpr) expr.expr;
				Pairs<ExprAtt, Address> p = checkExpr(ue.expr, vars, body);
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				MemoryRead mr = new MemoryRead(tmp, p.second, 4);
				body.add(mr);
				body.add(new ArithmeticExpr(tmp, tmp, ArithmeticOp.SUB, new IntegerConst(1)));
				MemoryWrite mw = new MemoryWrite(p.second, tmp, 4);
				body.add(mw);
				ExprAtt e = new ExprAtt(new IntType(), 0, 0, 0);
				return new Pairs<ExprAtt, Address>(e, tmp);
			}
			else pair2 = checkExpr(expr.expr, vars, body);
			
			ExprAtt exprAtt = pair2.first;
			ExprAtt e = new ExprAtt(exprAtt.type, 0, 0, 0);
			
			body.add(new ArithmeticExpr(pair2.second, pair2.second, ArithmeticOp.SUB, new IntegerConst(1)));
			
			if (tri != null) {
				MemoryWrite mw = new MemoryWrite(tri.second, pair2.second, getSize(pair2.first.type));
				body.add(mw);
			}
			
			return new Pairs<ExprAtt, Address>(e, pair2.second);
		}
		
		if (expr.op == UnaryOp.AMPERSAND) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Pairs<ExprAtt, Address> p = new Pairs<ExprAtt, Address>();
			if (expr.expr instanceof Identifier) {
				p = checkIdentifier((Identifier)expr.expr, vars, body);
				if (p.second instanceof Name) {
					AddressOf adrs = new AddressOf(tmp, (Name)p.second);
					body.add(adrs);
				} else {
					Assign assign = new Assign(tmp, p.second);
					body.add(assign);
				}
			} else if (expr.expr instanceof ArrayAccess) {
				return checkArrayAccessIr((ArrayAccess)expr.expr, vars, body);
			} else if (expr.expr instanceof PointerAccess) {
				return checkPointerAccessIr((PointerAccess)expr.expr, vars, body);
			} else if (expr.expr instanceof RecordAccess) {
				return checkRecordAccessIr((RecordAccess)expr.expr, vars, body);
			} else p = checkExpr(expr.expr, vars, body);
			ExprAtt exp = new ExprAtt(new PointerType(p.first.type), 0, 1, 0);
			return new Pairs<ExprAtt, Address>(exp, tmp);
		}
		
		Pairs<ExprAtt, Address> pair = checkExpr(expr.expr, vars, body);
		ExprAtt e = checkUnaryOp(pair.first, expr.op);
		if (expr.op == UnaryOp.SIZEOF) {
			//printType(e.type);
			int size = getSize(pair.first.type);
			IntegerConst i = new IntegerConst(size);
			return new Pairs<ExprAtt, Address>(e, i);
		}
		if (expr.op == UnaryOp.ASTERISK) {
			if (e.type instanceof StructType || e.type instanceof UnionType) {
				return new Pairs<ExprAtt, Address>(e, pair.second);
			}
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			MemoryRead pw = new MemoryRead(tmp, pair.second, min(4,getSize(e.type)));
			body.add(pw);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr.op == UnaryOp.MINUS) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, ArithmeticOp.MINUS, pair.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr.op == UnaryOp.TILDE) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, ArithmeticOp.TILDE, pair.second);
			body.add(arithExpr);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr.op == UnaryOp.PLUS) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			Assign assign = new Assign(tmp, pair.second);
			body.add(assign);
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		if (expr.op == UnaryOp.NOT) {
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));

			Assign assign = new Assign(tmp, new IntegerConst(1));
			body.add(assign);
			
			Label l = new Label();
			IfTrueGoto ift = new IfTrueGoto(pair.second, RelationalOp.EQ, new IntegerConst(0), l);
			body.add(ift);
			assign = new Assign(tmp, new IntegerConst(0));
			body.add(assign);
			body.add(l);
			
			return new Pairs<ExprAtt, Address>(e, tmp);
		}
		return new Pairs<ExprAtt, Address>(e, null);
	}
	
	public Type checkIncDecUnaryOp(Type t) {
		if (t instanceof IntType) return t;
		if (t instanceof CharType) return t;
		if (t instanceof PointerType) return t;
		return null;
	}
	
	public Type checkAsterisk(Type t) {
		if (t instanceof PointerType) {
			return ((PointerType) t).baseType;
		}
		if (t instanceof ArrayType) {
			return ((ArrayType) t).baseType;
		}
		return null;
	}
	
	public Type checkPMTUnaryOp(Type t) { //PLUS MINUS TILDE
		if (t instanceof IntType) {
			return new IntType();
		}
		if (t instanceof CharType) {
			return new IntType();
		}
		return null;
	}
	
	public Type checkNOTUnaryOp(Type t) {
		if (t instanceof IntType) return new IntType();
		if (t instanceof CharType) return new IntType();
		if (t instanceof PointerType) return new IntType();
		if (t instanceof ArrayType) return new IntType();
		return null;
	}
	
	public ExprAtt checkUnaryOp(ExprAtt e, UnaryOp op) {
		if (op == UnaryOp.INC) {
			if (e.lvalue == 0) error("unary inc lvalue error");
			Type type = checkIncDecUnaryOp(e.type);
			if (type == null) error("unary inc error");
			return new ExprAtt(type, 0, 0, 0);
		}
		if (op == UnaryOp.DEC) {
			if (e.lvalue == 0) error("unary dec lvalue error");
			Type type = checkIncDecUnaryOp(e.type);
			if (type == null) error("unary dec error");
			return new ExprAtt(type, 0, 0, 0);
		}
		if (op == UnaryOp.SIZEOF) {
			int value = getSize(e.type);
			//if (e.type instanceof IntType) value = 4;
			//if (e.type instanceof CharType) value = 1;
			return new ExprAtt(new IntType(), 0, 1, value);
		}
		if (op == UnaryOp.AMPERSAND) {
			//System.out.println("functionCall returns "+e.lvalue);
			if (e.lvalue == 0 && !(e.type instanceof ArrayType) && !(e.type instanceof PointerType))
				error("unary ampersand lvalue error");
			return new ExprAtt(new PointerType(e.type), 0, 1, 0);
		}
		if (op == UnaryOp.ASTERISK) {
			if (e.lvalue == 0 && !(e.type instanceof ArrayType) && !(e.type instanceof PointerType))
				error("unary asterisk lvalue error");
			Type type = checkAsterisk(e.type);
			//int lv;
			if (type == null) error("unary asterisk error");
			//if (!(type instanceof PointerType)) lv = 1;
			//else lv = 0;
			return new ExprAtt(type, 1, 0, 0);
		}
		if (op == UnaryOp.PLUS) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary plus error");
			int value = 0;
			if (e.isconst == 1) value = e.constvalue;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.MINUS) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary minus error");
			int value = 0;
			if (e.isconst == 1) value = -e.constvalue;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.TILDE) {
			Type type = checkPMTUnaryOp(e.type);
			if (type == null) error("unary tilde error");
			int value = 0;
			if (e.isconst == 1) value = ~(e.constvalue);
			return new ExprAtt(type, 0, e.isconst, value);
		}
		if (op == UnaryOp.NOT) {
			Type type = checkNOTUnaryOp(e.type);
			if (type == null) error("unary not error");
			int value = 0;
			if (e.isconst == 1)
				if (e.isconst != 0) value = 0;
				else value = 1;
			return new ExprAtt(type, 0, e.isconst, value);
		}
		return null;
	}
	
	public Pairs<ExprAtt, Address> checkIdentifier(Identifier expr, List<Variable> vars, List<Quadruple> body) {
		//System.out.println("hello");
		Decl decl = env.getDecl(expr.symbol.toString());
		Type type;
		ExprAtt exprAtt = null;
		int level = env.getFuncIdenBinder(expr.symbol.toString()).level;

		Name name = new Name(expr.symbol.toString()+level);
		
		if (decl instanceof StructDecl) {
			type = new StructType(((StructDecl) decl).tag);
			((StructType) type).level = ((StructDecl) decl).level;
			exprAtt = new ExprAtt(type, 1, 0, 0);
			
			//System.out.println("struct has been visited");
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			body.add(new AddressOf(tmp, name));
			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (decl instanceof UnionDecl) {
			type = new UnionType(((UnionDecl) decl).tag);
			type = new UnionType(((UnionDecl) decl).tag);
			((UnionType) type).level = ((UnionDecl) decl).level;
			exprAtt = new ExprAtt(type, 1, 0, 0);
			
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			body.add(new AddressOf(tmp, name));
			return new Pairs<ExprAtt, Address>(exprAtt, tmp);
		}
		if (decl instanceof FunctionDecl) {
			error(((Identifier)expr).symbol.toString()+" is not an id name");
		}
		if (decl instanceof VarDecl) {
			type = ((VarDecl) decl).type;
			/*if (type instanceof StructType)
				System.out.println(expr.symbol+" : "+((StructType)type).level);
			if (type instanceof UnionType)
				System.out.println(expr.symbol+" : "+((UnionType)type).level);*/
			if (type instanceof ArrayType || type instanceof StructType || type instanceof UnionType) {
				Temp tmp = new Temp();
				vars.add(new BasicVariable("#t"+tmp.num, 4));
				body.add(new AddressOf(tmp, name));
				exprAtt = new ExprAtt(type, 0, 1, 0);
				return new Pairs<ExprAtt, Address>(exprAtt, tmp);
			}
			if (type instanceof ArrayType) exprAtt = new ExprAtt(type, 0, 1, 0);
			else exprAtt = new ExprAtt(type, 1, 0, 0);
		}
		
		/*Temp tmp1 = new Temp();
		if (globalMap.containsKey(name.name)) {
			Variable v = globalMap.get(name.name);
			if (v instanceof BasicVariable) {
				vars.add(new BasicVariable("#t"+tmp1.num, 4));
				Assign assign = new Assign(tmp1, name);
				body.add(assign);
				return new Pairs<ExprAtt, Address>(exprAtt, tmp1);
			}
		}*/
		
		//if (globalMap.containsKey(name.name))  return new Pairs<ExprAtt, Address>(exprAtt, tmp1);
		//else 
		return new Pairs<ExprAtt, Address>(exprAtt, name);
	}
	
	public boolean checkCast(Type t1, Type t2) {
		if (t1 instanceof IntType || t1 instanceof CharType || t1 instanceof PointerType) {
			if (t2 instanceof IntType || t2 instanceof CharType || t2 instanceof PointerType || t2 instanceof ArrayType)
				return true;
		}
		//if (t1.getClass() == t2.getClass()) return true;
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType) {
				if (checkSameType(t1, t2)) return true;
			}
		}
		return false;
	}
	
	public Pairs<ExprAtt, Address> checkCastExpr(CastExpr expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt,Address> pair = checkExpr(expr.expr, vars, body);
		if (!checkCast(expr.cast, pair.first.type)) error("castExpr error");
		ExprAtt e = new ExprAtt(expr.cast, 0, pair.first.isconst, pair.first.constvalue);
		
		Temp tmp = new Temp();
		vars.add(new BasicVariable("#t"+tmp.num, 4));
		
		Assign assign = new Assign(tmp, pair.second);
		body.add(assign);
		return new Pairs<ExprAtt, Address>(e, tmp);
	}

	public Pairs<ExprAtt, Address> checkPointerAccessIr(PointerAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>();
		if (expr.body instanceof PointerAccess) {
			Triple<ExprAtt, Address, Address> tri = checkPointerAccess((PointerAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else if (expr.body instanceof ArrayAccess) {
			Triple<ExprAtt, Address, Address> tri = checkArrayAccess((ArrayAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else if (expr.body instanceof RecordAccess) {
			Triple<ExprAtt, Address, Address> tri = checkRecordAccess((RecordAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else pair = checkExpr(expr.body, vars, body);
		
		ExprAtt e = pair.first;
		if (!(e.type instanceof PointerType)) error("not a PointerType");
		Type type = ((PointerType) e.type).baseType;
		if (type instanceof StructType) {
			StructType t = (StructType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof StructDecl)) error(t.tag.toString()+" is not a structtype");
			boolean exist = false;
			Type getType = null;
			int offset = 0;
			for (Decl d: ((StructDecl) decl).fields) {
				if (d instanceof VarDecl) { 
					VarDecl vd = (VarDecl) d;
					if (vd.type instanceof IntType) {
						if (offset % 4 != 0) {
							offset = (offset/4+1)*4;
						}
					}
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
						break;
					}
					offset += getSize(vd.type);
				}
			}
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair.second, ArithmeticOp.ADD, new IntegerConst(offset));
			body.add(arithExpr);
			/*Temp dest = new Temp();
			vars.add(new BasicVariable("#t"+dest.num, 4));
			MemoryRead ar = new MemoryRead(dest, tmp, getSize(getType));
			body.add(ar);*/
			
			if (!exist) error("struct member does not exist");
			ExprAtt ex = null;
			if (getType instanceof ArrayType) ex = new ExprAtt(getType, 0, 0, 0);
			else ex = new ExprAtt(getType, 1, 0, 0);
			return new Pairs<ExprAtt, Address>(ex, tmp);
		}
		if (type instanceof UnionType) {
			UnionType t = (UnionType) type;
			Decl decl = env.getRecord(t.tag.toString());
			if (!(decl instanceof UnionDecl)) error(t.tag.toString()+" is not a UnionType");
			boolean exist = false;
			Type getType = null;
			int offset = 0;
			for (Decl d: ((UnionDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
						break;
					}
					//offset += getSize(vd.type);
				}
			}
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr arithExpr = new ArithmeticExpr(tmp, pair.second, ArithmeticOp.ADD, new IntegerConst(offset));
			body.add(arithExpr);
			/*Temp dest = new Temp();
			vars.add(new BasicVariable("#t"+dest.num, 4));
			MemoryRead ar = new MemoryRead(dest, tmp, getSize(getType));
			body.add(ar);*/
			
			if (!exist) error("struct member does not exist");
			ExprAtt ex = null;
			if (getType instanceof ArrayType) ex = new ExprAtt(getType, 0, 0, 0);
			else ex = new ExprAtt(getType, 1, 0, 0);
			return new Pairs<ExprAtt, Address>(ex, tmp);
		}
		return null;
	}
	
	public Triple<ExprAtt, Address, Address> checkPointerAccess(PointerAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair = checkPointerAccessIr(expr, vars, body);
		Temp tmp = new Temp();
		int size = getSize(pair.first.type);
		if (size <= 4) vars.add(new BasicVariable("#t"+tmp.num, 4));
		else vars.add(new ArrayVariable("#t"+tmp.num, size));
		//printType(pair.first.type);
		MemoryRead mr = new MemoryRead(tmp, pair.second, getSize(pair.first.type));
		body.add(mr);
		return new Triple<ExprAtt, Address, Address>(pair.first, pair.second, tmp);
	}

	public Pairs<ExprAtt, Address> checkRecordAccessIr(RecordAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair = new Pairs<ExprAtt, Address>();
		if (expr.body instanceof PointerAccess) {
			//System.out.println("PointerAccess");
			Triple<ExprAtt, Address, Address> tri = checkPointerAccess((PointerAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else if (expr.body instanceof ArrayAccess) {
			//System.out.println("ArrayAccess");
			Triple<ExprAtt, Address, Address> tri = checkArrayAccess((ArrayAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else if (expr.body instanceof RecordAccess) {
			//System.out.println("RecordAccess");
			Triple<ExprAtt, Address, Address> tri = checkRecordAccess((RecordAccess)expr.body, vars, body);
			pair.first = tri.first;
			if (pair.first.type instanceof PointerType) pair.second = tri.third;
			else pair.second = tri.second;
		}
		else {
			//System.out.println("Other");
			pair = checkExpr(expr.body, vars, body);
		}
		
		Type type = pair.first.type;
		if (type instanceof StructType) {
			StructType t = (StructType) type;
			Decl decl = env.getRecord(t.tag.toString(), t.level);
			if (!(decl instanceof StructDecl)) error(t.tag.toString()+" is not a structtype");
			boolean exist = false;
			Type getType = null;
			int offset = 0;
			for (Decl d: ((StructDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.type instanceof IntType) {
						if (offset % 4 != 0) {
							offset = (offset/4+1)*4;
						}
					}
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
						break;
					}
					offset += getSize(vd.type);
				}
			}
			Temp tmp = new Temp();
			vars.add(new BasicVariable("#t"+tmp.num, 4));
			ArithmeticExpr ae;
			
			ae = new ArithmeticExpr(tmp, pair.second, ArithmeticOp.ADD, new IntegerConst(offset));
			body.add(ae);
			//Temp dest = new Temp();
			//vars.add(new BasicVariable("#t"+dest.num, 4));
			//MemoryRead ar = new MemoryRead(dest, pair.second, new IntegerConst(offset), getSize(getType));
			//body.add(ar);
			
			if (!exist) error("struct member does not exist");
			ExprAtt ex = null;
			if (getType instanceof ArrayType) ex = new ExprAtt(getType, 0, 0, 0);
			else ex = new ExprAtt(getType, 1, 0, 0);
			return new Pairs<ExprAtt, Address>(ex, tmp);
		}
		if (type instanceof UnionType) {
			UnionType t = (UnionType) type;
			Decl decl = env.getRecord(t.tag.toString(), t.level);
			if (!(decl instanceof UnionDecl)) error(t.tag.toString()+" is not a UnionType");
			boolean exist = false;
			Type getType = null;
			int offset = 0;
			for (Decl d: ((UnionDecl) decl).fields) {
				if (d instanceof VarDecl) {
					VarDecl vd = (VarDecl) d;
					if (vd.name.equal(expr.attribute)) {
						exist = true;
						getType = vd.type;
						break;
					}
					//offset += getSize(vd.type);
				}
			}
			/*Temp dest = new Temp();
			vars.add(new BasicVariable("#t"+dest.num, 4));
			ArrayRead ar = new ArrayRead(dest, pair.second, new IntegerConst(offset), getSize(getType));
			body.add(ar);*/
			
			if (!exist) error("struct member does not exist");
			ExprAtt ex = null;
			if (getType instanceof ArrayType) ex = new ExprAtt(getType, 0, 0, 0);
			else ex = new ExprAtt(getType, 1, 0, 0);
			return new Pairs<ExprAtt, Address>(ex, pair.second);
		}
		error("RecordAccess error:expr not a struct/union type");
		return null;
	}
	
	public Triple<ExprAtt, Address, Address> checkRecordAccess(RecordAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair = checkRecordAccessIr(expr, vars, body);
		Temp t = new Temp();
		int size = getSize(pair.first.type);
		if (size <= 4) vars.add(new BasicVariable("#t"+t.num, 4));
		else vars.add(new ArrayVariable("#t"+t.num, size));
		MemoryRead mr = new MemoryRead(t, pair.second, getSize(pair.first.type));
		body.add(mr);
		return new Triple<ExprAtt, Address, Address>(pair.first, pair.second, t);
	}
	
	public void printType(Type type) {
		if (type == null) System.out.println("null");
		if (type instanceof IntType) System.out.println("IntType");
		if (type instanceof CharType) System.out.println("CharType");
		if (type instanceof ArrayType) System.out.println("ArrayType");
		if (type instanceof PointerType) System.out.println("PointerType");
		if (type instanceof StructType) System.out.println("StructType "+((StructType) type).tag.toString()+" "+((StructType) type).level);
		if (type instanceof UnionType) System.out.println("UnionType "+((UnionType) type).tag.toString()+" "+((StructType) type).level);
	}
	
	public Pairs<ExprAtt, Address> checkArrayAccessIr(ArrayAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair1 = new Pairs<ExprAtt, Address>();
		if (expr.body instanceof PointerAccess) {
			Triple<ExprAtt, Address, Address> tri = checkPointerAccess((PointerAccess)expr.body, vars, body);
			pair1.first = tri.first;
			if (pair1.first.type instanceof PointerType) pair1.second = tri.third;
			else pair1.second = tri.second;
		}
		else if (expr.body instanceof ArrayAccess) {
			Triple<ExprAtt, Address, Address> tri = checkArrayAccess((ArrayAccess)expr.body, vars, body);
			pair1.first = tri.first;
			if (pair1.first.type instanceof PointerType) pair1.second = tri.third;
			else pair1.second = tri.second;
		}
		else if (expr.body instanceof RecordAccess) {
			Triple<ExprAtt, Address, Address> tri = checkRecordAccess((RecordAccess)expr.body, vars, body);
			pair1.first = tri.first;
			if (pair1.first.type instanceof PointerType) pair1.second = tri.third;
			else pair1.second = tri.second;
		}
		else pair1 = checkExpr(expr.body, vars, body);
		
		ExprAtt ebody = pair1.first;
		
		Pairs<ExprAtt, Address> pair2 = checkExpr(expr.subscript, vars, body);
		ExprAtt esub = pair2.first;
		Type type = null;
		if (ebody.type instanceof ArrayType)
			type = ((ArrayType) ebody.type).baseType;
		if (ebody.type instanceof PointerType)
			type = ((PointerType) ebody.type).baseType;
		ExprAtt e = null;
		if (type instanceof ArrayType) e = new ExprAtt(type, 0, 0, 0);
		else e = new ExprAtt(type, 1, 0, 0);
		
		//ignoring PointerType is wrong
		
		Temp tmp = new Temp();
		vars.add(new BasicVariable("#t"+tmp.num, 4));
		
		if (ebody.type instanceof ArrayType) {
			ArrayType at = (ArrayType) ebody.type;
			//System.out.printf("size: %d dimsize: %d basesize: %d\n", at.size, at.dimSize, at.baseSize);
			ArithmeticExpr ae = new ArithmeticExpr(tmp, pair2.second, ArithmeticOp.MUL, new IntegerConst(at.size/at.dimSize*at.baseSize));
			body.add(ae);
		} else {
			PointerType pt = (PointerType) ebody.type;
			if (expr.body instanceof Identifier) {
				Temp t = new Temp();
				vars.add(new BasicVariable("#t"+t.num, 4));
				Assign mr = new Assign(t, pair1.second);
				body.add(mr);
				pair1 = new Pairs<ExprAtt, Address>(pair1.first, t);
			}
			ArithmeticExpr ae = new ArithmeticExpr(tmp, pair2.second, ArithmeticOp.MUL, new IntegerConst(getSize(type)));
			body.add(ae);
		}
		
		Temp dest = new Temp();
		vars.add(new BasicVariable("#t"+dest.num, 4));
		ArithmeticExpr ae2;
		if (pair1.second instanceof Name) {
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			body.add(new AddressOf(t, (Name)pair1.second));
			ae2 = new ArithmeticExpr(dest, t, ArithmeticOp.ADD, tmp);
			body.add(ae2);
			return new Pairs<ExprAtt, Address>(e, dest);
		}
		ae2 = new ArithmeticExpr(dest, pair1.second, ArithmeticOp.ADD, tmp);
		body.add(ae2);
		return new Pairs<ExprAtt, Address>(e, dest);
	}
	
	public Triple<ExprAtt, Address, Address> checkArrayAccess(ArrayAccess expr, List<Variable> vars, List<Quadruple> body) {
		Pairs<ExprAtt, Address> pair = checkArrayAccessIr(expr, vars, body);
		if (pair.first.type instanceof ArrayType) {
			return new Triple<ExprAtt, Address, Address>(pair.first, pair.second, pair.second);
		}
		Temp tmp = new Temp();
		
		int size = getSize(pair.first.type);
		if (size <= 4) vars.add(new BasicVariable("#t"+tmp.num, 4));
		else vars.add(new ArrayVariable("#t"+tmp.num, size));
		
		MemoryRead mr = new MemoryRead(tmp, pair.second,getSize(pair.first.type));
		body.add(mr);
		return new Triple<ExprAtt, Address, Address>(pair.first, pair.second, tmp);
	}
	
	public boolean checkFuncParas(Type t1, Type t2) {
		if (t1 instanceof IntType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof CharType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof PointerType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof ArrayType) {
			if (t2 instanceof IntType) return true;
			if (t2 instanceof CharType) return true;
			if (t2 instanceof PointerType) return true;
			if (t2 instanceof ArrayType) return true;
		}
		if (t1 instanceof StructType) {
			if (t2 instanceof StructType)
				if (checkSameType(t1, t2)) return true;
		}
		if (t1 instanceof UnionType) {
			if (t2 instanceof UnionType)
				if (checkSameType(t1, t2)) return true;
		}
		return false;
	}
	
	public Pairs<ExprAtt, Address> checkFunctionCall(FunctionCall expr, List<Variable> vars, List<Quadruple> body) {
		String s = expr.body.toString();
		if (s.equals("printf")) {
			int size = expr.args.size();
			//if (size == 0) error("printf args num error");
			//ExprAtt e = checkExpr(expr.args.get(0));
			//if (!checkCast(new PointerType(new CharType()), e.type)) error("printf args error");
			ExprAtt ea = new ExprAtt(new IntType(), 0, 0, 0);
			List<Param> param = new LinkedList<Param>();
			for (Expr e: expr.args) {
				Address adrs = checkExpr(e, vars, body).second;
				param.add(new BasicParam(adrs));
			}
			for (Param p: param) {
				body.add(p);
			}
			Call call = new Call(null, "printf", param.size());
			//mark ques
			body.add(call);
			return new Pairs<ExprAtt, Address>(ea, null);
		}
		if (s.equals("getchar")) {
			int size = expr.args.size();
			if (size != 0) error("getchar args num error");
			ExprAtt e = new ExprAtt(new CharType(), 0, 0, 0);
			
			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Param p = new BasicParam(t);
			
			Call call = new Call(p, "getchar", 0);
			body.add(call);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		if (s.equals("malloc")) {
			int size = expr.args.size();
			if (size != 1) error("malloc args num error");
			Pairs<ExprAtt, Address> pair = checkExpr(expr.args.get(0), vars, body);
			//if (!checkCast(new IntType(), pair.first.type)) error("malloc args error");
			ExprAtt e = new ExprAtt(new PointerType(new VoidType()), 0, 0, 0);
			Param param = new BasicParam(pair.second);
			body.add(param);

			Temp t = new Temp();
			vars.add(new BasicVariable("#t"+t.num, 4));
			Param p = new BasicParam(t);
			
			Call call = new Call(p, "malloc", 1);
			body.add(call);
			return new Pairs<ExprAtt, Address>(e, t);
		}
		
		Decl decl = env.getDecl(expr.body.toString());
		if (!(decl instanceof FunctionDecl)) error(expr.body.toString()+" is not functionDecl");
		FunctionDecl d = (FunctionDecl)decl;
		//if (d.params == null) System.out.println("d.params is null");
		//if (expr.args == null) System.out.println("expr.args is null");
		//if (d.params.size() != expr.args.size()) error("parameters number error");
		int size = d.params.size();
		boolean valid = true;
		
		Param returnParam = null;
		Temp t = new Temp(); // structType needs specific check
		if (!(d.returnType instanceof VoidType)) {
			if (d.returnType instanceof IntType || d.returnType instanceof CharType) {
				vars.add(new BasicVariable("#t"+t.num, 4));
				returnParam = new BasicParam(t);
			}
			if (d.returnType instanceof StructType) {
				StructType type = (StructType) d.returnType;
				vars.add(new ArrayVariable("#t"+t.num, getSize(type)));
				returnParam = new MemoryParam(t, getSize(type));
				//returnParam = new BasicParam(t, getSize(type));
			}
			if (d.returnType instanceof UnionType) {
				UnionType type = (UnionType) d.returnType;
				vars.add(new ArrayVariable("#t"+t.num, getSize(type)));
				returnParam = new MemoryParam(t, getSize(type));
				//returnParam = new BasicParam(t, getSize(type));
			}
			if (d.returnType instanceof PointerType) {
				PointerType type = (PointerType) d.returnType;
				vars.add(new BasicVariable("#t"+t.num, 4));
				returnParam = new BasicParam(t);
			}
			if(d.returnType instanceof ArrayType) {
				ArrayType type = (ArrayType) d.returnType;
				vars.add(new ArrayVariable("#t"+t.num, type.size*type.baseSize));
				returnParam = new MemoryParam(t, type.size*type.baseSize);
			} 
			//System.out.println("#lalalala, I'm here");
		}
		
		List<Param> list = new LinkedList<Param>();
		
		for (Expr e: expr.args) {
			Pairs<ExprAtt, Address> pair = checkExpr(e, vars, body);
			ExprAtt ex = pair.first;
			if (ex.type instanceof IntType) {
				Param p = new BasicParam(pair.second);
				list.add(p);
			}
			if (ex.type instanceof CharType) {
				Param p = new BasicParam(pair.second);
				list.add(p);
			}
			if (ex.type instanceof StructType) {
				Param p = new MemoryParam(pair.second, getSize(ex.type));
				list.add(p);
			}
			if (ex.type instanceof UnionType) {
				Param p = new MemoryParam(pair.second, getSize(ex.type));
				list.add(p);
			}
			if (ex.type instanceof ArrayType) {
				ArrayType at = (ArrayType) ex.type;
				//Param p = new MemoryParam(pair.second, at.baseSize*at.size);
				Param p = new BasicParam(pair.second);
				list.add(p);
			}
			if (ex.type instanceof PointerType) {
				Param p = new BasicParam(pair.second);
				list.add(p);
			}
		}
		
		for (Param param: list) {
			body.add(param);
		}
		
		Call call = new Call(returnParam, "_"+s, list.size());
		if (s.equals("main")) call.callee = "main";
		body.add(call);
		if (call.returnValue != null) body.add(call.returnValue);
		
		String tmps = expr.body.toString();
		//if (s.equals("move")) tmps = "move1";
		
		if (!valid) error("function "+tmps+" parameters type error");
		ExprAtt exprAtt = new ExprAtt(d.returnType, 0, 0, 0);
		return new Pairs<ExprAtt, Address>(exprAtt, t);
	}
}
