package compiler.ir;

public class Triple<Type1, Type2, Type3> {
	public Type1 first;
	public Type2 second;
	public Type3 third;

	public Triple(Type1 f, Type2 s, Type3 t) {
		this.first = f;
		this.second = s;
		this.third = t;
	}
}