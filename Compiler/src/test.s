.data
.align 2
string0:
.asciiz  "%d\n%d\n%d\n%d\n"
.text
printf:
printf_loop:
lb       $a0,0($a2)
beq      $a0,0,printf_end
add      $a2,$a2,1
beq      $a0,'%',printf_fmt
li       $v0,11
syscall
b        printf_loop
printf_fmt:
lb       $a0,0($a2)
add      $a2,$a2,1
beq      $a0,'d',printf_int
beq      $a0,'c',printf_char
beq      $a0,'s',printf_str
beq      $a0,'.',printf_width
beq      $a0,'0',printf_width
printf_int:
lw       $a0,0($a1)
sub      $a1,$a1,4
li       $v0,1
syscall
b        printf_loop
printf_str:
lw       $a0,0($a1)
sub      $a1,$a1,4
li       $v0,4
syscall
b        printf_loop
printf_char:
lb       $a0,0($a1)
sub      $a1,$a1,4
li       $v0,11
syscall
b        printf_loop
printf_width:
li       $v0,1
li       $a0,0
lb       $a3,0($a2)
add      $a2,$a2,2
add      $a3,$a3,-48
li       $t0,0
WidthLoop:
mul      $t0,$t0,10
add      $t0,$t0,9
add      $a3,$a3,-1
bne      $a3,0,WidthLoop
lw       $v1,0($a1)
ZeroLoop:
div      $t0,$t0,10
bgt      $v1,$t0,printf_int
syscall
b        ZeroLoop
jr       $31
printf_end:
j        $31
getchar:
li       $v0,12
syscall
sw       $v0,0($sp)
jr       $31
malloc:
li       $v0,9
syscall
sw       $v0,0($sp)
jr       $31
#  function: _start
_start:
add      $sp,$sp,-12
sw       $31,8($sp)
sw       $fp,4($sp)
move     $fp,$sp
lw       $31,8($sp)
lw       $fp,4($sp)
move     $sp,$fp
j        $31
#  function: main
main:
li       $k0,1
li       $k1,-1
li       $gp,2
sub      $sp,$sp,116
sw       $31,112($sp)
sw       $fp,108($sp)
move     $fp,$sp
#   = call _start, 0
#call _start
jal      _start
#  #t5=10
li       $t1,10
#  #t6=1
li       $t2,1
#  #t4 = call _gcd, 2
#call _gcd
sw       $t2,4($sp)
sw       $t1,8($sp)
jal      _gcd
lw       $t1,0($sp)
#  #t8=50
li       $t2,50
#  #t9=35
li       $t3,35
#  #t7 = call _gcd, 2
#call _gcd
sw       $t3,4($sp)
sw       $t2,8($sp)
sw       $t1,96($sp)
jal      _gcd
lw       $t1,96($sp)
lw       $t2,0($sp)
#  #t11=34986
li       $t3,34986
#  #t12=3087
li       $t4,3087
#  #t10 = call _gcd, 2
#call _gcd
sw       $t4,4($sp)
sw       $t3,8($sp)
sw       $t1,96($sp)
sw       $t2,84($sp)
jal      _gcd
lw       $t1,96($sp)
lw       $t2,84($sp)
lw       $t3,0($sp)
#  #t14=2907
li       $t4,2907
#  #t15=1539
li       $t5,1539
#  #t13 = call _gcd, 2
#call _gcd
sw       $t5,4($sp)
sw       $t4,8($sp)
sw       $t3,72($sp)
sw       $t1,96($sp)
sw       $t2,84($sp)
jal      _gcd
lw       $t3,72($sp)
lw       $t1,96($sp)
lw       $t2,84($sp)
lw       $t4,0($sp)
#   = call printf, 5
#call printf
sw       $t4,0($sp)
sw       $t3,4($sp)
sw       $t2,8($sp)
sw       $t1,12($sp)
la       $v1,string0
sw       $v1,16($sp)
lw       $a2,16($sp)
add      $a1,$sp,12
jal      printf
#  #t16=0
li       $t1,0
#  return Param #t16
sw       $t1,116($sp)
lw       $31,112($sp)
lw       $fp,108($sp)
j        $31
lw       $31,112($sp)
lw       $fp,108($sp)
j        $31
#  function: _gcd
_gcd:
sub      $sp,$sp,60
sw       $31,56($sp)
sw       $fp,52($sp)
move     $fp,$sp
lw       $t2,68($sp)
lw       $t1,64($sp)
#  #t0=x1%y1
div      $t2,$t1
mfhi     $t3
#  if false #t0==0 goto label0
bne      $t3,$zero,label0
#  return Param y1
sw       $t1,60($sp)
lw       $31,56($sp)
lw       $fp,52($sp)
move     $sp,$fp
j        $31
b        label1
label0:
#  #t3=x1%y1
div      $t2,$t1
mfhi     $t3
#  #t2 = call _gcd, 2
#call _gcd
sw       $t3,4($sp)
sw       $t1,8($sp)
sw       $t1,64($sp)
sw       $t2,68($sp)
jal      _gcd
lw       $t1,64($sp)
lw       $t2,68($sp)
lw       $t3,0($sp)
#  return Param #t2
sw       $t3,60($sp)
lw       $31,56($sp)
lw       $fp,52($sp)
move     $sp,$fp
j        $31
label1:
lw       $31,56($sp)
lw       $fp,52($sp)
move     $sp,$fp
j        $31
