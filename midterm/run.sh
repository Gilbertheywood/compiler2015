#!/bin/bash

make clean
make all

mkdir -p outputs && rm outputs/*

echo "------------1------------"

source midtermvars.sh
for i in $(ls testcases/CompileError); do
	cp testcases/CompileError/$i bin/data.c
	cd bin
	$CCHK data.c > ../outputs/${i%.*}.s
	echo "exiting status : $?"
	cd ..
done

echo "------------1 new-----------"

source midtermvars.sh
for i in $(ls testcases/CompileError-new); do
	cp testcases/CompileError-new/$i bin/data.c
	cd bin
	$CCHK data.c > ../outputs/${i%.*}.s
	echo "exiting status : $?"
	cd ..
done

echo "------------0------------"

source midtermvars.sh
for i in $(ls testcases/Passed); do
	cp testcases/Passed/$i bin/data.c
	cd bin
	$CCHK data.c > ../outputs/${i%.*}.s
	echo "exiting status : $?"
	cd ..
done

echo "-----------0 new-------------"

source midtermvars.sh
for i in $(ls testcases/Passed-new); do
	cp testcases/Passed-new/$i bin/data.c
	cd bin
	$CCHK data.c > ../outputs/${i%.*}.s
	echo "exiting status : $?"
	cd ..
done
