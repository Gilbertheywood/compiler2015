all:
	mkdir -p bin
	javac Compiler/src/compiler/*/*.java -classpath Compiler/src/lib/*.jar -d bin

clean:
	rm -rf bin
