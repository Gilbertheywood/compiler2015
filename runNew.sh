#!/bin/bash

make clean
make all

mkdir -p outputs && rm outputs/*

source finalvars.sh

for i in $(ls testcases/New); do
	cn=${i%.c}
	cp testcases/New/$i bin/data.c
	cd bin
	echo now testing $cn.c
	$CCHK < data.c > ../outputs/$cn.s
	gcc -m32 data.c -o data

	if [ -f ../testcases/New/New-InputSet/$cn.in ]; then
		
		cp ../testcases/New/New-InputSet/$cn.in data.in
		spim -stat_file spimstat -file ../outputs/$cn.s < data.in 1>data.out
		./data <data.in 1>std.out

		else

		spim -stat_file spimstat -file ../outputs/$cn.s 1>data.out
		./data 1>std.out
		
	fi

	if diff std.out data.out
	then 
	     echo "Pass Pass Pass"
	     echo "按任意键继续"
	     read -n 1
	else 
	     echo "not match!"
	     echo "按任意键继续"
	     read -n 1
	fi

	cd ..
done
