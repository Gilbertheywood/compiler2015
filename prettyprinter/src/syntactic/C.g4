grammar C;

@header {
package syntactic;
import ast.*;
import util.*;
import java.util.*;
}

program returns [Program ele] // a program
   locals [List<Node> list = new ArrayList<Node>(),Comment prec = null, Comment succ = null]
    @after {$ele = new Program($list, $prec, $succ);}
    : (c = comment {$prec = $c.ele;})?
    (
        dcltn = declaration {$list.add($dcltn.ele);}
      | 
        funcdef = function_definition {$list.add($funcdef.ele);}
    )+ 
      (c = comment {$succ = $c.ele;})?
    ;

comment returns [Comment ele]
    locals[List<SingleComment> list = new ArrayList<SingleComment>()]
    @after {$ele = new Comment($list);}
    : s = single_comment {$list.add($s.ele);}
      (s = single_comment {$list.add($s.ele);})*
    ;

single_comment returns [SingleComment ele]
    : m = Multi_comment {$ele = new SingleComment($m.text);}
      | l = Line_comment {$ele = new SingleComment($l.text);}
    ;

declaration returns [Declaration ele]
    : {InitDeclarators initdectmp = null;}
        tyspc = type_specifier 
        (initdec = init_declarators {initdectmp = $initdec.ele;})? ';' 
        {$ele = new Declaration($tyspc.ele, initdectmp);}
    ;

function_definition returns [FunctionDefinition ele]
    : {Parameters parastmp = null; Comment c1 = null; Comment c2 = null; Comment c3 = null;}
        tyspc = type_specifier 
        pldcltr = plain_declarator 
        (c = comment {c1 = $c.ele;})? '('
        (paras = parameters {parastmp = $paras.ele;})? 
        ')'(c = comment {c2 = $c.ele;})?  
        compstmt = compound_statement 
        (c = comment {c3 = $c.ele;})? 
        {$ele = new FunctionDefinition($tyspc.ele, $pldcltr.ele, parastmp, $compstmt.ele, c1, c2, c3);}
    ;

parameters returns [Parameters ele]
    locals [List<PlainDeclaration> list = new ArrayList<PlainDeclaration>(), boolean more = false]
    @after{$ele = new Parameters($list, $more);}
    :
        pldcltn = plain_declaration {$list.add($pldcltn.ele);}
        (',' pldcltn = plain_declaration {$list.add($pldcltn.ele);})* 
        (',' '...' {$more = true;})? 
    ;

declarators returns [Declarators ele]
    locals [List<Declarator> list = new ArrayList<Declarator>()]
    @after{$ele = new Declarators($list);}
    : 
        dcltr = declarator {$list.add($dcltr.ele);}
        (',' dcltr = declarator {$list.add($dcltr.ele);})* 
    ;

init_declarators returns [InitDeclarators ele] 
    locals [List<InitDeclarator> list = new ArrayList<InitDeclarator>()]
    @after {$ele = new InitDeclarators($list);}
    : 
        init = init_declarator {$list.add($init.ele);}
        (',' init = init_declarator {$list.add($init.ele);})* 
    ;

init_declarator returns [InitDeclarator ele]
    : {Initializer inittmp = null;}
        dcltr = declarator
        ('=' init = initializer {inittmp = $init.ele;})? 
    {$ele = new InitDeclarator($dcltr.ele, inittmp);}
    ;

initializer returns [Initializer ele]
    : 
        assignexpr = assignment_expression 
        {$ele = new Initializer($assignexpr.ele, null);}
        | {List<Initializer> list = new ArrayList<Initializer>();}
        '{' 
        init = initializer {list.add($init.ele);}
        (',' init = initializer {list.add($init.ele);})* '}' 
        {$ele = new Initializer(null, list);}
    ;

type_specifier returns [Type ele]
    : 'void' {$ele = new VoidType();}
    | 
      'char' {$ele = new CharType();}
    | 
      'int' {$ele = new IntType();}
    | {List<Pairs<Type, Declarators>> list = new ArrayList<Pairs<Type, Declarators>>();
       StructorUnion sou = null; Identifier idtmp = null;}
      sou = struct_or_union 
      (id = identifier {idtmp = $id.ele;})? '{' 
      (typspc = type_specifier dcltr = declarators ';' {list.add(new Pairs($typspc.ele,$dcltr.ele));})+ '}'
      {$ele = new RecordType($sou.ele, idtmp, list, 1);}
    | 
      sou = struct_or_union 
      id = identifier 
      {$ele = new RecordType($sou.ele, $id.ele, null, 0);}
    ;

struct_or_union returns [StructorUnion ele]
    : 
        'struct' {$ele = new StructType();}
    | 
        'union' {$ele = new UnionType();}
    ;

plain_declaration returns [PlainDeclaration ele]
    : 
        tyspc = type_specifier 
        dclr = declarator 
        {$ele = new PlainDeclaration($tyspc.ele, $dclr.ele);}
    ;

declarator returns [Declarator ele]
    : {Parameters parastmp = null;} 
        pldcltr = plain_declarator '(' (paras = parameters {parastmp = $paras.ele;} )? ')'
       {$ele = new FunctionDeclarator($pldcltr.ele, parastmp);}
    | {List<ConstExpr> list = new ArrayList<ConstExpr>();}
        pldcltr = plain_declarator ('[' cons = constant_expression  ']'{list.add($cons.ele);})* 
        {$ele = new ArrayDeclarator($pldcltr.ele, list);}
    ;

plain_declarator returns [PlainDeclarator ele]
    locals [int cnt = 0]
    : 
       ( '*' {$cnt++;})* id = identifier 
        {$ele = new PlainDeclarator($cnt, $id.ele);}
    ;


// statements


statement returns [Stmt ele]
    : 
        ex = expression_statement {$ele = $ex.ele;} 
          | compst = compound_statement {$ele = $compst.ele;}
          | selst = selection_statement {$ele = $selst.ele;}
          | itst = iteration_statement {$ele = $itst.ele;}
          | jpst = jump_statement {$ele = $jpst.ele;}
    ;

expression_statement returns [ExprStmt ele]
    : {Expr exprtmp = null;}
        (expr = expression {exprtmp = $expr.ele;})? ';' 
        {$ele = new ExprStmt(exprtmp);}
    ;

compound_statement returns [CompoundStmt ele]
    locals [List<Declaration> list1 = new ArrayList<Declaration>(),
       List<Stmt> list2 = new ArrayList<Stmt>(),Comment c1 = null,Comment c2 = null]
    @after {$ele = new CompoundStmt($list1, $list2, $c1, $c2);}
    :  
        '{'(c = comment {$c1 = $c.ele;})? 
        (dcltn = declaration {$list1.add($dcltn.ele);} )* 
        (stmt = statement {$list2.add($stmt.ele);} )*
        (c = comment {$c2 = $c.ele;})? '}' 
    ;

selection_statement returns [SelectionStmt ele]
    : {Stmt stmt = null;}
      'if' '(' expr = expression ')' stmt1 = statement ('else' stmt2 = statement {stmt = $stmt2.ele;})? 
      {$ele = new SelectionStmt($expr.ele, $stmt1.ele, stmt);}
    ;

iteration_statement returns [IterationStmt ele]
    :
        'while' '(' expr = expression ')' stmt = statement
        {$ele = new WhileStmt($expr.ele, $stmt.ele);}
    | {Expr tmpe1 = null; Expr tmpe2 = null; Expr tmpe3 = null;}
        'for' '(' (e1 = expression {tmpe1 = $e1.ele;})? ';' 
        (e2 = expression {tmpe2 = $e2.ele;})? ';' 
        (e3 = expression {tmpe3 = $e3.ele;})? ')' 
        stmt = statement 
        {$ele = new ForStmt(tmpe1, tmpe2, tmpe3, $stmt.ele);}
    ;

jump_statement returns [JumpStmt ele]
    : 'continue' ';' {$ele = new ContinueStmt();}
               | 'break' ';' {$ele = new BreakStmt();}
               | {Expr exprtmp = null;} 
                'return' (expr = expression {exprtmp = $expr.ele;})? ';' 
                {$ele = new ReturnStmt(exprtmp);}
    ;
                  
//  expression

expression returns [Expr ele]
    locals [List<AssignExpr> list = new ArrayList<AssignExpr>()]
    @after {$ele = new Expr($list);}
    : 
        assignexpr = assignment_expression {$list.add($assignexpr.ele);}
        (',' assignexpr = assignment_expression {$list.add($assignexpr.ele);})* 
    ;

 assignment_expression returns [AssignExpr ele]
     : 
         l = logical_or_expression {$ele = new AssignExpr($l.ele, null, null, null);}                 
     | 
         u = unary_expression ao = assignment_operator ae = assignment_expression 
         {$ele = new AssignExpr(null, $u.ele, $ao.ele, $ae.ele);}
     ;

 assignment_operator returns [BinaryOp ele]
     : '=' {$ele = BinaryOp.ASSIGN;}
     | 
       '*=' {$ele = BinaryOp.ASSIGN_MUL;}
     | 
       '/=' {$ele = BinaryOp.ASSIGN_DIV;}
     | 
       '%=' {$ele = BinaryOp.ASSIGN_MOD;}
     |
       '+=' {$ele = BinaryOp.ASSIGN_ADD;}
     |
       '-=' {$ele = BinaryOp.ASSIGN_SUB;}
     | 
       '<<=' {$ele = BinaryOp.ASSIGN_SHL;}
     | 
       '>>=' {$ele = BinaryOp.ASSIGN_SHR;}
     | 
       '&=' {$ele = BinaryOp.ASSIGN_AND;}
     | 
       '^=' {$ele = BinaryOp.ASSIGN_XOR;}
     |
       '|=' {$ele = BinaryOp.ASSIGN_OR;}
     ;

 constant_expression returns[ConstExpr ele]
	: logical_or_expression {$ele = new ConstExpr($logical_or_expression.ele);}
	;

 logical_or_expression returns[LogicalOrExpr ele]
    locals[List<LogicalAndExpr> list = new ArrayList<LogicalAndExpr>()]
    @after {$ele = new LogicalOrExpr($list);}
	: l = logical_and_expression {$list.add($l.ele);}
	  ('||' l = logical_and_expression {$list.add($l.ele);})*
	;

 logical_and_expression returns [LogicalAndExpr ele]
    locals[List<InclusiveOrExpr> list = new ArrayList<InclusiveOrExpr>()]
    @after {$ele = new LogicalAndExpr($list);}
	: i = inclusive_or_expression {$list.add($i.ele);}
	  ('&&' i = inclusive_or_expression {$list.add($i.ele);})*
	;
 
 inclusive_or_expression returns [InclusiveOrExpr ele]
    locals[List<ExclusiveOrExpr> list = new ArrayList<ExclusiveOrExpr>()]
    @after {$ele = new InclusiveOrExpr($list);}
	: e = exclusive_or_expression  {$list.add($e.ele);}
	  ('|' e = exclusive_or_expression {$list.add($e.ele);})*
	;

 exclusive_or_expression returns [ExclusiveOrExpr ele]
    locals[List<AndExpr> list = new ArrayList<AndExpr>()]
    @after {$ele = new ExclusiveOrExpr($list);}
	: a = and_expression {$list.add($a.ele);}
	  ('^' a = and_expression {$list.add($a.ele);})*
	;

 and_expression returns [AndExpr ele]
    locals[List<EqualExpr> list = new ArrayList<EqualExpr>()]
    @after {$ele = new AndExpr($list);}
	: e = equality_expression {$list.add($e.ele);}
	  ('&' e = equality_expression {$list.add($e.ele);})*
	;

 equality_expression returns [EqualExpr ele]
    locals[List<RelationExpr> expr = new ArrayList<RelationExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
    @after {$ele = new EqualExpr($expr, $binOp);}
	: r = relational_expression {$expr.add($r.ele);}
	  (e = equality_operator {$binOp.add($e.ele);}
	  r = relational_expression {$expr.add($r.ele);})*
	;

 equality_operator returns [BinaryOp ele]
	: '==' {$ele = BinaryOp.EQ;}
	| '!=' {$ele = BinaryOp.NE;}
	;

 relational_expression returns [RelationExpr ele]
    locals[List<ShiftExpr> expr = new ArrayList<ShiftExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
    @after {$ele = new RelationExpr($expr, $binOp);}
	: s = shift_expression {$expr.add($s.ele);}
	  (r = relational_operator {$binOp.add($r.ele);}
	  s = shift_expression {$expr.add($s.ele);})*
	;

 relational_operator returns [BinaryOp ele]
	: '<' {$ele = BinaryOp.LT;}
	| '>' {$ele = BinaryOp.GT;}
	| '<=' {$ele = BinaryOp.LE;}
	| '>=' {$ele = BinaryOp.GE;}
	;

 shift_expression returns [ShiftExpr ele]
    locals[List<AddExpr> expr = new ArrayList<AddExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
    @after {$ele = new ShiftExpr($expr, $binOp);}
	: a = additive_expression {$expr.add($a.ele);}
	  (s = shift_operator {$binOp.add($s.ele);}
	  a = additive_expression {$expr.add($a.ele);})*
	;

 shift_operator returns [BinaryOp ele]
	: '<<' {$ele = BinaryOp.SHL;}
	| '>>' {$ele = BinaryOp.SHR;}
	;

 additive_expression returns [AddExpr ele]
    locals[List<MultiplyExpr> expr = new ArrayList<MultiplyExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
    @after {$ele = new AddExpr($expr, $binOp);}
    : m = multiplicative_expression {$expr.add($m.ele);}
	  (a = additive_operator {$binOp.add($a.ele);}
      m = multiplicative_expression {$expr.add($m.ele);})*
	;

 additive_operator returns [BinaryOp ele]
	: '+' {$ele = BinaryOp.ADD;}
	| '-' {$ele = BinaryOp.SUB;}
	;

 multiplicative_expression returns [MultiplyExpr ele]
    locals[List<CastExpr> expr = new ArrayList<CastExpr>(),
	   List<BinaryOp> binOp = new ArrayList<BinaryOp>()]
    @after {$ele = new MultiplyExpr($expr, $binOp);}
	: c = cast_expression {$expr.add($c.ele);}
	  (m = multiplicative_operator {$binOp.add($m.ele);}
	  c = cast_expression {$expr.add($c.ele);})*
	;

 multiplicative_operator returns [BinaryOp ele]
	: '*' {$ele = BinaryOp.MUL;}
	| '/' {$ele = BinaryOp.DIV;}
	| '%' {$ele = BinaryOp.MOD;}
	;

 cast_expression returns [CastExpr ele]
	: u = unary_expression {$ele = new CastExpr(null, $u.ele);}
	| '(' t = type_name ')' c = cast_expression
	  {$ele = new CastExpr($t.ele, $c.ele);}
	;

 type_name returns [TypeName ele]
	: {int cnt = 0;}
	  t = type_specifier ('*' {cnt++;})*
	  {$ele = new TypeName(cnt, $t.ele);}
	;

 unary_expression returns [UnaryExpr ele]
	: p = postfix_expression {$ele=new UnaryExpr(UnaryOp.POSTFIX, $p.ele);}
	| '++' u = unary_expression {$ele = new UnaryExpr(UnaryOp.PREINC, $u.ele);}
	| '--' u = unary_expression {$ele = new UnaryExpr(UnaryOp.PREDEC, $u.ele);}
	| o = unary_operator c=cast_expression
	  {$ele = new UnaryExpr($o.ele, $c.ele);}
	| 'sizeof' u = unary_expression
    {$ele = new UnaryExpr(UnaryOp.SIZEOF, $u.ele);}
	| 'sizeof' '(' t = type_name ')'
    {$ele = new UnaryExpr(UnaryOp.SIZEOFNAME, $t.ele);}
	;

 unary_operator  returns [UnaryOp ele]
	: '&' {$ele = UnaryOp.AND;}
	| '*' {$ele = UnaryOp.STAR;}
	| '+' {$ele = UnaryOp.PLUS;}
	| '-' {$ele = UnaryOp.MINUS;}
	| '~' {$ele = UnaryOp.TILDE;}
	| '!' {$ele = UnaryOp.NOT;}
	;

 postfix_expression returns [PostfixExpr ele]
    locals[ArrayList<Postfix> list = new ArrayList<Postfix>(), PrimaryExpr expr]
    @after{$ele = new PostfixExpr($list, $expr);}
	: p = primary_expression{$expr = $p.ele;}
	  (postfix {$list.add($postfix.ele);})*
	;

 postfix returns [Postfix ele]
	: '[' expression ']' {$ele = new ArrayPostfix($expression.ele);}
	|  {Arguments argu = null;}
	  '(' (arguments {argu = $arguments.ele;})? ')'
	  {$ele = new FunctionPostfix(argu);}
	| '.' identifier {$ele = new ValueAttributePostfix($identifier.ele);}
	| '->' identifier {$ele = new PointerAttributePostfix($identifier.ele);}
	| '++' {$ele = new SelfIncPostfix();}
	| '--' {$ele = new SelfDecPostfix();}
	;

 arguments returns [Arguments ele]
    locals [List<AssignExpr> list=new ArrayList<AssignExpr>()]
    @after {$ele = new Arguments($list);}
	: a = assignment_expression{$list.add($a.ele);}
	  (',' e = assignment_expression {$list.add($e.ele);})*
	;

 primary_expression returns [PrimaryExpr ele]
	: identifier {$ele = new PrimaryExpr($identifier.ele);}
	| constant {$ele = new PrimaryExpr($constant.ele);}
	| String {$ele = new PrimaryExpr(new StringBuilder($String.text).toString());}
	| '(' expression ')' {$ele = new PrimaryExpr($expression.ele);}
	;

 constant returns [Constant ele]
	: i = integer_constant {$ele = $i.ele;}
	| c = character_constant {$ele = $c.ele;}
	;

 identifier returns [Identifier ele]
     : 
         i = Identifier {$ele = new Identifier($i.text);}
     ;

 integer_constant returns[IntConst ele]
  : Hex {$ele = new IntConst(Integer.parseInt($Hex.text.substring(2), 16));}
  | Dec {$ele = new IntConst(Integer.parseInt($Dec.text,10));}
  | Oct {$ele = new IntConst(Integer.parseInt($Oct.text,8));}
  ;
 
  character_constant returns [ChrConst ele]
      : Chr {$ele = new ChrConst(new StringBuilder($Chr.text).toString());}
      ;
 
// Lexer 

 Multi_comment : '/*' .*? '*/';
 Line_comment: '//' ~[\r\n]*;
  
 Identifier: Letter (Letter|'0'..'9')* ;
 
    Dec: '0' | [1-9] [0-9]* ;
 
    Oct: '0' [0-7]+ ;
 
    Hex: '0' ('x'|'X') [0-9a-fA-F]+ ;
 
 fragment
     Letter: [a-zA-Z] | '$' | '_' ;
 
 
 fragment EscapeChar : ('\\' ('\''|'"'|'?'|'\\'|'a'|'b'|'f'|'n'|'r'|'t'|'v')
                       | '\\' ([0-7] | [0-7] [0-7] | [0-7] [0-7] [0-7])
                       | '\\x' [0-9A-Fa-f]+ )
    ;
 
 Chr : '\'' (~[\'\\\n] | EscapeChar)+ '\'' ;
 
 String: '\"' ('\\'([btnfr] | '\'' | '\"' | '\\') | ~('\\'|'\"'|'\n'|'\r'))* '\"';

 WhiteSpace: [ \t\n\r]+ -> channel(HIDDEN) ;
 
 fragment 
     EOL: '\r' | '\n' | '\r''\n' ;

 Lib: '#' ~[\n]* '\n' -> skip ;