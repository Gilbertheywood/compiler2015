package ast;

public class PlainDeclaration extends Node {
	public Type type;
	public Declarator declarator;
	
	public PlainDeclaration(Type type, Declarator declarator) {
		this.type = type;
		this.declarator = declarator;
	}
}
