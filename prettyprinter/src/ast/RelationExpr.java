package ast;

import java.util.List;

public class RelationExpr extends Expression {
	public List<ShiftExpr> list;
	public List<BinaryOp> binOp;
	
	public RelationExpr(List<ShiftExpr> list, List<BinaryOp> binOp) {
		this.list = list;
		this.binOp = binOp;
	}
}
