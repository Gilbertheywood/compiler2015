package ast;

public class UnaryExpr extends Expression {
	public UnaryOp unaryOp;
	public Expression expr;
	
	public UnaryExpr(UnaryOp unaryOp, Expression expr) {
		this.unaryOp = unaryOp;
		this.expr = expr;
	}
}
