package ast;

public class ArrayPostfix extends Postfix {
	public Expr expr;
	
	public ArrayPostfix(Expr expr) {
		this.expr = expr;
	}
}
