package ast;

public class PointerAttributePostfix extends Postfix {
	public Identifier id;
	
	public PointerAttributePostfix(Identifier id) {
		this.id = id;
	}
}
