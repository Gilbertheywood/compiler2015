package ast;

import java.util.*;
import util.Pairs;

public class RecordType extends Type {
	public StructorUnion sou = null;
	public Identifier id = null;
	public List<Pairs<Type, Declarators>> list = null;
	public int mark;
	
	public RecordType(StructorUnion sou, Identifier id, List<Pairs<Type, Declarators>> list, int mark) {
		this.sou = sou;
		this.id = id;
		this.list = list;
		this.mark = mark;
	}
}
