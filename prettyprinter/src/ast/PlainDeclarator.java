package ast;

public class PlainDeclarator extends Node {
	public Identifier id;
	public int cnt = 0;
	
	public PlainDeclarator(int cnt, Identifier id) {
		this.cnt = cnt;
		this.id = id;
	}
}
