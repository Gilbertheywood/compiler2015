package ast;

import java.util.List;

public class ArrayDeclarator extends Declarator {
	public PlainDeclarator plainDeclarator;
	public List<ConstExpr> list;
	
	public ArrayDeclarator(PlainDeclarator plainDeclarator, List<ConstExpr> list) {
		this.plainDeclarator = plainDeclarator;
		this.list = list;
	}
}
