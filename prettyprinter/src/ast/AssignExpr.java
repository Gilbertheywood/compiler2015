package ast;

public class AssignExpr extends Expression {
	public LogicalOrExpr logicalOrExpr;
	public UnaryExpr unaryExpr;
	public BinaryOp binaryOp;
	public AssignExpr assignExpr;
	
	public AssignExpr(LogicalOrExpr logicalOrExpr, UnaryExpr unaryExpr, BinaryOp binaryOp, AssignExpr assignExpr) {
		this.logicalOrExpr = logicalOrExpr;
		this.unaryExpr = unaryExpr;
		this.binaryOp = binaryOp;
		this.assignExpr = assignExpr;
	}
}
