package ast;

import java.util.List;

public class AndExpr extends Expression {
	public List<EqualExpr> list;
	
	public AndExpr(List<EqualExpr> list) {
		this.list = list;
	}
}
