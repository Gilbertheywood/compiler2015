package ast;

import java.util.List;

public class AddExpr extends Expression {
	public List<MultiplyExpr> expr;
	public List<BinaryOp> binOp;
	
	public AddExpr(List<MultiplyExpr> expr, List<BinaryOp> binOp) {
		this.expr = expr;
		this.binOp = binOp;
	}
}
