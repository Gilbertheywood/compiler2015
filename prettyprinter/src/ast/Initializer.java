package ast;

import java.util.List;

public class Initializer extends Node {
	public List<Initializer> list = null;
	public AssignExpr assignExpr = null;
	
	public Initializer(AssignExpr assignExpr, List<Initializer> list) {
		this.list = list;
		this.assignExpr = assignExpr;
	}
}
