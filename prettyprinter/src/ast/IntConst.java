package ast;

public class IntConst extends Constant {
	public int value;
	
	public IntConst(int value) {
		this.value = value;
	}
}
