package ast;

import java.util.List;

public class PostfixExpr extends Expression {
	public List<Postfix> list = null;
	public PrimaryExpr expr = null;
	
	public PostfixExpr(List<Postfix> list, PrimaryExpr expr) {
		this.list = list;
		this.expr = expr;
	}
}
