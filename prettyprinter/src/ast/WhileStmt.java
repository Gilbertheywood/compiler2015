package ast;

public class WhileStmt extends IterationStmt {
	public Expr expr;
	public Stmt stmt;
	
	public WhileStmt(Expr expr, Stmt stmt) {
		this.expr = expr;
		this.stmt = stmt;
	}
}
