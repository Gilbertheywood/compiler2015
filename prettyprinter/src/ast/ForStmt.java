package ast;

public class ForStmt extends IterationStmt {
	public Expr e1, e2, e3;
	public Stmt stmt;
	
	public ForStmt(Expr e1, Expr e2, Expr e3, Stmt stmt) {
		this.e1 = e1;
		this.e2 = e2;
		this.e3 = e3;
		this.stmt = stmt;
	}
}
