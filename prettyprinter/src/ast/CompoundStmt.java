package ast;

import java.util.List;

public class CompoundStmt extends Stmt {
	public List<Declaration> list1 = null;
	public List<Stmt> list2 = null;
	public Comment c1, c2;
	
	public CompoundStmt(List<Declaration> list1, List<Stmt> list2, Comment c1, Comment c2) {
		this.list1 = list1;
		this.list2 = list2;
		this.c1 = c1;
		this.c2 = c2;
	}
}
