package ast;

import java.util.List;

public class InitDeclarators extends Node {
	public List<InitDeclarator> list = null;
	
	public InitDeclarators(List<InitDeclarator> list) {
		this.list = list;
	}
}
