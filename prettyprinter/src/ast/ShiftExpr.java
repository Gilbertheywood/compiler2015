package ast;

import java.util.List;

public class ShiftExpr extends Expression {
	public List<AddExpr> expr;
	public List<BinaryOp> binOp;
	
	public ShiftExpr(List<AddExpr> expr, List<BinaryOp> binOp) {
		this.expr = expr;
		this.binOp = binOp;
	}
}
