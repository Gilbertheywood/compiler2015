package ast;

import java.util.List;

public class Parameters extends Node {
	public List<PlainDeclaration> list = null;
	boolean more;
	
	public Parameters(List<PlainDeclaration> list, boolean more) {
		this.list = list;
		this.more = more;
	}
}
