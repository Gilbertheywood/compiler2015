package ast;

public class ValueAttributePostfix extends Postfix {
	public Identifier id;
	
	public ValueAttributePostfix(Identifier id) {
		this.id = id;
	}
}
