package ast;

import java.util.List;

public class ExclusiveOrExpr extends Expression {
	public List<AndExpr> list;
	
	public ExclusiveOrExpr(List<AndExpr> list) {
		this.list = list;
	}
}
