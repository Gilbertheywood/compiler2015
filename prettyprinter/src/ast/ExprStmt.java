package ast;

public class ExprStmt extends Stmt {
	public Expr expr;
	
	public ExprStmt(Expr expr) {
		this.expr = expr;
	}
}
