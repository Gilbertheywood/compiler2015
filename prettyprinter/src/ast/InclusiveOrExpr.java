package ast;

import java.util.List;

public class InclusiveOrExpr extends Expression {
	public List<ExclusiveOrExpr> list;
	
	public InclusiveOrExpr(List<ExclusiveOrExpr> list) {
		this.list = list;
	}
}
