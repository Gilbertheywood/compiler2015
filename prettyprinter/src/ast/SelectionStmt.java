package ast;

public class SelectionStmt extends Stmt {
	public Expr expr;
	public Stmt stmt1;
	public Stmt stmt2;
	
	public SelectionStmt(Expr expr, Stmt stmt1, Stmt stmt2) {
		this.expr = expr;
		this.stmt1 = stmt1;
		this.stmt2 = stmt2;
	}
}
