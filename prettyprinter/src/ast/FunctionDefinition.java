package ast;

public class FunctionDefinition extends Node {
	public Type type;
	public PlainDeclarator plainDeclarator;
	public Parameters parameters;
	public CompoundStmt compoundStmt;
	public Comment c1, c2, c3;
	
	public FunctionDefinition(Type type, PlainDeclarator plainDeclarator, Parameters parameters, CompoundStmt compoundStmt, Comment c1, Comment c2, Comment c3) {
		this.type = type;
		this.plainDeclarator = plainDeclarator;
		this.parameters = parameters;
		this.compoundStmt = compoundStmt;
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
	}
}
