package ast;

import java.util.List;

public class LogicalAndExpr extends Expression {
	public List<InclusiveOrExpr> list = null;
	
	public LogicalAndExpr(List<InclusiveOrExpr> list) {
		this.list = list;
	}
}
