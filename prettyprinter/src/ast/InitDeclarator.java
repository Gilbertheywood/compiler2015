package ast;

import java.util.List;

public class InitDeclarator extends Node {
	public Declarator declarator;
	public Initializer initializer;
	
	public InitDeclarator(Declarator declarator, Initializer initializer) {
		this.declarator = declarator;
		this.initializer = initializer;
	}
}
