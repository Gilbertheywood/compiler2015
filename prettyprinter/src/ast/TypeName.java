package ast;

public class TypeName extends Expression {
	public int cnt;
	public Type type;
	
	public TypeName(int cnt, Type type) {
		this.cnt = cnt;
		this.type = type;
	}
}
