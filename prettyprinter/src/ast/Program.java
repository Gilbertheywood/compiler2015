package ast;

import java.util.List;

public class Program {
	public List<Node> list = null;
	public Comment prec, succ;
	
	public Program(List<Node> list, Comment prec, Comment succ) {
		this.list = list;
		this.prec = prec;
		this.succ = succ;
		//System.out.println("i'm a program.");
	}
}
