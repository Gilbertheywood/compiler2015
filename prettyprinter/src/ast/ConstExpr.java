package ast;

public class ConstExpr extends Expression {
	public LogicalOrExpr logicalOrExpr;
	
	public ConstExpr(LogicalOrExpr logicalOrExpr) {
		this.logicalOrExpr = logicalOrExpr;
	}
}
