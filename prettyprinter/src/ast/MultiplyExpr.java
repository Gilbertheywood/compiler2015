package ast;

import java.util.List;

public class MultiplyExpr extends Expression {
	public List<CastExpr> expr;
	public List<BinaryOp> binOp;
	
	public MultiplyExpr(List<CastExpr> expr, List<BinaryOp> binOp) {
		this.expr = expr;
		this.binOp = binOp;
	}
}
