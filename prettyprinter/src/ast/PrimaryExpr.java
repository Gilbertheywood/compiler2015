package ast;

public class PrimaryExpr extends Expression {
	public Identifier id = null;
	public Constant constant = null;
	public String s = null;
	public Expr expr= null;
	
	public PrimaryExpr(Identifier id) {
		this.id = id;
	}
	
	public PrimaryExpr(Constant constant) {
		this.constant = constant;
	}
	
	public PrimaryExpr(String s) {
		this.s = s;
	}
	
	public PrimaryExpr(Expr expr) {
		this.expr = expr;
	}
}
