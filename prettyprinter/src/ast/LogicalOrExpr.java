package ast;

import java.util.List;

public class LogicalOrExpr extends Expression {
	public List<LogicalAndExpr> list = null;
	
	public LogicalOrExpr(List<LogicalAndExpr> list) {
		this.list = list;
	}
}
