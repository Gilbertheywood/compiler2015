package ast;

public class Declaration extends Node {
	public Type type = null;
	public InitDeclarators initDeclarators = null;
	
	public Declaration(Type type, InitDeclarators initDeclarators) {
		this.type = type;
		this.initDeclarators = initDeclarators;
	}
}
