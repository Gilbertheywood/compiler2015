package ast;

public class FunctionDeclarator extends Declarator {
	public PlainDeclarator plainDeclarator;
	public Parameters parameters;
	
	public FunctionDeclarator(PlainDeclarator plainDeclarator, Parameters parameters) {
		this.plainDeclarator = plainDeclarator;
		this.parameters = parameters;
	}
}
