package ast;

public class ReturnStmt extends JumpStmt {
	public Expr expr;
	
	public ReturnStmt(Expr expr) {
		this.expr = expr;
	}
}
