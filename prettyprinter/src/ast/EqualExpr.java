package ast;

import java.util.List;

public class EqualExpr extends Expression {
	public List<RelationExpr> list;
	public List<BinaryOp> binOp;
	
	public EqualExpr(List<RelationExpr> list, List<BinaryOp> binOp) {
		this.list = list;
		this.binOp = binOp;
	}
}
