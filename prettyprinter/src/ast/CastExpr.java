package ast;

public class CastExpr extends Expression {
	public TypeName typeName;
	public Expression expr;
	
	public CastExpr(TypeName typeName, Expression expr) {
		this.typeName = typeName;
		this.expr = expr;
	}
}
