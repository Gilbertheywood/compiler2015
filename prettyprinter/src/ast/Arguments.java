package ast;

import java.util.List;

public class Arguments extends Node {
	public List<AssignExpr> list = null;
	
	public Arguments() {
		list = null;
	}
	
	public Arguments(List<AssignExpr> list) {
		this.list = list;
	}
}
