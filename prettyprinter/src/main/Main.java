package main;

import ast.*;
import syntactic.*;
import util.*;

import java.io.*;

import org.antlr.v4.runtime.*; 
import org.antlr.v4.runtime.tree.*;

public class Main {
	public static void main(String[] args) throws Exception {
		//ANTLRFileStream input = new ANTLRFileStream("E:\\git_repo\\prettyprinter\\src\\main\\test.c");
		ANTLRInputStream input = new ANTLRInputStream(System.in);
		CLexer lexer = new CLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CParser parser = new CParser(tokens);
		
		Program prog = parser.program().ele;
		PrettyPrinter prettyPrinter = new PrettyPrinter(prog);
		prettyPrinter.Print();
	}
}
