package main;

import ast.*;

import java.io.*;

import util.Pairs;

public class PrettyPrinter {
	
	Program prog;
	//FileWriter out = new FileWriter("E:\\git_repo\\prettyprinter\\src\\main\\out.c");
	FileWriter out = new FileWriter("out.c");
	
	public PrettyPrinter(Program prog) throws Exception {
		this.prog = prog;
	}
	
	protected void finalize() throws Exception {
		out.flush();
		out.close();
	}
	
	public void Print() throws Exception {
		//out.write("dasfasdfas");
		PrintComment(prog.prec);
		for (Node node: prog.list) {
			if (node instanceof Declaration) {
				PrintDeclaration((Declaration)node, 0);
			} else {
				PrintFunctionDefinition((FunctionDefinition)node, 0);
			}
			out.write("\n");
		}
		PrintComment(prog.succ);
		out.flush();
	}
	
	public void PrintComment(Comment c) throws Exception {
		if (c == null) return;
		for (SingleComment s: c.list) {
			out.write(s.s);
			out.write("\n");
		}
	}
	
	public void PrintDeclaration(Declaration decl, int tabs) throws Exception {
		if (decl == null) return;
		PrintType(decl.type, tabs);out.write(" ");
		PrintInitDeclarators(decl.initDeclarators, 0);
		out.write(";"+"\n");
	}
	
	public void PrintFunctionDefinition(FunctionDefinition decl, int tabs) throws Exception {
		if (decl == null) return;
		for (int i = 0; i < tabs; i++) out.write("    ");
		//for (int i = 0; i < tabs; i++) out.write("aaaa");
		PrintType(decl.type, tabs);out.write(" ");
		PrintPlainDeclarator(decl.plainDeclarator, 0);
		PrintComment(decl.c1);out.write("(");
		PrintParameters(decl.parameters, 0);
		out.write(") ");PrintComment(decl.c2);
		PrintCompoundStmt(decl.compoundStmt, tabs+1);
		PrintComment(decl.c3);
	}
	
	public void PrintType(Type type, int tabs) throws Exception {
		if (type == null) return;
		//for (int i = 0; i < tabs; i++) out.write("bbbb");
		for (int i = 0; i < tabs; i++) out.write("    ");
		if (type instanceof IntType) out.write("int");
		if (type instanceof VoidType) out.write("void");
		if (type instanceof CharType) out.write("char");
		if (type instanceof RecordType) {
			PrintRecordType((RecordType)type, tabs);
		}
	}
	
	public void PrintRecordType(RecordType type, int tabs) throws Exception {
		if (type.sou instanceof StructType) out.write("struct");
		else out.write("union");
		out.write(" ");
		if (type.id != null) {
			out.write(type.id.s);
			out.write(" ");
		}
		
		if (type.mark == 1) out.write(" {\n");
		
		if (type.list != null) {
			for (Pairs<Type, Declarators> p: type.list) {
				PrintType(p.first, tabs+1);out.write(" ");
				PrintDeclarators(p.second, tabs+1);
				out.write(";\n");
			}
		}
		
		if (type.mark == 1) {
			//for (int i = 0; i < tabs; i++) out.write("cccc");
			for (int i = 0; i < tabs; i++) out.write("    ");
			out.write("}");
		}
	}
	
	public void PrintDeclarators(Declarators decls, int tabs) throws Exception {
		if (decls == null) return;
		int size = decls.list.size(), cnt = 0;
		for (Declarator decl: decls.list) {
			PrintDeclarator(decl, 0);
			cnt++;
			if (cnt < size) out.write(", ");
		}
	}
	
	public void PrintInitDeclarators(InitDeclarators init, int tabs) throws Exception {
		if (init == null) return;
		//for (int i = 0; i < tabs; i++) out.write("dddd");
		//for (int i = 0; i < tabs; i++) out.write("    ");
		int size = init.list.size(), cnt = 0;
		for (InitDeclarator i: init.list) {
			//out.write(", ");
			PrintInitDeclarator(i, 0);
			cnt++;
			if (cnt < size) out.write(", ");
		}
	}
	
	public void PrintInitDeclarator(InitDeclarator init, int tabs) throws Exception {
		if (init == null) return;
		PrintDeclarator(init.declarator, 0);
		if (init.initializer != null) {
			out.write(" = ");
			PrintInitializer(init.initializer, tabs);
		}
	}
	
	public void PrintPlainDeclarator(PlainDeclarator decl, int tabs) throws Exception {
		if (decl == null) return;
		for (int i = 0; i < decl.cnt; i++) out.write("*");
		out.write(decl.id.s);
	}
	
	public void PrintParameters(Parameters params, int tabs) throws Exception {
		if (params == null) return;
		int size = params.list.size(), cnt = 0;
		for (PlainDeclaration decl: params.list) {
			PrintPlainDeclaration(decl, 0);
			cnt++;
			if (cnt < size) out.write(", ");
		}
	}
	
	public void PrintDeclarator(Declarator decl, int tabs) throws Exception {
		if (decl == null) return;
		if (decl instanceof ArrayDeclarator)
			PrintArrayDeclarator((ArrayDeclarator)decl, 0);
	}
	
	public void PrintInitializer(Initializer init, int tabs) throws Exception {
		if (init == null) return;
		if (init.assignExpr != null) {
			PrintAssignExpr(init.assignExpr, 0);
		} else {
			out.write("{");
			int size = init.list.size(), cnt = 0;
			for (Initializer i: init.list) {
				PrintInitializer(i, 0);
				cnt++;
				if (cnt < size) out.write(", ");
			}
			out.write("}");
		}
	}
	
	public void PrintPlainDeclaration(PlainDeclaration decl, int tabs) throws Exception {
		if (decl == null) return;
		PrintType(decl.type, 0);out.write(" ");
		PrintDeclarator(decl.declarator, 0);
	}
	
	public void PrintArrayDeclarator(ArrayDeclarator decl, int tabs) throws Exception {
		if (decl == null) return;
		PrintPlainDeclarator(decl.plainDeclarator, 0);
		for (ConstExpr expr: decl.list) {
			out.write("[");
			PrintConstExpr(expr, 0);
			out.write("]");
		}
	}
	
	// Statements
	
	public void PrintCompoundStmt(CompoundStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		out.write("{\n");PrintComment(stmt.c1);
		for (Declaration decl: stmt.list1) {
			PrintDeclaration(decl, tabs);
		}
		for (Stmt s: stmt.list2) {
			PrintStmt(s, tabs);
		}
		//for (int i = 1; i < tabs; i++) out.write("eeee");
		for (int i = 1; i < tabs; i++) out.write("    ");
		PrintComment(stmt.c2);
		out.write("}"+"\n");
	}
	
	public void PrintStmt(Stmt s, int tabs) throws Exception {
		if (s == null) return;
		//for (int i = 0; i< tabs; i++) out.write("    ");
		if (s instanceof ExprStmt) {
			PrintExprStmt((ExprStmt)s, tabs);
		}
		if (s instanceof CompoundStmt) {
			PrintCompoundStmt((CompoundStmt)s, tabs);
		}
		if (s instanceof SelectionStmt) {
			PrintSelectionStmt((SelectionStmt)s, tabs);
		}
		if (s instanceof IterationStmt) {
			PrintIterationStmt((IterationStmt)s, tabs);
		}
		if (s instanceof JumpStmt) {
			PrintJumpStmt((JumpStmt)s, tabs);
		}
	}
	
	public void PrintExprStmt(ExprStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		if (stmt.expr == null) return;
		//for (int i = 0; i < tabs; i++) out.write("ffff");
		for (int i = 0; i < tabs; i++) out.write("    ");
		PrintExpr(stmt.expr, 0);out.write(";\n");
	}
	
	public void PrintSelectionStmt(SelectionStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		//for (int i = 0; i < tabs; i++) out.write("gggg");
		for (int i = 0; i < tabs; i++) out.write("    ");
		out.write("if (");
		PrintExpr(stmt.expr, 0);
		out.write(") ");
		PrintNestedStmt(stmt.stmt1, tabs);
		if (stmt.stmt2 != null) {
			//for (int i = 0; i < tabs; i++) out.write("gggg");
			for (int i = 0; i < tabs; i++) out.write("    ");
			out.write("else ");
			PrintNestedStmt(stmt.stmt2, tabs);
		}
	}
	
	public void PrintNestedStmt(Stmt stmt, int tabs) throws Exception {
		if (stmt instanceof CompoundStmt)
			PrintCompoundStmt((CompoundStmt)stmt, tabs+1);
		if (stmt instanceof SelectionStmt) {
			out.write("\n");
			PrintSelectionStmt((SelectionStmt)stmt, tabs+1);
		}
		if (stmt instanceof IterationStmt) {
			out.write("\n");
			PrintIterationStmt((IterationStmt)stmt, tabs+1);
		}
		if (stmt instanceof JumpStmt)
			PrintJumpStmt((JumpStmt)stmt, 0);
		if (stmt instanceof ExprStmt) {
			out.write("\n");
			PrintExprStmt((ExprStmt)stmt, tabs+1);
		}
	}
	
	public void PrintIterationStmt(IterationStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		if (stmt instanceof ForStmt) PrintForStmt((ForStmt)stmt, tabs);
		if (stmt instanceof WhileStmt) PrintWhileStmt((WhileStmt)stmt, tabs);
	}
	
	public void PrintForStmt(ForStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		//for (int i = 0; i < tabs; i++) out.write("iiii");
		for (int i = 0; i < tabs; i++) out.write("    ");
		out.write("for (");
		PrintExpr(stmt.e1, 0);
		out.write("; ");
		PrintExpr(stmt.e2, 0);
		out.write("; ");
		PrintExpr(stmt.e3, 0);
		out.write(")");
		PrintNestedStmt(stmt.stmt, tabs);
	}
	
	public void PrintWhileStmt(WhileStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		//for (int i = 0; i < tabs; i++) out.write("jjjj");
		for (int i = 0; i < tabs; i++) out.write("    ");
		out.write("while (");
		PrintExpr(stmt.expr, 0);
		out.write(") ");
		PrintNestedStmt(stmt.stmt, tabs);
	}
	
	public void PrintJumpStmt(JumpStmt stmt, int tabs) throws Exception {
		if (stmt == null) return;
		for (int i = 0; i < tabs; i++) out.write("    ");
		if (stmt instanceof ContinueStmt) out.write("continue");
		if (stmt instanceof BreakStmt) out.write("break");
		if (stmt instanceof ReturnStmt) {
			out.write("return ");
			ReturnStmt s = (ReturnStmt)stmt;
			PrintExpr(s.expr, 0);
			//if (s.expr instanceof Expr) System.out.print("Yes\n");
		}
		out.write(";\n");
	}
	
	// Expressions
	
	public void PrintExpr(Expr expr, int tabs) throws Exception {
		if (expr == null) return;
		//for (int i = 0; i < tabs; i++) out.write("kkkk");
		for (int i = 0; i < tabs; i++) out.write("    ");
		int size = expr.list.size(), cnt = 0;
		for (AssignExpr e: expr.list) {
			PrintAssignExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(", ");
		}
	}
	
	public void PrintAssignExpr(AssignExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		//for (int i = 0; i < tabs; i++) out.write("llll");
		for (int i = 0; i < tabs; i++) out.write("    ");
		if (expr.logicalOrExpr != null) {
			PrintLogicalOrExpr(expr.logicalOrExpr, 0);
		} else {
			PrintUnaryExpr(expr.unaryExpr, 0);out.write(" ");
			PrintBinaryOp(expr.binaryOp, 0);out.write(" ");
			PrintAssignExpr(expr.assignExpr, 0);
		}
	}
	
	public void PrintBinaryOp(BinaryOp op, int tabs) throws Exception {
		if (op == BinaryOp.ADD) out.write("+");
		if (op == BinaryOp.ASSIGN) out.write("=");
		if (op == BinaryOp.ASSIGN_ADD) out.write("+=");
		if (op == BinaryOp.ASSIGN_AND) out.write("&=");
		if (op == BinaryOp.ASSIGN_DIV) out.write("/=");
		if (op == BinaryOp.ASSIGN_MOD) out.write("%=");
		if (op == BinaryOp.ASSIGN_MUL) out.write("*=");
		if (op == BinaryOp.ASSIGN_OR) out.write("|=");
		if (op == BinaryOp.ASSIGN_SHL) out.write("<<=");
		if (op == BinaryOp.ASSIGN_SHR) out.write(">>=");
		if (op == BinaryOp.ASSIGN_SUB) out.write("-=");
		if (op == BinaryOp.ASSIGN_XOR) out.write("^=");
		if (op == BinaryOp.DIV) out.write("/");
		if (op == BinaryOp.EQ) out.write("==");
		if (op == BinaryOp.GE) out.write(">=");
		if (op == BinaryOp.GT) out.write(">");
		if (op == BinaryOp.LE) out.write("<=");
		if (op == BinaryOp.LT) out.write("<");
		if (op == BinaryOp.LOGICAL_AND) out.write("&&");
		if (op == BinaryOp.LOGICAL_OR) out.write("||");
		if (op == BinaryOp.MOD) out.write("%");
		if (op == BinaryOp.MUL) out.write("*");
		if (op == BinaryOp.NE) out.write("!=");
		if (op == BinaryOp.SHL) out.write("<<");
		if (op == BinaryOp.SHR) out.write(">>");
		if (op == BinaryOp.SUB) out.write("-");
	}
	
	public void PrintConstExpr(ConstExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		PrintLogicalOrExpr(expr.logicalOrExpr, tabs);
	}
	
	public void PrintLogicalOrExpr(LogicalOrExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (LogicalAndExpr e: expr.list) {
			PrintLogicalAndExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(" || ");
		}
	}
	
	public void PrintLogicalAndExpr(LogicalAndExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (InclusiveOrExpr e: expr.list) {
			PrintInclusiveOrExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(" && ");
		}
	}
	
	public void PrintInclusiveOrExpr(InclusiveOrExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (ExclusiveOrExpr e: expr.list) {
			PrintExclusiveOrExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(" | ");
		}
	}
	
	public void PrintExclusiveOrExpr(ExclusiveOrExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (AndExpr e: expr.list) {
			PrintAndExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(" ^ ");
		}
	}
	
	public void PrintAndExpr(AndExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (EqualExpr e: expr.list) {
			PrintEqualExpr(e, 0);
			cnt++;
			if (cnt < size) out.write(" & ");
		}
	}
	
	public void PrintEqualExpr(EqualExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (int i = 0; i < size; i++) {
			PrintRelationExpr(expr.list.get(i), 0);
			cnt++;
			if (cnt < size) {
				out.write(" ");
				PrintBinaryOp(expr.binOp.get(i), 0);
				out.write(" ");
			}
		}
	}
	
	public void PrintRelationExpr(RelationExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.list.size(), cnt = 0;
		for (int i = 0; i < size; i++) {
			PrintShiftExpr(expr.list.get(i), 0);
			cnt++;
			if (cnt < size) {
				out.write(" ");
				PrintBinaryOp(expr.binOp.get(i), 0);
				out.write(" ");
			}
		}
	}
	
	public void PrintShiftExpr(ShiftExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.expr.size();
		for (int i = 0; i < size; i++) {
			PrintAddExpr(expr.expr.get(i), 0);
			if (i < size-1) {
				out.write(" ");
				PrintBinaryOp(expr.binOp.get(i), 0);
				out.write(" ");
			}
		}
	}
	
	public void PrintAddExpr(AddExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.expr.size();
		for (int i = 0; i < size; i++) {
			PrintMultiplyExpr(expr.expr.get(i), 0);
			if (i < size-1) {
				out.write(" ");
				PrintBinaryOp(expr.binOp.get(i), 0);
				out.write(" ");
			}
		}
	}
	
	public void PrintMultiplyExpr(MultiplyExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		int size = expr.expr.size();
		for (int i = 0; i < size; i++) {
			PrintCastExpr(expr.expr.get(i), 0);
			if (i < size-1) {
				out.write(" ");
				PrintBinaryOp(expr.binOp.get(i), 0);
				out.write(" ");
			}
		}
	}
	
	public void PrintCastExpr(CastExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		if (expr.expr instanceof UnaryExpr) {
			PrintUnaryExpr((UnaryExpr)(expr.expr), 0);
		} else {
			out.write("(");
			PrintTypeName((TypeName)(expr.typeName), 0);
			out.write(")");
			PrintCastExpr((CastExpr)(expr.expr), 0);
		}
	}
	
	public void PrintTypeName(TypeName type, int tabs) throws Exception {
		if (type == null) return;
		PrintType(type.type, 0);
		for (int i = 0; i < type.cnt; i++) out.write("*");
	}
	
	public void PrintUnaryExpr(UnaryExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		PrintUnaryOp(expr.unaryOp, 0);
		//if (expr.unaryOp == UnaryOp.SIZEOFNAME) out.write("(");
		PrintExpression(expr.expr, 0);
		if (expr.unaryOp == UnaryOp.SIZEOFNAME) out.write(")");
	}
	
	public void PrintUnaryOp(UnaryOp op, int tabs) throws Exception {
		if (op == UnaryOp.AND) out.write("&");
		if (op == UnaryOp.MINUS) out.write("-");
		if (op == UnaryOp.NOT) out.write("!");
		if (op == UnaryOp.PLUS) out.write("+");
		if (op == UnaryOp.POSTDEC) out.write("--");
		if (op == UnaryOp.POSTINC) out.write("++");
		if (op == UnaryOp.PREDEC) out.write("--");
		if (op == UnaryOp.PREINC) out.write("++");
		if (op == UnaryOp.SIZEOF) out.write("sizeof ");
		if (op == UnaryOp.SIZEOFNAME) out.write("sizeof(");
		if (op == UnaryOp.STAR) out.write("*");
		if (op == UnaryOp.TILDE) out.write("~");
	}
	
	public void PrintExpression(Expression expression, int tabs) throws Exception {
		if (expression == null) return;
		if (expression instanceof PostfixExpr) 
			PrintPostfixExpr((PostfixExpr)expression, 0);
		if (expression instanceof UnaryExpr)
			PrintUnaryExpr((UnaryExpr)expression, 0);
		if (expression instanceof CastExpr)
			PrintCastExpr((CastExpr)expression, 0);
		if (expression instanceof TypeName)
			PrintTypeName((TypeName)expression, 0);
	}
	
	public void PrintPostfixExpr(PostfixExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		PrintPrimaryExpr(expr.expr, 0);
		for (Postfix p: expr.list) {
			PrintPostfix(p, 0);
		}
	}
	
	public void PrintPostfix(Postfix p, int tabs) throws Exception {
		if (p == null) return;
		if (p instanceof ArrayPostfix) {
			out.write("[");
			ArrayPostfix tmp = (ArrayPostfix)p; 
			PrintExpr(tmp.expr, 0);
			out.write("]");
		}
		if (p instanceof FunctionPostfix) {
			out.write("(");
			FunctionPostfix tmp = (FunctionPostfix)p;
			PrintArguments(tmp.argu, 0);
			out.write(")");
		}
		if (p instanceof ValueAttributePostfix) {
			out.write(".");
			ValueAttributePostfix tmp = (ValueAttributePostfix)p;
			out.write(tmp.id.s);
		}
		if (p instanceof PointerAttributePostfix) {
			out.write("->");
			PointerAttributePostfix tmp = (PointerAttributePostfix)p;
			out.write(tmp.id.s);
		}
		if (p instanceof SelfIncPostfix) {
			out.write("++");
		}
		if (p instanceof SelfDecPostfix) {
			out.write("--");
		}
	}
	
	public void PrintArguments(Arguments argu, int tabs) throws Exception {
		if (argu == null) return;
		int size = argu.list.size(), cnt = 0;
		for (AssignExpr expr: argu.list) {
			PrintAssignExpr(expr, 0);
			cnt++;
			if (cnt < size) out.write(", ");
		}
	}
	
	public void PrintPrimaryExpr(PrimaryExpr expr, int tabs) throws Exception {
		if (expr == null) return;
		if (expr.id != null)
			out.write(expr.id.s);
		if (expr.s != null)
			out.write(expr.s);
		if (expr.constant != null)
			PrintConstant(expr.constant, 0);
		if (expr.expr != null) {
			out.write("(");
			PrintExpr(expr.expr, 0);
			out.write(")");
		}
	}
	
	public void PrintConstant(Constant c, int tabs) throws Exception {
		if (c == null) return;
		if (c instanceof IntConst) {
			IntConst i = (IntConst)c;
			out.write(String.valueOf(i.value));
		}
		if (c instanceof ChrConst) {
			ChrConst chr = (ChrConst)c;
			out.write(chr.s);
		}
	}
}
