package util;

public class Pairs<Type1, Type2> {
	public Type1 first;
	public Type2 second;

	public Pairs(Type1 f, Type2 s) {
		this.first = f;
		this.second = s;
	}
}